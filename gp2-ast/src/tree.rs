use crate::Error;

#[derive(Debug, Clone)]
pub struct Node<T> {
	pub children: Vec<Node<T>>,
	pub data: T,
}

impl<T> Node<T> {
	pub fn new(t: T) -> Self {
		Self {
			children: Vec::new(),
			data: t,
		}
	}

	pub fn add_child(&mut self, child: Self) {
		self.children.push(child);
	}

	#[allow(dead_code)]
	pub fn visit<F>(&self, f: &F) -> Result<(), Error>
	where
		F: Fn(&Self) -> Result<(), Error>,
	{
		f(self)?;
		for child in &self.children {
			child.visit(f)?;
		}
		Ok(())
	}

	pub fn visit_mut<F>(&mut self, f: &mut F) -> Result<(), Error>
	where
		F: FnMut(&mut Self) -> Result<(), Error>,
	{
		f(self)?;
		for child in &mut self.children {
			child.visit_mut(f)?;
		}
		Ok(())
	}
}
