use crate::{
	nodes::{
		Access, ArgumentList, Assignment, BinaryOp, Block, Branch, ExpressionStatement,
		FreeFunctionCall, GetField, Group, LiteralValue, MethodCall, StaticFunctionCall,
		TypeAnnotation, Variable, VariableDeclaration, WhileLoop,
	},
	BinaryOperator, Expression, FailedParseInfo, Literal, Span, Statement,
};

fn literal_bool(val: bool, span: Span) -> Expression {
	Expression::Literal(LiteralValue {
		span,
		value: Literal::Bool(val),
	})
}

fn literal_int(val: i64, span: Span) -> Expression {
	Expression::Literal(LiteralValue {
		span,
		value: Literal::Integer(val),
	})
}

fn literal_float(val: f32, span: Span) -> Expression {
	Expression::Literal(LiteralValue {
		span,
		value: Literal::Float(val),
	})
}

fn literal_string(val: &str, span: Span) -> Expression {
	Expression::Literal(LiteralValue {
		span,
		value: Literal::String(val.to_string()),
	})
}

fn type_annotation(access: Access, ty: &str, span: Span) -> TypeAnnotation {
	TypeAnnotation {
		span,
		access,
		ty: ty.to_string(),
	}
}

fn argument_list(arguments: Vec<Expression>, span: Span) -> ArgumentList {
	ArgumentList { span, arguments }
}

fn static_function_call(
	class: &str,
	name: &str,
	arguments: ArgumentList,
	span: Span,
) -> Expression {
	Expression::StaticFunctionCall(StaticFunctionCall {
		span,
		class: class.to_string(),
		name: name.to_string(),
		arguments,
	})
}

fn group(body: Box<Expression>, span: Span) -> Expression {
	Expression::Group(Group { span, body })
}

fn variable_declaration(
	is_mut: bool,
	name: &str,
	ty: Option<TypeAnnotation>,
	initializer: Option<Expression>,
	span: Span,
) -> Statement {
	Statement::VariableDeclaration(VariableDeclaration {
		span,
		is_extern: false,
		is_mut,
		name: name.to_string(),
		ty,
		initializer,
	})
}

fn branch(
	condition: Box<Expression>,
	then_branch: Block,
	else_branch: Option<Box<Expression>>,
	span: Span,
) -> Expression {
	Expression::Branch(Branch {
		span,
		condition,
		then_branch,
		else_branch,
	})
}

fn block(body: Vec<Statement>, span: Span) -> Block {
	Block {
		span,
		body,
		catch_returns: false,
	}
}

fn block_expression(body: Vec<Statement>, span: Span) -> Expression {
	Expression::Block(block(body, span))
}

fn while_loop(condition: Expression, body: Block, span: Span) -> Statement {
	Statement::While(WhileLoop {
		span,
		condition,
		body,
	})
}

fn variable(name: &str, span: Span) -> Expression {
	Expression::Variable(Variable::new(span, name.to_string()))
}

fn free_function_call(name: &str, arguments: ArgumentList, span: Span) -> Expression {
	Expression::FreeFunctionCall(FreeFunctionCall {
		span,
		name: name.to_string(),
		arguments,
	})
}

fn method_call(name: &str, object: Expression, arguments: ArgumentList, span: Span) -> Expression {
	Expression::MethodCall(MethodCall {
		span,
		object: object.into(),
		name: name.to_string(),
		arguments,
	})
}

fn get_field(name: &str, object: Expression, span: Span) -> Expression {
	Expression::GetField(GetField {
		span,
		object: Box::new(object),
		name: name.to_string(),
	})
}

fn expression(body: Expression, has_semicolon: bool, span: Span) -> Statement {
	Statement::Expression(ExpressionStatement {
		span,
		body,
		has_semicolon,
	})
}

#[test]
fn literals() {
	assert_eq!(
		parse_expression("false").unwrap(),
		literal_bool(false, 0..5)
	);
	assert_eq!(parse_expression("true").unwrap(), literal_bool(true, 0..4));
	assert_eq!(parse_expression("0").unwrap(), literal_int(0, 0..1));
	assert_eq!(parse_expression("99999").unwrap(), literal_int(99999, 0..5));
	assert_eq!(parse_expression("0.0").unwrap(), literal_float(0.0, 0..3),);
	assert_eq!(
		parse_expression("99999.999").unwrap(),
		literal_float(99999.999, 0..9),
	);
	assert_eq!(
		parse_expression(r#""hello""#).unwrap(),
		literal_string("hello", 0..7)
	);
}

#[test]
fn test_group() {
	assert_eq!(
		parse_expression("(1)").unwrap(),
		group(Box::new(literal_int(1, 1..2)), 0..3)
	);
	assert_eq!(
		parse_expression("(1 + 2)").unwrap(),
		group(
			Box::new(Expression::BinaryOp(BinaryOp {
				span: 3..4,
				left: Box::new(literal_int(1, 1..2)),
				operator: BinaryOperator::Add,
				right: Box::new(literal_int(2, 5..6)),
			})),
			0..7
		)
	);
}

#[test]
fn test_assignment() {
	assert_eq!(
		parse_statement("a = 1;").unwrap(),
		Statement::Assignment(Assignment {
			span: 0..6,
			name: "a".into(),
			value: literal_int(1, 4..5)
		})
	);
}

#[test]
fn test_variable_declaration() {
	assert_eq!(
		parse_statement("let a;").unwrap(),
		variable_declaration(false, "a", None, None, 0..6)
	);
	assert_eq!(
		parse_statement("let mut a;").unwrap(),
		variable_declaration(true, "a", None, None, 0..10)
	);
	assert_eq!(
		parse_statement("let a = 1;").unwrap(),
		variable_declaration(false, "a", None, Some(literal_int(1, 8..9)), 0..10)
	);
	assert_eq!(
		parse_statement("let a: i64;").unwrap(),
		variable_declaration(
			false,
			"a",
			Some(type_annotation(Access::Owned, "i64", 5..10)),
			None,
			0..11
		)
	);
	assert_eq!(
		parse_statement("let a: i64 = 1;").unwrap(),
		variable_declaration(
			false,
			"a",
			Some(type_annotation(Access::Owned, "i64", 5..10)),
			Some(literal_int(1, 13..14)),
			0..15
		)
	);
	assert_eq!(
		parse_statement("let mut a: i64 = 1;").unwrap(),
		variable_declaration(
			true,
			"a",
			Some(type_annotation(Access::Owned, "i64", 9..14)),
			Some(literal_int(1, 17..18)),
			0..19
		)
	);
}

#[test]
fn test_variable() {
	assert_eq!(parse_expression("a").unwrap(), variable("a", 0..1));
	assert_eq!(
		parse_expression("a.b.c").unwrap(),
		get_field("c", get_field("b", variable("a", 0..1), 0..3), 0..5)
	);
}

#[test]
fn test_method_call() {
	assert_eq!(
		parse_expression("a.b.c()").unwrap(),
		method_call(
			"c",
			get_field("b", variable("a", 0..1), 0..3),
			argument_list(Vec::new(), 5..7),
			0..7
		)
	);

	assert_eq!(
		parse_expression("a.b.c(1, 2, 3)").unwrap(),
		method_call(
			"c",
			get_field("b", variable("a", 0..1), 0..3),
			argument_list(
				vec![
					literal_int(1, 6..7),
					literal_int(2, 9..10),
					literal_int(3, 12..13),
				],
				5..14
			),
			0..14
		)
	);
}

#[test]
fn nested_method_call() {
	assert_eq!(
		parse_expression("a.b().c()").unwrap(),
		method_call(
			"c",
			method_call(
				"b",
				variable("a", 0..1),
				argument_list(Vec::new(), 3..5),
				0..5
			),
			argument_list(Vec::new(), 7..9),
			0..9
		)
	);

	assert_eq!(
		parse_expression("A::b().c()").unwrap(),
		method_call(
			"c",
			static_function_call("A", "b", argument_list(Vec::new(), 4..6), 0..6),
			argument_list(Vec::new(), 8..10),
			0..10
		)
	);
}

#[test]
fn test_free_function_call() {
	assert_eq!(
		parse_expression("a()").unwrap(),
		free_function_call("a", argument_list(vec![], 1..3), 0..3)
	);

	assert_eq!(
		parse_expression("a(1, 2, 3)").unwrap(),
		free_function_call(
			"a",
			argument_list(
				vec![
					literal_int(1, 2..3),
					literal_int(2, 5..6),
					literal_int(3, 8..9),
				],
				1..10
			),
			0..10
		)
	);
}

#[test]
fn call_static_function() {
	assert_eq!(
		parse_expression("A::a()").unwrap(),
		static_function_call("A", "a", argument_list(Vec::new(), 4..6), 0..6)
	);

	assert_eq!(
		parse_expression("A::a(1, 2, 3)").unwrap(),
		static_function_call(
			"A",
			"a",
			argument_list(
				vec![
					literal_int(1, 5..6),
					literal_int(2, 8..9),
					literal_int(3, 11..12),
				],
				4..13
			),
			0..13
		)
	);
}

#[test]
fn test_branch() {
	assert_eq!(
		parse_expression("if true { 0 }").unwrap(),
		branch(
			Box::new(literal_bool(true, 3..7)),
			block(
				vec![expression(literal_int(0, 10..11), false, 10..11)],
				8..13
			),
			None,
			0..13
		),
	);

	assert_eq!(
		parse_expression("if true { 0 } else { 1 }").unwrap(),
		branch(
			Box::new(literal_bool(true, 3..7)),
			block(
				vec![expression(literal_int(0, 10..11), false, 10..11)],
				8..13
			),
			Some(Box::new(block_expression(
				vec![expression(literal_int(1, 21..22), false, 21..22)],
				19..24
			))),
			0..24
		)
	);

	assert_eq!(
		parse_expression("if true { 0 } else if false { 1 }").unwrap(),
		branch(
			Box::new(literal_bool(true, 3..7)),
			block(
				vec![expression(literal_int(0, 10..11), false, 10..11)],
				8..13
			),
			Some(Box::new(branch(
				Box::new(literal_bool(false, 22..27)),
				block(
					vec![expression(literal_int(1, 30..31), false, 30..31)],
					28..33
				),
				None,
				19..33
			))),
			0..33
		)
	);
}

#[test]
fn test_while_loop() {
	assert_eq!(
		parse_statement("while true { 0 }").unwrap(),
		while_loop(
			literal_bool(true, 6..10),
			block(
				vec![expression(literal_int(0, 13..14), false, 13..14)],
				11..16
			),
			0..16
		)
	);
}

fn parse_expression(src: &str) -> Result<Expression, FailedParseInfo> {
	let res = crate::parse(src)?;

	if let Expression::Block(Block { mut body, .. }) = res {
		if let Statement::Expression(ExpressionStatement { body, .. }) = body.remove(0) {
			return Ok(body);
		}
	}

	panic!();
}

fn parse_statement(src: &str) -> Result<Statement, FailedParseInfo> {
	let res = crate::parse(src)?;
	if let Expression::Block(Block { mut body, .. }) = res {
		Ok(body.remove(0))
	} else {
		panic!();
	}
}
