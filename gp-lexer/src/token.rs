use logos::Logos;

#[derive(Logos, Debug, Clone, PartialEq)]
pub enum Token {
	#[token("let")]
	Let,

	#[token("if")]
	If,

	#[token("else")]
	Else,

	#[token("fn")]
	Fn,

	#[token("for")]
	For,

	#[token("while")]
	While,

	//#[token("return")]
	//Return,
	#[token("mut")]
	Mut,

	#[token("in")]
	In,

	#[token("extern")]
	Extern,

	#[token("&")]
	Ref,

	#[token("=")]
	Assignment,

	#[token("{")]
	BraceOpen,

	#[token("}")]
	BraceClose,

	#[token("(")]
	ParenthesisOpen,

	#[token(")")]
	ParenthesisClose,

	#[token(",")]
	Comma,

	#[token(";")]
	Semicolon,

	#[token(":")]
	Colon,

	#[token("+")]
	Plus,

	#[token("-")]
	Min,

	#[token("*")]
	Mul,

	#[token("/")]
	Div,

	#[token("<")]
	LessThan,

	#[token(">")]
	GreaterThan,

	#[token("<=")]
	LessEqual,

	#[token(">=")]
	GreaterEqual,

	#[token("!")]
	Bang,

	#[token("==")]
	Equal,

	#[token("!=")]
	NotEqual,

	#[token("and")]
	And,

	#[token("or")]
	Or,

	#[token("::")]
	ScopeResolution,

	#[token(".")]
	Dot,

	#[token("..")]
	Range,

	#[token("->")]
	Arrow,

	#[regex("[//][//].*", lex_single_line_comment)]
	#[regex("[//][*]", lex_multi_line_comment)]
	Comment(String),

	#[regex("\"([^\"]*)\"", parse_string_literal)]
	Literal(String),

	#[regex("[a-zA-Z_][a-zA-Z_0-9]*", |lex| lex.slice().to_owned())]
	Identifier(String),

	#[regex("[0-9]+[.][0-9]+", |lex| lex.slice().parse())]
	Float(f32),

	#[regex("[0-9]+", |lex| lex.slice().parse())]
	Integer(i64),

	#[token("true", parse_bool)]
	#[token("false", parse_bool)]
	Bool(bool),

	// Logos requires one token variant to handle errors,
	// it can be named anything you wish.
	#[error]
	// We can also use this variant to define whitespace,
	// or any other matches we wish to skip.
	#[regex(r"[ \t\n\f\r]+", logos::skip)]
	Error,
}

fn parse_bool(lexer: &mut logos::Lexer<Token>) -> Option<bool> {
	match lexer.slice() {
		"true" => Some(true),
		"false" => Some(false),
		_ => None,
	}
}

fn parse_string_literal(lexer: &mut logos::Lexer<Token>) -> String {
	let s = lexer.slice();
	s[1..s.len() - 1].to_owned() // Strip the " from the beginning and end
}

fn lex_single_line_comment(lexer: &mut logos::Lexer<Token>) -> String {
	let s = lexer.slice();
	s[2..].trim().to_owned()
}

fn find_matching_comment_close(s: &str, offset: usize) -> Option<usize> {
	let start = s.find("/*");
	let end = s.find("*/")?;

	if let Some(start) = start {
		if end < start {
			return Some(end + offset);
		}
		return find_matching_comment_close(&s[end + 2..], offset + end + 2);
	}
	Some(end + offset)
}

fn lex_multi_line_comment(lexer: &mut logos::Lexer<Token>) -> Option<String> {
	let res = find_matching_comment_close(lexer.remainder(), 0)?;
	lexer.bump(res);

	let s = lexer.slice();
	lexer.bump(2);

	return Some(s[2..].trim().to_owned());
}

impl Token {
	#[must_use]
	pub fn variant_eq(&self, other: &Token) -> bool {
		match self {
			Token::Comment(_) => matches!(other, Token::Comment(_)),
			Token::Literal(_) => matches!(other, Token::Literal(_)),
			Token::Identifier(_) => matches!(other, Token::Identifier(_)),
			Token::Float(_) => matches!(other, Token::Float(_)),
			Token::Integer(_) => matches!(other, Token::Integer(_)),
			Token::Bool(_) => matches!(other, Token::Bool(_)),
			_ => other == self,
		}
	}
}

impl std::fmt::Display for Token {
	fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
		let s = match self {
			Self::Let => "let",
			Self::If => "if",
			Self::Else => "else",
			Self::Fn => "fn",
			Self::For => "for",
			Self::While => "while",
			//Self::Return => "return",
			Self::Mut => "mut",
			Self::In => "in",
			Self::Extern => "extern",
			Self::Ref => "&",
			Self::Assignment => "=",
			Self::BraceOpen => "{",
			Self::BraceClose => "}",
			Self::ParenthesisOpen => "(",
			Self::ParenthesisClose => ")",
			Self::Comma => ",",
			Self::Semicolon => ";",
			Self::Colon => ":",
			Self::Plus => "+",
			Self::Min => "-",
			Self::Mul => "*",
			Self::Div => "/",
			Self::LessThan => "<",
			Self::GreaterThan => ">",
			Self::LessEqual => "<=",
			Self::GreaterEqual => ">=",
			Self::Bang => "!",
			Self::Equal => "==",
			Self::NotEqual => "!=",
			Self::And => "and",
			Self::Or => "or",
			Self::ScopeResolution => "::",
			Self::Dot => ".",
			Self::Range => "..",
			Self::Arrow => "->",
			Self::Comment(_) => "comment",
			Self::Literal(_) => "literal",
			Self::Identifier(_) => "identifier",
			Self::Float(_) => "float",
			Self::Integer(_) => "integer",
			Self::Bool(_) => "bool",
			Self::Error => "error",
		};
		write!(f, "{s}")
	}
}

impl Token {
	#[must_use]
	pub fn to_src(&self) -> String {
		match self {
			Self::Let => "let".to_string(),
			Self::If => "if".to_string(),
			Self::Else => "else".to_string(),
			Self::Fn => "fn".to_string(),
			Self::For => "for".to_string(),
			Self::While => "while".to_string(),
			//Self::Return => "return".to_string(),
			Self::Mut => "mut".to_string(),
			Self::In => "in".to_string(),
			Self::Extern => "extern".to_string(),
			Self::Ref => "&".to_string(),
			Self::Assignment => "=".to_string(),
			Self::BraceOpen => "{".to_string(),
			Self::BraceClose => "}".to_string(),
			Self::ParenthesisOpen => "(".to_string(),
			Self::ParenthesisClose => ")".to_string(),
			Self::Comma => ".to_string(),".to_string(),
			Self::Semicolon => ";".to_string(),
			Self::Colon => ":".to_string(),
			Self::Plus => "+".to_string(),
			Self::Min => "-".to_string(),
			Self::Mul => "*".to_string(),
			Self::Div => "/".to_string(),
			Self::LessThan => "<".to_string(),
			Self::GreaterThan => ">".to_string(),
			Self::LessEqual => "<=".to_string(),
			Self::GreaterEqual => ">=".to_string(),
			Self::Bang => "!".to_string(),
			Self::Equal => "==".to_string(),
			Self::NotEqual => "!=".to_string(),
			Self::And => "and".to_string(),
			Self::Or => "or".to_string(),
			Self::ScopeResolution => "::".to_string(),
			Self::Dot => ".".to_string(),
			Self::Range => "..".to_string(),
			Self::Arrow => "->".to_string(),
			Self::Comment(val) => format!("/*{val}*/"),
			Self::Literal(val) => format!("\"{val}\""),
			Self::Identifier(val) => val.clone(),
			Self::Float(val) => format!("{val}"),
			Self::Integer(val) => format!("{val}"),
			Self::Bool(val) => format!("{val}"),
			Self::Error => "ERROR".to_string(),
		}
	}
}
