use crate::nodes::ArgumentList;
use lexer::Span;

/// Parses "foo(expr, expr2, ...)"
#[derive(Debug, PartialEq, Clone)]
pub struct FreeFunctionCall {
	pub span: Span,
	pub name: String,
	pub arguments: ArgumentList,
}
