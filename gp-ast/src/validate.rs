use crate::{
	nodes::{Call, CallType},
	Context, Declarations, Error, Externs, INode, Node, VariableStates,
};
use log::trace;
use reflect::meta::Meta;

pub trait Validate {
	fn validate(&mut self, meta: &Meta, ctx: &mut Context) -> Result<(), Error> {
		self.pass1(meta, ctx)?;
		self.check(meta, ctx)
	}

	fn pass1(&mut self, meta: &Meta, ctx: &mut Context) -> Result<(), Error>;

	fn check(&self, _meta: &Meta, _ctx: &Context) -> Result<(), Error> {
		Ok(())
	}

	fn check_variable_access(
		&self,
		_variables: &mut VariableStates,
		_declarations: &Declarations,
	) -> Result<(), Error> {
		Ok(())
	}
}

impl Validate for Node {
	fn pass1(&mut self, meta: &Meta, ctx: &mut Context) -> Result<(), Error> {
		trace!("node: {}", self);
		match self {
			Node::Group(node) => node.validate(meta, ctx),
			Node::Assignment(node) => node.validate(meta, ctx),
			Node::Literal(node) => node.validate(meta, ctx),
			Node::Call(node) => node.validate(meta, ctx),
			Node::Block(node) => node.validate(meta, ctx),
			Node::Branch(node) => node.validate(meta, ctx),
			Node::ExpressionStatement(node) => node.validate(meta, ctx),
			Node::VariableDeclaration(node) => node.validate(meta, ctx),
			Node::While(node) => node.validate(meta, ctx),
			Node::For(node) => node.validate(meta, ctx),
			Node::FunctionDeclaration(node) => node.validate(meta, ctx),
			//Node::ReturnStatement(node) => node.validate(meta, ctx),
			Node::GetField(node) => node.validate(meta, ctx),
			Node::Variable(node) => node.validate(meta, ctx),
		}
	}
}

pub(crate) fn validate_node(
	node: &mut dyn INode,
	meta: &Meta,
	ctx: &mut Context,
) -> Result<(), Error> {
	replace_free_with_extern_call_nodes(node, &ctx.externs);
	node.validate(meta, ctx)?;
	//node.walk(&mut |node| node.check(meta, &ctx))?;
	node.walk(&mut |node| node.check_variable_access(&mut ctx.variable_states, &ctx.declarations))
}

pub(crate) fn replace_free_with_extern_call_nodes(node: &mut dyn INode, externs: &Externs) {
	log::trace!("replacing free function calls with externs");

	node.walk(&mut |node| {
		if let Some(n) = node.as_any_mut().downcast_mut::<Call>() {
			if let CallType::Free { .. } = n.call_type {
				if externs.has_function(&n.name) {
					n.call_type = CallType::Extern { id: None };
				}
			}
		}
		Ok(())
	})
	.unwrap();

	log::trace!("replaced free function calls with externs");
}
