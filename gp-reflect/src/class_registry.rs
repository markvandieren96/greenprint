use crate::{
	meta::{ClassId, Meta},
	register_std_types, Class, Error, Register, TypeName,
};
use std::any::TypeId;
use std::collections::HashMap;

pub type DelayedRegisterFunc = Box<dyn FnOnce(&mut ClassRegistry) + Send + Sync>;

pub struct ClassRegistry {
	classes: HashMap<TypeId, Class>,
	names: HashMap<String, TypeId>,
	register_queue: HashMap<ClassId, DelayedRegisterFunc>,
}

impl Default for ClassRegistry {
	fn default() -> Self {
		let mut registry = Self {
			classes: HashMap::new(),
			names: HashMap::new(),
			register_queue: HashMap::new(),
		};

		register_std_types(&mut registry);
		#[cfg(test)]
		crate::tests::helper::init_test_classes(&mut registry);

		registry
	}
}

impl ClassRegistry {
	pub fn register(&mut self, class: Class) {
		self.names.insert(class.name().to_owned(), class.id());
		self.classes.insert(class.id(), class);
	}

	pub fn queue_register<T: 'static + TypeName + Register>(&mut self) {
		if self.classes.get(&std::any::TypeId::of::<T>()).is_none() {
			self.register_queue.insert(
				ClassId(T::type_name()),
				Box::new(|registry| T::register(registry)),
			);
		}
	}

	pub fn process_queue(&mut self) {
		while !self.register_queue.is_empty() {
			let queue = std::mem::take(&mut self.register_queue);
			for (_, func) in queue {
				func(self);
			}
		}
	}

	#[cfg(test)]
	pub(crate) fn modify<F>(&mut self, name: &str, f: F)
	where
		F: FnOnce(&mut Class),
	{
		if let Some(id) = self.names.get(name).copied() {
			let class = self.classes.get_mut(&id).unwrap();
			f(class);
		} else {
			panic!("Could not find class to modify");
		}
	}

	#[must_use]
	pub fn get(&self, id: TypeId) -> Option<&Class> {
		self.classes.get(&id)
	}

	#[must_use]
	pub fn lookup(&self, name: &str) -> Option<TypeId> {
		self.names.get(name).copied()
	}

	pub fn find(&self, name: &str) -> Result<&Class, Error> {
		self.lookup(name)
			.and_then(|id| self.get(id))
			.ok_or_else(|| Error::no_such_class(&name))
	}

	pub fn log(&self) {
		println!("Classes");
		for (name, id) in &self.names {
			println!("\t{name} ({id:?})");
		}
	}

	#[must_use]
	pub fn meta(&self) -> Meta {
		let mut classes = HashMap::new();
		for (name, id) in &self.names {
			let class = self.classes.get(id).unwrap();
			classes.insert(ClassId(name.to_string()), class.meta(self));
		}
		Meta { classes }
	}
}
