use crate::{
	counter::Counter,
	node::{Node, ToNode, BOOL_COLOR, CORNER_RADIUS, EXEC_COLOR, INT_COLOR, UNKNOWN_COLOR},
};
use eframe::egui;
use egui::{Color32 as Color, Ui, Vec2};
use program::{
	ast,
	ast::nodes::{
		Block, Branch, Call, ExpressionStatement, For, FunctionDeclaration, GetField, Literal,
		Variable, VariableDeclaration, While,
	},
};

#[derive(Default)]
pub struct AstView {
	counter_depth: usize,
}

impl AstView {
	pub fn display_window(
		&mut self,
		ctx: &egui::CtxRef,
		show: &mut bool,
		ast: &ast::Node,
		src: &str,
	) {
		egui::Window::new("ast").open(show).show(ctx, |ui| {
			egui::ScrollArea::auto_sized().show(ui, |ui| {
				self.display(ui, ast, src);
			});
		});
	}

	pub fn display(&mut self, ui: &mut Ui, ast: &ast::Node, src: &str) {
		let Self { counter_depth, .. } = self;

		ui.painter().rect(
			ui.max_rect(),
			CORNER_RADIUS,
			Color::BLACK,
			egui::Stroke::new(2.0, Color::LIGHT_GRAY),
		);
		egui::ScrollArea::auto_sized().show(ui, |ui| {
			ui.add_space(10.0);
			ui.style_mut().spacing.item_spacing = Vec2::new(20.0, 15.0);
			let mut counter = Counter::default();
			let viz_node = ast.to_node(&mut counter, src);
			*counter_depth = (*counter_depth).max(counter.get());
			viz_node.draw(ui, None);
		});
	}

	pub fn destroy(&mut self, ui: &mut Ui) {
		let mut counter = Counter::default();
		for _ in 0..self.counter_depth {
			let id = node_id(&mut counter);
			ui.memory().id_data_temp.remove(&id);
		}
	}
}

fn node_id(counter: &mut Counter) -> egui::Id {
	egui::Id::new(format!("ast_view_node_{}", counter.get()))
}

impl ToNode for Block {
	fn to_node(&self, counter: &mut Counter, src: &str) -> Node {
		let Self { span, body, .. } = self;

		let text = "block";
		let mut node = Node::new(node_id(counter))
			.header(&text)
			.hover_src(src, span);

		for (index, statement) in body.iter().enumerate() {
			node = node.named_child(counter, statement, &index, EXEC_COLOR, src);
		}

		node
	}
}

#[allow(clippy::too_many_lines)] // TODO break this up
impl ToNode for ast::Node {
	fn to_node(&self, counter: &mut Counter, src: &str) -> Node {
		match self {
			ast::Node::Group(ast::nodes::Group { span, body, .. }) => {
				let text = "({})";
				Node::new(node_id(counter))
					.header(&text)
					.hover_src(src, span)
					.child(counter, &**body, src)
			}
			ast::Node::Assignment(ast::nodes::Assignment {
				span, name, value, ..
			}) => {
				let text = format!("{} = ...;", name);
				Node::new(node_id(counter))
					.header(&text)
					.hover_src(src, span)
					.named_child(counter, &**value, &"value", UNKNOWN_COLOR, src)
			}
			ast::Node::Literal(Literal { value, .. }) => {
				let text = format!("{}", value);
				Node::new(node_id(counter)).header(&text)
			}
			ast::Node::Variable(Variable {
				span,
				name,
				scope,
				resolved_id,
				..
			}) => {
				let text = format!(
					"{} (scope={:?}, resolved_id={:?})",
					name, scope, resolved_id
				);
				Node::new(node_id(counter))
					.header(&text)
					.hover_src(src, span)
			}
			ast::Node::Call(Call {
				span,
				name,
				arguments,
				call_type,
				..
			}) => match call_type {
				ast::nodes::CallType::Free { inlined_body, .. } => {
					let text = format!(
						"{}({})",
						name,
						if arguments.is_empty() { "" } else { "..." }
					);
					let mut n = Node::new(node_id(counter))
						.header(&text)
						.hover_src(src, span);
					if let Some(body) = inlined_body {
						n = n.named_child(counter, &*body, &"body", UNKNOWN_COLOR, src);
					}
					n
				}
				ast::nodes::CallType::Method { object, self_type } => {
					let text = format!(
						"obj.{}({}self, {})",
						name,
						self_type.as_src(),
						if arguments.is_empty() { "" } else { "..." }
					);
					Node::new(node_id(counter))
						.header(&text)
						.hover_src(src, span)
						.child(counter, &**object, src)
				}
				ast::nodes::CallType::Extern { id } => {
					let text = format!(
						"extern {}(id={})({})",
						name,
						id.expect("missing ExternCall id"),
						if arguments.is_empty() { "" } else { "..." }
					);
					Node::new(node_id(counter))
						.header(&text)
						.hover_src(src, span)
				}
				ast::nodes::CallType::Static { class } => {
					let text = format!(
						"{}::{}({})",
						class,
						name,
						if arguments.is_empty() { "" } else { "..." }
					);
					Node::new(node_id(counter))
						.header(&text)
						.hover_src(src, span)
				}
			},
			ast::Node::Block(block) => block.to_node(counter, src),
			ast::Node::Branch(Branch {
				span,
				condition,
				then_branch,
				else_branch,
				..
			}) => {
				let text = "if";
				let mut node = Node::new(node_id(counter))
					.header(&text)
					.hover_src(src, span)
					.named_child(counter, &**condition, &"condition", BOOL_COLOR, src)
					.named_child(counter, then_branch, &"then", EXEC_COLOR, src);

				if let Some(else_branch) = else_branch {
					node = node.named_child(counter, &**else_branch, &"else", EXEC_COLOR, src);
				}
				node
			}
			ast::Node::ExpressionStatement(ExpressionStatement {
				span,
				body,
				has_semicolon,
				..
			}) => {
				let text = format!("expr{}", if *has_semicolon { ";" } else { "" });
				Node::new(node_id(counter))
					.header(&text)
					.hover_src(src, span)
					.named_child(counter, &**body, &"body", EXEC_COLOR, src)
			}
			ast::Node::VariableDeclaration(VariableDeclaration {
				span,
				is_mut,
				name,
				ty,
				initializer,
				..
			}) => {
				let text = format!(
					"let {}{}{};",
					if *is_mut { "mut " } else { "" },
					name,
					ty.as_ref()
						.map(|ty| format!(": {}", ty))
						.unwrap_or_default(),
				);

				let mut node = Node::new(node_id(counter))
					.header(&text)
					.hover_src(src, span);

				if let Some(initializer) = initializer {
					node =
						node.named_child(counter, &**initializer, &"initializer", EXEC_COLOR, src);
				}

				node
			}
			ast::Node::While(While {
				span,
				condition,
				body,
				..
			}) => {
				let text = "while";
				Node::new(node_id(counter))
					.header(&text)
					.hover_src(src, span)
					.named_child(counter, &**condition, &"condition", BOOL_COLOR, src)
					.named_child(counter, body, &"body", EXEC_COLOR, src)
			}
			ast::Node::For(For {
				span,
				name,
				range_begin,
				range_end,
				range_inclusive,
				body,
				..
			}) => {
				let text = format!("for {}", name);
				Node::new(node_id(counter))
					.header(&text)
					.hover_src(src, span)
					.named_child(counter, &**range_begin, &"start", INT_COLOR, src)
					.named_child(
						counter,
						&**range_end,
						&if *range_inclusive { "=end" } else { "end" },
						INT_COLOR,
						src,
					)
					.named_child(counter, body, &"body", EXEC_COLOR, src)
			}
			ast::Node::FunctionDeclaration(FunctionDeclaration {
				span,
				name,
				parameters,
				return_type,
				body,
				..
			}) => {
				let text = format!(
					"fn {}({}){} {{ ... }}",
					name,
					if parameters.parameters.is_empty() {
						""
					} else {
						"..."
					},
					return_type
						.as_ref()
						.map(|ty| format!(" -> {}", ty))
						.unwrap_or_default()
				);
				Node::new(node_id(counter))
					.header(&text)
					.hover_src(src, span)
					.named_child(counter, body, &"body", EXEC_COLOR, src)
			}
			/*ast::Node::ReturnStatement(ReturnStatement { span, value }) => {
				let text = format!("return{};", if value.is_some() { " ..." } else { "" });
				let mut node = Node::new(node_id(counter)).header(&text).hover_src(src, span);

				if let Some(value) = value {
					node = node.named_child(counter, &**value, &"value", UNKNOWN_COLOR, src);
				}

				node
			}*/
			ast::Node::GetField(GetField {
				span, object, name, ..
			}) => {
				let text = format!("obj.{}", name);
				Node::new(node_id(counter))
					.header(&text)
					.hover_src(src, span)
					.child(counter, &**object, src)
			}
		}
	}
}
