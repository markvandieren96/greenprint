use crate::{Function, ScopeId, Variable};

#[derive(Debug)]
#[derive(Default)]
pub struct Scope {
	pub variables: Vec<Variable>,
	pub functions: Vec<Function>,
	pub children: Vec<ScopeId>,
}

impl std::fmt::Display for Scope {
	fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
		let contents = self
			.variables
			.iter()
			.map(std::string::ToString::to_string)
			.chain(self.functions.iter().map(std::string::ToString::to_string))
			.collect::<Vec<_>>()
			.join(", ");
		write!(f, "{{{contents}}}")
	}
}


