use eframe::egui;
use egui::{Color32 as Color, Ui};
use parser::SpannedErrorMessage;
use program::{ast, parser, reflect};
use reflect::meta::Meta;

pub struct Validator {
	src: String,
	state: State,
}

enum State {
	ParseError(parser::FailedParseInfo),
	TypecheckError(ast::Error),
	Valid,
}

impl Validator {
	pub fn new(src: &str, meta: &Meta) -> Self {
		let state = match program::Program::new("".to_owned(), meta, src) {
			Ok(_) => State::Valid,
			Err(e) => match e {
				program::CompileError::Parse(e) => State::ParseError(e),
				program::CompileError::TypeCheck(e) => State::TypecheckError(e.e),
			},
		};

		Self {
			src: src.to_string(),
			state,
		}
	}

	pub fn display(&mut self, ui: &mut Ui) {
		ui.horizontal(|ui| {
			match self.state {
				State::ParseError(_) => {
					ui.add(egui::Label::new("parse error").text_color(Color::RED));
				}
				State::TypecheckError(_) => {
					ui.add(egui::Label::new("typecheck error").text_color(Color::RED));
				}
				State::Valid => {
					ui.add(egui::Label::new("valid").text_color(Color::GREEN));
				}
			}

			ui.separator();
		});

		ui.separator();

		self.parse_error(ui);
		self.type_check_error(ui);
	}

	fn parse_error(&mut self, ui: &mut Ui) {
		let Self { src, state, .. } = self;

		let error = if let State::ParseError(error) = state {
			error
		} else {
			return;
		};
		let error = &error.errors[0];
		display_error(ui, src, error);
	}

	fn type_check_error(&mut self, ui: &mut Ui) {
		let Self { src, state, .. } = self;

		let error = if let State::TypecheckError(error) = state {
			error
		} else {
			return;
		};

		display_error(ui, src, error);
	}
}

fn display_error(ui: &mut Ui, src: &str, error: &dyn SpannedErrorMessage) {
	let error_span = error.span().unwrap_or((src.len() - 1)..src.len());
	let mut bytes_passed = 0;
	let mut processed = String::new();
	for (index, line) in src.lines().enumerate() {
		let line_nr = index + 1;
		let current_span = bytes_passed..bytes_passed + line.len();

		let s = src.get(current_span.clone()).unwrap_or("");

		if s != line {
			println!("MISMATCH");
			println!("line={}\nsubs={}", line, s);
		}

		let prefix = format!("[{}] ", line_nr);
		if current_span.is_empty()
			|| error_span.start > current_span.end
			|| error_span.end < current_span.start
		{
			ui.monospace(format!("{}{}", prefix, line));
		} else {
			let start = error_span.start.saturating_sub(current_span.start);
			let end = error_span
				.end
				.saturating_sub(current_span.start)
				.min(line.len());

			let before = error_span
				.start
				.checked_sub(current_span.start)
				.and_then(|start| line.get(..start))
				.unwrap_or("");
			let middle = line.get(start..end).unwrap_or("[INVALID RANGE]");
			let after = line.get(end..).unwrap_or("[INVALID RANGE]");
			ui.horizontal(|ui| {
				ui.style_mut().spacing.item_spacing.x = 0.0;
				ui.monospace(prefix);
				ui.monospace(before);
				ui.add(egui::Label::new(middle).monospace().text_color(Color::RED))
					.on_hover_text(error.message());
				ui.monospace(after);
			});
		}

		bytes_passed += line.len();
		processed += line;
		while let Some(c) = src.get(bytes_passed..=bytes_passed) {
			match c {
				"\r" | "\n" => {
					bytes_passed += 1;
					processed += c;
				}
				_ => break,
			}
		}
	}
}
