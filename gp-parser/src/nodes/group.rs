use crate::{expression, Errors, Expression, LexerExpectExt, ParseResult};
use lexer::{merge_spans, Lexer, LexerExt, Token};

/// Parses "(expr)"
#[derive(Debug, PartialEq, Clone)]
pub struct Group {
	pub span: lexer::Span,
	pub body: Box<Expression>,
}

impl Group {
	pub fn try_parse(lexer: &mut Lexer, errors: &mut Errors) -> ParseResult<Self> {
		let span_start = if lexer.peek_match(&Token::ParenthesisOpen) {
			lexer.next().unwrap().1
		} else {
			return Ok(None);
		};

		let body = expression(lexer, errors);
		let span_end = lexer.expect(Token::ParenthesisClose, "Group")?;

		Ok(Some(Self {
			span: merge_spans(&span_start, &span_end),
			body: Box::new(body),
		}))
	}
}
