use crate::TypeMap;
use gp_parser::nodes::Access;
use stid::{Id, Stid};

#[derive(PartialEq, Eq, Hash, Clone, Copy, Debug)]
pub enum Type {
	Owned(Id),
	Borrow(Id),
	BorrowMut(Id),
}

impl Type {
	#[must_use]
	pub fn owned<T: Stid>() -> Self {
		Self::Owned(T::ID)
	}

	#[must_use]
	pub fn borrow<T: Stid>() -> Self {
		Self::Borrow(T::ID)
	}

	#[must_use]
	pub fn borrow_mut<T: Stid>() -> Self {
		Self::BorrowMut(T::ID)
	}

	#[must_use]
	pub fn from_owned(id: Id) -> Self {
		Self::Owned(id)
	}

	#[must_use]
	pub fn from_borrow(id: Id) -> Self {
		Self::Borrow(id)
	}

	#[must_use]
	pub fn from_borrow_mut(id: Id) -> Self {
		Self::BorrowMut(id)
	}

	#[must_use]
	pub fn access(self) -> Access {
		match self {
			Type::Owned(_) => Access::Owned,
			Type::Borrow(_) => Access::Ref,
			Type::BorrowMut(_) => Access::RefMut,
		}
	}

	#[must_use]
	pub fn id(self) -> Id {
		match self {
			Type::Owned(id) | Type::Borrow(id) | Type::BorrowMut(id) => id,
		}
	}

	#[must_use]
	pub fn can_coerce(&self, target: &Self) -> bool {
		self.id() == target.id()
			&& match target {
				Type::Owned(_) => matches!(self, Type::Owned(_)),
				Type::Borrow(_) => true,
				Type::BorrowMut(_) => matches!(self, Type::Owned(_) | Type::BorrowMut(_)),
			}
	}

	#[must_use]
	pub fn display(&self, typemap: &TypeMap) -> String {
		match self {
			Type::Owned(id) => typemap.get(*id).unwrap().to_string(),
			Type::Borrow(id) => format!("&{}", typemap.get(*id).unwrap()),
			Type::BorrowMut(id) => format!("&mut {}", typemap.get(*id).unwrap()),
		}
	}
}
