use eframe::egui;
use egui::Ui;
use std::collections::HashSet;
use std::path::PathBuf;

pub struct FileBrowser {
	working_dir_prefix: PathBuf,
	files: Vec<PathBuf>,
	selected: HashSet<PathBuf>,
	allow_multi_select: bool,
	hide_multi_select: bool,
}

impl Default for FileBrowser {
	fn default() -> Self {
		let working_dir_prefix: PathBuf = crate::arg::get("working_dir", "").into();
		let files: Vec<PathBuf> = io::find_all_files(&working_dir_prefix, true)
			.into_iter()
			.filter(|path| path.extension().map_or(false, |ext| ext == "gp"))
			.collect();

		Self {
			files,
			selected: HashSet::new(),
			working_dir_prefix,
			allow_multi_select: false,
			hide_multi_select: false,
		}
	}
}

impl FileBrowser {
	pub fn hide_multi_select(mut self) -> Self {
		self.hide_multi_select = true;
		self
	}

	/// Returns true if the selection has changed
	pub fn display(&mut self, ui: &mut Ui) -> bool {
		let Self {
			working_dir_prefix,
			files,
			selected,
			allow_multi_select,
			hide_multi_select,
		} = self;

		if !*hide_multi_select {
			ui.checkbox(allow_multi_select, "multi-select");
		}

		let mut working_dir = std::env::current_dir().unwrap_or_default();
		working_dir.push(&working_dir_prefix);
		if let Ok(dir) = working_dir.canonicalize() {
			working_dir = dir;
		}

		let mut selection_changed = false;
		for raw_path in files {
			let path = raw_path.canonicalize().unwrap();
			let mut path = path
				.strip_prefix(&working_dir)
				.unwrap_or(&path)
				.with_extension("");
			if path.extension().map_or(false, |ext| ext == "gpscript") {
				path = path.with_extension("");
			}

			let text: &str = &path.to_string_lossy();
			let is_selected = selected.iter().any(|selected| selected == raw_path);
			if ui.selectable_label(is_selected, text).clicked() {
				selection_changed = true;
				if *allow_multi_select {
					if is_selected {
						selected.remove(raw_path);
					} else {
						selected.insert(raw_path.clone());
					}
				} else {
					selected.clear();
					selected.insert(raw_path.clone());
				}
			}
		}

		selection_changed
	}

	pub fn selection(&self) -> impl Iterator<Item = &PathBuf> {
		self.selected.iter()
	}
}
