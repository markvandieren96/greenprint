use crate::{nodes::ArgumentList, Expression};
use lexer::Span;

/// Parses "foo.bar(expr, expr2, ...)"
#[derive(Debug, PartialEq, Clone)]
pub struct MethodCall {
	pub span: Span,
	pub object: Box<Expression>,
	pub name: String,
	pub arguments: ArgumentList,
}
