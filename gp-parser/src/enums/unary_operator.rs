use lexer::Token;

#[derive(Debug, PartialEq, Clone)]
pub enum UnaryOperator {
	Bang,
	Neg,
}

impl std::convert::TryFrom<Token> for UnaryOperator {
	type Error = ();

	fn try_from(token: Token) -> Result<Self, Self::Error> {
		match token {
			Token::Bang => Ok(UnaryOperator::Bang),
			Token::Min => Ok(UnaryOperator::Neg),
			_ => Err(()),
		}
	}
}

impl std::fmt::Display for UnaryOperator {
	fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
		match self {
			Self::Bang => write!(f, "!"),
			Self::Neg => write!(f, "-"),
		}
	}
}

impl UnaryOperator {
	#[must_use]
	pub fn name(&self) -> &'static str {
		match self {
			Self::Bang => "op_bang",
			Self::Neg => "op_neg",
		}
	}
}
