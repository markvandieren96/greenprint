use crate::{ptr::Ptr, Error};
use stid::{Id, Stid};

pub struct Value {
	type_name: &'static str,
	type_id: Id,
	ptr: Ptr,
}

#[derive(Debug, Clone, Copy)]
pub struct TypeMismatch {
	expected: &'static str,
	got: &'static str,
}

impl Value {
	pub fn new<T: 'static + Stid>(t: T) -> Self {
		Self {
			type_name: T::NAME,
			type_id: T::ID,
			ptr: Ptr::new(t),
		}
	}

	pub fn new_ref<T: 'static + Stid>(t: &T) -> Self {
		let ptr: *const () = (t as *const T).cast::<()>();
		Self {
			type_name: T::NAME,
			type_id: T::ID,
			ptr: Ptr::Ref(ptr),
		}
	}

	pub fn new_ref_mut<T: 'static + Stid>(t: &mut T) -> Self {
		let ptr: *mut () = (t as *mut T).cast::<()>();
		Self {
			type_name: T::NAME,
			type_id: T::ID,
			ptr: Ptr::RefMut(ptr),
		}
	}

	/*fn downcast<T: 'static + Stid>(self) -> Result<T, Error> {
		self.check_type_id::<T>()?;
		Ok(unsafe { std::ptr::read(self.ptr.ptr_as::<T>()) })
	}*/

	pub fn downcast_copy<T: 'static + Stid + Copy>(&self) -> Result<T, Error> {
		self.check_type_id::<T>()?;
		Ok(unsafe { *self.ptr.ptr_as::<T>() })
	}

	pub fn downcast_ref<T: 'static + Stid>(&self) -> Result<&T, Error> {
		self.check_type_id::<T>()?;
		Ok(unsafe { &*self.ptr.ptr_as::<T>() })
	}

	pub fn downcast_mut<T: 'static + Stid>(&mut self) -> Result<&mut T, Error> {
		self.check_type_id::<T>()?;
		Ok(unsafe { &mut *self.ptr.ptr_mut_as::<T>()? })
	}

	pub fn assign<T: 'static + Stid>(&mut self, t: T) -> Result<(), Error> {
		*self.downcast_mut::<T>()? = t;
		Ok(())
	}

	#[must_use]
	pub fn create_ref(&self) -> Self {
		Self {
			type_name: self.type_name,
			type_id: self.type_id,
			ptr: self.ptr.get_ref(),
		}
	}

	pub fn create_ref_mut(&mut self) -> Result<Self, Error> {
		Ok(Self {
			type_name: self.type_name,
			type_id: self.type_id,
			ptr: self.ptr.get_ref_mut()?,
		})
	}

	fn check_type_id<T: Stid>(&self) -> Result<(), TypeMismatch> {
		if self.type_id != T::ID {
			return Err(TypeMismatch {
				expected: self.type_name,
				got: T::NAME,
			});
		}
		Ok(())
	}

	#[must_use]
	pub fn id(&self) -> Id {
		self.type_id
	}
}

#[cfg(test)]
mod tests {
	use super::*;

	#[test]
	fn new() {
		let val = Value::new(6);
		assert_eq!(6, val.downcast_copy::<i32>().unwrap());
	}

	#[test]
	fn assign() {
		let mut val = Value::new::<i32>(0);
		val.assign(6).unwrap();
		assert_eq!(6, val.downcast_copy::<i32>().unwrap());
	}

	#[test]
	fn assign_wrong_type() {
		let mut val = Value::new::<i32>(0);
		let res = val.assign(6.0);
		assert!(res.unwrap_err().is_type_mismatch());
	}

	#[test]
	fn downcast_copy_wrong_type() {
		let val = Value::new(6);
		let res = val.downcast_copy::<f32>();
		assert!(res.unwrap_err().is_type_mismatch());
	}

	#[test]
	fn downcast_ref_wrong_type() {
		let val = Value::new(6);
		let res = val.downcast_ref::<f32>();
		assert!(res.unwrap_err().is_type_mismatch());
	}

	#[test]
	fn downcast_mut_wrong_type() {
		let mut val = Value::new(6);
		let res = val.downcast_mut::<f32>();
		assert!(res.unwrap_err().is_type_mismatch());
	}

	#[test]
	fn downcast_mut_on_const() {
		let mut val = Value::new_ref(&6);
		let res = val.downcast_mut::<i32>();
		assert!(res.unwrap_err().is_const_violation());
	}

	#[test]
	fn access_tmp_ref() {
		let val = Value::new_ref(&6);
		let res = val.downcast_copy::<i32>().unwrap();
		assert_eq!(res, 6);
	}

	#[test]
	fn create_ref() {
		let val = Value::new(6);
		let val2 = val.create_ref();
		let res = val2.downcast_copy::<i32>().unwrap();
		assert_eq!(res, 6);
	}

	#[test]
	fn create_ref_mut() {
		let mut val = Value::new(6);
		let mut val2 = val.create_ref_mut().unwrap();
		val2.assign(7).unwrap();
		let res1 = val.downcast_copy::<i32>().unwrap();
		assert_eq!(res1, 7);
		let res2 = val2.downcast_copy::<i32>().unwrap();
		assert_eq!(res2, 7);
	}
}
