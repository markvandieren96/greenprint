use logos::Span;

#[must_use]
pub fn convert_span_bytes_to_lines_numbers(span: Span, src: &str) -> std::ops::Range<usize> {
	let begin = count_newlines_up_to(span.start, src);
	let end = count_newlines_up_to(span.end, src);
	begin..end
}

fn count_newlines_up_to(byte_index: usize, src: &str) -> usize {
	let src = &src[0..byte_index];
	src.chars().filter(|c| *c == '\n').count() + 1
}
