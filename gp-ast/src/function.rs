use crate::{nodes::Block, Access, FunctionId, Identifier, ScopeId};
use reflect::meta::{ClassId, Meta};

#[derive(Debug)]
pub struct Function {
	pub(crate) name: String,
	pub(crate) parameters: Vec<Parameter>,
	pub(crate) return_type: Option<ClassId>,
	pub body: Block,
	pub extern_call_scope: ScopeId,
	pub id: FunctionId,
}

impl std::fmt::Display for Function {
	fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
		write!(f, "{}(", self.name)?;
		let parameters = self
			.parameters
			.iter()
			.map(|p| format!("{p}"))
			.collect::<Vec<_>>()
			.join(", ");
		write!(f, "{parameters})")?;

		if let Some(ty) = &self.return_type {
			write!(f, " -> {ty}")?;
		}
		Ok(())
	}
}

#[derive(Debug, Clone)]
pub struct Parameter {
	pub name: Identifier,
	pub access: Access,
	pub ty: ClassId,
}

impl Parameter {
	pub fn from(p: parser::nodes::Parameter, meta: &Meta) -> Result<Self, reflect::Error> {
		let ty = ClassId(p.ty.ty);
		Ok(Self {
			name: p.name.into(),
			access: Access::from(&p.ty.access, &ty, meta)?,
			ty,
		})
	}
}

impl std::fmt::Display for Parameter {
	fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
		write!(f, "{}: {}{}", self.name, self.access.as_src(), self.ty)
	}
}
