mod function;
mod registry;
mod r#type;

pub use function::*;
pub use r#type::*;
pub use registry::*;

use crate::Value;
use gp2_macro::{derive, function};
use stid::Stid;

pub fn register_std(reg: &mut Registry) {
	reg.register_type::<()>();

	derive!(bool, equality, logical, op_bang, default, clone, copy, to_string);
	derive!(String, equality, default, clone);
	derive!(i8, arithmetic, equality, ordering, op_neg, default, clone, copy);
	derive!(i16, arithmetic, equality, ordering, op_neg, default, clone, copy);
	derive!(i32, arithmetic, equality, ordering, op_neg, default, clone, copy);
	derive!(i64, arithmetic, equality, ordering, op_neg, default, clone, copy);
	derive!(i128, arithmetic, equality, ordering, op_neg, default, clone, copy);
	derive!(isize, arithmetic, equality, ordering, op_neg, default, clone, copy);
	derive!(u8, arithmetic, equality, ordering, default, clone, copy);
	derive!(u16, arithmetic, equality, ordering, default, clone, copy);
	derive!(u32, arithmetic, equality, ordering, default, clone, copy);
	derive!(u64, arithmetic, equality, ordering, default, clone, copy);
	derive!(u128, arithmetic, equality, ordering, default, clone, copy);
	derive!(usize, arithmetic, equality, ordering, default, clone, copy);
	derive!(f32, arithmetic, ordering, op_neg, default, clone, copy);
	derive!(f64, arithmetic, ordering, op_neg, default, clone, copy);

	derive!(
		i8, to_string, to_i16, to_i32, to_i64, to_i128, to_isize, to_u8, to_u16, to_u32, to_u64,
		to_u128, to_usize, to_f32, to_f64
	);
	derive!(
		i16, to_string, to_i8, to_i32, to_i64, to_i128, to_isize, to_u8, to_u16, to_u32, to_u64,
		to_u128, to_usize, to_f32, to_f64
	);
	derive!(
		i32, to_string, to_i8, to_i16, to_i64, to_i128, to_isize, to_u8, to_u16, to_u32, to_u64,
		to_u128, to_usize, to_f32, to_f64
	);
	derive!(
		i64, to_string, to_i8, to_i16, to_i32, to_i128, to_isize, to_u8, to_u16, to_u32, to_u64,
		to_u128, to_usize, to_f32, to_f64
	);
	derive!(
		i128, to_string, to_i8, to_i16, to_i32, to_i64, to_isize, to_u8, to_u16, to_u32, to_u64,
		to_u128, to_usize, to_f32, to_f64
	);
	derive!(
		isize, to_string, to_i8, to_i16, to_i32, to_i64, to_i128, to_u8, to_u16, to_u32, to_u64,
		to_u128, to_usize, to_f32, to_f64
	);

	derive!(
		u8, to_string, to_i8, to_i16, to_i32, to_i64, to_i128, to_isize, to_u16, to_u32, to_u64,
		to_u128, to_usize, to_f32, to_f64
	);
	derive!(
		u16, to_string, to_i8, to_i16, to_i32, to_i64, to_i128, to_isize, to_u8, to_u32, to_u64,
		to_u128, to_usize, to_f32, to_f64
	);
	derive!(
		u32, to_string, to_i8, to_i16, to_i32, to_i64, to_i128, to_isize, to_u8, to_u16, to_u64,
		to_u128, to_usize, to_f32, to_f64
	);
	derive!(
		u64, to_string, to_i8, to_i16, to_i32, to_i64, to_i128, to_isize, to_u8, to_u16, to_u32,
		to_u128, to_usize, to_f32, to_f64
	);
	derive!(
		u128, to_string, to_i8, to_i16, to_i32, to_i64, to_i128, to_isize, to_u8, to_u16, to_u32,
		to_u64, to_usize, to_f32, to_f64
	);
	derive!(
		usize, to_string, to_i8, to_i16, to_i32, to_i64, to_i128, to_isize, to_u8, to_u16, to_u32,
		to_u64, to_u128, to_f32, to_f64
	);

	derive!(
		f32, to_string, to_i8, to_i16, to_i32, to_i64, to_i128, to_isize, to_u8, to_u16, to_u32,
		to_u64, to_u128, to_usize, to_f64
	);
	derive!(
		f64, to_string, to_i8, to_i16, to_i32, to_i64, to_i128, to_isize, to_u8, to_u16, to_u32,
		to_u64, to_u128, to_usize, to_f32
	);

	reg.register_function(function!(
		fn op_add(a: &String, b: &String) -> String {
			format!("{a}{b}")
		}
	));
}

#[cfg(test)]
mod tests {
	use super::*;
	use gp2_macro::signature;

	#[test]
	fn test1() {
		let reg = Registry::new();
		let sig = signature!(op_add(&i32, &i32) -> i32);
		let f = reg.get_function(&sig).unwrap();

		assert_eq!(
			4,
			f.0(&mut [Value::new(2), Value::new(2)])
				.unwrap()
				.unwrap()
				.downcast_copy::<i32>()
				.unwrap()
		);
	}

	#[test]
	fn test2() {
		let reg = Registry::new();
		let sig = signature!(default() -> i8);
		let f = reg.get_static_function(i8::ID, &sig).unwrap();

		assert_eq!(
			0,
			f.0(&mut [])
				.unwrap()
				.unwrap()
				.downcast_copy::<i8>()
				.unwrap()
		);
	}
}
