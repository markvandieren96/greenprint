#[macro_export]
macro_rules! field {
	($class:expr, $name:ident: const $field_type:ty) => {
		$crate::class_field!($class, $name: const $field_type);
	};
	($class:expr, $name:ident: $field_type:ty) => {
		$crate::class_field!($class, $name: $field_type);
	};
}

#[macro_export]
macro_rules! class_field {
	($class:expr, $name:ident: const $field_type:ty) => {
		$class.add_field(
			stringify!($name),
			$crate::Field::new(
				std::any::TypeId::of::<$field_type>(),
				Box::new(|obj| {
					let obj = obj.downcast_ref::<Self>().unwrap();
					let field: &$field_type = &obj.$name;
					Object::new_ref(field)
				}),
			),
		);
	};
	($class:expr, $name:ident: $field_type:ty) => {
		$class.add_field(
			stringify!($name),
			$crate::Field::new_mut(
				std::any::TypeId::of::<$field_type>(),
				Box::new(|obj| {
					let obj = obj.downcast_ref::<Self>().unwrap();
					let field: &$field_type = &obj.$name;
					Object::new_ref(field)
				}),
				Box::new(|obj| {
					let obj = obj.downcast_mut::<Self>().unwrap();
					let field: &mut $field_type = &mut obj.$name;
					Object::new_ref_mut(field)
				}),
			),
		);
	};
}
