use crate::{
	and, comparison, equality, factor, term, unary, BinaryOperator, Error, Errors, Expression,
};
use lexer::{Lexer, LexerExt, Span, Token};
use std::convert::TryInto;

#[derive(Debug, PartialEq, Clone)]
pub struct BinaryOp {
	pub span: Span,
	pub left: Box<Expression>,
	pub operator: BinaryOperator,
	pub right: Box<Expression>,
}

impl BinaryOp {
	pub fn try_parse_equality(
		lexer: &mut Lexer,
		errors: &mut Errors,
		mut expr: Expression,
	) -> Result<Expression, Error> {
		while let Some((token, span_start)) = lexer.matches_any(&[Token::Equal, Token::NotEqual]) {
			let operator: BinaryOperator = token.try_into().unwrap();
			let right = comparison(lexer, errors)?;
			expr = Expression::BinaryOp(BinaryOp {
				span: span_start, // TODO merge with the spans of the operands
				left: Box::new(expr),
				operator,
				right: right.into(),
			});
		}

		Ok(expr)
	}

	pub fn try_parse_comparisson(
		lexer: &mut Lexer,
		errors: &mut Errors,
		mut expr: Expression,
	) -> Result<Expression, Error> {
		while let Some((token, span_start)) = lexer.matches_any(&[
			Token::LessThan,
			Token::GreaterThan,
			Token::LessEqual,
			Token::GreaterEqual,
		]) {
			let operator: BinaryOperator = token.try_into().unwrap();
			let right = term(lexer, errors)?;
			expr = Expression::BinaryOp(BinaryOp {
				span: span_start, // TODO merge with the spans of the operands
				left: Box::new(expr),
				operator,
				right: right.into(),
			});
		}

		Ok(expr)
	}

	pub fn try_parse_term(
		lexer: &mut Lexer,
		errors: &mut Errors,
		mut expr: Expression,
	) -> Result<Expression, Error> {
		while let Some((token, span_start)) = lexer.matches_any(&[Token::Plus, Token::Min]) {
			let operator: BinaryOperator = token.try_into().unwrap();
			let right = factor(lexer, errors)?;
			expr = Expression::BinaryOp(BinaryOp {
				span: span_start, // TODO merge with the spans of the operands
				left: Box::new(expr),
				operator,
				right: right.into(),
			});
		}

		Ok(expr)
	}

	pub fn try_parse_factor(
		lexer: &mut Lexer,
		errors: &mut Errors,
		mut expr: Expression,
	) -> Result<Expression, Error> {
		while let Some((token, span_start)) = lexer.matches_any(&[Token::Mul, Token::Div]) {
			let operator: BinaryOperator = token.try_into().unwrap();
			let right = unary(lexer, errors)?;
			expr = Expression::BinaryOp(BinaryOp {
				span: span_start, // TODO merge with the spans of the operands
				left: Box::new(expr),
				operator,
				right: right.into(),
			});
		}

		Ok(expr)
	}

	pub fn try_parse_or(
		lexer: &mut Lexer,
		errors: &mut Errors,
		mut expr: Expression,
	) -> Result<Expression, Error> {
		while let Some(span_start) = lexer.matches_spanned(&Token::Or) {
			let right = and(lexer, errors)?;
			expr = Expression::BinaryOp(BinaryOp {
				span: span_start, // TODO merge with the spans of the operands
				left: Box::new(expr),
				operator: BinaryOperator::Or,
				right: right.into(),
			});
		}

		Ok(expr)
	}

	pub fn try_parse_and(
		lexer: &mut Lexer,
		errors: &mut Errors,
		mut expr: Expression,
	) -> Result<Expression, Error> {
		while let Some(span_start) = lexer.matches_spanned(&Token::And) {
			let right = equality(lexer, errors)?;
			expr = Expression::BinaryOp(BinaryOp {
				span: span_start, // TODO merge with the spans of the operands
				left: Box::new(expr),
				operator: BinaryOperator::And,
				right: right.into(),
			});
		}

		Ok(expr)
	}
}
