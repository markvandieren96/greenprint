mod component;
mod dummy;
mod helper;
mod resource;

pub use component::*;

pub use helper::*;
pub use resource::*;

use super::*;
use assert_approx_eq::assert_approx_eq;
use program::{ast::VariableAccessType, TypeCheckError};
use specs::Builder;

#[test]
#[cfg_attr(miri, ignore)]
fn create_entity() {
	let meta = init();

	let program = Program::new(String::new(), &meta, "Entity::new()").unwrap();
	let mut state = program.create_state();
	let world = World::new();
	let _entity = program
		.eval(&mut state, &world)
		.expect("eval error")
		.expect("no return value")
		.downcast_ref::<specs::Entity>()
		.expect("return value is not Entity");
}

#[test]
#[cfg_attr(miri, ignore)]
fn delete_entity() {
	let meta = init();

	let program = Program::new(
		String::new(),
		&meta,
		"let extern entity: Entity; entity.delete();",
	)
	.unwrap();
	let mut state = program.create_state();
	let mut world = World::new();
	let entity = world.create_entity().build();

	*state.find_mut("entity").unwrap() = Object::new(true, entity);

	assert!(program
		.eval(&mut state, &world)
		.expect("eval error")
		.is_none());
}

#[test]
fn derived_traits() {
	let meta = init();

	assert_eq!(eval::<i64>(&meta, "i64::default()"), 0);
	assert_eq!(eval::<i64>(&meta, "1 + 2"), 3);
	assert_eq!(eval::<i64>(&meta, "3 - 2"), 1);
	assert_eq!(eval::<i64>(&meta, "3 * 2"), 6);
	assert_eq!(eval::<i64>(&meta, "6 / 2"), 3);
	assert_eq!(eval::<i64>(&meta, "let a = 2; -a"), -2);
	assert_eq!(eval::<i64>(&meta, "2.clone()"), 2);
	assert_eq!(eval::<i64>(&meta, "2.copy()"), 2);
	assert!(eval::<bool>(&meta, "2 == 2"));
	assert!(!eval::<bool>(&meta, "2 == 1"));
	assert!(!eval::<bool>(&meta, "2 != 2"));
	assert!(eval::<bool>(&meta, "2 != 1"));
	assert!(eval::<bool>(&meta, "2 < 3"));
	assert!(!eval::<bool>(&meta, "3 < 2"));
	assert!(eval::<bool>(&meta, "2 <= 2"));
	assert!(!eval::<bool>(&meta, "3 <= 2"));
	assert!(!eval::<bool>(&meta, "2 > 3"));
	assert!(eval::<bool>(&meta, "3 > 2"));
	assert!(eval::<bool>(&meta, "2 >= 2"));
	assert!(!eval::<bool>(&meta, "2 >= 3"));

	assert!(eval::<bool>(&meta, "true and true"));
	assert!(!eval::<bool>(&meta, "true and false"));
	assert!(!eval::<bool>(&meta, "false or false"));
	assert!(eval::<bool>(&meta, "false or true"));
}

#[test]
fn test_std_methods() {
	let meta = init();

	assert_approx_eq!(eval::<f32>(&meta, "6.to_f32()"), 6.0);
	assert_eq!(eval::<i32>(&meta, "6.to_i32()"), 6_i32);
	assert_eq!(eval::<String>(&meta, "6.to_string()"), "6".to_owned());

	assert_eq!(eval::<i32>(&meta, "6.0.to_i32()"), 6_i32);
	assert_eq!(eval::<i64>(&meta, "6.0.to_i64()"), 6_i64);
}

#[test]
#[cfg_attr(miri, ignore)]
fn overloaded_rand() {
	let meta = init();

	eval::<i64>(&meta, "rand_i()");
	assert!((1..10).contains(&eval::<i64>(&meta, "rand_i(1, 10)")));
	assert_eq!(eval::<i64>(&meta, "rand_i(9, 10)"), 9);
	assert!((0.0..1.0).contains(&eval::<f32>(&meta, "rand_f()")));
	assert!((5.0..10.0).contains(&eval::<f32>(&meta, "rand_f(5.0, 10.0)")));
}

#[test]
fn program_has_fn() {
	let meta = init();

	let program = Program::new(String::new(), &meta, "fn hello_world() {}")
		.expect("Failed to compile program");

	println!("Program: {program:#?}");

	assert!(program.has_fn("hello_world"));
}

#[test]
fn program_call_fn() {
	let meta = init();

	let program = Program::new(String::new(), &meta, "fn hello_world() { 6 }")
		.expect("Failed to compile program");

	println!("Program: {program:#?}");

	let mut state = program.create_state();

	let res = program
		.call(&mut state, "hello_world", &World::new(), &mut [])
		.expect("call failed")
		.expect("no return value");
	assert_eq!(*res.downcast_ref::<i64>().expect("downcast failed"), 6);
}

#[test]
fn program_call_fn_with_args() {
	let meta = init();

	let program = Program::new(
		String::new(),
		&meta,
		"fn hello_world(a: i64, b: i64) { 6 + a + b }",
	)
	.expect("Failed to compile program");

	println!("Program: {program:#?}");

	let mut state = program.create_state();

	let res = program
		.call(
			&mut state,
			"hello_world",
			&World::new(),
			&mut [Object::new(true, 1_i64), Object::new(true, 2_i64)],
		)
		.expect("call failed")
		.expect("no return value");
	assert_eq!(*res.downcast_ref::<i64>().expect("downcast failed"), 9);
}

#[test]
fn program_get_value() {
	let meta = init();

	let program =
		Program::new(String::new(), &meta, "let a = 6;").expect("Failed to compile program");
	let mut state = program.create_state();
	program
		.eval(&mut state, &World::new())
		.expect("Eval failed");

	assert_eq!(
		*program
			.get_value(&state, "a")
			.expect("No value")
			.downcast_ref::<i64>()
			.expect("Downcast failed"),
		6
	);
}

#[test]
fn state_find_value() {
	let meta = init();

	let program =
		Program::new(String::new(), &meta, "let a = 6;").expect("Failed to compile program");
	let mut state = program.create_state();
	program
		.eval(&mut state, &World::new())
		.expect("Eval failed");

	assert_eq!(
		*state
			.find("a")
			.expect("No value")
			.downcast_ref::<i64>()
			.expect("Downcast failed"),
		6
	);
}

#[test]
fn option() {
	let meta = init();

	assert_eq!(eval::<i64>(&meta, "Option_i64::some(6).unwrap()"), 6);
	assert!(eval::<bool>(&meta, "Option_i64::some(6).is_some()"));
	assert!(!eval::<bool>(&meta, "Option_i64::none().is_some()"));
	assert!(!eval::<bool>(&meta, "Option_i64::some(6).is_none()"));
	assert!(eval::<bool>(&meta, "Option_i64::none().is_none()"));
	//TODO
	/*assert_eq!(
		eval::<i64>(
			"
	let a = Option_i64::some(6);
	let mut b = a.unwrap_mut();
	b = 7;
	a.unwrap()"
		),
		7
	);*/
}

/*#[test]
fn branch() {
	let meta = init();
	enable_tracing();

	let src = "let a = true;
		if a {
			return 1;
		} else {
			return 2;
		}
	";

	assert_eq!(eval::<i64>(&meta, src), 1);
}

#[test]
fn while_loop() {
	let meta = init();

	let src = "let a = false;
		while a {
			return 1;
		}
		2
	";

	assert_eq!(eval::<i64>(&meta, src), 2);
}*/

#[test]
fn fn_arg() {
	let meta = init();

	let src = "fn hello_world(i: i64) -> i64 {
		i + 1
	}
	let a = 5;
	hello_world(a)
	";

	assert_eq!(eval::<i64>(&meta, src), 6);
}

/*#[test]
fn function_order() {
	let meta = init();

	let src = "return hello_world();
	fn hello_world() -> i64 {
		1
	}
	2
	";

	assert_eq!(eval::<i64>(&meta, src), 1);
}*/

#[test]
fn use_after_move1() {
	let meta = init();

	let src = r#"
	let a = "hello";
	println(a);
	println(a);
	"#;

	let res = Program::new(String::new(), &meta, src);
	if let Err(program::CompileError::TypeCheck(error)) = res {
		if let TypeCheckError {
			e:
				program::ast::Error::VariableAccess {
					ty: VariableAccessType::UseAfterMove,
					..
				},
			..
		} = *error
		{
		} else {
			panic!();
		}
	} else {
		panic!();
	}
}

#[test]
fn use_after_move2() {
	let meta = init();

	let src = r#"
	let mut a = "hello";
	println(a);
	a = "new_str";
	println(a);
	"#;

	let program = Program::new(String::new(), &meta, src).expect("Failed to compile program");
	let mut state = program.create_state();
	program
		.eval(&mut state, &World::new())
		.expect("Eval failed");
}

#[test]
fn use_of_untyped_variable() {
	let meta = init();

	let src = r"
	let mut a;
	println(a);
	";

	let res = Program::new(String::new(), &meta, src);
	if let Err(program::CompileError::TypeCheck(error)) = res {
		if let TypeCheckError {
			e:
				program::ast::Error::VariableAccess {
					ty: VariableAccessType::Untyped,
					..
				},
			..
		} = *error
		{
		} else {
			panic!();
		}
	} else {
		panic!();
	}
}

#[test]
fn use_of_uninitialized_variable() {
	let meta = init();

	let src = r"
	let mut a: String;
	println(a);
	";

	let res = Program::new(String::new(), &meta, src);
	if let Err(program::CompileError::TypeCheck(error)) = res {
		if let TypeCheckError {
			e:
				program::ast::Error::VariableAccess {
					ty: VariableAccessType::Uninitialized,
					..
				},
			..
		} = *error
		{
		} else {
			panic!();
		}
	} else {
		panic!();
	}
}

/*#[test]
fn repeated_move() {
	let meta = init();

	let src = r#"
	let a = "Hello, world!";
	while true {
		println(a);
	}
	"#;

	let res = Program::new("".to_owned(), &meta, src);
	if let Err(program::CompileError::TypeCheck(program::ast::Error::UseAfterMove { .. })) = res {
	} else {
		println!("Res: {:#?}", res);
		panic!()
	}
}
*/

/*#[test]
fn test_return() {
	let meta = init();

	let src = "
	fn test() {
		if true {
			return 1;
		}
		2
	}

	test()
	";

	assert_eq!(eval::<i64>(&meta, src), 1);
}
*/

#[test]
fn extern_call_inside_function_definition() {
	let meta = init();

	let src = "
	fn hello_world() {
		let b = rand_bool();
	}
	hello_world();
	1
	";

	assert_eq!(eval::<i64>(&meta, src), 1);
}

#[test]
fn extern_call_inside_method_call_object() {
	// The == operator gets desugared into a methodcall, where the left operand is the self/object node
	let meta = init();

	let src = "
	let a = rand_bool() == true;
	1
	";

	assert_eq!(eval::<i64>(&meta, src), 1);
}

#[test]
fn shadowing() {
	let meta = init();

	let src = "
	let a = 1;
	let a = a == 1;
	a
	";

	assert!(eval::<bool>(&meta, src));
}

mod drop_tests {
	use super::*;

	#[allow(clippy::module_name_repetitions)]
	#[derive(Default)]
	pub struct StructWithDrop {
		foo: i64,
	}

	impl_type_name!(StructWithDrop);

	impl Register for StructWithDrop {
		fn register(registry: &mut ClassRegistry) {
			let mut class = Class::new::<Self>();
			reflect::field!(&mut class, foo: i64);
			reflect::block!(derive(default, drop));
			registry.register(class);
		}
	}

	#[test]
	fn regular_drop() {
		let meta = init();

		let src = "
	let a = StructWithDrop::default();
	0
	";

		assert_eq!(eval::<i64>(&meta, src), 0);
	}

	#[test]
	fn drop_in_block() {
		let meta = init();

		let src = "
		{
			let a = StructWithDrop::default();
		}
		0
	";

		assert_eq!(eval::<i64>(&meta, src), 0);
	}

	#[test]
	fn drop_in_branch() {
		let meta = init();

		let src = "
		if true {
			let a = StructWithDrop::default();
		}
		0
	";

		assert_eq!(eval::<i64>(&meta, src), 0);
	}

	#[test]
	fn drop_in_function() {
		let meta = init();

		let src = "
		fn hello() {
			let a = StructWithDrop::default();
		}
		hello();
		0
	";

		assert_eq!(eval::<i64>(&meta, src), 0);
	}

	#[test]
	fn drop_in_branch_in_function() {
		let meta = init();

		let src = "
		fn hello() {
			if true {
				let a = StructWithDrop::default();
			}
		}
		hello();
		0
	";

		assert_eq!(eval::<i64>(&meta, src), 0);
	}
}

#[test]
fn variable_declaration_in_function() {
	let meta = init();

	let src = "
	fn hello_world() -> bool {
		let a = true;
		a
	}

	hello_world()
	";

	assert!(eval::<bool>(&meta, src));
}

#[test]
fn use_extern_variable_in_fn() {
	enable_tracing();
	let meta = init();

	let src = "
	let extern a: bool;
	fn get_a() -> bool {
		a
	}
	";

	let program = Program::new(String::new(), &meta, src).expect("Failed to compile program");
	let mut state = program.create_state();
	*state.find_mut("a").expect("Failed to find variable") = Object::new(true, true);
	let world = World::new();
	program.eval(&mut state, &world).expect("eval error");
	let res = program
		.call(&mut state, "get_a", &world, &mut [])
		.expect("Failed to call function");

	assert!(res
		.expect("Function did not return a value")
		.downcast_ref::<bool>()
		.expect("Result was not a bool"));
}
