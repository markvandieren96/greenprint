#![warn(clippy::all)]
#![warn(clippy::pedantic)]
#![allow(clippy::missing_errors_doc)]
#![allow(clippy::missing_panics_doc)]

pub use greenprint_derive::{Reflect, TypeName};
pub use program::reflect::specs::World;

pub use program::program::*;

pub use program::eval::State;
pub use program::program::Program;
pub use program::reflect;
pub use program::reflect::{
	impl_type_name, meta::Meta, specs, Class, ClassRegistry, FreeFunc, MemberMutFunc,
	MemberOwnedFunc, MemberRefFunc, Object, Register, Type, TypeName, CLASSES, CLASS_REGISTRY,
};
pub use specs::{
	shred::{Fetch, FetchMut},
	Component, Entity, WorldExt,
};

#[cfg(test)]
mod tests;
