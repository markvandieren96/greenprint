use crate::{Class, Register, CLASSES};
use std::any::Any;

pub trait GetClass: 'static {
	fn class(&self) -> &'static Class {
		if let Some(class) = CLASSES.get(self.type_id()) {
			class
		} else {
			println!(
				"Class with {:?} was not registered in CLASSES",
				self.type_id()
			);
			CLASSES.log();
			panic!();
		}
	}
}

impl<T: 'static + Register> GetClass for T {}
