use crate::Expression;
use lexer::Span;

/// Parses "foo.bar = expr"
#[derive(Debug, PartialEq, Clone)]
pub struct SetField {
	pub span: Span,
	pub object: Expression,
	pub name: String,
	pub value: Expression,
}
