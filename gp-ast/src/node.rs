use crate::{
	nodes::{
		Assignment, Block, Branch, Call, ExpressionStatement, For, FunctionDeclaration, GetField,
		Group, Literal, Variable, VariableDeclaration, While,
	},
	Validate, Walkable,
};
use parser::Span;

#[derive(Debug, Clone)]
pub enum Node {
	Group(Group),
	Assignment(Assignment),
	Literal(Literal),
	Variable(Variable),
	Call(Call),
	Block(Block),
	Branch(Branch),
	ExpressionStatement(ExpressionStatement),
	VariableDeclaration(VariableDeclaration),
	While(While),
	For(For),
	FunctionDeclaration(FunctionDeclaration),
	//ReturnStatement(ReturnStatement),
	GetField(GetField),
}

macro_rules! impl_expect {
	($name:ident, $ty:ty, $item:ident) => {
		#[must_use]
		pub fn $name(self) -> $ty {
			if let Self::$item(node) = self {
				node
			} else {
				panic!()
			}
		}
	};
}

impl Node {
	impl_expect!(expect_assignment, Assignment, Assignment);
	impl_expect!(expect_variable, Variable, Variable);
	impl_expect!(
		expect_variable_declaration,
		VariableDeclaration,
		VariableDeclaration
	);
	impl_expect!(expect_block, Block, Block);
}

#[allow(clippy::module_name_repetitions)]
pub trait INode: std::fmt::Display + std::fmt::Debug + Validate + Walkable {
	#[must_use]
	fn span(&self) -> Span;

	fn node_type_name(&self) -> &'static str;

	#[must_use]
	fn to_src(&self, depth: usize) -> String;

	fn boxed_clone(&self) -> Box<dyn INode>;

	fn as_any(&self) -> &dyn std::any::Any;
	fn as_any_mut(&mut self) -> &mut dyn std::any::Any;
}

impl Clone for Box<dyn INode> {
	fn clone(&self) -> Box<dyn INode> {
		self.boxed_clone()
	}
}

impl std::ops::Deref for Node {
	type Target = dyn INode;

	fn deref(&self) -> &Self::Target {
		match self {
			Node::Group(node) => node,
			Node::Assignment(node) => node,
			Node::Literal(node) => node,
			Node::Variable(node) => node,
			Node::Call(node) => node,
			Node::Block(node) => node,
			Node::Branch(node) => node,
			Node::ExpressionStatement(node) => node,
			Node::VariableDeclaration(node) => node,
			Node::While(node) => node,
			Node::For(node) => node,
			Node::FunctionDeclaration(node) => node,
			//Node::ReturnStatement(node) => node,
			Node::GetField(node) => node,
		}
	}
}

impl std::ops::DerefMut for Node {
	fn deref_mut(&mut self) -> &mut Self::Target {
		match self {
			Node::Group(node) => node,
			Node::Assignment(node) => node,
			Node::Literal(node) => node,
			Node::Variable(node) => node,
			Node::Call(node) => node,
			Node::Block(node) => node,
			Node::Branch(node) => node,
			Node::ExpressionStatement(node) => node,
			Node::VariableDeclaration(node) => node,
			Node::While(node) => node,
			Node::For(node) => node,
			Node::FunctionDeclaration(node) => node,
			//Node::ReturnStatement(node) => node,
			Node::GetField(node) => node,
		}
	}
}

impl std::fmt::Display for Node {
	fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
		match self {
			Self::Group(node) => node.fmt(f),
			Self::Assignment(node) => node.fmt(f),
			Self::Literal(node) => node.fmt(f),
			Self::Variable(node) => node.fmt(f),
			Self::Call(node) => node.fmt(f),
			Self::Block(node) => node.fmt(f),
			Self::Branch(node) => node.fmt(f),
			Self::ExpressionStatement(node) => node.fmt(f),
			Self::VariableDeclaration(node) => node.fmt(f),
			Self::While(node) => node.fmt(f),
			Self::For(node) => node.fmt(f),
			Self::FunctionDeclaration(node) => node.fmt(f),
			//Self::ReturnStatement(node) => node.fmt(f),
			Self::GetField(node) => node.fmt(f),
		}
	}
}

#[macro_export]
macro_rules! impl_node {
	($name:literal) => {
		fn span(&self) -> Span {
			self.span.clone()
		}

		fn node_type_name(&self) -> &'static str {
			$name
		}

		fn boxed_clone(&self) -> Box<dyn INode> {
			Box::new(self.clone())
		}

		fn as_any(&self) -> &dyn std::any::Any {
			self
		}

		fn as_any_mut(&mut self) -> &mut dyn std::any::Any {
			self
		}
	};
}

impl Walkable for Node {
	fn walk(&mut self, f: &mut crate::Callback) -> Result<(), crate::Error> {
		match self {
			Node::Group(n) => n.walk(f),
			Node::Assignment(n) => n.walk(f),
			Node::Literal(n) => n.walk(f),
			Node::Variable(n) => n.walk(f),
			Node::Call(n) => n.walk(f),
			Node::Block(n) => n.walk(f),
			Node::Branch(n) => n.walk(f),
			Node::ExpressionStatement(n) => n.walk(f),
			Node::VariableDeclaration(n) => n.walk(f),
			Node::While(n) => n.walk(f),
			Node::For(n) => n.walk(f),
			Node::FunctionDeclaration(n) => n.walk(f),
			Node::GetField(n) => n.walk(f),
		}
	}
}
