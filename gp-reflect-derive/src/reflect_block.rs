use super::function::FnDeclaration;
use crate::derive;
use proc_macro2::TokenStream;
use quote::{quote, ToTokens, TokenStreamExt};
use syn::parse::{Parse, ParseStream, Result};
use syn::{Ident, Token};

pub struct ReflectBlock {
	functions: Vec<FnDeclaration>,
	derives: Vec<derive::Block>,
}

impl Parse for ReflectBlock {
	fn parse(input: ParseStream) -> Result<Self> {
		let mut functions = Vec::new();
		let mut derives = Vec::new();
		while !input.is_empty() {
			if input.peek(Token![fn]) {
				let function: FnDeclaration = input.parse()?;
				functions.push(function);
			} else if input.peek(Ident) {
				let ident: Ident = input.parse()?;
				if ident == "derive" {
					let block = input.parse::<derive::Block>()?;
					derives.push(block);
				} else {
					return Err(input.error("Unknown identifier"));
				}
			} else {
				return Err(input.error("Unexpected token"));
			}
		}

		Ok(Self { functions, derives })
	}
}

impl ToTokens for ReflectBlock {
	fn to_tokens(&self, tokens: &mut TokenStream) {
		for function in &self.functions {
			tokens.append_all(quote! { #function });
		}

		for block in &self.derives {
			tokens.append_all(quote! { #block });
		}
	}
}
