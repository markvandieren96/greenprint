use crate::{nodes::Variable, Access, Context, Error, Node};
use parser::Span;
use reflect::meta::{ClassId, Meta};

pub trait SetAccess {
	fn set_access(&mut self, ctx: &Context, mode: Access) -> Result<(), Error>;

	fn set_access_opt_copy_or_move(
		&mut self,
		ty: Option<ClassId>,
		meta: &Meta,
		ctx: &mut Context,
		span: &Span,
	) -> Result<(), Error> {
		match &ty {
			Some(ty) => self.set_access_copy_or_move(ty, meta, ctx, span),
			None => self.set_access(ctx, Access::Copy),
		}
	}

	fn set_access_copy_or_move(
		&mut self,
		ty: &ClassId,
		meta: &Meta,
		ctx: &mut Context,
		span: &Span,
	) -> Result<(), Error> {
		if meta
			.get(ty)
			.map_err(|error| Error::reflect(span, error))?
			.has_method_ref("copy")
		{
			self.set_access(ctx, Access::Copy)
		} else {
			self.set_access(ctx, Access::Move)
		}
	}
}

impl SetAccess for Node {
	fn set_access(&mut self, ctx: &Context, mode: Access) -> Result<(), Error> {
		match self {
			Node::Variable(Variable { access, .. }) => *access = Some(mode),
			Node::GetField(crate::nodes::GetField { object, access, .. }) => {
				*access = Some(mode);
				match mode {
					Access::Copy | Access::Ref => object.set_access(ctx, Access::Ref)?,
					Access::Move | Access::RefMut => object.set_access(ctx, Access::RefMut)?,
				}
			}
			_ => (),
		}

		Ok(())
	}
}
