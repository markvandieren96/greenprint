use super::{Context, Error, Node};
use reflect::meta::{ClassId, Meta};

pub(crate) trait GetType {
	fn get_type(&self, _meta: &Meta, _context: &Context) -> Result<Option<ClassId>, Error>;
}

impl GetType for Node {
	fn get_type(&self, meta: &Meta, ctx: &Context) -> Result<Option<ClassId>, Error> {
		log::trace!("get type of {}", self.node_type_name());

		let ty = match self {
			Node::Literal(node) => node.get_type(meta, ctx),
			Node::Variable(node) => node.get_type(meta, ctx),
			Node::Call(node) => node.get_type(meta, ctx),
			Node::Block(node) => node.get_type(meta, ctx),
			Node::Branch(node) => node.get_type(meta, ctx),
			Node::ExpressionStatement(node) => node.get_type(meta, ctx),
			Node::VariableDeclaration(node) => node.get_type(meta, ctx),
			Node::While(node) => node.get_type(meta, ctx),
			Node::For(node) => node.get_type(meta, ctx),
			Node::FunctionDeclaration(node) => node.get_type(meta, ctx),
			//Node::ReturnStatement(node) => node.get_type(meta, ctx),
			Node::GetField(node) => node.get_type(meta, ctx),
			Node::Group(node) => node.get_type(meta, ctx),
			Node::Assignment(node) => node.get_type(meta, ctx),
		}?;
		log::trace!("got type of {}: {:?}", self.node_type_name(), ty);
		Ok(ty)
	}
}
