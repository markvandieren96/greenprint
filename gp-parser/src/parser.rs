use crate::{
	nodes::{
		ArgumentList, Assignment, BinaryOp, Block, Branch, ExpressionStatement, ForLoop,
		FreeFunctionCall, FunctionDeclaration, GetField, Group, LiteralValue, MethodCall, SetField,
		StaticFunctionCall, UnaryOp, Variable, VariableDeclaration, WhileLoop,
	},
	Error, Expression, LexerExpectExt, Statement,
};
use lexer::{merge_spans, Lexer, LexerExt, Token};
use log::trace;

pub type Errors = Vec<Error>;

#[derive(Debug)]
pub struct FailedParseInfo {
	pub expression: Expression,
	pub errors: Vec<Error>,
}

pub fn parse(src: &str) -> Result<Expression, FailedParseInfo> {
	let mut errors = Errors::default();

	let mut lexer = Lexer::new(src);
	if src.starts_with('{') && src.ends_with('}') {
		let expr = expression(&mut lexer, &mut errors);
		if errors.is_empty() {
			Ok(expr)
		} else {
			Err(FailedParseInfo {
				expression: expr,
				errors: std::mem::take(&mut errors),
			})
		}
	} else {
		let mut statements = Vec::new();

		while lexer.peek().is_some() {
			let num_errors_before = errors.len();
			match declaration(&mut lexer, &mut errors) {
				Ok(statement) => statements.push(statement),
				Err(e) => {
					errors.push(e.clone());
					statements.push(Statement::Error(e));
				}
			}

			let num_errors_after = errors.len();
			if num_errors_before != num_errors_after {
				while lexer.next().is_some() {
					if lexer.matches(&Token::Semicolon) {
						break;
					}
				}
			}
		}

		let expr = Expression::Block(Block {
			span: 0..src.len(),
			body: statements,
			catch_returns: true,
		});

		if errors.is_empty() {
			Ok(expr)
		} else {
			Err(FailedParseInfo {
				expression: expr,
				errors: std::mem::take(&mut errors),
			})
		}
	}
}

pub(crate) fn expression(lexer: &mut Lexer, errors: &mut Errors) -> Expression {
	let res = or(lexer, errors);
	match res {
		Ok(expr) => expr,
		Err(e) => {
			errors.push(e.clone());
			Expression::Error(e)
		}
	}
}

pub(crate) fn or(lexer: &mut Lexer, errors: &mut Errors) -> Result<Expression, Error> {
	trace!("or");

	let mut expr = and(lexer, errors)?;
	expr = BinaryOp::try_parse_or(lexer, errors, expr)?;
	Ok(expr)
}

pub(crate) fn and(lexer: &mut Lexer, errors: &mut Errors) -> Result<Expression, Error> {
	trace!("and");

	let mut expr = equality(lexer, errors)?;
	expr = BinaryOp::try_parse_and(lexer, errors, expr)?;
	Ok(expr)
}

pub(crate) fn declaration(lexer: &mut Lexer, errors: &mut Errors) -> Result<Statement, Error> {
	trace!("declaration");

	if let Some(function_declaration) = FunctionDeclaration::try_parse(lexer, errors)? {
		return Ok(Statement::FunctionDeclaration(function_declaration));
	}
	if let Some(variable_declaration) = VariableDeclaration::try_parse(lexer, errors)? {
		return Ok(Statement::VariableDeclaration(variable_declaration));
	}
	if let Some(for_loop) = ForLoop::try_parse(lexer, errors)? {
		return Ok(Statement::For(for_loop));
	}
	if let Some(branch) = Branch::try_parse(lexer, errors)? {
		return Ok(Statement::Expression(ExpressionStatement {
			span: branch.span.clone(),
			body: Expression::Branch(branch),
			has_semicolon: false,
		}));
	}
	/*if let Some(return_statement) = ReturnStatement::try_parse(lexer, errors)? {
		return Ok(Statement::Return(return_statement));
	}*/
	if let Some(while_loop) = WhileLoop::try_parse(lexer, errors)? {
		return Ok(Statement::While(while_loop));
	}
	expression_statement(lexer, errors)
}

pub(crate) fn expression_statement(
	lexer: &mut Lexer,
	errors: &mut Errors,
) -> Result<Statement, Error> {
	trace!("expression_statement");

	let expr = expression(lexer, errors);

	if lexer.matches(&Token::Assignment) {
		trace!("assignment");

		let value = expression(lexer, errors);

		if let Expression::Variable(Variable { span, name }) = expr {
			let span_end = lexer.expect(Token::Semicolon, "Assignment")?;
			return Ok(Statement::Assignment(Assignment {
				span: merge_spans(&span, &span_end),
				name,
				value,
			}));
		} else if let Expression::GetField(GetField { span, object, name }) = expr {
			let span_end = lexer.expect(Token::Semicolon, "GetField")?;

			return Ok(Statement::SetField(SetField {
				span: merge_spans(&span, &span_end),
				object: *object,
				name,
				value,
			}));
		} else if let Expression::Error { .. } = expr {
		} else {
			println!("Attempted to assign to {expr:?}");
			panic!();
		}
	}

	let semicolon_span = lexer.matches_spanned(&Token::Semicolon);

	let expr_span = expr.span();
	Ok(Statement::Expression(ExpressionStatement {
		span: expr_span.start
			..semicolon_span
				.as_ref()
				.map_or(expr_span.end, |span| span.end),
		body: expr,
		has_semicolon: semicolon_span.is_some(),
	}))
}

pub(crate) fn equality(lexer: &mut Lexer, errors: &mut Errors) -> Result<Expression, Error> {
	trace!("equality");

	let mut expr = comparison(lexer, errors)?;
	expr = BinaryOp::try_parse_equality(lexer, errors, expr)?;
	Ok(expr)
}

pub(crate) fn comparison(lexer: &mut Lexer, errors: &mut Errors) -> Result<Expression, Error> {
	trace!("comparison");

	let mut expr = term(lexer, errors)?;
	expr = BinaryOp::try_parse_comparisson(lexer, errors, expr)?;
	Ok(expr)
}

pub(crate) fn term(lexer: &mut Lexer, errors: &mut Errors) -> Result<Expression, Error> {
	trace!("term");

	let mut expr = factor(lexer, errors)?;
	expr = BinaryOp::try_parse_term(lexer, errors, expr)?;
	Ok(expr)
}

pub(crate) fn factor(lexer: &mut Lexer, errors: &mut Errors) -> Result<Expression, Error> {
	trace!("factor");

	let mut expr = unary(lexer, errors)?;
	expr = BinaryOp::try_parse_factor(lexer, errors, expr)?;
	Ok(expr)
}

pub(crate) fn unary(lexer: &mut Lexer, errors: &mut Errors) -> Result<Expression, Error> {
	trace!("unary");

	if let Some(unary_op) = UnaryOp::try_parse(lexer, errors)? {
		return Ok(Expression::UnaryOp(unary_op));
	}

	call(lexer, errors)
}

pub(crate) fn call(lexer: &mut Lexer, errors: &mut Errors) -> Result<Expression, Error> {
	trace!("call");

	let mut expr = primary(lexer, errors)?;
	let expr_span = expr.span();

	loop {
		if let Some(arguments) = ArgumentList::try_parse(lexer, errors)? {
			let name = if let Expression::Variable(Variable { name, .. }) = expr {
				name
			} else {
				return Err(Error::expected_identifier(lexer, "Call")); // This yield the wrong span, but meh
			};
			return Ok(Expression::FreeFunctionCall(FreeFunctionCall {
				span: expr_span.start..arguments.span.end,
				name,
				arguments,
			}));
		} else if lexer.matches(&Token::Dot) {
			if let Some((Token::Identifier(name), nested_ident_span)) = lexer.peek() {
				lexer.next();

				if let Some(arguments) = ArgumentList::try_parse(lexer, errors)? {
					let expr2 = Expression::MethodCall(MethodCall {
						span: expr_span.start..arguments.span.end,
						object: expr.into(),
						name,
						arguments,
					});
					if lexer.peek_match(&Token::Dot) {
						expr = expr2;
						continue;
					}
					return Ok(expr2);
				}
				expr = Expression::GetField(GetField {
					span: expr_span.start..nested_ident_span.end,
					object: expr.into(),
					name,
				});
			} else {
				return Err(Error::expected_identifier(lexer, "Call"));
			}
		} else {
			break;
		}
	}

	Ok(expr)
}

pub(crate) fn primary(lexer: &mut Lexer, errors: &mut Errors) -> Result<Expression, Error> {
	trace!("primary");

	if let Some(res) = LiteralValue::try_parse(lexer)? {
		return Ok(res);
	}

	if let Some(static_function_call) = StaticFunctionCall::try_parse(lexer, errors)? {
		return Ok(Expression::StaticFunctionCall(static_function_call));
	}

	if let Some(variable) = Variable::try_parse(lexer)? {
		return Ok(Expression::Variable(variable));
	}

	if let Some(group) = Group::try_parse(lexer, errors)? {
		return Ok(Expression::Group(group));
	}

	if let Some(block) = Block::try_parse(lexer, errors)? {
		return Ok(Expression::Block(block));
	}

	if let Some(branch) = Branch::try_parse(lexer, errors)? {
		return Ok(Expression::Branch(branch));
	}

	lexer.peek().map_or_else(
		|| {
			Err(Error::UnexpectedEndOfFile {
				expected: None,
				ctx: "Expression".to_string(),
			})
		},
		|got| Err(Error::ExpectedExpression { got }),
	)
}
