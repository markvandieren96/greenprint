use crate::{
	Class, ClassRegistry, FreeFunc, MemberMutFunc, MemberOwnedFunc, MemberRefFunc, Object,
	Register, Type, TypeName,
};
use gp_reflect_derive::reflect;
use specs::Entity;

pub(crate) fn register_std_types(registry: &mut ClassRegistry) {
	<()>::register(registry);
	bool::register(registry);
	i32::register(registry);
	i64::register(registry);
	f32::register(registry);
	String::register(registry);
	specs::Entity::register(registry);

	glam::f32::Vec3::register(registry);
	glam::f32::Vec2::register(registry);
}

impl Register for () {
	fn register(registry: &mut ClassRegistry) {
		let mut class = Class::new::<Self>();
		reflect!(
			derive(=, default, clone, copy)
		);
		registry.register(class);
	}
}

impl<T> Register for Option<T>
where
	T: 'static + Send + Sync + Register + TypeName,
{
	fn register(registry: &mut ClassRegistry) {
		let mut class = Class::new::<Self>();
		reflect!(
			derive(=, default)

			fn some(t: T) -> Self {
				Some(t)
			}

			fn none() -> Self {
				None
			}

			fn is_some(&self) -> bool {
				obj.is_some()
			}

			fn is_none(&self) -> bool {
				obj.is_none()
			}

			fn unwrap(self) -> T {
				obj.unwrap()
			}

			fn unwrap_ref(&self) -> &T {
				obj.as_ref().unwrap()
			}

			fn unwrap_mut(&mut self) -> &mut T {
				obj.as_mut().unwrap()
			}
		);
		registry.register(class);
	}
}

impl Register for bool {
	fn register(registry: &mut ClassRegistry) {
		let mut class = Class::new::<Self>();
		reflect!(
			derive(=, !, logical, equality, default, clone, copy, to_string)
		);
		registry.register(class);
	}
}

impl Register for i8 {
	fn register(registry: &mut ClassRegistry) {
		let mut class = Class::new::<Self>();
		reflect!(
			derive(=, arithmetic, equality, ordering, neg, default, clone, copy, to_string, to_i16, to_i32, to_i64, to_u8, to_u16, to_u32, to_u64, to_f32, to_f64, to_usize)
		);
		registry.register(class);
	}
}

impl Register for i16 {
	fn register(registry: &mut ClassRegistry) {
		let mut class = Class::new::<Self>();
		reflect!(
			derive(=, arithmetic, equality, ordering, neg,default, clone, copy, to_string, to_i8, to_i32, to_i64, to_u8, to_u16, to_u32, to_u64, to_f32, to_f64, to_usize)
		);
		registry.register(class);
	}
}

impl Register for i32 {
	fn register(registry: &mut ClassRegistry) {
		let mut class = Class::new::<Self>();
		reflect!(
			derive(=, arithmetic, equality, ordering, neg, default, clone, copy, to_string, to_i8, to_i16, to_i64, to_u8, to_u16, to_u32, to_u64, to_f32, to_f64, to_usize)
		);
		registry.register(class);
	}
}

impl Register for i64 {
	fn register(registry: &mut ClassRegistry) {
		let mut class = Class::new::<Self>();
		reflect!(
			derive(=, arithmetic, equality, ordering, neg, default, clone, copy, to_string, to_i8, to_i16, to_i32, to_u8, to_u16, to_u32, to_u64, to_f32, to_f64, to_usize)
		);
		registry.register(class);
	}
}

impl Register for u8 {
	fn register(registry: &mut ClassRegistry) {
		let mut class = Class::new::<Self>();
		reflect!(
			derive(=, arithmetic, equality, ordering, default, clone, copy, to_string, to_i8, to_i16, to_i32, to_i64, to_u16, to_u32, to_u64, to_f32, to_f64, to_usize)
		);
		registry.register(class);
	}
}

impl Register for u16 {
	fn register(registry: &mut ClassRegistry) {
		let mut class = Class::new::<Self>();
		reflect!(
			derive(=, arithmetic, equality, ordering, default, clone, copy, to_string, to_i8, to_i16, to_i32, to_i64, to_u8, to_u32, to_u64, to_f32, to_f64, to_usize)
		);
		registry.register(class);
	}
}

impl Register for u32 {
	fn register(registry: &mut ClassRegistry) {
		let mut class = Class::new::<Self>();
		reflect!(
			derive(=, arithmetic, equality, ordering, default, clone, copy, to_string, to_i8, to_i16, to_i32, to_i64, to_u8, to_u16, to_u64, to_f32, to_f64, to_usize)
		);
		registry.register(class);
	}
}

impl Register for u64 {
	fn register(registry: &mut ClassRegistry) {
		let mut class = Class::new::<Self>();
		reflect!(
			derive(=, arithmetic, equality, ordering, default, clone, copy, to_string, to_i8, to_i16, to_i32, to_i64, to_u8, to_u16, to_u32, to_f32, to_f64, to_usize)
		);
		registry.register(class);
	}
}

impl Register for usize {
	fn register(registry: &mut ClassRegistry) {
		let mut class = Class::new::<Self>();
		reflect!(
			derive(=, arithmetic, equality, ordering, default, clone, copy, to_string, to_i8, to_i16, to_i32, to_i64, to_u8, to_u16, to_u32, to_u64, to_f32, to_f64)
		);
		registry.register(class);
	}
}

impl Register for f32 {
	fn register(registry: &mut ClassRegistry) {
		let mut class = Class::new::<Self>();
		reflect!(
			derive(=, arithmetic, ordering, neg, default, clone, copy, to_string, to_i8, to_i16, to_i32, to_i64, to_u8, to_u16, to_u32, to_u64, to_f64, to_usize)

			fn op_eq(&self, rhs: &Self) -> bool {
				(obj - rhs).abs() < Self::EPSILON
			}

			fn op_ne(&self, rhs: &Self) -> bool {
				(obj - rhs).abs() >= Self::EPSILON
			}
		);
		registry.register(class);
	}
}

#[allow(clippy::cast_possible_truncation)]
impl Register for f64 {
	fn register(registry: &mut ClassRegistry) {
		let mut class = Class::new::<Self>();
		reflect!(
			derive(=, arithmetic, ordering, neg, default, clone, copy, to_string,  to_i8, to_i16, to_i32, to_i64, to_u8, to_u16, to_u32, to_u64, to_f32, to_usize)

			fn op_eq(&self, rhs: &Self) -> bool {
				(obj - rhs).abs() < Self::EPSILON
			}

			fn op_ne(&self, rhs: &Self) -> bool {
				(obj - rhs).abs() >= Self::EPSILON
			}
		);
		registry.register(class);
	}
}

impl Register for String {
	fn register(registry: &mut ClassRegistry) {
		let mut class = Class::new::<Self>();
		reflect!(
			derive(=, default, clone)

			fn op_add(&self, rhs: &String) -> String {
				format!("{obj}{rhs}")
			}
		);
		registry.register(class);
	}
}

impl Register for Entity {
	fn register(registry: &mut ClassRegistry) {
		use specs::WorldExt;

		let mut class = Class::new::<Self>();
		reflect!(
			derive(=, clone, copy)

			fn new() -> Entity {
				world.entities().create()
			}

			fn delete(&self) {
				world.entities().delete(*obj).unwrap();
			}
		);
		registry.register(class);
	}
}

impl<T> Register for specs::ReadStorage<'static, T>
where
	T: specs::Component + Send + Sync + Register + TypeName,
{
	fn register(registry: &mut ClassRegistry) {
		let mut class = Class::new::<Self>();
		reflect!(
			derive(drop)

			fn get(&self, entity: Entity) -> &T {
				obj.get(entity).unwrap()
			}
		);
		registry.register(class);
	}
}

impl<T> Register for specs::WriteStorage<'static, T>
where
	T: specs::Component + Send + Sync + Register + TypeName,
{
	fn register(registry: &mut ClassRegistry) {
		let mut class = Class::new::<Self>();
		reflect!(
			derive(drop)

			fn get(&self, entity: Entity) -> &T {
				obj.get(entity).unwrap()
			}

			fn get_mut(&mut self, entity: Entity) -> &mut T {
				obj.get_mut(entity).unwrap()
			}
		);
		registry.register(class);
	}
}

impl<T> Register for specs::shred::Fetch<'static, T>
where
	T: 'static + Send + Sync + Register + TypeName,
{
	fn register(registry: &mut ClassRegistry) {
		let mut class = Class::new::<Self>();
		reflect!(
			derive(drop)

			fn get(&self) -> &T {
				obj
			}
		);
		registry.register(class);
	}
}

impl<T> Register for specs::shred::FetchMut<'static, T>
where
	T: 'static + Send + Sync + Register + TypeName,
{
	fn register(registry: &mut ClassRegistry) {
		let mut class = Class::new::<Self>();
		reflect!(
			derive(drop)

			fn get(&self) -> &T {
				obj
			}

			fn get_mut(&mut self) -> &mut T {
				obj
			}
		);
		registry.register(class);
	}
}
