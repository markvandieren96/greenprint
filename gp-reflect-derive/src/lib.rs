#![warn(clippy::all)]
#![warn(clippy::pedantic)]

extern crate proc_macro;

mod access;
mod derive;
mod function;
mod reflect_block;
mod type_signature;

use access::Access;
use reflect_block::ReflectBlock;
use type_signature::{Type, TypeSignature};

use quote::quote;
use syn::parse_macro_input;

#[proc_macro]
pub fn reflect(input: proc_macro::TokenStream) -> proc_macro::TokenStream {
	let block = parse_macro_input!(input as ReflectBlock);
	proc_macro::TokenStream::from(quote! { #block })
}
