#![warn(clippy::all)]
#![warn(clippy::pedantic)]
#![allow(clippy::missing_errors_doc)]
#![allow(clippy::missing_panics_doc)]

pub mod nodes {
	mod assignment;
	mod block;
	mod branch;
	mod call;
	mod expression_statement;
	mod for_loop;
	mod function_declaration;
	mod get_field;
	mod group;
	mod literal;
	mod return_statement;
	mod variable;
	mod variable_declaration;
	mod while_loop;

	pub use assignment::*;
	pub use block::*;
	pub use branch::*;
	pub use call::*;
	pub use expression_statement::*;
	pub use for_loop::*;
	pub use function_declaration::*;
	pub use get_field::*;
	pub use group::*;
	pub use literal::*;
	
	pub use variable::*;
	pub use variable_declaration::*;
	pub use while_loop::*;
}

mod access;
mod ctx;
mod error;
mod extern_function;
mod from;
mod function;
mod get_type;
mod identifier;
mod node;
mod scope;
mod set_access;
mod validate;
mod variable;
mod walkable;

pub use access::*;
pub use ctx::*;
pub use error::*;
pub use extern_function::*;

pub use function::*;
pub(crate) use get_type::GetType;
pub use identifier::*;
pub use node::*;
pub use scope::*;
pub use set_access::*;
pub use validate::*;
pub use variable::*;
pub use walkable::*;

pub use parser;
pub use reflect;

use reflect::meta::Meta;

pub fn type_check(
	expression: parser::Expression,
	meta: &Meta,
	externs: Externs,
) -> ((Node, Context), Result<(), Error>) {
	log::trace!("doing type check");
	let mut node: Node = expression.into();
	let mut ctx = Context::new(externs);

	let res = validate_node(&mut *node, meta, &mut ctx);

	((node, ctx), res)
}

#[cfg(test)]
mod test_util;
