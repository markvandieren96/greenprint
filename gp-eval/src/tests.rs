mod helper;

use assert_approx_eq::assert_approx_eq;
use helper::{eval, init};

#[test]
fn test1() {
	let meta = init();

	let res = eval(
		&meta,
		"
		let transform = Transform::new(Vec2::new(1.0, 2.0), Vec2::new(3.0, 4.0));
		let x = transform.position.get_x();
		x
	",
	)
	.unwrap()
	.unwrap();

	assert_approx_eq!(*res.downcast_ref::<f32>().unwrap(), 1.0);
}

#[test]
fn test2() {
	let meta = init();

	let res = eval(
		&meta,
		"
	let a = 1;
	let b = 2;
	let c = a + b;
	c
	",
	)
	.unwrap()
	.unwrap();

	assert_eq!(*res.downcast_ref::<i64>().unwrap(), 3);
}

#[test]
fn test3() {
	let meta = init();

	let res = eval(
		&meta,
		"
		let mut transform = Transform::new(Vec2::new(1.0, 2.0), Vec2::new(3.0, 4.0));
		transform.position.set_x(5.0);
		transform.position.x
		",
	)
	.unwrap()
	.unwrap();

	assert_approx_eq!(*res.downcast_ref::<f32>().unwrap(), 5.0);
}

#[test]
fn test4() {
	let meta = init();

	let res = eval(
		&meta,
		"
		let mut a = 1;
		{
			a = a + 2;
		}
		a
		",
	)
	.unwrap()
	.unwrap();

	assert_eq!(*res.downcast_ref::<i64>().unwrap(), 3);
}

#[test]
fn branch() {
	let meta = init();

	let res = eval(
		&meta,
		"
		if true {
			1
		} else {
			2
		}
		",
	)
	.unwrap()
	.unwrap();

	assert_eq!(*res.downcast_ref::<i64>().unwrap(), 1);
}

#[test]
fn branch_else() {
	let meta = init();

	let res = eval(
		&meta,
		"
		if false {
			1
		} else {
			2
		}
		",
	)
	.unwrap()
	.unwrap();

	assert_eq!(*res.downcast_ref::<i64>().unwrap(), 2);
}

#[test]
fn for_loop_1() {
	let meta = init();

	let res = eval(
		&meta,
		"
		let mut a = 0;
		for i in 0..10 {
			a = i;
		}
		a
		",
	)
	.unwrap()
	.unwrap();

	assert_eq!(*res.downcast_ref::<i64>().unwrap(), 9);
}

#[test]
fn for_loop_inclusive() {
	let meta = init();

	let res = eval(
		&meta,
		"
		let mut a = 0;
		for i in 0..=10 {
			a = i;
		}
		a
		",
	)
	.unwrap()
	.unwrap();

	assert_eq!(*res.downcast_ref::<i64>().unwrap(), 10);
}

#[test]
fn for_loop_with_vars() {
	let meta = init();

	let res = eval(
		&meta,
		"
		let start = 5;
		let end = 5;
		let mut a = 0;
		for i in -start..end {
			a = i;
		}
		a
		",
	)
	.unwrap()
	.unwrap();

	assert_eq!(*res.downcast_ref::<i64>().unwrap(), 4);
}

#[test]
fn while_loop() {
	let meta = init();

	let res = eval(
		&meta,
		"
		let mut a = 0;
		let mut i = 0;
		while i < 10 {
			a = i;
			i = i + 1;
		}
		a
		",
	)
	.unwrap()
	.unwrap();

	assert_eq!(*res.downcast_ref::<i64>().unwrap(), 9);
}

#[test]
fn function() {
	let meta = init();

	let res = eval(
		&meta,
		"
		let mut a = 0;
		fn hello_world() {
			a = 1;
		}
		hello_world();
		a
		",
	)
	.unwrap()
	.unwrap();

	assert_eq!(*res.downcast_ref::<i64>().unwrap(), 1);
}

#[test]
fn function_with_parameters() {
	let meta = init();

	let res = eval(
		&meta,
		"
		let mut a = 0;
		fn hello_world(b: i64) {
			a = b;
		}
		hello_world(1);
		a
		",
	)
	.unwrap()
	.unwrap();

	assert_eq!(*res.downcast_ref::<i64>().unwrap(), 1);
}

#[test]
fn function_with_return() {
	let meta = init();

	let res = eval(
		&meta,
		"
		let mut a = 0;
		fn hello_world() -> i64 {
			1
		}
		a = hello_world();
		a
		",
	)
	.unwrap()
	.unwrap();

	assert_eq!(*res.downcast_ref::<i64>().unwrap(), 1);
}

#[test]
fn function_with_parameters_and_return() {
	let meta = init();

	let res = eval(
		&meta,
		"
		let mut a = 0;
		fn hello_world(a: i64, b: i64) -> i64 {
			a + b
		}
		hello_world(6, 2)
		
		",
	)
	.unwrap()
	.unwrap();

	assert_eq!(*res.downcast_ref::<i64>().unwrap(), 8);
}

/*#[test]
fn return_statement() {
	let meta = init();

	let res = eval(
		&meta,
		"
		return 6;
		",
	)
	.unwrap()
	.unwrap();

	assert_eq!(*res.downcast_ref::<i64>().unwrap(), 6);
}

#[test]
fn function_with_return_statement1() {
	let meta = init();

	let res = eval(
		&meta,
		"
		let mut a = 0;
		fn hello_world(a: i64, b: i64) -> i64 {
			return a + b;
		}
		a = hello_world(6, 2);
		a
		",
	)
	.unwrap()
	.unwrap();

	assert_eq!(*res.downcast_ref::<i64>().unwrap(), 8);
}

#[test]
fn function_with_return_statement2() {
	let meta = init();

	let res = eval(
		&meta,
		"
		fn hello_world(a: i64, b: i64) -> i64 {
			return a + b;
		}
		1
		",
	)
	.unwrap()
	.unwrap();

	assert_eq!(*res.downcast_ref::<i64>().unwrap(), 1);
}*/

#[test]
fn group() {
	let meta = init();

	let res = eval(
		&meta,
		"
		(2 * 3) + (3 * 4)
		",
	)
	.unwrap()
	.unwrap();

	assert_eq!(*res.downcast_ref::<i64>().unwrap(), 18);
}
