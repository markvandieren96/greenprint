use crate::{nodes::ArgumentList, Error, Errors, LexerExpectExt, ParseResult};
use lexer::{merge_spans, Lexer, LexerExt, Span, Token};

/// Parses "`::foo(expr`, expr2, ...)"
#[derive(Debug, PartialEq, Clone)]
pub struct StaticFunctionCall {
	pub span: Span,
	pub class: String,
	pub name: String,
	pub arguments: ArgumentList,
}

impl StaticFunctionCall {
	pub fn try_parse(lexer: &mut Lexer, errors: &mut Errors) -> ParseResult<Self> {
		let (class, span_start) = match lexer.lookahead(|lexer, restore_state| {
			let (class, span_start) = match lexer.peek() {
				Some((Token::Identifier(class), span)) => {
					lexer.next();
					(class, span)
				}
				_ => return None,
			};

			if !lexer.matches(&Token::ScopeResolution) {
				return None;
			}

			*restore_state = false;
			Some((class, span_start))
		}) {
			Some(ret) => ret,
			None => {
				return Ok(None);
			}
		};

		let (name, name_span) = lexer.expect_identifier("StaticFunctionCall")?;
		let arguments = ArgumentList::try_parse(lexer, errors)?
			.ok_or(Error::MissingArgumentList { at: name_span.end })?;

		Ok(Some(Self {
			span: merge_spans(&span_start, &arguments.span),
			class,
			name,
			arguments,
		}))
	}
}
