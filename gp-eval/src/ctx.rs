use ast::{nodes::Block, Externs};

pub struct Context {
	pub(crate) functions: Vec<Vec<Block>>,
	pub(crate) extern_functions: Externs,
}

impl Context {
	#[must_use]
	pub fn new(ctx: &ast::Context) -> Self {
		Self {
			functions: ctx.export_functions(),
			extern_functions: ctx.export_externs(),
		}
	}
}

impl std::fmt::Debug for Context {
	fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
		write!(f, "Context{{ functions: {:?} }}", self.functions)
	}
}
