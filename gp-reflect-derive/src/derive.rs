use super::{proc_macro, ReflectBlock};
use proc_macro2::TokenStream;
use quote::{quote, ToTokens, TokenStreamExt};
use syn::parse::{Parse, ParseStream, Result};
use syn::{parenthesized, parse_macro_input, Ident, Token};

#[derive(Debug)]
pub enum Item {
	Assign,
	Bang,
	Add,
	Sub,
	Mul,
	Div,
	Eq,
	Ne,
	Lt,
	Gt,
	Le,
	Ge,
	Neg,
	Clone,
	Copy,
	And,
	Or,
	Default,
	Drop,
	Component,
	Resource,
	ToString,
	EqualityGroup,
	OrderingGroup,
	ArithmeticGroup,
	LogicalGroup,
	ToI8,
	ToI16,
	ToI32,
	ToI64,
	ToU8,
	ToU16,
	ToU32,
	ToU64,
	ToF32,
	ToF64,
	ToUsize,
}

impl Parse for Item {
	fn parse(input: ParseStream) -> Result<Self> {
		if input.peek(Token![==]) {
			input.parse::<Token![==]>()?;
			Ok(Self::Eq)
		} else if input.peek(Token![!=]) {
			input.parse::<Token![!=]>()?;
			Ok(Self::Ne)
		} else if input.peek(Token![<=]) {
			input.parse::<Token![<=]>()?;
			Ok(Self::Le)
		} else if input.peek(Token![>=]) {
			input.parse::<Token![>=]>()?;
			Ok(Self::Ge)
		} else if input.peek(Token![&&]) {
			input.parse::<Token![&&]>()?;
			Ok(Self::And)
		} else if input.peek(Token![||]) {
			input.parse::<Token![||]>()?;
			Ok(Self::Or)
		} else if input.peek(Token![=]) {
			input.parse::<Token![=]>()?;
			Ok(Self::Assign)
		} else if input.peek(Token![!]) {
			input.parse::<Token![!]>()?;
			Ok(Self::Bang)
		} else if input.peek(Token![+]) {
			input.parse::<Token![+]>()?;
			Ok(Self::Add)
		} else if input.peek(Token![-]) {
			input.parse::<Token![-]>()?;
			Ok(Self::Sub)
		} else if input.peek(Token![*]) {
			input.parse::<Token![*]>()?;
			Ok(Self::Mul)
		} else if input.peek(Token![/]) {
			input.parse::<Token![/]>()?;
			Ok(Self::Div)
		} else if input.peek(Token![<]) {
			input.parse::<Token![<]>()?;
			Ok(Self::Lt)
		} else if input.peek(Token![>]) {
			input.parse::<Token![>]>()?;
			Ok(Self::Gt)
		} else if input.peek(Ident) {
			let ident: Ident = input.parse()?;
			match ident.to_string().as_str() {
				"neg" => Ok(Self::Neg),
				"clone" => Ok(Self::Clone),
				"copy" => Ok(Self::Copy),
				"default" => Ok(Self::Default),
				"drop" => Ok(Self::Drop),
				"component" => Ok(Self::Component),
				"resource" => Ok(Self::Resource),
				"to_string" => Ok(Self::ToString),
				"equality" => Ok(Self::EqualityGroup),
				"ordering" => Ok(Self::OrderingGroup),
				"arithmetic" => Ok(Self::ArithmeticGroup),
				"logical" => Ok(Self::LogicalGroup),
				"to_i8" => Ok(Self::ToI8),
				"to_i16" => Ok(Self::ToI16),
				"to_i32" => Ok(Self::ToI32),
				"to_i64" => Ok(Self::ToI64),
				"to_u8" => Ok(Self::ToU8),
				"to_u16" => Ok(Self::ToU16),
				"to_u32" => Ok(Self::ToU32),
				"to_u64" => Ok(Self::ToU64),
				"to_f32" => Ok(Self::ToF32),
				"to_f64" => Ok(Self::ToF64),
				"to_usize" => Ok(Self::ToUsize),
				_ => Err(input.error("Unknown identifier")),
			}
		} else {
			Err(input.error("Expected derive item"))
		}
	}
}

#[allow(clippy::too_many_lines)]
impl ToTokens for Item {
	fn to_tokens(&self, tokens: &mut TokenStream) {
		match self {
			Self::Assign => {
				tokens.append_all(reprocess(quote! {
					fn op_assign(&mut self, other: Self) {
						*obj = other;
					}
				}));
			}
			Self::Bang => {
				tokens.append_all(reprocess(quote! {
					fn op_bang(&self) -> Self {
						!*obj
					}
				}));
			}
			Self::Add => {
				tokens.append_all(reprocess(quote! {
					fn op_add(&self, other: &Self) -> Self {
						*obj + *other
					}
				}));
			}
			Self::Sub => {
				tokens.append_all(reprocess(quote! {
					fn op_sub(&self, other: &Self) -> Self {
						*obj - *other
					}
				}));
			}
			Self::Mul => {
				tokens.append_all(reprocess(quote! {
					fn op_mul(&self, other: &Self) -> Self {
						*obj * *other
					}
				}));
			}
			Self::Div => {
				tokens.append_all(reprocess(quote! {
					fn op_div(&self, other: &Self) -> Self {
						*obj / *other
					}
				}));
			}
			Self::Eq => {
				tokens.append_all(reprocess(quote! {
					fn op_eq(&self, other: &Self) -> bool {
						*obj == *other
					}
				}));
			}
			Self::Ne => {
				tokens.append_all(reprocess(quote! {
					fn op_ne(&self, other: &Self) -> bool {
						*obj != *other
					}
				}));
			}
			Self::Lt => {
				tokens.append_all(reprocess(quote! {
					fn op_lt(&self, other: &Self) -> bool {
						*obj < *other
					}
				}));
			}
			Self::Gt => {
				tokens.append_all(reprocess(quote! {
					fn op_gt(&self, other: &Self) -> bool {
						*obj > *other
					}
				}));
			}
			Self::Le => {
				tokens.append_all(reprocess(quote! {
					fn op_le(&self, other: &Self) -> bool {
						*obj <= *other
					}
				}));
			}
			Self::Ge => {
				tokens.append_all(reprocess(quote! {
					fn op_ge(&self, other: &Self) -> bool {
						*obj >= *other
					}
				}));
			}
			Self::Neg => {
				tokens.append_all(reprocess(quote! {
					fn op_neg(&self) -> Self {
						-*obj
					}
				}));
			}
			Self::Clone => {
				tokens.append_all(reprocess(quote! {
					fn clone(&self) -> Self {
						(*obj).clone()
					}
				}));
			}
			Self::Copy => {
				tokens.append_all(reprocess(quote! {
					fn copy(&self) -> Self {
						(*obj).clone()
					}
				}));
			}
			Self::And => {
				tokens.append_all(reprocess(quote! {
					fn op_and(&self, other: &Self) -> bool {
						*obj && *other
					}
				}));
			}
			Self::Or => {
				tokens.append_all(reprocess(quote! {
					fn op_or(&self, other: &Self) -> bool {
						*obj || *other
					}
				}));
			}
			Self::Default => {
				tokens.append_all(reprocess(quote! {
					fn default() -> Self {
						Self::default()
					}
				}));
			}
			Self::Drop => {
				tokens.append_all(reprocess(quote! {
					fn op_drop(self) {}
				}));
			}
			Self::Component => {
				tokens.append_all(reprocess(quote! {
					fn add_to_entity(self, entity: Entity) {
						world.write_storage::<Self>().insert(entity, obj).unwrap();
					}

					fn storage() -> ReadStorage<'static, Self> {
						world.read_storage::<Self>()
					}

					fn storage_mut() -> WriteStorage<'static, Self> {
						world.write_storage::<Self>()
					}
				}));
			}
			Self::Resource => {
				tokens.append_all(reprocess(quote! {
					fn resource() -> Fetch<'static, Self> {
						world.read_resource::<Self>()
					}

					fn resource_mut() -> FetchMut<'static, Self> {
						world.write_resource::<Self>()
					}
				}));
			}
			Self::ToString => {
				tokens.append_all(reprocess(quote! {
					fn to_string(&self) -> String {
						obj.to_string()
					}
				}));
			}
			Self::EqualityGroup => {
				append(tokens, &[Self::Eq, Self::Ne]);
			}
			Self::OrderingGroup => {
				append(tokens, &[Self::Lt, Self::Le, Self::Gt, Self::Ge]);
			}
			Self::ArithmeticGroup => {
				append(tokens, &[Self::Add, Self::Sub, Self::Mul, Self::Div]);
			}
			Self::LogicalGroup => {
				append(tokens, &[Self::And, Self::Or]);
			}
			Self::ToI8 => {
				tokens.append_all(reprocess(quote! {
					fn to_i8(&self) -> i8 {
						*obj as i8
					}
				}));
			}
			Self::ToI16 => {
				tokens.append_all(reprocess(quote! {
					fn to_i16(&self) -> i16 {
						*obj as i16
					}
				}));
			}
			Self::ToI32 => {
				tokens.append_all(reprocess(quote! {
					fn to_i32(&self) -> i32 {
						*obj as i32
					}
				}));
			}
			Self::ToI64 => {
				tokens.append_all(reprocess(quote! {
					fn to_i64(&self) -> i64 {
						*obj as i64
					}
				}));
			}
			Self::ToU8 => {
				tokens.append_all(reprocess(quote! {
					fn to_u8(&self) -> u8 {
						*obj as u8
					}
				}));
			}
			Self::ToU16 => {
				tokens.append_all(reprocess(quote! {
					fn to_u16(&self) -> u16 {
						*obj as u16
					}
				}));
			}
			Self::ToU32 => {
				tokens.append_all(reprocess(quote! {
					fn to_u32(&self) -> u32 {
						*obj as u32
					}
				}));
			}
			Self::ToU64 => {
				tokens.append_all(reprocess(quote! {
					fn to_u64(&self) -> u64 {
						*obj as u64
					}
				}));
			}
			Self::ToF32 => {
				tokens.append_all(reprocess(quote! {
					fn to_f32(&self) -> f32 {
						*obj as f32
					}
				}));
			}
			Self::ToF64 => {
				tokens.append_all(reprocess(quote! {
					fn to_f64(&self) -> f64 {
						*obj as f64
					}
				}));
			}
			Self::ToUsize => {
				tokens.append_all(reprocess(quote! {
					fn to_usize(&self) -> usize {
						*obj as usize
					}
				}));
			}
		}
	}
}

fn append(tokens: &mut TokenStream, v: &[Item]) {
	for op in v {
		op.to_tokens(tokens);
	}
}

fn reprocess(input: TokenStream) -> TokenStream {
	reprocess_impl(input.into()).into()
}

fn reprocess_impl(input: proc_macro::TokenStream) -> proc_macro::TokenStream {
	let block: ReflectBlock = parse_macro_input!(input as ReflectBlock);
	let expanded = quote! {
		#block
	};
	expanded.into()
}

pub struct Block {
	items: Vec<Item>,
}

impl Parse for Block {
	fn parse(input: ParseStream) -> Result<Self> {
		let mut items: Vec<Item> = Vec::new();
		let params;
		let _paren_token = parenthesized!(params in input);
		while let Ok(item) = params.parse::<Item>() {
			items.push(item);

			if params.peek(Token![,]) {
				params.parse::<Token![,]>()?;
			} else {
				break;
			}
		}

		Ok(Block { items })
	}
}

impl ToTokens for Block {
	fn to_tokens(&self, tokens: &mut TokenStream) {
		for item in &self.items {
			tokens.append_all(quote! {
				#item
			});
		}
	}
}
