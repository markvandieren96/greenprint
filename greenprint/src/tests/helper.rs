use super::*;
use program::reflect::{meta::Meta, CLASSES, CLASS_REGISTRY};

#[allow(dead_code)]
pub fn enable_tracing() {
	simple_logger::SimpleLogger::new()
		.with_level(log::LevelFilter::Off)
		.with_module_level("greenprint", log::LevelFilter::Trace)
		.with_module_level("gp_ast", log::LevelFilter::Trace)
		.with_module_level("gp_eval", log::LevelFilter::Trace)
		.with_module_level("gp_program", log::LevelFilter::Trace)
		.init()
		.unwrap();
}

pub fn init() -> Meta {
	CLASS_REGISTRY.register::<DeltaTime>();
	CLASS_REGISTRY.register::<Position>();
	CLASS_REGISTRY.register::<Option<i64>>();
	CLASS_REGISTRY.register::<GenericComponent<bool>>();
	CLASS_REGISTRY.register::<super::drop_tests::StructWithDrop>();
	CLASS_REGISTRY.register::<super::dummy::Dummy>();
	CLASSES.meta()
}

pub fn eval<T: 'static + Clone>(meta: &Meta, s: &str) -> T {
	match Program::new(String::new(), meta, s) {
		Ok(program) => {
			let mut state = program.create_state();
			program
				.eval(&mut state, &World::new())
				.expect("eval error")
				.expect("eval returned None")
				.downcast_ref::<T>()
				.expect("downcast failed")
				.clone()
		}
		Err(e) => {
			println!("Failed to compile program: {e:#?}");
			panic!()
		}
	}
}

pub fn eval_with_world(meta: &Meta, world: &World, s: &str) -> Option<Object> {
	let program = Program::new(String::new(), meta, s).expect("Failed to compile program");
	let mut state = program.create_state();
	program.eval(&mut state, world).expect("eval error")
}
