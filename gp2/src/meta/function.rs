use crate::{Error, Type, TypeMap, Value};

#[derive(Clone, Copy)]
pub struct Function(pub fn(&mut [Value]) -> Result<Option<Value>, Error>);

impl std::fmt::Debug for Function {
	fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
		f.debug_tuple("Function").field(&"f(...)").finish()
	}
}

#[derive(Debug)]
pub struct FunctionSignature {
	pub name: &'static str,
	pub args: Vec<Type>,
	pub returns: Option<Type>,
}

impl FunctionSignature {
	#[must_use]
	pub fn new(name: &'static str) -> Self {
		Self {
			name,
			args: Vec::new(),
			returns: None,
		}
	}

	#[must_use]
	pub fn with_arg(mut self, arg: Type) -> Self {
		self.args.push(arg);
		self
	}

	#[must_use]
	pub fn with_args(mut self, args: impl Iterator<Item = Type>) -> Self {
		for arg in args {
			self.args.push(arg);
		}
		self
	}

	#[must_use]
	pub fn returns(mut self, returns: Type) -> Self {
		assert!(self.returns.is_none());
		self.returns = Some(returns);
		self
	}

	#[must_use]
	pub fn lite(&self) -> FunctionSignatureLite {
		FunctionSignatureLite {
			name: self.name.to_owned(),
			args: self.args.clone(),
		}
	}

	#[must_use]
	pub fn display(&self, typemap: &TypeMap) -> String {
		format!(
			"{}({}){}",
			self.name,
			self.args
				.iter()
				.map(|ty| ty.display(typemap))
				.collect::<Vec<_>>()
				.join(", "),
			self.returns
				.map_or_else(String::new, |ty| format!(" -> {}", ty.display(typemap)))
		)
	}

	/// Function signatures are considered a match when the name matches and the arguments (with coercion) matches; the return type is ignored
	#[must_use]
	pub fn matches(&self, other: &FunctionSignature) -> bool {
		self.name == other.name
			&& self.args.len() == other.args.len()
			&& self
				.args
				.iter()
				.zip(other.args.iter())
				.all(|(a, b)| b.can_coerce(a))
	}

	/// Function signatures are considered a match when the name matches and the arguments (with coercion) matches; the return type is ignored
	#[must_use]
	pub fn matches_lite(&self, other: &FunctionSignatureLite) -> bool {
		self.name == other.name
			&& self.args.len() == other.args.len()
			&& self
				.args
				.iter()
				.zip(other.args.iter())
				.all(|(a, b)| b.can_coerce(a))
	}
}

pub struct FunctionSignatureLite {
	pub name: String,
	pub args: Vec<Type>,
}

impl FunctionSignatureLite {
	#[must_use]
	pub fn new(name: String) -> Self {
		Self {
			name,
			args: Vec::new(),
		}
	}

	#[must_use]
	pub fn with_arg(mut self, arg: Type) -> Self {
		self.args.push(arg);
		self
	}

	#[must_use]
	pub fn with_args(mut self, args: impl Iterator<Item = Type>) -> Self {
		for arg in args {
			self.args.push(arg);
		}
		self
	}

	#[must_use]
	pub fn display(&self, typemap: &TypeMap) -> String {
		format!(
			"{}({}) -> ?",
			self.name,
			self.args
				.iter()
				.map(|ty| ty.display(typemap))
				.collect::<Vec<_>>()
				.join(", "),
		)
	}
}

#[cfg(test)]
mod tests {
	use super::*;
	use gp2_macro::{function, signature};

	#[test]
	fn function_signature_macro() {
		signature!(hello_world);
		signature!(hello_world());
		signature!(hello_world() -> String);
		signature!(hello_world(i32));
		signature!(hello_world(&i32));
		signature!(hello_world(&i32, &mut f32));
		signature!(hello_world(&i32, &mut f32) -> i32);
	}

	#[test]
	fn function_macro() {
		let _a: (FunctionSignature, Function) = function!(
			fn hello_world(a: &i32, b: &i32) -> i32 {
				*a + *b
			}
		);
	}
}
