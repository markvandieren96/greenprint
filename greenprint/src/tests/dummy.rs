use super::*;
use crate as greenprint;
use crate::{reflect, Reflect, TypeName};

#[derive(TypeName, Reflect)]
pub struct Dummy {
	foo: i32,
}

#[test]
fn test_reflect_derive() {
	let meta = init();

	let program = Program::new(
		String::new(),
		&meta,
		"let extern dummy: Dummy;
		dummy.foo",
	)
	.unwrap();
	let mut state = program.create_state();
	*state.find_mut("dummy").unwrap() = Object::new(true, Dummy { foo: 6 });
	let world = World::new();
	let val = *program
		.eval(&mut state, &world)
		.expect("eval error")
		.expect("no return value")
		.downcast_ref::<i32>()
		.expect("return value is not i32");
	assert_eq!(val, 6);
}
