use crate::{ast, reflect};
use reflect::{meta::ClassId, Object, TypeName};
use std::rc::Rc;

#[cfg(miri)]
pub fn create(externs: &mut ast::Externs) {}

#[cfg(not(miri))]
pub fn create(externs: &mut ast::Externs) {
	externs.define_function(
		"println".into(),
		vec![("s".into(), ClassId(String::type_name()))],
		None,
		Rc::new(|_, args| {
			let arg = args.first().unwrap();
			let arg = arg.downcast_ref::<String>().unwrap();
			println!("{arg}");
			None
		}),
	);
	externs.define_function(
		"rand_bool".into(),
		Vec::new(),
		Some(ClassId(bool::type_name())),
		Rc::new(|_, _| Some(Object::new(true, fastrand::bool()))),
	);
	externs.define_function(
		"rand_i".into(),
		Vec::new(),
		Some(ClassId(i64::type_name())),
		Rc::new(|_, _| {
			let min = std::i64::MIN;
			let max = std::i64::MAX;
			Some(Object::new(true, fastrand::i64(min..max)))
		}),
	);
	externs.define_function(
		"rand_i".into(),
		vec![
			("min".into(), ClassId(i64::type_name())),
			("max".into(), ClassId(i64::type_name())),
		],
		Some(ClassId(i64::type_name())),
		Rc::new(|_, args| {
			let min = args.first().unwrap().downcast_ref::<i64>().unwrap();
			let max = args.get(1).unwrap().downcast_ref::<i64>().unwrap();
			Some(Object::new(true, fastrand::i64(min..max)))
		}),
	);
	externs.define_function(
		"rand_f".into(),
		Vec::new(),
		Some(ClassId(f32::type_name())),
		Rc::new(|_, _| Some(Object::new(true, fastrand::f32()))),
	);
	externs.define_function(
		"rand_f".into(),
		vec![
			("min".into(), ClassId(f32::type_name())),
			("max".into(), ClassId(f32::type_name())),
		],
		Some(ClassId(f32::type_name())),
		Rc::new(|_, args| {
			let min = args.first().unwrap().downcast_ref::<f32>().unwrap();
			let max = args.get(1).unwrap().downcast_ref::<f32>().unwrap();
			let r = (fastrand::f32() * (max - min)) + min;
			Some(Object::new(true, r))
		}),
	);

	/*
	program.functions.insert(
		"debug_print".to_owned(),
		Box::new(|_, args| {
			println!("{:?}", args);
			Value::Unit
		}),
	);
	program.functions.insert(
		"trace".to_owned(),
		Box::new(|_, args| {
			trace!("{:?}", args);
			Value::Unit
		}),
	);
	program.functions.insert(
		"print".to_owned(),
		Box::new(|_, args| {
			let mut out = String::new();
			for arg in args {
				if let Value::String(s) = arg {
					out = format!("{}{}", out, s);
				}
			}
			println!("{}", out);
			Value::Unit
		}),
	);
	program.functions.insert("panic".to_owned(), Box::new(|_, args| panic!("Script panic! {:?}", args)));
	*/
}
