use crate::{
	impl_node, Callback, Context, Error, GetType, INode, Node, ScopeId, Validate, Walkable,
};
use parser::Span;
use reflect::meta::{ClassId, Meta};

#[derive(Debug, Clone)]
pub struct Group {
	pub span: Span,
	pub body: Box<Node>,
	pub scope: Option<ScopeId>,
}

impl Group {
	#[must_use]
	pub fn into_node(self) -> Node {
		Node::Group(self)
	}
}

impl INode for Group {
	impl_node!("Group");

	#[must_use]
	fn to_src(&self, depth: usize) -> String {
		format!("({})", self.body.to_src(depth))
	}
}

impl std::fmt::Display for Group {
	fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
		write!(f, "Group({})", self.body)
	}
}

impl From<parser::nodes::Group> for Group {
	fn from(node: parser::nodes::Group) -> Self {
		Self {
			span: node.span,
			scope: None,
			body: Box::new(node.body.into()),
		}
	}
}

impl GetType for Group {
	fn get_type(&self, meta: &Meta, ctx: &Context) -> Result<Option<ClassId>, Error> {
		self.body.get_type(meta, ctx)
	}
}

impl Validate for Group {
	fn pass1(&mut self, meta: &Meta, ctx: &mut Context) -> Result<(), Error> {
		self.scope = Some(ctx.current_scope_expect());
		self.body.validate(meta, ctx)
	}
}

impl Walkable for Group {
	fn walk(&mut self, f: &mut Callback) -> Result<(), Error> {
		f(self)?;
		self.body.walk(f)
	}
}
