use crate::{
	impl_node, Access, Callback, Context, Declarations, Error, GetType, INode, Identifier, Node,
	ScopeId, State, Validate, VariableId, VariableStates, Walkable,
};
use parser::Span;
use reflect::meta::{ClassId, Meta};

#[derive(Debug, Clone)]
pub struct Variable {
	pub span: Span,
	pub scope: Option<ScopeId>,
	pub name: Identifier,
	pub resolved_id: Option<VariableId>,
	pub access: Option<Access>,
}

impl Variable {
	#[must_use]
	pub fn new(span: Span, name: &str) -> Self {
		Self {
			span,
			scope: None,
			name: name.into(),
			resolved_id: None,
			access: None,
		}
	}

	#[must_use]
	pub fn with_access(mut self, access: Access) -> Self {
		self.access = Some(access);
		self
	}

	#[must_use]
	pub fn into_node(self) -> Node {
		Node::Variable(self)
	}
}

impl INode for Variable {
	impl_node!("Variable");

	#[must_use]
	fn to_src(&self, _depth: usize) -> String {
		self.name.to_string()
	}
}

impl From<parser::nodes::Variable> for Variable {
	fn from(node: parser::nodes::Variable) -> Self {
		Self {
			span: node.span,
			scope: None,
			name: node.name.into(),
			resolved_id: None,
			access: None,
		}
	}
}

impl GetType for Variable {
	fn get_type(&self, _meta: &Meta, ctx: &Context) -> Result<Option<ClassId>, Error> {
		let Self {
			span,
			name,
			resolved_id,
			..
		} = self;

		ctx.declarations[resolved_id.expect("Missing resolved VariableId")]
			.ty
			.clone()
			.ok_or_else(|| Error::untyped_variable(span, name))
			.map(Some)
	}
}

impl Validate for Variable {
	fn pass1(
		&mut self,
		_meta: &reflect::meta::Meta,
		ctx: &mut crate::Context,
	) -> Result<(), crate::Error> {
		let Self {
			span,
			scope,
			name,
			resolved_id,
			..
		} = self;

		*scope = Some(ctx.current_scope_expect());

		ctx.resolve_variable(resolved_id, span, name)
	}

	fn check_variable_access(
		&self,
		variables: &mut VariableStates,
		_declarations: &Declarations,
	) -> Result<(), Error> {
		let Self {
			span, name, access, ..
		} = self;
		let id = self.resolved_id.expect("Missing resolved variable id");
		let state = variables.get(id);

		if state == crate::State::Moved {
			return Err(Error::moved_variable(span, name));
		}

		if state == crate::State::Uninitialized {
			return Err(Error::uninitialized_variable(span, name));
		}

		let access = match access {
			Some(access) => access,
			None => {
				println!("Missing variable access for {name}");
				panic!("Missing variable access");
			}
		};
		if *access == Access::Move {
			variables.set(id, State::Moved);
		}

		Ok(())
	}
}

impl std::fmt::Display for Variable {
	fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
		write!(
			f,
			"Variable({}{})",
			match self.access {
				Some(Access::Copy) => "mv-",
				Some(Access::Move) => "cp-",
				Some(Access::Ref) => "&",
				Some(Access::RefMut) => "&mut ",
				None => "?",
			},
			self.name
		)
	}
}

impl Walkable for Variable {
	fn walk(&mut self, f: &mut Callback) -> Result<(), Error> {
		f(self)
	}
}

#[cfg(test)]
mod tests {
	use super::*;
	use crate::test_util::{create_meta, create_variable};

	#[test]
	fn variable_get_type() {
		let meta = create_meta();
		let mut ctx = Context::default();
		let mut node = create_variable().with_access(Access::Copy);

		ctx.scope(&meta, &(0..1), |ctx| {
			let id =
				ctx.declare_variable("x".into(), false, false, Some(ClassId("i64".to_string())));
			ctx.assign_variable("x", ClassId("i64".to_string()));
			ctx.variable_states.set(id, State::Initialized);
			node.validate(&meta, ctx)?;
			let res = node.get_type(&meta, ctx).expect("get_type failed");
			assert_eq!(res.expect("missing type"), ClassId("i64".to_string()));

			Ok(())
		})
		.unwrap();

		assert_eq!(
			node.resolved_id.expect("missing resolved name"),
			VariableId(ScopeId(0), 0)
		);
	}
}
