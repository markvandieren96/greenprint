use crate::{
	ctx::State, impl_node, Callback, Context, Declarations, Error, GetType, INode, Identifier,
	Node, ScopeId, SetAccess, Validate, VariableId, VariableStates, Walkable,
};
use parser::Span;
use reflect::meta::{ClassId, Meta};

#[derive(Debug, Clone)]
pub struct Assignment {
	pub span: Span,
	pub scope: Option<ScopeId>,
	pub name: Identifier,
	pub resolved_id: Option<VariableId>,
	pub value: Box<Node>,
	pub inferred_ty: Option<ClassId>,
}

impl Assignment {
	#[must_use]
	pub fn new(span: Span, name: &str, value: Box<Node>) -> Self {
		Self {
			span,
			scope: None,
			name: name.into(),
			resolved_id: None,
			value,
			inferred_ty: None,
		}
	}

	#[must_use]
	pub fn into_node(self) -> Node {
		Node::Assignment(self)
	}
}

impl INode for Assignment {
	impl_node!("Assignment");

	#[must_use]
	fn to_src(&self, depth: usize) -> String {
		format!("{} = {};", self.name, self.value.to_src(depth))
	}
}

impl std::fmt::Display for Assignment {
	fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
		write!(f, "Assignment({} = {})", self.name, self.value)
	}
}

impl From<parser::nodes::Assignment> for Assignment {
	fn from(node: parser::nodes::Assignment) -> Self {
		Self {
			span: node.span,
			scope: None,
			name: node.name.into(),
			resolved_id: None,
			value: Box::new(node.value.into()),
			inferred_ty: None,
		}
	}
}

impl GetType for Assignment {
	fn get_type(&self, _meta: &Meta, _context: &Context) -> Result<Option<ClassId>, Error> {
		Ok(None)
	}
}

impl Validate for Assignment {
	fn pass1(&mut self, meta: &Meta, ctx: &mut Context) -> Result<(), Error> {
		let Self {
			span,
			scope,
			name,
			value,
			resolved_id,
			inferred_ty,
			..
		} = self;

		*scope = Some(ctx.current_scope_expect());

		ctx.resolve_variable(resolved_id, span, name)?;

		value.validate(meta, ctx)?;

		let ty = value.get_type(meta, ctx)?.ok_or_else(|| {
			Error::assignment_wrong_type(span)
				.expected(ClassId::unknown())
				.build()
		})?;
		*inferred_ty = Some(ty.clone());

		value.set_access_copy_or_move(&ty, meta, ctx, span)?;
		Ok(())
	}

	fn check(&self, _meta: &Meta, ctx: &Context) -> Result<(), Error> {
		let Self {
			value,
			resolved_id,
			inferred_ty,
			..
		} = self;

		let var = &ctx.declarations[resolved_id.expect("Missing resolved variable id")];
		let ty = inferred_ty
			.as_ref()
			.expect("Assignment missing inferred type");
		if let Some(var_ty) = &var.ty {
			if var_ty != ty {
				return Err(Error::assignment_wrong_type(&value.span())
					.expected(var_ty.clone())
					.got(ty.clone())
					.build());
			}
		}

		Ok(())
	}

	fn check_variable_access(
		&self,
		variables: &mut VariableStates,
		declarations: &Declarations,
	) -> Result<(), Error> {
		let Self { span, name, .. } = self;

		let id = self.resolved_id.expect("Missing resolved variable id");

		let var = &declarations[id];
		let state = variables.get(id);
		if !var.is_mut && state == State::Initialized {
			return Err(Error::assignment_to_const(span, name));
		}

		variables.set(id, State::Initialized);
		Ok(())
	}
}

impl Walkable for Assignment {
	fn walk(&mut self, f: &mut Callback) -> Result<(), Error> {
		f(self)?;
		self.value.walk(f)
	}
}

#[cfg(test)]
mod tests {
	use super::*;
	use crate::test_util::{create_assignment, create_meta};

	#[test]
	fn assignment_get_type() {
		let meta = Meta::default();
		let ctx = Context::default();
		let node = create_assignment();
		let res = node.get_type(&meta, &ctx).expect("get_type failed");
		assert_eq!(res, None);
	}

	#[test]
	fn assignment_validate() {
		let mut ctx = Context::default();
		let meta = create_meta();
		let mut node = create_assignment().into_node();

		ctx.scope(&meta, &(0..1), |ctx| {
			ctx.declare_variable("x".into(), false, true, None);
			node.validate(&meta, ctx).expect("validate failed");
			Ok(())
		})
		.unwrap();

		let node = node.expect_assignment();

		assert_eq!(node.scope.expect("missing scope id"), ScopeId(0));
		assert_eq!(
			node.resolved_id.expect("missing resolved ident"),
			VariableId(ScopeId(0), 0)
		);
	}
}
