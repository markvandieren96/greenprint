use crate::{Access, Error};
use serde::{Deserialize, Serialize};
use std::collections::HashMap;
use std::io::{Read, Write};

#[derive(Default, Debug, Serialize, Deserialize)]
pub struct Meta {
	pub classes: HashMap<ClassId, Class>,
}

#[derive(Debug, Default, Serialize, Deserialize)]
pub struct Class {
	pub fields: HashMap<String, ClassId>,
	pub methods: HashMap<String, Method>,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct Method {
	pub self_param: SelfParam,
	pub parameters: Vec<TypeWithAccess>,
	pub returns: Option<TypeWithAccess>,
}

#[derive(Debug, PartialEq, Eq, Serialize, Deserialize)]
pub enum SelfParam {
	Static,
	Owned,
	Ref,
	RefMut,
}

impl SelfParam {
	#[must_use]
	pub fn to_src(&self) -> String {
		match self {
			Self::Static => String::new(),
			Self::Owned => "self".to_string(),
			Self::Ref => "&self".to_string(),
			Self::RefMut => "&mut self".to_string(),
		}
	}
}

#[derive(Debug, Serialize, Deserialize)]
pub struct TypeWithAccess {
	pub access: Access,
	pub ty: ClassId,
}

#[derive(Clone, Debug, PartialEq, Eq, Hash, Serialize, Deserialize)]
#[serde(transparent)]
pub struct ClassId(pub String);

impl ClassId {
	#[must_use]
	pub fn none() -> Self {
		Self("<NONE>".to_owned())
	}

	#[must_use]
	pub fn any() -> Self {
		Self("<ANY>".to_owned())
	}

	#[must_use]
	pub fn unknown() -> Self {
		Self("<UNKNOWN>".to_owned())
	}
}

impl std::fmt::Display for ClassId {
	fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
		write!(f, "{}", self.0)
	}
}

#[derive(Debug)]
pub enum LoadError {
	Serialization(ron::Error),
	Io(std::io::Error),
}

impl Meta {
	pub fn get(&self, id: &ClassId) -> Result<&Class, Error> {
		self.classes.get(id).ok_or_else(|| Error::no_such_class(id))
	}

	pub fn save(&self, path: impl AsRef<std::path::Path>) -> Result<(), LoadError> {
		let config = ron::ser::PrettyConfig::new().with_indentor("\t".to_owned());
		let s = ron::ser::to_string_pretty(self, config).map_err(LoadError::Serialization)?;

		let mut file = std::fs::File::create(path).map_err(LoadError::Io)?;
		file.write_all(s.as_bytes()).map_err(LoadError::Io)
	}

	pub fn load(path: impl AsRef<std::path::Path>) -> Result<Self, LoadError> {
		let mut file = std::fs::File::open(path).map_err(LoadError::Io)?;
		let mut buffer = String::new();
		file.read_to_string(&mut buffer).map_err(LoadError::Io)?;

		ron::de::from_str(&buffer).map_err(LoadError::Serialization)
	}
}

impl Class {
	#[must_use]
	pub fn has_method(&self, name: &str) -> bool {
		self.methods.get(name).is_some()
	}

	#[must_use]
	pub fn has_method_static(&self, name: &str) -> bool {
		self.get_method_with_self(name, &SelfParam::Static)
			.is_some()
	}

	#[must_use]
	pub fn has_method_owned(&self, name: &str) -> bool {
		self.get_method_with_self(name, &SelfParam::Owned).is_some()
	}

	#[must_use]
	pub fn has_method_ref(&self, name: &str) -> bool {
		self.get_method_with_self(name, &SelfParam::Ref).is_some()
	}

	#[must_use]
	pub fn has_method_ref_mut(&self, name: &str) -> bool {
		self.get_method_with_self(name, &SelfParam::RefMut)
			.is_some()
	}

	#[must_use]
	pub fn get_method_static(&self, name: &str) -> Option<&Method> {
		self.get_method_with_self(name, &SelfParam::Static)
	}
	#[must_use]
	pub fn get_method_owned(&self, name: &str) -> Option<&Method> {
		self.get_method_with_self(name, &SelfParam::Owned)
	}

	#[must_use]
	pub fn get_method_ref(&self, name: &str) -> Option<&Method> {
		self.get_method_with_self(name, &SelfParam::Ref)
	}

	#[must_use]
	pub fn get_method_ref_mut(&self, name: &str) -> Option<&Method> {
		self.get_method_with_self(name, &SelfParam::RefMut)
	}

	#[must_use]
	pub fn get_field(&self, name: &str) -> Option<&ClassId> {
		self.fields.get(name)
	}

	fn get_method_with_self(&self, name: &str, self_param: &SelfParam) -> Option<&Method> {
		let res = self.methods.get(name);
		if let Some(r) = res {
			if r.self_param == *self_param {
				return res;
			}
		}
		None
	}
}
