use crate::Expression;
use lexer::Span;

/// Parses "foo = expr"
#[derive(Debug, PartialEq, Clone)]
pub struct Assignment {
	pub span: Span,
	pub name: String,
	pub value: Expression,
}
