use lexer::Token;

#[derive(Debug, PartialEq, Clone)]
pub enum BinaryOperator {
	Add,
	Sub,
	Mul,
	Div,
	Less,
	LessEqual,
	Greater,
	GreaterEqual,
	Equal,
	NotEqual,
	And,
	Or,
}

impl std::convert::TryFrom<Token> for BinaryOperator {
	type Error = ();

	fn try_from(token: Token) -> Result<Self, Self::Error> {
		match token {
			Token::Plus => Ok(Self::Add),
			Token::Min => Ok(Self::Sub),
			Token::Mul => Ok(Self::Mul),
			Token::Div => Ok(Self::Div),
			Token::LessThan => Ok(Self::Less),
			Token::LessEqual => Ok(Self::LessEqual),
			Token::GreaterThan => Ok(Self::Greater),
			Token::GreaterEqual => Ok(Self::GreaterEqual),
			Token::Equal => Ok(Self::Equal),
			Token::NotEqual => Ok(Self::NotEqual),
			Token::And => Ok(Self::And),
			Token::Or => Ok(Self::Or),
			_ => Err(()),
		}
	}
}

impl std::fmt::Display for BinaryOperator {
	fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
		match self {
			Self::Add => write!(f, "+"),
			Self::Sub => write!(f, "-"),
			Self::Mul => write!(f, "*"),
			Self::Div => write!(f, "/"),
			Self::Less => write!(f, "<"),
			Self::LessEqual => write!(f, "<="),
			Self::Greater => write!(f, ">"),
			Self::GreaterEqual => write!(f, ">="),
			Self::Equal => write!(f, "=="),
			Self::NotEqual => write!(f, "!="),
			Self::And => write!(f, "and"),
			Self::Or => write!(f, "or"),
		}
	}
}

impl BinaryOperator {
	#[must_use]
	pub fn name(&self) -> &'static str {
		match self {
			Self::Add => "op_add",
			Self::Sub => "op_sub",
			Self::Mul => "op_mul",
			Self::Div => "op_div",
			Self::Less => "op_lt",
			Self::LessEqual => "op_le",
			Self::Greater => "op_gt",
			Self::GreaterEqual => "op_ge",
			Self::Equal => "op_eq",
			Self::NotEqual => "op_ne",
			Self::And => "op_and",
			Self::Or => "op_or",
		}
	}
}
