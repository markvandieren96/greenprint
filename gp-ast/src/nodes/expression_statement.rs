use crate::{
	impl_node, Callback, Context, Error, GetType, INode, Node, ScopeId, SetAccess, Validate,
	Walkable,
};
use parser::Span;
use reflect::meta::{ClassId, Meta};

#[derive(Debug, Clone)]
pub struct ExpressionStatement {
	pub span: Span,
	pub scope: Option<ScopeId>,
	pub body: Box<Node>,
	pub has_semicolon: bool,
}

impl ExpressionStatement {
	#[must_use]
	pub fn into_node(self) -> Node {
		Node::ExpressionStatement(self)
	}
}

impl INode for ExpressionStatement {
	impl_node!("ExpressionStatement");

	#[must_use]
	fn to_src(&self, depth: usize) -> String {
		format!("ExpressionStatement({})", self.body.to_src(depth))
	}
}

impl From<parser::nodes::ExpressionStatement> for ExpressionStatement {
	fn from(node: parser::nodes::ExpressionStatement) -> Self {
		Self {
			span: node.span,
			scope: None,
			body: Box::new(node.body.into()),
			has_semicolon: node.has_semicolon,
		}
	}
}

impl GetType for ExpressionStatement {
	fn get_type(&self, meta: &Meta, ctx: &Context) -> Result<Option<ClassId>, Error> {
		if self.has_semicolon {
			Ok(None)
		} else {
			self.body.get_type(meta, ctx)
		}
	}
}

impl Validate for ExpressionStatement {
	fn pass1(&mut self, meta: &Meta, ctx: &mut Context) -> Result<(), Error> {
		let Self {
			span, scope, body, ..
		} = self;

		*scope = Some(ctx.current_scope_expect());

		body.validate(meta, ctx)?;
		body.set_access_opt_copy_or_move(body.get_type(meta, ctx)?, meta, ctx, span)?;

		Ok(())
	}
}

impl std::fmt::Display for ExpressionStatement {
	fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
		write!(
			f,
			"ExpressionStatement({}{})",
			self.body,
			if self.has_semicolon { ";" } else { "" }
		)
	}
}

impl Walkable for ExpressionStatement {
	fn walk(&mut self, f: &mut Callback) -> Result<(), Error> {
		f(self)?;
		self.body.walk(f)
	}
}
