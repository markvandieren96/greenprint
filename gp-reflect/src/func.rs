use crate::{meta::ClassId, Object, Type};
use specs::World;

pub struct Func<T> {
	func: T,
	pub parameters: Vec<Type>,
	pub return_type: Option<Type>,
}

impl<T> Func<T> {
	pub fn new(func: T, parameters: Vec<Type>, return_type: Option<Type>) -> Self {
		Self {
			func,
			parameters,
			return_type,
		}
	}

	pub fn return_type_id(&self) -> Option<ClassId> {
		self.return_type.as_ref().map(|ty| ty.ty.clone())
	}
}

impl<T> std::ops::Deref for Func<T> {
	type Target = T;

	fn deref(&self) -> &Self::Target {
		&self.func
	}
}

pub type FreeFunc =
	Func<Box<dyn Fn(&'static World, &mut [Object]) -> Option<Object> + Send + Sync>>;
pub type MemberRefFunc =
	Func<Box<dyn Fn(&'static World, &Object, &mut [Object]) -> Option<Object> + Send + Sync>>;
pub type MemberMutFunc =
	Func<Box<dyn Fn(&'static World, &mut Object, &mut [Object]) -> Option<Object> + Send + Sync>>;
pub type MemberOwnedFunc =
	Func<Box<dyn Fn(&'static World, Object, &mut [Object]) -> Option<Object> + Send + Sync>>;
