use std::collections::HashMap;

pub struct Container<T> {
	pub(crate) names: HashMap<&'static str, usize>,
	pub(crate) fields: Vec<T>,
}

impl<T> Default for Container<T> {
	fn default() -> Self {
		Self {
			names: HashMap::new(),
			fields: Vec::new(),
		}
	}
}

impl<T> Container<T> {
	#[must_use]
	pub fn new() -> Self {
		Self::default()
	}

	pub fn insert(&mut self, name: &'static str, f: T) {
		let index = self.fields.len();
		self.fields.push(f);
		self.names.insert(name, index);
	}

	#[must_use]
	pub fn find(&self, name: &str) -> Option<&T> {
		self.names
			.get(name)
			.and_then(|index| self.fields.get(*index))
	}
}

impl<T> std::fmt::Debug for Container<T> {
	fn fmt(&self, f: &mut std::fmt::Formatter) -> Result<(), std::fmt::Error> {
		let names: Vec<_> = self.names.keys().collect();
		f.debug_list().entries(names.iter()).finish()
	}
}

#[test]
pub fn test_container_find() {
	let mut container = Container::new();
	container.insert("a", 1);
	container.insert("b", 2);
	assert_eq!(*container.find("a").unwrap(), 1);
	assert_eq!(*container.find("b").unwrap(), 2);
}
