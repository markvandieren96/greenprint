#![warn(clippy::all)]
#![warn(clippy::pedantic)]
#![allow(clippy::missing_errors_doc)]
#![allow(clippy::too_many_lines)] // TODO fix this

mod ctx;
mod error;
mod eval;
mod state;

pub use ctx::*;
pub use error::*;
pub use eval::*;
pub use state::*;

pub use ast;

#[cfg(test)]
mod tests;
