pub fn get(name: &str, fallback: &str) -> String {
	for arg in std::env::args() {
		if let Some(working_dir) = arg.strip_prefix(&format!("{}=", name)) {
			return working_dir.into();
		}
	}
	fallback.to_string()
}
