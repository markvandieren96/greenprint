pub(crate) mod helper;

use crate::{Object, CLASSES};
use assert_approx_eq::assert_approx_eq;
use helper::{Bar, Foo, Transform, Vec2};
use specs::{World, WorldExt};

#[test]
pub fn test_reflection_method() {
	let obj = Object::new(true, 6);
	let obj_as_f = obj.call("to_f32", &World::new(), &mut []).unwrap().unwrap();
	assert_approx_eq!(*obj_as_f.downcast_ref::<f32>().unwrap(), 6.0);
}

#[test]
pub fn test_reflection_field() {
	let obj = Object::new(true, Foo { i: 6, i2: 7 });
	let ret = obj.field("i").unwrap();
	assert_eq!(*ret.downcast_ref::<i32>().unwrap(), 6);
}

#[test]
pub fn test_reflection_field_mut() {
	let mut obj = Object::new(true, Foo { i: 6, i2: 7 });
	let ret = obj.field_mut("i").unwrap();
	assert_eq!(*ret.downcast_ref::<i32>().unwrap(), 6);
}

#[test]
pub fn test_reflection_field_mut2() {
	let mut obj = Object::new(
		true,
		Bar {
			foo: Foo { i: 6, i2: 7 },
		},
	);
	let mut field = obj.field_mut("foo").unwrap();
	field
		.call_mut(
			"op_assign",
			&World::new(),
			&mut [Object::new(true, Foo { i: 7, i2: 7 })],
		)
		.unwrap();
	let bar = obj.downcast_ref::<Bar>().unwrap();
	assert_eq!(bar.foo.i, 7);
}

#[test]
pub fn test_reflection_field_mut_nested() {
	let mut obj = Object::new(
		true,
		Bar {
			foo: Foo { i: 6, i2: 7 },
		},
	);
	let mut field = obj.field_mut("foo").unwrap();
	let mut field = field.field_mut("i").unwrap();
	field
		.call_mut("op_assign", &World::new(), &mut [Object::new(true, 7)])
		.unwrap();

	let bar = obj.downcast_ref::<Bar>().unwrap();
	assert_eq!(bar.foo.i, 7);
}

#[test]
pub fn test_modify_class() {
	let obj = Object::new(true, Foo { i: 1, i2: 7 });
	let ret = obj.call("get_6", &World::new(), &mut []).unwrap().unwrap();
	assert_eq!(*ret.downcast_ref::<i32>().unwrap(), 6);
}

#[test]
pub fn test_modify_class2() {
	let obj: Object = Object::new(true, Foo { i: 1, i2: 7 });
	let ret = obj.call("get_7", &World::new(), &mut []).unwrap().unwrap();
	assert_eq!(*ret.downcast_ref::<i32>().unwrap(), 7);
}

#[test]
pub fn test_vec2_constructor_value() {
	let class = CLASSES.find("Vec2").unwrap();
	let vec2 = class
		.call(
			"new",
			&World::new(),
			&mut [Object::new(true, 1.0_f32), Object::new(true, 2.0_f32)],
		)
		.unwrap()
		.unwrap();
	assert_eq!(
		*vec2.downcast_ref::<Vec2>().unwrap(),
		Vec2 { x: 1.0, y: 2.0 }
	);
}

#[test]
pub fn test_vec2_constructor_ref() {
	let class = CLASSES.find("Vec2").unwrap();
	let a: f32 = 1.0;
	let b: f32 = 2.0;
	let vec2 = class
		.call(
			"new",
			&World::new(),
			&mut [Object::new_ref(&a), Object::new_ref(&b)],
		)
		.unwrap()
		.unwrap();
	assert_eq!(
		*vec2.downcast_ref::<Vec2>().unwrap(),
		Vec2 { x: 1.0, y: 2.0 }
	);
}

#[test]
pub fn test_vec2_constructor_ref_mut() {
	let class = CLASSES.find("Vec2").unwrap();
	let mut a: f32 = 1.0;
	let mut b: f32 = 2.0;
	let vec2 = class
		.call(
			"new",
			&World::new(),
			&mut [Object::new_ref_mut(&mut a), Object::new_ref_mut(&mut b)],
		)
		.unwrap()
		.unwrap();
	assert_eq!(
		*vec2.downcast_ref::<Vec2>().unwrap(),
		Vec2 { x: 1.0, y: 2.0 }
	);
}

#[test]
pub fn test_transform() {
	let class = CLASSES.find("Transform").unwrap();
	let mut transform = class.call("new", &World::new(), &mut []).unwrap().unwrap();

	{
		let mut position = transform.field_mut("position").unwrap();
		position
			.call_mut(
				"op_assign",
				&World::new(),
				&mut [Object::new(true, Vec2 { x: 1.0, y: 2.0 })],
			)
			.unwrap();
	}

	let transform = transform.downcast_ref::<Transform>().unwrap();

	assert_eq!(
		*transform,
		Transform {
			position: Vec2 { x: 1.0, y: 2.0 },
			scale: Vec2 { x: 0.0, y: 0.0 }
		}
	);
}

#[test]
pub fn test_operator() {
	{
		let obj: Object = Object::new(true, 6);
		let ret = obj.call("op_neg", &World::new(), &mut []).unwrap().unwrap();
		assert_eq!(*ret.downcast_ref::<i32>().unwrap(), -6);
	}

	{
		let lhs: Object = Object::new(true, "hello".to_owned());
		let rhs: Object = Object::new(true, ", world!".to_owned());
		let ret = lhs
			.call("op_add", &World::new(), &mut [rhs])
			.unwrap()
			.unwrap();
		assert_eq!(
			*ret.downcast_ref::<String>().unwrap(),
			"hello, world!".to_owned()
		);
	}
}
