use crate::{
	impl_node, nodes::Block, Access, Callback, Context, Error, GetType, INode, Identifier, Node,
	ScopeId, SetAccess, State, Validate, VariableId, Walkable,
};
use parser::Span;
use reflect::{
	meta::{ClassId, Meta},
	TypeName,
};

#[derive(Debug, Clone)]
pub struct For {
	pub span: Span,
	pub scope: Option<ScopeId>,
	pub name: Identifier,
	pub resolved_id: Option<VariableId>,
	pub range_begin: Box<Node>,
	pub range_end: Box<Node>,
	pub range_inclusive: bool,
	pub body: Block,
	pub drops: Vec<VariableId>,
	pub body_scope: Option<ScopeId>,
}

impl For {
	#[must_use]
	pub fn into_node(self) -> Node {
		Node::For(self)
	}
}

impl INode for For {
	impl_node!("For");

	#[must_use]
	fn to_src(&self, _depth: usize) -> String {
		format!("for {}", self.name)
	}
}

impl std::fmt::Display for For {
	fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
		write!(f, "for {}", self.name)
	}
}

impl GetType for For {
	fn get_type(&self, meta: &Meta, ctx: &Context) -> Result<Option<ClassId>, Error> {
		self.body.get_type(meta, ctx)
	}
}

impl From<parser::nodes::ForLoop> for For {
	fn from(node: parser::nodes::ForLoop) -> Self {
		Self {
			span: node.span,
			scope: None,
			name: node.variable.into(),
			resolved_id: None,
			range_begin: Box::new(node.range_begin.into()),
			range_end: Box::new(node.range_end.into()),
			range_inclusive: node.range_inclusive,
			body: node.body.into(),
			drops: Vec::new(),
			body_scope: None,
		}
	}
}

impl Validate for For {
	fn pass1(&mut self, meta: &Meta, ctx: &mut Context) -> Result<(), Error> {
		let Self {
			span,
			scope,
			name,
			resolved_id,
			range_begin,
			range_end,
			body,
			drops,
			body_scope,
			..
		} = self;

		*scope = Some(ctx.current_scope_expect());

		*body = std::mem::take(body).replace_free_with_extern_call_nodes(ctx);

		let dropped_variables = ctx.scope(meta, span, |ctx| {
			*body_scope = Some(ctx.current_scope_expect());
			*resolved_id = Some(ctx.declare_variable(
				name.to_string(),
				false,
				true,
				Some(ClassId(i64::type_name())),
			));
			ctx.assign_variable(name, ClassId(i64::type_name()));

			range_begin.set_access(ctx, Access::Copy)?;
			range_begin.validate(meta, ctx)?;

			range_end.set_access(ctx, Access::Copy)?;
			range_end.validate(meta, ctx)?;

			body.validate(meta, ctx)
		})?;
		drops.extend(dropped_variables);
		Ok(())
	}

	fn check(&self, meta: &Meta, ctx: &Context) -> Result<(), Error> {
		let Self {
			range_begin,
			range_end,
			..
		} = self;
		validate_range_type(&range_begin.span(), &range_begin.get_type(meta, ctx)?)?;
		validate_range_type(&range_end.span(), &range_end.get_type(meta, ctx)?)?;
		Ok(())
	}

	fn check_variable_access(
		&self,
		variables: &mut crate::VariableStates,
		_declarations: &crate::Declarations,
	) -> Result<(), Error> {
		println!("for_loop check_variable_access {}", self.name);
		let id = self.resolved_id.expect("Missing resolved variable id");
		variables.set(id, State::Initialized);
		Ok(())
	}
}

fn validate_range_type(span: &Span, ty: &Option<ClassId>) -> Result<(), Error> {
	match ty {
		Some(ty) => {
			if *ty != ClassId(i64::type_name()) {
				return Err(Error::range_wrong_type(span)
					.expected(i64::class_id())
					.got(ty.clone())
					.build());
			}
		}
		None => {
			return Err(Error::range_wrong_type(span)
				.expected(i64::class_id())
				.build());
		}
	}
	Ok(())
}

impl Walkable for For {
	fn walk(&mut self, f: &mut Callback) -> Result<(), Error> {
		f(self)?;
		self.range_begin.walk(f)?;
		self.range_end.walk(f)?;
		self.body.walk(f)
	}
}
