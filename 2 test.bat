@echo off
REM set RUST_BACKTRACE=1

cargo +stable test --lib || goto wait

goto eof

:wait
pause

:eof