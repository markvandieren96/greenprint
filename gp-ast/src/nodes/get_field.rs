use crate::{
	impl_node, Access, Callback, Context, Error, GetType, INode, Identifier, Node, ScopeId,
	Validate, Walkable,
};
use parser::Span;
use reflect::meta::{ClassId, Meta};

#[derive(Debug, Clone)]
pub struct GetField {
	pub span: Span,
	pub scope: Option<ScopeId>,
	pub object: Box<Node>,
	pub name: Identifier,
	pub access: Option<Access>,
}

impl GetField {
	#[must_use]
	pub fn into_node(self) -> Node {
		Node::GetField(self)
	}
}

impl INode for GetField {
	impl_node!("GetField");

	#[must_use]
	fn to_src(&self, depth: usize) -> String {
		format!("{}.{}", self.object.to_src(depth), self.name)
	}
}

impl std::fmt::Display for GetField {
	fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
		write!(f, "GetField({}.{})", self.object, self.name)
	}
}

impl From<parser::nodes::GetField> for GetField {
	fn from(node: parser::nodes::GetField) -> Self {
		Self {
			span: node.span,
			scope: None,
			object: Box::new(node.object.into()),
			name: node.name.into(),
			access: None,
		}
	}
}

impl GetType for GetField {
	fn get_type(&self, meta: &Meta, ctx: &Context) -> Result<Option<ClassId>, Error> {
		let Self {
			span, object, name, ..
		} = self;

		let object_type = object
			.get_type(meta, ctx)?
			.ok_or_else(|| Error::get_field_on_undefined(span, name))?;
		let class = meta.get(&object_type).expect("Class not registered");
		let name = class
			.get_field(name)
			.ok_or_else(|| Error::undefined_field(span, object_type, name))?;
		Ok(Some(name.clone()))
	}
}

impl Validate for GetField {
	fn pass1(&mut self, meta: &Meta, ctx: &mut Context) -> Result<(), Error> {
		self.scope = Some(ctx.current_scope_expect());
		self.object.validate(meta, ctx)
	}
}

impl Walkable for GetField {
	fn walk(&mut self, f: &mut Callback) -> Result<(), Error> {
		f(self)?;
		self.object.walk(f)
	}
}
