use crate::Class;

#[derive(Debug, PartialEq)]
pub enum Error {
	ConstViolation(String),
	NoSuchMethod { method: String, class: String },
	NoSuchStaticFunction { method: String, class: String },
	NoSuchField { field: String, class: String },
	NoSuchClass { class: String },
	FieldNotMutable,
}

impl Error {
	pub fn const_violation(s: impl AsRef<str>) -> Error {
		Error::ConstViolation(s.as_ref().to_owned())
	}

	pub fn no_such_method(method: impl AsRef<str>, class: &Class) -> Error {
		Error::NoSuchMethod {
			method: method.as_ref().to_owned(),
			class: class.name().to_owned(),
		}
	}

	pub fn no_such_static_function(method: impl AsRef<str>, class: &Class) -> Error {
		Error::NoSuchStaticFunction {
			method: method.as_ref().to_owned(),
			class: class.name().to_owned(),
		}
	}

	pub fn no_such_field(field: impl AsRef<str>, class: &Class) -> Error {
		Error::NoSuchField {
			field: field.as_ref().to_owned(),
			class: class.name().to_owned(),
		}
	}

	pub fn no_such_class(class: &impl ToString) -> Error {
		Error::NoSuchClass {
			class: class.to_string(),
		}
	}
}

impl<T> From<Error> for Result<T, Error> {
	fn from(e: Error) -> Self {
		Err(e)
	}
}

impl std::fmt::Display for Error {
	fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
		match self {
			Error::ConstViolation(s) => write!(f, "ConstViolation({s})"),
			Error::NoSuchMethod { method, class } => {
				write!(f, "No such method \"{method}\" in class \"{class}\"")
			}
			Error::NoSuchStaticFunction { method, class } => {
				write!(
					f,
					"No such static method \"{method}\" in class \"{class}\""
				)
			}
			Error::NoSuchField { field, class } => {
				write!(f, "No such field \"{field}\" in class \"{class}\"")
			}
			Error::NoSuchClass { class } => {
				write!(f, "No such class \"{class}\"")
			}
			Error::FieldNotMutable => {
				write!(f, "Field is not mutable")
			}
		}
	}
}
