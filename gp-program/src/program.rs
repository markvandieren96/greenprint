use crate::{ast, parser, reflect};
use crate::{line_range::LineRange, std_lib};
use ast::Node;
use eval::{Eval, State};
use log::info;
use parser::SpannedErrorMessage;
use reflect::{meta::Meta, specs::World, Object};

#[derive(Debug)]
pub struct TypeCheckError {
	pub e: ast::Error,
	pub lines: LineRange,
	pub ast: Node,
	pub ctx: ast::Context,
}

#[derive(Debug)]
pub enum CompileError {
	Parse(parser::FailedParseInfo),
	TypeCheck(Box<TypeCheckError>),
}

#[derive(Debug)]
pub enum RuntimeError {
	InvalidNumberOfArguments {
		func_name: String,
		expected: usize,
		got: usize,
	},
	Eval {
		e: eval::Error,
		lines: LineRange,
	},
}

#[derive(Debug)]
pub struct Program {
	pub debug_name: String,
	pub src: String,
	pub ast: Node,
	pub ctx: ast::Context,
	pub eval_context: eval::Context,
}

impl Program {
	pub fn new(debug_name: String, meta: &Meta, src: &str) -> Result<Self, CompileError> {
		info!("program({})::new()", debug_name);
		let expr = parser::parse(src).map_err(CompileError::Parse)?;
		let mut externs = ast::Externs::default();
		std_lib::create(&mut externs);
		let ((node, ctx), error) = ast::type_check(expr, meta, externs);
		if let Err(e) = error {
			let line_range = LineRange::new(e.span().unwrap_or(0..src.len()), src);
			return Err(CompileError::TypeCheck(Box::new(TypeCheckError {
				e,
				lines: line_range,
				ast: node,
				ctx,
			})));
		}

		let eval_context = eval::Context::new(&ctx);

		Ok(Self {
			debug_name,
			src: src.to_owned(),
			ast: node,
			ctx,
			eval_context,
		})
	}

	#[must_use]
	pub fn create_state(&self) -> State {
		info!("program({}).create_state()", self.debug_name);
		State::new(&self.ctx)
	}

	pub fn eval(&self, state: &mut State, world: &World) -> Result<Option<Object>, RuntimeError> {
		info!("program({}).eval()", self.debug_name);
		self.ast
			.eval(world, &self.eval_context, state, 0)
			.map_err(|e| {
				let line_range = LineRange::new(e.span().unwrap_or(0..self.src.len()), &self.src);
				RuntimeError::Eval {
					e,
					lines: line_range,
				}
			})
	}

	#[must_use]
	pub fn has_fn(&self, name: &str) -> bool {
		info!("program({}).has_fn({})", self.debug_name, name);
		self.ctx.get_function(name).is_some()
	}

	pub fn call(
		&self,
		state: &mut State,
		name: &str,
		world: &World,
		args: &mut [Object],
	) -> Result<Option<Object>, RuntimeError> {
		info!(
			"program({}).call({}, args_len={})",
			self.debug_name,
			name,
			args.len()
		);

		let func = self.ctx.get_function(name).unwrap();
		let vars = state.get_scope_mut(func.extern_call_scope);
		if vars.len() != args.len() {
			return Err(RuntimeError::InvalidNumberOfArguments {
				func_name: name.to_owned(),
				expected: vars.len(),
				got: args.len(),
			});
		}

		for (var, arg) in vars.iter_mut().zip(args.iter_mut()) {
			*var = std::mem::take(arg);
		}

		func.body
			.eval(world, &self.eval_context, state, 0)
			.map_err(|e| {
				let line_range = LineRange::new(e.span().unwrap_or(0..self.src.len()), &self.src);
				RuntimeError::Eval {
					e,
					lines: line_range,
				}
			})
	}

	#[must_use]
	pub fn get_value<'state>(&self, state: &'state State, name: &str) -> Option<&'state Object> {
		info!("program({}).get_value({})", self.debug_name, name);
		self.ctx
			.resolve_variable_raw(name)
			.map(|variable_id| state.get(variable_id))
	}
}

impl std::fmt::Display for CompileError {
	fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
		match self {
			CompileError::Parse(e) => write!(f, "{e:#?}"),
			CompileError::TypeCheck(e) => write!(f, "{}: {}", e.lines, e.e),
		}
	}
}

impl std::fmt::Display for RuntimeError {
	fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
		match self {
			RuntimeError::InvalidNumberOfArguments {
				func_name,
				expected,
				got,
			} => {
				write!(
					f,
					"Invalid number of arguments for \"{func_name}\". Expected {expected}, got {got}."
				)
			}
			RuntimeError::Eval { e, lines } => write!(f, "{lines}: {e:#?}"),
		}
	}
}
