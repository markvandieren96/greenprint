use crate::{Error, Inner, Object};
use std::any::{Any, TypeId};

pub(crate) type Func = Box<dyn Fn(&dyn Any) -> Object + Send + Sync>;
pub(crate) type FuncMut = Box<dyn Fn(&mut dyn Any) -> Object + Send + Sync>;

pub struct Field {
	pub ty: TypeId,
	get: Func,
	get_mut: Option<FuncMut>,
}

impl Field {
	#[must_use]
	pub fn new(ty: TypeId, get: Func) -> Self {
		Self {
			ty,
			get,
			get_mut: None,
		}
	}

	#[must_use]
	pub fn new_mut(ty: TypeId, get: Func, get_mut: FuncMut) -> Self {
		Self {
			ty,
			get,
			get_mut: Some(get_mut),
		}
	}

	pub(crate) fn get(&self, object: &Inner) -> Object {
		(self.get)(object.as_any())
	}

	pub(crate) fn get_mut(&self, object: &mut Inner) -> Result<Object, Error> {
		match &self.get_mut {
			Some(get_mut) => Ok((get_mut)(object.as_any_mut()?)),
			None => Err(Error::FieldNotMutable),
		}
	}
}
