use crate::{Error, INode};

pub trait Walkable {
	fn walk(&mut self, f: &mut Callback<'_>) -> Result<(), Error>;
}

pub type Callback<'a> = dyn FnMut(&mut dyn INode) -> Result<(), Error> + 'a;

impl<T: Walkable> Walkable for Option<T> {
	fn walk(&mut self, f: &mut Callback) -> Result<(), Error> {
		if let Some(t) = self {
			t.walk(f)?;
		}
		Ok(())
	}
}

impl<T: Walkable> Walkable for Vec<T> {
	fn walk(&mut self, f: &mut Callback) -> Result<(), Error> {
		for t in self {
			t.walk(f)?;
		}
		Ok(())
	}
}

impl<T: Walkable> Walkable for Box<T> {
	fn walk(&mut self, f: &mut Callback) -> Result<(), Error> {
		(self as &mut T).walk(f)
	}
}
