use crate::{
	impl_node, nodes::Block, Access, Callback, Context, Error, GetType, INode, Node, ScopeId,
	SetAccess, Validate, Walkable,
};
use parser::Span;
use reflect::{
	meta::{ClassId, Meta},
	TypeName,
};

#[derive(Debug, Clone)]
pub struct While {
	pub span: Span,
	pub scope: Option<ScopeId>,
	pub condition: Box<Node>,
	pub body: Block,
}

impl While {
	#[must_use]
	pub fn into_node(self) -> Node {
		Node::While(self)
	}
}

impl INode for While {
	impl_node!("While");

	#[must_use]
	fn to_src(&self, depth: usize) -> String {
		format!("while {}", self.condition.to_src(depth))
	}
}

impl std::fmt::Display for While {
	fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
		write!(f, "While(while {} {{ {} }})", self.condition, self.body)
	}
}

impl From<parser::nodes::WhileLoop> for While {
	fn from(node: parser::nodes::WhileLoop) -> Self {
		Self {
			span: node.span,
			condition: Box::new(node.condition.into()),
			body: node.body.into(),
			scope: None,
		}
	}
}

impl GetType for While {
	fn get_type(&self, meta: &Meta, ctx: &Context) -> Result<Option<ClassId>, Error> {
		self.body.get_type(meta, ctx)
	}
}

impl Validate for While {
	fn pass1(&mut self, meta: &Meta, ctx: &mut Context) -> Result<(), Error> {
		let Self {
			scope,
			condition,
			body,
			..
		} = self;

		*scope = Some(ctx.current_scope_expect());

		*body = std::mem::take(body).replace_free_with_extern_call_nodes(ctx);

		condition.set_access(ctx, Access::Copy)?;
		condition.validate(meta, ctx)?;

		body.validate(meta, ctx)
	}

	fn check(&self, meta: &Meta, ctx: &Context) -> Result<(), Error> {
		let Self { condition, .. } = self;

		let ty = condition.get_type(meta, ctx)?.ok_or_else(|| {
			Error::loop_condition_wrong_type(&condition.span())
				.expected(bool::class_id())
				.build()
		})?;
		if ty != ClassId(bool::type_name()) {
			return Err(Error::loop_condition_wrong_type(&condition.span())
				.expected(bool::class_id())
				.got(ty)
				.build());
		}
		Ok(())
	}
}

impl Walkable for While {
	fn walk(&mut self, f: &mut Callback) -> Result<(), Error> {
		f(self)?;
		self.condition.walk(f)?;
		self.body.walk(f)
	}
}
