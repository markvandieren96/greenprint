use std::any::Any;

pub enum Ptr {
	Owned(Box<dyn Any>),
	Ref(*const ()),
	RefMut(*mut ()),
}

#[derive(Debug, Clone, Copy)]
pub struct ConstViolation;

impl Ptr {
	pub fn new<T: 'static>(t: T) -> Self {
		let b = Box::new(t);
		Self::Owned(b)
	}

	pub fn ptr(&self) -> *const () {
		match self {
			Ptr::Owned(b) => {
				let b: &Box<dyn Any> = b;
				let b: &dyn Any = &**b;
				let b: *const dyn Any = b;
				b.cast::<()>()
			}
			Ptr::RefMut(ptr) => *ptr,
			Ptr::Ref(ptr) => *ptr,
		}
	}

	pub fn ptr_mut(&mut self) -> Result<*mut (), ConstViolation> {
		match self {
			Ptr::Owned(b) => {
				let b: &mut Box<dyn Any> = b;
				let b: &mut dyn Any = &mut **b;
				let b: *mut dyn Any = b;
				Ok(b.cast::<()>())
			}
			Ptr::RefMut(ptr) => Ok(*ptr),
			Ptr::Ref(_) => Err(ConstViolation),
		}
	}

	pub fn ptr_as<T>(&self) -> *const T {
		self.ptr().cast::<T>()
	}

	pub fn ptr_mut_as<T>(&mut self) -> Result<*mut T, ConstViolation> {
		Ok((self.ptr_mut()?).cast::<T>())
	}

	pub fn get_ref(&self) -> Self {
		Self::Ref(self.ptr())
	}

	pub fn get_ref_mut(&mut self) -> Result<Self, ConstViolation> {
		Ok(Self::RefMut(self.ptr_mut()?))
	}
}
