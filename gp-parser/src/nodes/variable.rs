use crate::ParseResult;
use lexer::{Lexer, Span, Token};

#[derive(Debug, PartialEq, Clone)]
pub struct Variable {
	pub span: Span,
	pub name: String,
}

impl Variable {
	#[must_use]
	pub fn new(span: Span, name: String) -> Self {
		Self { span, name }
	}

	pub fn try_parse(lexer: &mut Lexer) -> ParseResult<Self> {
		match lexer.peek() {
			Some((Token::Identifier(name), span)) => {
				lexer.next();
				Ok(Some(Self { span, name }))
			}
			_ => Ok(None),
		}
	}
}
