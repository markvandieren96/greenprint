use crate::Error;
use lexer::{Lexer, LexerExt, Span, Token};

pub trait LexerExpectExt {
	fn expect(&mut self, token: Token, ctx: &str) -> Result<Span, Error>;
	fn expect_identifier(&mut self, ctx: &str) -> Result<(String, Span), Error>;
}

impl<'a> LexerExpectExt for Lexer<'a> {
	fn expect(&mut self, token: Token, ctx: &str) -> Result<Span, Error> {
		if self.peek_match(&token) {
			Ok(self.next().unwrap().1)
		} else {
			Err(Error::expected(self, token, ctx))
		}
	}

	fn expect_identifier(&mut self, ctx: &str) -> Result<(String, Span), Error> {
		if let Some((Token::Identifier(ident), span)) = self.peek() {
			let _token = self.next();
			Ok((ident, span))
		} else {
			Err(Error::expected_identifier(self, ctx))
		}
	}
}
