use crate::{expression, nodes::Block, Error, Errors, Expression, ParseResult, Span};
use lexer::{merge_spans, Lexer, LexerExt, Token};

/// Parses "if expr { ... } else if expr2 { ... } else { ... }"
#[derive(Debug, PartialEq, Clone)]
pub struct Branch {
	pub span: Span,
	pub condition: Box<Expression>,
	pub then_branch: Block,
	pub else_branch: Option<Box<Expression>>,
}

impl Branch {
	pub fn try_parse(lexer: &mut Lexer, errors: &mut Errors) -> ParseResult<Self> {
		let span_start = if lexer.peek_match(&Token::If) {
			lexer.next().unwrap().1
		} else {
			return Ok(None);
		};

		let condition = expression(lexer, errors);

		let then_branch = Block::try_parse(lexer, errors)?
			.ok_or_else(|| Error::expected(lexer, Token::BraceOpen, "Branch then"))?;

		let mut span_end = then_branch.span.clone();
		let else_branch = if lexer.peek_match(&Token::Else) {
			lexer.next();

			if let Some(else_if) = Branch::try_parse(lexer, errors)? {
				span_end = else_if.span.clone();
				Some(Box::new(Expression::Branch(else_if)))
			} else if let Some(block) = Block::try_parse(lexer, errors)? {
				span_end = block.span.clone();
				Some(Box::new(Expression::Block(block)))
			} else {
				return Err(Error::expected(lexer, Token::BraceOpen, "Branch else"));
			}
		} else {
			None
		};

		Ok(Some(Self {
			span: merge_spans(&span_start, &span_end),
			condition: Box::new(condition),
			then_branch,
			else_branch,
		}))
	}
}
