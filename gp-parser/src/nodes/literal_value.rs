use crate::{Expression, Literal, ParseResult};
use lexer::{Lexer, Token};

#[derive(Debug, PartialEq, Clone)]
pub struct LiteralValue {
	pub span: lexer::Span,
	pub value: Literal,
}

impl LiteralValue {
	pub fn try_parse(lexer: &mut Lexer) -> ParseResult<Expression> {
		match lexer.peek() {
			Some((Token::Bool(val), span)) => {
				lexer.next();
				Ok(Some(Expression::Literal(Self {
					span,
					value: Literal::Bool(val),
				})))
			}
			Some((Token::Integer(val), span)) => {
				lexer.next();
				Ok(Some(Expression::Literal(Self {
					span,
					value: Literal::Integer(val),
				})))
			}
			Some((Token::Float(val), span)) => {
				lexer.next();
				Ok(Some(Expression::Literal(Self {
					span,
					value: Literal::Float(val),
				})))
			}
			Some((Token::Literal(val), span)) => {
				lexer.next();
				Ok(Some(Expression::Literal(Self {
					span,
					value: Literal::String(val),
				})))
			}
			_ => Ok(None),
		}
	}

	#[must_use]
	pub fn to_src(&self) -> String {
		format!("{}", self.value)
	}
}
