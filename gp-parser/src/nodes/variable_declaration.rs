use crate::{expression, nodes::TypeAnnotation, Errors, Expression, LexerExpectExt, ParseResult};
use lexer::{merge_spans, Lexer, LexerExt, Span, Token};

/// Parses "let mut foo: T = ...;"
#[derive(Debug, PartialEq, Clone)]
pub struct VariableDeclaration {
	pub span: Span,
	pub is_extern: bool,
	pub is_mut: bool,
	pub name: String,
	pub ty: Option<TypeAnnotation>,
	pub initializer: Option<Expression>,
}

impl VariableDeclaration {
	pub fn try_parse(lexer: &mut Lexer, errors: &mut Errors) -> ParseResult<Self> {
		let span_start = if lexer.peek_match(&Token::Let) {
			lexer.next().unwrap().1
		} else {
			return Ok(None);
		};

		let is_extern = lexer.matches(&Token::Extern);
		let is_mut = lexer.matches(&Token::Mut);
		let (name, _) = lexer.expect_identifier("VariableDeclaration")?;
		let ty = TypeAnnotation::try_parse(lexer)?;

		let mut initializer = None;
		if lexer.matches(&Token::Assignment) {
			initializer = Some(expression(lexer, errors));
		}

		let span_end = lexer.expect(Token::Semicolon, "VariableDeclaration")?;

		Ok(Some(Self {
			span: merge_spans(&span_start, &span_end),
			is_extern,
			is_mut,
			name,
			ty,
			initializer,
		}))
	}
}
