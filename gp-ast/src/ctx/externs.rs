use log::trace;
use reflect::meta::ClassId;
use std::collections::HashMap;

use crate::{ExternFn, ExternFunction, Identifier};

#[derive(Debug, Default, Clone)]
pub struct Externs {
	functions: HashMap<String, Vec<ExternFunction>>,
}

impl Externs {
	pub fn define_function(
		&mut self,
		name: String,
		parameters: Vec<(Identifier, ClassId)>,
		return_type: Option<ClassId>,
		body: ExternFn,
	) {
		if let Some(return_type) = &return_type {
			trace!("define extern fn {} -> {}", name, return_type);
		} else {
			trace!("define extern fn {}", name);
		}

		self.functions
			.entry(name.clone())
			.or_default()
			.push(ExternFunction {
				name,
				parameters,
				return_type,
				body,
			});
	}

	#[must_use]
	pub fn find_function(&self, name: &str, args: &[ClassId]) -> Option<(usize, &ExternFunction)> {
		self.functions.get(name).and_then(|v| {
			v.iter().enumerate().find(|(_, function)| {
				function.parameters.len() == args.len()
					&& function
						.parameters
						.iter()
						.map(|(_, ty)| ty)
						.zip(args.iter())
						.all(|(a, b)| a == b)
			})
		})
	}

	#[must_use]
	pub fn has_function(&self, name: &str) -> bool {
		self.functions.get(name).is_some()
	}

	pub fn get_return_type(&self, name: &str) -> Result<Option<ClassId>, FunctionNotFound> {
		match self.functions.get(name) {
			Some(v) => Ok(v.first().unwrap().return_type.clone()),
			None => Err(FunctionNotFound),
		}
	}

	#[must_use]
	pub fn get_function(&self, name: &str, id: usize) -> Option<&ExternFunction> {
		self.functions.get(name).and_then(|v| v.get(id))
	}
}

#[derive(Debug)]
pub struct FunctionNotFound;
