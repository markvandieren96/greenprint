#![warn(clippy::all)]
#![warn(clippy::pedantic)]
#![allow(clippy::module_name_repetitions)]
#![allow(clippy::missing_panics_doc)]
#![allow(clippy::missing_errors_doc)]

mod error;
mod meta;
mod ptr;
mod value;

pub use error::*;
pub use meta::*;
pub use value::*;

pub use gp2_macro;
pub use stid;

#[cfg(feature = "specs_types")]
mod specs_types;
#[cfg(feature = "specs_types")]
pub use specs_types::*;
