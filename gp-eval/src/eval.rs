use super::{Context, Error, State};
use ast::{
	nodes::{
		Assignment, Block, Branch, For, FunctionDeclaration, GetField, Group, Literal, Variable,
		VariableDeclaration, While,
	},
	parser, reflect, Access, Node,
};
use log::trace;
use reflect::{specs::World, Object, CLASSES};

pub trait Eval {
	fn eval(
		&self,
		world: &World,
		ctx: &Context,
		state: &mut State,
		depth: usize,
	) -> Result<Option<Object>, Error>;
}

impl Eval for Block {
	fn eval(
		&self,
		world: &World,
		ctx: &Context,
		state: &mut State,
		depth: usize,
	) -> Result<Option<Object>, Error> {
		let Self {
			span,
			body,
			drops,
			catch_returns,
			..
		} = self;

		let mut ret = Ok(None);
		for node in body {
			if *catch_returns {
				ret = node.eval(world, ctx, state, depth + 1);
				match ret {
					Err(Error::Return(value)) => {
						ret = Ok(value);
						break;
					}
					Err(_) => break,
					Ok(..) => (),
				}
			} else {
				ret = Ok(node.eval(world, ctx, state, depth + 1)?);
			}
		}

		for drop in drops {
			state
				.get_mut(*drop)
				.take()
				.call_owned("op_drop", world, &mut [])
				.map_err(|e| Error::Reflect(e, span.clone()))?;
		}

		ret
	}
}

impl Eval for Node {
	fn eval(
		&self,
		world: &World,
		ctx: &Context,
		state: &mut State,
		depth: usize,
	) -> Result<Option<Object>, Error> {
		trace!("{}[node] {}", "    ".repeat(depth), self);

		let ret = match self {
			Self::Group(Group { body, .. }) => body.eval(world, ctx, state, depth + 1),
			Self::Assignment(Assignment {
				resolved_id, value, ..
			}) => {
				let id = resolved_id.unwrap();
				*state.get_mut(id) = value
					.eval(world, ctx, state, depth + 1)?
					.expect("Assignment value did not evaluate to a value");
				Ok(None)
			}
			Self::Literal(Literal { value, .. }) => Ok(Some(convert_literal(value.clone()))),
			Self::Variable(Variable {
				span,
				name,
				resolved_id,
				access,
				..
			}) => {
				if access.is_none() {
					println!(
						"Variable({name}) (span {span:?}) did not have an access mode"
					);
					panic!("Missing variable access mode");
				}
				match access.unwrap() {
					Access::Copy => Ok(Some(state.copy(resolved_id.unwrap(), world))),
					Access::Move => Ok(Some(state.take(resolved_id.unwrap()))),
					Access::Ref => Ok(Some(state.get(resolved_id.unwrap()).create_ref())),
					Access::RefMut => Ok(Some(
						state
							.get_mut(resolved_id.unwrap())
							.create_ref_mut()
							.map_err(|e| Error::Reflect(e, span.clone()))?,
					)),
				}
			}
			Self::Call(ast::nodes::Call {
				name,
				span,
				arguments,
				call_type,
				..
			}) => match call_type {
				ast::nodes::CallType::Free {
					inlined_body,
					argument_ids,
					..
				} => {
					for (id, argument) in argument_ids.iter().zip(arguments.iter()) {
						*state.get_mut(*id) = argument
							.eval(world, ctx, state, depth + 1)?
							.expect("Function argument did not have a value");
					}

					inlined_body
						.as_ref()
						.unwrap()
						.eval(world, ctx, state, depth + 1)
				}
				ast::nodes::CallType::Method { object, self_type } => {
					let mut object = object.eval(world, ctx, state, depth + 1)?.unwrap();
					let mut arguments = arguments
						.iter()
						.map(|arg| {
							arg.eval(world, ctx, state, depth + 1)
								.map(std::option::Option::unwrap)
						})
						.collect::<Result<Vec<Object>, Error>>()?;
					match self_type {
						Access::Copy | Access::Move => object
							.call_owned(name, world, &mut arguments)
							.map_err(|e| Error::Reflect(e, span.clone())),
						Access::Ref => object
							.call(name, world, &mut arguments)
							.map_err(|e| Error::Reflect(e, span.clone())),
						Access::RefMut => object
							.call_mut(name, world, &mut arguments)
							.map_err(|e| Error::Reflect(e, span.clone())),
					}
				}
				ast::nodes::CallType::Extern { id } => {
					let mut arguments = arguments
						.iter()
						.map(|arg| {
							arg.eval(world, ctx, state, depth + 1)
								.map(std::option::Option::unwrap)
						})
						.collect::<Result<Vec<Object>, Error>>()?;
					let func = ctx
						.extern_functions
						.get_function(name, id.expect("missing ExternCall id"))
						.unwrap();
					let res = (func.body)(world, &mut arguments);
					Ok(res)
				}
				ast::nodes::CallType::Static { class } => {
					let class = CLASSES.find(&class.0).expect("Class was not registered");
					let mut arguments = arguments
						.iter()
						.map(|arg| {
							arg.eval(world, ctx, state, depth + 1)
								.map(std::option::Option::unwrap)
						})
						.collect::<Result<Vec<Object>, Error>>()?;
					class
						.call(name, world, &mut arguments)
						.map_err(|e| Error::Reflect(e, span.clone()))
				}
			},
			Self::Block(block) => block.eval(world, ctx, state, depth),
			Self::Branch(Branch {
				condition,
				then_branch,
				else_branch,
				..
			}) => {
				let ret = *condition
					.eval(world, ctx, state, depth + 1)?
					.expect("Branch condition did not evaluate to a value")
					.downcast_ref::<bool>()
					.expect("Branch condition did not evaluate to a bool");

				if ret {
					then_branch.eval(world, ctx, state, depth + 1)
				} else {
					else_branch.as_ref().map_or(Ok(None), |else_branch| {
						else_branch.eval(world, ctx, state, depth + 1)
					})
				}
			}
			Self::ExpressionStatement(ast::nodes::ExpressionStatement {
				body,
				has_semicolon,
				..
			}) => {
				let ret = body.eval(world, ctx, state, depth + 1)?;
				if *has_semicolon {
					Ok(None)
				} else {
					Ok(ret)
				}
			}
			Self::VariableDeclaration(VariableDeclaration {
				resolved_id,
				initializer,
				..
			}) => {
				if let Some(initializer) = initializer {
					let value = initializer.eval(world, ctx, state, depth + 1)?.unwrap();
					*state.get_mut(resolved_id.unwrap()) = value;
				}
				Ok(None)
			}
			Self::While(While {
				condition, body, ..
			}) => {
				let mut ret = None;
				while *condition
					.eval(world, ctx, state, depth + 1)?
					.expect("While condition did not evaluate to a value")
					.downcast_ref::<bool>()
					.expect("While condition did not evaluate to a bool")
				{
					ret = body.eval(world, ctx, state, depth + 1)?;
				}

				Ok(ret)
			}
			Self::For(For {
				span,
				resolved_id,
				range_begin,
				range_end,
				range_inclusive,
				body,
				drops,
				..
			}) => {
				let start = *range_begin
					.eval(world, ctx, state, depth + 1)?
					.expect("For range_begin no return value")
					.downcast_ref::<i64>()
					.expect("Downcast failed");
				let end = *range_end
					.eval(world, ctx, state, depth + 1)?
					.expect("For range_end no return value")
					.downcast_ref::<i64>()
					.expect("Downcast failed");
				let mut ret = None;
				if *range_inclusive {
					for i in start..=end {
						let var = state.get_mut(resolved_id.expect("For missing resolved name"));
						*var = Object::new(true, i);
						ret = body.eval(world, ctx, state, depth + 1)?;
					}
				} else {
					for i in start..end {
						let var = state.get_mut(resolved_id.expect("For missing resolved name"));
						*var = Object::new(true, i);
						ret = body.eval(world, ctx, state, depth + 1)?;
					}
				};

				for drop in drops {
					state
						.get_mut(*drop)
						.take()
						.call_owned("op_drop", world, &mut [])
						.map_err(|e| Error::Reflect(e, span.clone()))?;
				}

				Ok(ret)
			}
			Self::FunctionDeclaration(FunctionDeclaration { .. }) => Ok(None),
			/*Self::ReturnStatement(ReturnStatement { value, .. }) => {
				let val = if let Some(val) = value { val.eval(world, ctx, state, depth + 1)? } else { None };
				Err(Error::Return(val))
			}*/
			Self::GetField(GetField {
				span,
				object,
				name,
				access,
				..
			}) => {
				let mut obj = object
					.eval(world, ctx, state, depth + 1)?
					.expect("GetField was called on None");
				match access.expect("GetField did not have an access mode") {
					Access::Copy => obj
						.field(name)
						.map(|obj| Some(obj.copy(world)))
						.map_err(|e| Error::Reflect(e, span.clone())),
					Access::Move => obj
						.field_mut(name)
						.map(|mut obj| Some(obj.take()))
						.map_err(|e| Error::Reflect(e, span.clone())),
					Access::Ref => obj
						.field(name)
						.map(Some)
						.map_err(|e| Error::Reflect(e, span.clone())),
					Access::RefMut => obj
						.field_mut(name)
						.map(Some)
						.map_err(|e| Error::Reflect(e, span.clone())),
				}
			}
		};

		ret
	}
}

fn convert_literal(literal: parser::Literal) -> Object {
	match literal {
		parser::Literal::Bool(val) => Object::new(true, val),
		parser::Literal::Integer(val) => Object::new(true, val),
		parser::Literal::Float(val) => Object::new(true, val),
		parser::Literal::String(val) => Object::new(true, val),
	}
}
