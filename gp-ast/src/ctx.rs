mod declarations;
mod externs;
mod function_id;
mod scope_id;
mod variable_id;
mod variable_states;

pub use declarations::Declarations;
pub use externs::Externs;
pub use function_id::FunctionId;
pub use scope_id::ScopeId;
pub use variable_id::VariableId;
pub use variable_states::{State, VariableStates};

use crate::{
	nodes::Block, Error, ExternFn, ExternFunction, Function, Identifier, Parameter, Scope,
	Validate, Variable,
};
use log::trace;
use parser::{nodes::ParameterList, Span};
use reflect::{
	meta::{ClassId, Meta},
	Object,
};

#[derive(Debug, Default)]
pub struct Context {
	pub declarations: Declarations,
	stack: Vec<ScopeId>,
	pub externs: Externs,
	pub variable_states: VariableStates,
}

impl Context {
	#[must_use]
	pub fn new(externs: Externs) -> Self {
		Self {
			declarations: Declarations::default(),
			stack: Vec::new(),
			externs,
			variable_states: VariableStates::default(),
		}
	}

	pub(crate) fn current_scope_expect(&self) -> ScopeId {
		*self.stack.last().expect("No current scope")
	}

	pub(crate) fn current_scope(&self) -> Option<ScopeId> {
		self.stack.last().copied()
	}

	pub(crate) fn scope<F>(
		&mut self,
		meta: &Meta,
		span: &Span,
		f: F,
	) -> Result<Vec<VariableId>, Error>
	where
		F: FnOnce(&mut Self) -> Result<(), Error>,
	{
		let scope_id = self.create_and_push_scope();
		f(self)?;
		let drops = self.collect_current_scope_drops(scope_id, meta, span)?;
		self.stack.pop();
		Ok(drops)
	}

	fn create_and_push_scope(&mut self) -> ScopeId {
		let scope = Scope::default();
		let scope_id = ScopeId(self.declarations.scopes.len());
		if let Some(current_scope) = self.current_scope() {
			self.declarations.scopes[current_scope.0]
				.children
				.push(scope_id);
		}
		self.stack.push(scope_id);
		self.declarations.scopes.push(scope);
		scope_id
	}

	fn collect_current_scope_drops(
		&mut self,
		scope_id: ScopeId,
		meta: &Meta,
		span: &Span,
	) -> Result<Vec<VariableId>, Error> {
		let mut drops = Vec::new();
		for (index, var) in self.declarations.scopes[scope_id.0]
			.variables
			.iter()
			.enumerate()
		{
			if let Some(ty) = &var.ty {
				if meta
					.get(ty)
					.map_err(|error| Error::reflect(span, error))?
					.has_method_owned("op_drop")
				{
					drops.push(VariableId(scope_id, index));
				}
			}
		}
		Ok(drops)
	}

	pub(crate) fn declare_variable(
		&mut self,
		name: String,
		is_extern: bool,
		is_mut: bool,
		ty: Option<ClassId>,
	) -> VariableId {
		trace!("declare {}{}", if is_mut { "mut " } else { "" }, name);

		let scope_id = self.current_scope_expect();
		let variable_id = VariableId(
			scope_id,
			self.declarations.scopes[scope_id.0].variables.len(),
		);
		self.declarations.scopes[scope_id.0]
			.variables
			.push(Variable {
				name,
				is_extern,
				is_mut,
				ty,
				id: variable_id,
			});
		variable_id
	}

	pub(crate) fn assign_variable(&mut self, name: &str, ty: ClassId) {
		trace!("assign var {}: {}", name, ty);

		for scope_id in self.stack.iter().rev().copied() {
			if let Some(var) = self.declarations.scopes[scope_id.0]
				.variables
				.iter_mut()
				.rev()
				.find(|var| var.name == name)
			{
				var.ty = Some(ty);
				return;
			}
		}

		println!("define_variable({name}) could not find the variable");
		panic!("define_variable could not find the variable");
	}

	pub(crate) fn define_function(
		&mut self,
		meta: &Meta,
		span: &Span,
		name: String,
		parameters: &ParameterList,
		return_type: Option<ClassId>,
		mut body: Block,
	) -> Result<(), Error> {
		if let Some(return_type) = &return_type {
			trace!("define fn {} -> {}", name, return_type);
		} else {
			trace!("define fn {}", name);
		}

		body = body.replace_free_with_extern_call_nodes(self);

		let scope_id = self.current_scope_expect();
		let mut extern_call_scope = None;
		let dropped_variables = self.scope(meta, span, |ctx| {
			extern_call_scope = Some(ctx.current_scope_expect());

			for parameter in &parameters.parameters {
				let ty_id = ClassId(parameter.ty.ty.clone());
				let var_id =
					ctx.declare_variable(parameter.name.clone(), false, true, Some(ty_id.clone()));
				ctx.assign_variable(&parameter.name, ty_id);
				ctx.variable_states.set(var_id, State::Initialized);
			}

			body.validate(meta, ctx)?;
			Ok(())
		})?;

		body.drops.extend(dropped_variables);

		let function_id = FunctionId(
			scope_id,
			self.declarations.scopes[scope_id.0].functions.len(),
		);
		let parameters: Result<Vec<Parameter>, reflect::Error> = parameters
			.parameters
			.iter()
			.map(|p| Parameter::from(p.clone(), meta))
			.collect();
		self.declarations.scopes[scope_id.0]
			.functions
			.push(Function {
				name,
				parameters: parameters.map_err(|e| Error::reflect(span, e))?,
				return_type,
				body,
				extern_call_scope: extern_call_scope.unwrap(),
				id: function_id,
			});
		Ok(())
	}

	#[cfg(test)]
	pub(crate) fn get_variable(&self, name: &str) -> Option<&Variable> {
		for scope_id in self.stack.iter().rev().copied() {
			if let Some(var) = self.declarations.scopes[scope_id.0]
				.variables
				.iter()
				.rev()
				.find(|var| var.name == name)
			{
				return Some(var);
			}
		}
		None
	}

	#[must_use]
	pub fn resolve_variable_raw(&self, name: &str) -> Option<VariableId> {
		for (scope_id, scope) in self.declarations.scopes.iter().enumerate() {
			if let Some((index, _)) = scope
				.variables
				.iter()
				.enumerate()
				.rev()
				.find(|(_, var)| var.name == name)
			{
				return Some(VariableId(ScopeId(scope_id), index));
			}
		}
		None
	}

	pub fn resolve_variable(
		&self,
		resolved_id: &mut Option<VariableId>,
		span: &Span,
		name: &Identifier,
	) -> Result<(), Error> {
		*resolved_id = Some(
			self.resolve_variable_scoped(name)
				.ok_or_else(|| Error::undefined_variable(span, name))?,
		);
		Ok(())
	}

	pub fn resolve_function(
		&self,
		resolved_id: &mut Option<FunctionId>,
		span: &Span,
		name: &Identifier,
	) -> Result<(), Error> {
		*resolved_id = Some(
			self.resolve_function_scoped(name)
				.ok_or_else(|| Error::undefined_function(span, name))?,
		);
		Ok(())
	}

	pub(crate) fn resolve_variable_scoped(&self, name: &str) -> Option<VariableId> {
		for scope_id in self.stack.iter().rev().copied() {
			if let Some((index, _)) = self.declarations.scopes[scope_id.0]
				.variables
				.iter()
				.enumerate()
				.rev()
				.find(|(_, var)| var.name == name)
			{
				return Some(VariableId(scope_id, index));
			}
		}
		None
	}

	#[must_use]
	pub fn get_function_scoped(&self, name: &str) -> Option<&Function> {
		self.resolve_function_scoped(name)
			.map(|id| &self.declarations.scopes[id.0 .0].functions[id.1])
	}

	#[must_use]
	pub fn resolve_function_scoped(&self, name: &str) -> Option<FunctionId> {
		for scope_id in self.stack.iter().rev().copied() {
			if let Some(function) = self.declarations.scopes[scope_id.0]
				.functions
				.iter()
				.rev()
				.find(|function| function.name == name)
			{
				return Some(function.id);
			}
		}
		None
	}

	#[must_use]
	pub fn get_function(&self, name: &str) -> Option<&Function> {
		for scope in &self.declarations.scopes {
			if let Some(function) = scope
				.functions
				.iter()
				.rev()
				.find(|function| function.name == name)
			{
				return Some(function);
			}
		}
		None
	}

	pub fn define_extern_function(
		&mut self,
		name: String,
		parameters: Vec<(Identifier, ClassId)>,
		return_type: Option<ClassId>,
		body: ExternFn,
	) {
		self.externs
			.define_function(name, parameters, return_type, body);
	}

	pub(crate) fn find_extern_function(
		&self,
		name: &str,
		args: &[ClassId],
	) -> Option<(usize, &ExternFunction)> {
		self.externs.find_function(name, args)
	}

	pub(crate) fn get_return_type(
		&self,
		name: &str,
	) -> Result<Option<ClassId>, externs::FunctionNotFound> {
		self.externs.get_return_type(name)
	}

	#[must_use]
	pub fn allocate_variables(&self) -> (Vec<Vec<String>>, Vec<Vec<Object>>) {
		(
			self.declarations
				.scopes
				.iter()
				.map(|scope| {
					scope
						.variables
						.iter()
						.map(|variable| (variable.name.clone()))
						.collect()
				})
				.collect(),
			self.declarations
				.scopes
				.iter()
				.map(|scope| {
					std::iter::repeat_with(|| Object::new(true, ()))
						.take(scope.variables.len())
						.collect()
				})
				.collect(),
		)
	}

	#[must_use]
	pub fn export_functions(&self) -> Vec<Vec<Block>> {
		self.declarations
			.scopes
			.iter()
			.map(|scope| {
				scope
					.functions
					.iter()
					.map(|function| function.body.clone())
					.collect()
			})
			.collect()
	}

	#[must_use]
	pub fn export_externs(&self) -> Externs {
		self.externs.clone()
	}
}

pub enum VariableAccessError {
	UndefinedVariable,
	Uninitialized,
	Untyped,
	UseAfterMove,
}
