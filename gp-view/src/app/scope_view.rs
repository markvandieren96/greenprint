use eframe::{egui, egui::Ui};
use program::{ast, ast::Context};

#[derive(Default)]
pub struct ScopeView {
	pub flat_view: bool,
}

impl ScopeView {
	pub fn display_window(&mut self, ctx: &egui::CtxRef, context: &Context, show: &mut bool) {
		egui::Window::new("scopes").open(show).show(ctx, |ui| {
			egui::ScrollArea::auto_sized().show(ui, |ui| {
				self.display(ui, context);
			});
		});
	}

	pub fn display(&mut self, ui: &mut Ui, context: &Context) {
		ui.checkbox(&mut self.flat_view, "flat view");
		ui.separator();

		if self.flat_view {
			view_scopes_raw(ui, context);
		} else {
			view_scopes(ui, context);
		}
	}
}

fn view_scopes(ui: &mut Ui, context: &ast::Context) {
	view_scope(ui, context, ast::ScopeId(0));
}

fn view_scope(ui: &mut Ui, context: &ast::Context, scope_id: ast::ScopeId) {
	let scope = &context.declarations.scopes[scope_id.0];
	ui.monospace(format!("scope {}", scope_id.0));
	ui.indent(format!("scope_{}", scope_id.0), |ui| {
		for var in &scope.variables {
			ui.monospace(format!("let {};", var));
		}

		for func in &scope.functions {
			ui.monospace(format!(
				"fn {} (ext_call_scope={:?})",
				func, func.extern_call_scope
			));
		}

		for scope_id in &scope.children {
			view_scope(ui, context, *scope_id);
		}
	});
}

fn view_scopes_raw(ui: &mut Ui, context: &ast::Context) {
	for (index, scope) in context.declarations.scopes.iter().enumerate() {
		ui.monospace(format!("scope {}", index));
		ui.indent(format!("raw_scope_{}", index), |ui| {
			for var in &scope.variables {
				ui.monospace(format!("let {};", var));
			}

			for func in &scope.functions {
				ui.monospace(format!("fn {}", func));
			}
		});
	}
}
