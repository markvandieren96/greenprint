use crate::{register_std, Function, FunctionSignature, FunctionSignatureLite};
use std::collections::HashMap;
use stid::{Id, Stid};

#[derive(Copy, Clone)]
pub struct FunctionId(usize);

#[derive(Default)]
pub struct Registry {
	pub typemap: TypeMap,
	pub functions: Vec<FunctionEntry>,
	pub static_functions: HashMap<Id, Vec<FunctionEntry>>,
}

pub struct FunctionEntry {
	pub signature: FunctionSignature,
	pub func: Function,
}

impl Registry {
	#[must_use]
	pub fn new() -> Self {
		let mut reg = Self::default();
		register_std(&mut reg);
		#[cfg(feature = "specs_types")]
		crate::register_specs(&mut reg);
		reg
	}

	pub fn register_type<T: Stid>(&mut self) {
		self.typemap.insert::<T>();
	}

	#[must_use]
	pub fn find_type(&self, name: &str) -> Option<Id> {
		self.typemap.find(name)
	}

	pub fn register_function(&mut self, (signature, func): (FunctionSignature, Function)) {
		for f in &self.functions {
			assert!(
				!f.signature.matches(&signature),
				"A function with signature \"{}\" is already registered",
				f.signature.display(&self.typemap)
			);
		}

		self.functions.push(FunctionEntry { signature, func });
	}

	pub fn register_static_function(
		&mut self,
		ty: Id,
		(signature, func): (FunctionSignature, Function),
	) {
		let v = self.static_functions.entry(ty).or_default();

		for f in v.iter() {
			assert!(
				!f.signature.matches(&signature),
				"A static function with signature \"{}\" is already registered",
				f.signature.display(&self.typemap)
			);
		}

		v.push(FunctionEntry { signature, func });
	}

	#[must_use]
	pub fn get_function(&self, signature: &FunctionSignature) -> Option<&Function> {
		for f in &self.functions {
			if f.signature.matches(signature) {
				return Some(&f.func);
			}
		}
		None
	}

	#[must_use]
	pub fn get_function_lite(
		&self,
		signature: &FunctionSignatureLite,
	) -> Option<(&FunctionSignature, &Function)> {
		for f in &self.functions {
			if f.signature.matches_lite(signature) {
				return Some((&f.signature, &f.func));
			}
		}
		None
	}

	#[must_use]
	pub fn get_function_by_name(&self, name: &str) -> Option<(&FunctionSignature, &Function)> {
		for f in &self.functions {
			if f.signature.name == name {
				return Some((&f.signature, &f.func));
			}
		}
		None
	}

	#[must_use]
	pub fn get_function_by_name_and_arg_type_ids(
		&self,
		name: &str,
		args: &[Id],
	) -> Option<(&FunctionSignature, &Function)> {
		for f in &self.functions {
			if f.signature.name == name
				&& f.signature.args.len() == args.len()
				&& f.signature
					.args
					.iter()
					.zip(args.iter())
					.all(|(a, b)| a.id() == *b)
			{
				return Some((&f.signature, &f.func));
			}
		}
		None
	}

	#[must_use]
	pub fn get_static_function_by_name_and_arg_type_ids(
		&self,
		class: Id,
		name: &str,
		args: &[Id],
	) -> Option<(&FunctionSignature, &Function)> {
		self.static_functions.get(&class).and_then(|v| {
			for f in v {
				if f.signature.name == name
					&& f.signature.args.len() == args.len()
					&& f.signature
						.args
						.iter()
						.zip(args.iter())
						.all(|(a, b)| a.id() == *b)
				{
					return Some((&f.signature, &f.func));
				}
			}
			None
		})
	}

	#[must_use]
	pub fn get_function_x(
		&self,
		class: Option<Id>,
		name: &str,
		args: &[Id],
	) -> Option<(&FunctionSignature, &Function)> {
		class.map_or_else(
			|| self.get_function_by_name_and_arg_type_ids(name, args),
			|class| self.get_static_function_by_name_and_arg_type_ids(class, name, args),
		)
	}

	#[must_use]
	pub fn get_static_function(&self, id: Id, signature: &FunctionSignature) -> Option<&Function> {
		self.static_functions.get(&id).and_then(|map| {
			for f in map {
				if f.signature.matches(signature) {
					return Some(&f.func);
				}
			}
			None
		})
	}

	pub fn debug_print(&self) {
		self.typemap.debug_print();

		println!("Functions:");
		for f in &self.functions {
			println!("    {}", f.signature.display(&self.typemap));
		}

		println!("Static functions:");
		for (id, v) in &self.static_functions {
			let name = self.typemap.get(*id).unwrap_or("<UNKNOWN TYPE>");
			for f in v {
				println!("    {}::{}", name, f.signature.display(&self.typemap));
			}
		}
	}
}

#[derive(Default)]
pub struct TypeMap {
	pub types: HashMap<Id, &'static str>,
}

impl TypeMap {
	pub fn insert<T: Stid>(&mut self) {
		self.types.insert(T::ID, T::NAME);
	}

	#[must_use]
	pub fn find(&self, name: &str) -> Option<Id> {
		self.types
			.iter()
			.find(|(_k, v)| **v == name)
			.map(|(k, _v)| *k)
	}

	#[must_use]
	pub fn get(&self, id: Id) -> Option<&str> {
		self.types.get(&id).copied()
	}

	pub fn debug_print(&self) {
		println!("Types:");
		let mut types: Vec<_> = self.types.iter().collect();
		types.sort_by_key(|(_k, v)| v.to_lowercase());
		for (id, name) in &types {
			println!("    {name}:\t{id}");
		}
	}
}
