#[derive(Debug)]
pub enum Error {
	ArgumentHasNoType,
	FunctionNotFound,
	BlockDidNotReturnAValue,
	ExpressionWithoutBody,
	Reflect(gp2::Error),
	VariableNotFound,
	VariableHasNoValue,
	MissingVariableAccess,
	BranchHasNoCondition,
	BranchConditionHasNoValue,
	BranchHasNoBody,
	FunctionDeclarationHasNoBody,
	TypeIsNotRegistered,
}

impl From<gp2::Error> for Error {
	fn from(err: gp2::Error) -> Self {
		Self::Reflect(err)
	}
}
