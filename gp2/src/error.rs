use crate::{ptr::ConstViolation, TypeMismatch};

#[derive(Debug)]
pub enum Error {
	ConstViolation,
	TypeMismatch(TypeMismatch),
}

impl Error {
	#[must_use]
	pub fn is_const_violation(&self) -> bool {
		matches!(self, Error::ConstViolation)
	}

	#[must_use]
	pub fn is_type_mismatch(&self) -> bool {
		matches!(self, Error::TypeMismatch { .. })
	}
}

impl From<ConstViolation> for Error {
	fn from(_: ConstViolation) -> Self {
		Error::ConstViolation
	}
}

impl From<TypeMismatch> for Error {
	fn from(e: TypeMismatch) -> Self {
		Error::TypeMismatch(e)
	}
}
