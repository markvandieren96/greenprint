use crate::{expression, Errors, Expression, LexerExpectExt, ParseResult};
use lexer::{merge_spans, Lexer, LexerExt, Span, Token};

/// Parses "(expr, expr2, ...)"
#[derive(Debug, PartialEq, Clone)]
pub struct ArgumentList {
	pub span: Span,
	pub arguments: Vec<Expression>,
}

impl ArgumentList {
	pub fn try_parse(lexer: &mut Lexer, errors: &mut Errors) -> ParseResult<Self> {
		let span_start = if lexer.peek_match(&Token::ParenthesisOpen) {
			lexer.next().unwrap().1
		} else {
			return Ok(None);
		};
		let mut arguments = Vec::new();
		if !lexer.peek_match(&Token::ParenthesisClose) {
			loop {
				arguments.push(expression(lexer, errors));
				if !lexer.matches(&Token::Comma) {
					break;
				}
			}
		}

		let span_end = lexer.expect(Token::ParenthesisClose, "ArgumentList")?;

		Ok(Some(Self {
			span: merge_spans(&span_start, &span_end),
			arguments,
		}))
	}
}
