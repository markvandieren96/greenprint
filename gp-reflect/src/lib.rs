#![warn(clippy::all)]
#![warn(clippy::pedantic)]
#![allow(clippy::missing_errors_doc)]
#![allow(clippy::missing_panics_doc)]

mod access;
mod class;
mod class_registry;
mod container;
mod error;
mod field;
#[allow(clippy::module_name_repetitions)]
mod func;
mod get_class;
mod impls;
mod impls_glam;
pub mod meta;
mod object;
mod reflect_macro;
mod register;
mod threadsafe_class_registry;
mod ty;
mod type_name;
mod value;

pub use access::*;
pub use class::*;
pub use class_registry::*;
pub use container::*;
pub use error::*;
pub use field::*;
pub use func::*;
pub use get_class::*;
pub(crate) use impls::register_std_types;
pub use object::*;

pub use register::*;
pub use threadsafe_class_registry::*;
pub use ty::*;
pub use type_name::*;


pub use specs;

pub use gp_reflect_derive::reflect as block;

#[cfg(test)]
mod tests;
