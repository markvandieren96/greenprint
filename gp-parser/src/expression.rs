use crate::{
	nodes::{
		BinaryOp, Block, Branch, FreeFunctionCall, GetField, Group, LiteralValue, MethodCall,
		StaticFunctionCall, UnaryOp, Variable,
	},
	Error, Span, SpannedErrorMessage,
};

#[derive(Debug, PartialEq, Clone)]
pub enum Expression {
	Group(Group),
	Literal(LiteralValue),
	Variable(Variable),
	UnaryOp(UnaryOp),
	BinaryOp(BinaryOp),
	FreeFunctionCall(FreeFunctionCall),
	StaticFunctionCall(StaticFunctionCall),
	MethodCall(MethodCall),
	Block(Block),
	Branch(Branch),
	GetField(GetField),
	Error(Error),
}

impl Expression {
	#[must_use]
	pub fn span(&self) -> Span {
		match self {
			Expression::Group(val) => val.span.clone(),
			Expression::Literal(val) => val.span.clone(),
			Expression::Variable(val) => val.span.clone(),
			Expression::UnaryOp(val) => val.span.clone(),
			Expression::BinaryOp(val) => val.span.clone(),
			Expression::FreeFunctionCall(val) => val.span.clone(),
			Expression::StaticFunctionCall(val) => val.span.clone(),
			Expression::MethodCall(val) => val.span.clone(),
			Expression::Block(val) => val.span.clone(),
			Expression::Branch(val) => val.span.clone(),
			Expression::GetField(val) => val.span.clone(),
			Expression::Error(val) => val.span().unwrap_or(0..0),
		}
	}
}
