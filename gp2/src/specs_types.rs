use crate::{Function, FunctionSignature, Registry, Type, Value};
use gp2_macro::function;
use specs::prelude::*;

pub fn register_specs(reg: &mut Registry) {
	reg.register_type::<specs::Entity>();
	reg.register_type::<specs::World>();

	reg.register_function(function!(
		fn create_entity(world: &World) -> Entity {
			world.entities().create()
		}
	));

	reg.register_function(function!(
		fn delete_entity(world: &World, entity: &Entity) {
			world.entities().delete(*entity).unwrap();
		}
	));
}

#[cfg(test)]
mod tests {
	use super::*;
	use gp2_macro::signature;

	#[test]
	fn entity_value() {
		let mut world = World::new();
		let entity = world.create_entity().build();
		let val = Value::new(entity);
		assert_eq!(entity, val.downcast_copy::<Entity>().unwrap());
	}

	#[test]
	fn create_entity() {
		let reg = Registry::new();

		let world = World::new();

		let meta = signature!(create_entity(&World) -> Entity);
		let f = reg.get_function(&meta).unwrap();

		f.0(&mut [Value::new_ref(&world)])
			.unwrap()
			.unwrap()
			.downcast_copy::<Entity>()
			.unwrap();
	}

	#[test]
	fn delete_entity() {
		let reg = Registry::new();

		let mut world = World::new();
		let e = world.create_entity().build();

		let meta = signature!(delete_entity(&World, &Entity));

		let f = reg.get_function(&meta).unwrap();

		assert!(f.0(&mut [Value::new_ref(&world), Value::new(e)])
			.unwrap()
			.is_none());
	}
}
