#![warn(clippy::all)]
#![warn(clippy::pedantic)]
#![allow(clippy::missing_errors_doc)]
#![allow(clippy::missing_panics_doc)]

mod enums {
	mod binary_operator;
	mod literal;
	mod unary_operator;

	pub use binary_operator::*;
	pub use literal::*;
	pub use unary_operator::*;
}
pub use enums::*;

pub mod nodes {
	mod argument_list;
	mod assignment;
	mod binary_op;
	mod block;
	mod branch;
	mod expression_statement;
	mod for_loop;
	mod free_function_call;
	mod function_declaration;
	mod get_field;
	mod group;
	mod literal_value;
	mod method_call;
	mod parameter;
	mod parameter_list;
	mod return_statement;
	mod return_type;
	mod set_field;
	mod static_function_call;
	mod type_annotation;
	mod unary_op;
	mod variable;
	mod variable_declaration;
	mod while_loop;

	pub use argument_list::*;
	pub use assignment::*;
	pub use binary_op::*;
	pub use block::*;
	pub use branch::*;
	pub use expression_statement::*;
	pub use for_loop::*;
	pub use free_function_call::*;
	pub use function_declaration::*;
	pub use get_field::*;
	pub use group::*;
	pub use literal_value::*;
	pub use method_call::*;
	pub use parameter::*;
	pub use parameter_list::*;
	
	pub use return_type::*;
	pub use set_field::*;
	pub use static_function_call::*;
	pub use type_annotation::*;
	pub use unary_op::*;
	pub use variable::*;
	pub use variable_declaration::*;
	pub use while_loop::*;
}

mod error;
mod expression;
mod lexer_expect_ext;
mod parser;
mod statement;

pub use error::*;
pub use expression::*;
pub use lexer_expect_ext::*;
pub use parser::*;
pub use statement::*;

pub use lexer;
pub use lexer::Span;

#[cfg(test)]
mod tests;
