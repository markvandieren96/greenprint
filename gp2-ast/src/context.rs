use crate::{
	convert::{AstNode, FunctionDeclaration},
	Error,
};
use gp2::{FunctionSignatureLite, Registry, Type, Value};

pub struct Var {
	pub name: String,
	pub value: Option<Value>,
}

#[derive(Clone)]
pub struct Func {
	pub declaration: FunctionDeclaration,
	pub body: AstNode,
}

#[derive(Default)]
pub struct Context {
	pub variables: Vec<Var>,
	pub functions: Vec<Func>,
}

impl Context {
	#[must_use]
	pub fn new() -> Self {
		Self::default()
	}

	pub fn declare_var(&mut self, var: Var) {
		self.variables.push(var);
	}

	pub fn declare_func(&mut self, func: Func) {
		self.functions.push(func);
	}

	pub fn get(&self, name: &str) -> Result<&Option<Value>, Error> {
		for var in self.variables.iter().rev() {
			if var.name == name {
				if var.value.is_some() {
					return Ok(&var.value);
				}
				return Err(Error::VariableHasNoValue);
			}
		}
		Err(Error::VariableNotFound)
	}

	pub fn get_mut(&mut self, name: &str) -> Result<&mut Option<Value>, Error> {
		for var in self.variables.iter_mut().rev() {
			if var.name == name {
				if var.value.is_some() {
					return Ok(&mut var.value);
				}
				return Err(Error::VariableHasNoValue);
			}
		}
		Err(Error::VariableNotFound)
	}

	pub fn get_func(&self, name: &str) -> Option<&Func> {
		self.functions.iter().rev().find(|&func| func.declaration.name == name)
	}

	pub fn scope<F, T>(&mut self, reg: &Registry, f: F) -> Result<T, Error>
	where
		F: FnOnce(&mut Self) -> Result<T, Error>,
	{
		let num_vars = self.variables.len();
		let num_funcs = self.functions.len();
		let out = f(self);
		while self.variables.len() > num_vars {
			if let Some(var) = self.variables.pop() {
				if let Some(val) = var.value {
					if let Some((_sig, func)) = reg.get_function_lite(
						&FunctionSignatureLite::new("op_drop".to_owned())
							.with_arg(Type::from_owned(val.id())),
					) {
						func.0(&mut [val])?;
					}
				}
			}
		}
		while self.functions.len() > num_funcs {
			self.functions.pop();
		}
		out
	}
}
