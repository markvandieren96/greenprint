use crate::Expression;
use lexer::Span;

/// Parses "foo.bar"
#[derive(Debug, PartialEq, Clone)]
pub struct GetField {
	pub span: Span,
	pub object: Box<Expression>,
	pub name: String,
}
