use crate::{
	meta, ClassRegistry, Container, Error, Field, FreeFunc, MemberMutFunc, MemberOwnedFunc,
	MemberRefFunc, Object, Register, TypeName,
};
use meta::{ClassId, SelfParam, TypeWithAccess};
use specs::World;
use std::any::TypeId;

pub(crate) fn extend_world_lifetime(world: &specs::World) -> &'static specs::World {
	unsafe { std::mem::transmute(world) }
}

#[derive(Debug)]
pub struct Class {
	name: String,
	type_id: TypeId,
	pub(crate) static_methods: Container<FreeFunc>,
	pub(crate) fields: Container<Field>,
	pub(crate) methods: Container<MemberRefFunc>,
	pub(crate) methods_mut: Container<MemberMutFunc>,
	pub(crate) methods_owned: Container<MemberOwnedFunc>,
}

impl Class {
	#[must_use]
	pub fn new<T: 'static + Register + TypeName>() -> Self {
		Self {
			name: T::type_name(),
			type_id: TypeId::of::<T>(),
			static_methods: Container::new(),
			fields: Container::new(),
			methods: Container::new(),
			methods_mut: Container::new(),
			methods_owned: Container::new(),
		}
	}

	pub fn add_static_method(&mut self, name: &'static str, f: FreeFunc) {
		self.static_methods.insert(name, f);
	}

	#[must_use]
	pub fn get_static_method(&self, name: &str) -> Option<&FreeFunc> {
		self.static_methods.find(name)
	}

	pub fn call(
		&self,
		name: &str,
		world: &World,
		args: &mut [Object],
	) -> Result<Option<Object>, Error> {
		Ok(self.static_methods.find(name).ok_or_else(|| {
			Error::no_such_static_function(name, self)
		})?(extend_world_lifetime(world), args))
	}

	pub fn add_field(&mut self, name: &'static str, field: Field) {
		self.fields.insert(name, field);
	}

	#[must_use]
	pub fn field(&self, name: &str) -> Option<&Field> {
		self.fields.find(name)
	}

	pub fn add_method(&mut self, name: &'static str, f: MemberRefFunc) {
		self.methods.insert(name, f);
	}

	#[must_use]
	pub fn get_method(&self, name: &str) -> Option<&MemberRefFunc> {
		self.methods.find(name)
	}

	pub fn add_mut_method(&mut self, name: &'static str, f: MemberMutFunc) {
		self.methods_mut.insert(name, f);
	}

	#[must_use]
	pub fn get_mut_method(&self, name: &str) -> Option<&MemberMutFunc> {
		self.methods_mut.find(name)
	}

	pub fn add_owned_method(&mut self, name: &'static str, f: MemberOwnedFunc) {
		self.methods_owned.insert(name, f);
	}

	#[must_use]
	pub fn get_owned_method(&self, name: &str) -> Option<&MemberOwnedFunc> {
		self.methods_owned.find(name)
	}

	#[must_use]
	pub fn name(&self) -> &str {
		&self.name
	}

	#[must_use]
	pub fn id(&self) -> TypeId {
		self.type_id
	}

	#[must_use]
	pub fn meta(&self, registry: &ClassRegistry) -> meta::Class {
		let mut fields = std::collections::HashMap::new();

		for (field_name, index) in &self.fields.names {
			let field = &self.fields.fields[*index];
			let ty = registry
				.get(field.ty)
				.expect("Class not registered")
				.name()
				.to_string();
			assert!(fields
				.insert((*field_name).to_string(), ClassId(ty))
				.is_none());
		}

		let mut methods = std::collections::HashMap::new();

		for (name, index) in &self.static_methods.names {
			let method = &self.static_methods.fields[*index];
			assert!(methods
				.insert((*name).to_string(), method_meta(method, SelfParam::Static))
				.is_none());
		}

		for (name, index) in &self.methods.names {
			let method = &self.methods.fields[*index];
			assert!(methods
				.insert((*name).to_string(), method_meta(method, SelfParam::Ref))
				.is_none());
		}

		for (name, index) in &self.methods_mut.names {
			let method = &self.methods_mut.fields[*index];
			assert!(methods
				.insert((*name).to_string(), method_meta(method, SelfParam::RefMut))
				.is_none());
		}

		for (name, index) in &self.methods_owned.names {
			let method = &self.methods_owned.fields[*index];
			assert!(methods
				.insert((*name).to_string(), method_meta(method, SelfParam::Owned))
				.is_none());
		}

		meta::Class { fields, methods }
	}
}

fn method_meta<T>(method: &crate::func::Func<T>, self_param: SelfParam) -> meta::Method {
	let returns = method.return_type.as_ref().map(|ty| TypeWithAccess {
		access: ty.access,
		ty: ty.ty.clone(),
	});
	let parameters = method
		.parameters
		.iter()
		.map(|ty| TypeWithAccess {
			access: ty.access,
			ty: ty.ty.clone(),
		})
		.collect();
	meta::Method {
		self_param,
		parameters,
		returns,
	}
}
