/*use crate::{impl_node, Context, Error, GetType, INode, Node, SetAccess, Validate};
use parser::Span;
use reflect::meta::{ClassId, Meta};

#[derive(Debug, Clone)]
pub struct ReturnStatement {
	pub span: Span,
	pub value: Option<Box<Node>>,
}

impl INode for ReturnStatement {
	impl_node!("ReturnStatement");

	#[must_use]
	fn to_src(&self, depth: usize) -> String {
		format!("return{};", self.value.as_ref().map(|value| format!(" {}", value.to_src(depth))).unwrap_or_default())
	}
}

impl std::fmt::Display for ReturnStatement {
	fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
		write!(f, "ReturnStatement({})", self.value.as_ref().map(|value| format!(" {}", value)).unwrap_or_default())
	}
}

impl From<parser::nodes::ReturnStatement> for ReturnStatement {
	fn from(node: parser::nodes::ReturnStatement) -> Self {
		Self {
			span: node.span,
			value: node.value.map(|value| Box::new(value.into())),
		}
	}
}

impl GetType for ReturnStatement {
	fn get_type(&self, meta: &Meta, ctx: &Context) -> Result<Option<ClassId>, Error> {
		self.value.as_ref().map_or(Ok(None), |value| value.get_type(meta, ctx))
	}
}

impl Validate for ReturnStatement {
	fn validate(&mut self, meta: &Meta, ctx: &mut Context) -> Result<(), Error> {
		let Self { span, value } = self;

		if let Some(value) = value {
			value.set_access_opt_copy_or_move(value.get_type(meta, ctx)?, meta, ctx, span.clone())?;
			value.validate(meta, ctx)
		} else {
			Ok(())
		}
	}
}
*/
