use crate::Identifier;
use parser::{Span, SpannedErrorMessage};
use reflect::meta::ClassId;

#[derive(Debug)]
pub enum Error {
	Reflect {
		span: Span,
		error: reflect::Error,
	},
	AssignmentToConst {
		span: Span,
		name: Identifier,
	},
	IfReturnTypeNoElseBranch {
		span: Span,
		expected: ClassId,
	},
	MethodCallOnUndefined {
		span: Span,
		name: Identifier,
	},
	GetFieldOnUndefined {
		span: Span,
		name: Identifier,
	},
	CallWrongNumberOfArguments {
		span: Span,
		context: CallContext,
		name: Identifier,
		expected: usize,
		got: usize,
	},
	WrongType {
		span: Span,
		expected: Option<ClassId>,
		got: Option<ClassId>,
		ctx: WrongTypeContext,
	},
	Undefined {
		span: Span,
		ctx: UndefinedContext,
	},
	VariableAccess {
		span: Span,
		name: Identifier,
		ty: VariableAccessType,
	},
}

#[derive(Debug)]
pub enum CallContext {
	Free,
	Method { class: ClassId },
	Static { class: ClassId },
}

impl CallContext {
	#[must_use]
	pub fn new_static(class: &ClassId) -> Self {
		Self::Static {
			class: class.clone(),
		}
	}

	#[must_use]
	pub fn new_method(class: &ClassId) -> Self {
		Self::Method {
			class: class.clone(),
		}
	}

	#[must_use]
	pub fn new_free() -> Self {
		Self::Free
	}
}

impl std::fmt::Display for CallContext {
	fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
		match self {
			CallContext::Free => Ok(()),
			CallContext::Method { class } => write!(f, "{class}."),
			CallContext::Static { class } => write!(f, "{class}::"),
		}
	}
}

#[derive(Debug, Copy, Clone)]
pub enum VariableAccessType {
	Uninitialized,
	Untyped,
	UseAfterMove,
}

#[derive(Debug, Clone)]
pub enum UndefinedContext {
	Assignment { name: Identifier },
	Variable { name: Identifier },
	Method { class: ClassId, name: Identifier },
	Function { name: Identifier },
	StaticFunction { class: ClassId, name: Identifier },
	Field { class: ClassId, name: Identifier },
}

#[allow(clippy::module_name_repetitions)]
#[derive(Debug, Copy, Clone)]
pub enum WrongTypeContext {
	Assignment,
	BranchCondition,
	BranchElsePath,
	VariableDeclarationInitializer,
	LoopCondition,
	FunctionDeclaration,
	FunctionCall,
	Range,
}

impl Error {
	#[must_use]
	pub(crate) fn reflect(span: &Span, e: reflect::Error) -> Self {
		Self::Reflect {
			span: span.clone(),
			error: e,
		}
	}

	#[must_use]
	pub(crate) fn assignment_to_const(span: &Span, name: &Identifier) -> Self {
		Self::AssignmentToConst {
			span: span.clone(),
			name: name.clone(),
		}
	}

	#[must_use]
	pub(crate) fn branch_missing_else(span: &Span, expected: ClassId) -> Self {
		Self::IfReturnTypeNoElseBranch {
			span: span.clone(),
			expected,
		}
	}

	#[must_use]
	pub(crate) fn method_call_on_undefined(span: &Span, name: &Identifier) -> Self {
		Self::MethodCallOnUndefined {
			span: span.clone(),
			name: name.clone(),
		}
	}

	#[must_use]
	pub(crate) fn get_field_on_undefined(span: &Span, name: &Identifier) -> Self {
		Self::GetFieldOnUndefined {
			span: span.clone(),
			name: name.clone(),
		}
	}

	#[must_use]
	pub(crate) fn call_wrong_number_of_arguments(
		span: &Span,
		context: CallContext,
		name: &Identifier,
		expected: usize,
		got: usize,
	) -> Self {
		Self::CallWrongNumberOfArguments {
			span: span.clone(),
			context,
			name: name.clone(),
			expected,
			got,
		}
	}

	#[must_use]
	pub(crate) fn undefined_variable(span: &Span, name: &Identifier) -> Self {
		Self::Undefined {
			span: span.clone(),
			ctx: UndefinedContext::Variable { name: name.clone() },
		}
	}

	#[must_use]
	pub(crate) fn undefined_method(span: &Span, class: ClassId, name: &Identifier) -> Self {
		Self::Undefined {
			span: span.clone(),
			ctx: UndefinedContext::Method {
				class,
				name: name.clone(),
			},
		}
	}

	#[must_use]
	pub(crate) fn undefined_function(span: &Span, name: &Identifier) -> Self {
		Self::Undefined {
			span: span.clone(),
			ctx: UndefinedContext::Function { name: name.clone() },
		}
	}

	#[must_use]
	pub(crate) fn undefined_static_function(
		span: &Span,
		class: ClassId,
		name: &Identifier,
	) -> Self {
		Self::Undefined {
			span: span.clone(),
			ctx: UndefinedContext::StaticFunction {
				class,
				name: name.clone(),
			},
		}
	}

	#[must_use]
	pub(crate) fn undefined_field(span: &Span, class: ClassId, name: &Identifier) -> Self {
		Self::Undefined {
			span: span.clone(),
			ctx: UndefinedContext::Field {
				class,
				name: name.clone(),
			},
		}
	}

	#[must_use]
	pub(crate) fn uninitialized_variable(span: &Span, name: &Identifier) -> Self {
		Self::VariableAccess {
			span: span.clone(),
			name: name.clone(),
			ty: VariableAccessType::Uninitialized,
		}
	}

	#[must_use]
	pub(crate) fn untyped_variable(span: &Span, name: &Identifier) -> Self {
		Self::VariableAccess {
			span: span.clone(),
			name: name.clone(),
			ty: VariableAccessType::Untyped,
		}
	}

	#[must_use]
	pub(crate) fn moved_variable(span: &Span, name: &Identifier) -> Self {
		Self::VariableAccess {
			span: span.clone(),
			name: name.clone(),
			ty: VariableAccessType::UseAfterMove,
		}
	}

	#[must_use]
	pub(crate) fn assignment_wrong_type(span: &Span) -> WrongTypeErrorBuilder {
		WrongTypeErrorBuilder::new(span, WrongTypeContext::Assignment)
	}

	#[must_use]
	pub(crate) fn branch_condition_wrong_type(span: &Span) -> WrongTypeErrorBuilder {
		WrongTypeErrorBuilder::new(span, WrongTypeContext::BranchCondition)
	}

	#[must_use]
	pub(crate) fn branch_else_wrong_type(
		span: &Span,
		expected: Option<ClassId>,
		got: Option<ClassId>,
	) -> Self {
		Self::WrongType {
			span: span.clone(),
			expected,
			got,
			ctx: WrongTypeContext::BranchElsePath,
		}
	}

	#[must_use]
	pub(crate) fn variable_initializer_wrong_type(span: &Span) -> WrongTypeErrorBuilder {
		WrongTypeErrorBuilder::new(span, WrongTypeContext::VariableDeclarationInitializer)
	}

	#[must_use]
	pub(crate) fn loop_condition_wrong_type(span: &Span) -> WrongTypeErrorBuilder {
		WrongTypeErrorBuilder::new(span, WrongTypeContext::LoopCondition)
	}

	#[must_use]
	pub(crate) fn function_declaration_wrong_type(
		span: &Span,
		expected: Option<ClassId>,
		got: Option<ClassId>,
	) -> Self {
		Self::WrongType {
			span: span.clone(),
			expected,
			got,
			ctx: WrongTypeContext::FunctionDeclaration,
		}
	}

	#[must_use]
	pub(crate) fn function_call_wrong_type(span: &Span) -> WrongTypeErrorBuilder {
		WrongTypeErrorBuilder::new(span, WrongTypeContext::FunctionCall)
	}

	#[must_use]
	pub(crate) fn range_wrong_type(span: &Span) -> WrongTypeErrorBuilder {
		WrongTypeErrorBuilder::new(span, WrongTypeContext::Range)
	}
}

impl SpannedErrorMessage for Error {
	#[must_use]
	fn span(&self) -> Option<Span> {
		match self {
			Self::Reflect { span, .. }
			| Self::AssignmentToConst { span, .. }
			| Self::IfReturnTypeNoElseBranch { span, .. }
			| Self::MethodCallOnUndefined { span, .. }
			| Self::GetFieldOnUndefined { span, .. }
			| Self::CallWrongNumberOfArguments { span, .. }
			| Self::Undefined { span, .. }
			| Self::WrongType { span, .. }
			| Self::VariableAccess { span, .. } => Some(span.clone()),
		}
	}

	#[must_use]
	fn message(&self) -> String {
		self.to_string()
	}
}

impl std::fmt::Display for Error {
	fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
		match self {
			Error::Reflect { error, .. } => write!(f, "{error}"),
			Error::AssignmentToConst { name, .. } => {
				write!(f, "Can not assign to a non-mutable value({name})")
			}
			Error::IfReturnTypeNoElseBranch { .. } => {
				write!(
					f,
					"Can not return a value from a branch without an else branch"
				)
			}
			Error::MethodCallOnUndefined { name, .. } => {
				write!(f, "Can not call a method({name}) on a null object")
			}
			Error::GetFieldOnUndefined { name, .. } => {
				write!(f, "Can't get a field({name}) on a null object")
			}
			Error::CallWrongNumberOfArguments {
				context,
				name,
				expected,
				got,
				..
			} => {
				write!(
					f,
					"Wrong number of arguments for method call({context}{name}). Expected {expected}, got {got}"
				)
			}
			Error::Undefined { ctx, .. } => {
				let ctx = match ctx {
					UndefinedContext::Assignment { name } => {
						format!("variable \"{name}\" for assignment")
					}
					UndefinedContext::Variable { name } => format!("variable \"{name}\""),
					UndefinedContext::Method { class, name } => {
						format!("method \"{name}\" on class \"{class}\"")
					}
					UndefinedContext::Function { name } => format!("function \"{name}\""),
					UndefinedContext::StaticFunction { class, name } => {
						format!("static function \"{name}\" in class \"{class}\"")
					}
					UndefinedContext::Field { class, name } => {
						format!("field \"{name}\" on class \"{class}\"")
					}
				};
				write!(f, "Undefined {ctx}")
			}
			Error::WrongType {
				expected, got, ctx, ..
			} => {
				let none = ClassId::none();
				let expected = expected.as_ref().map_or(&none, |expected| expected);
				let got = got.as_ref().map_or(&none, |got| got);

				write!(
					f,
					"Wrong type, expected \"{expected}\", got \"{got}\" (in {ctx:?})"
				)
			}
			Error::VariableAccess { name, ty, .. } => match ty {
				VariableAccessType::Uninitialized => {
					write!(f, "Use of uninitialized variable \"{name}\"")
				}
				VariableAccessType::Untyped => write!(f, "Use of untyped variable \"{name}\""),
				VariableAccessType::UseAfterMove => write!(f, "Use after move \"{name}\""),
			},
		}
	}
}

pub struct WrongTypeErrorBuilder {
	pub span: Span,
	pub expected: Option<ClassId>,
	pub got: Option<ClassId>,
	pub ctx: WrongTypeContext,
}

impl WrongTypeErrorBuilder {
	#[must_use]
	pub(crate) fn new(span: &Span, ctx: WrongTypeContext) -> Self {
		Self {
			span: span.clone(),
			expected: None,
			got: None,
			ctx,
		}
	}

	#[must_use]
	pub(crate) fn expected(mut self, expected: ClassId) -> Self {
		self.expected = Some(expected);
		self
	}

	#[must_use]
	pub(crate) fn got(mut self, got: ClassId) -> Self {
		self.got = Some(got);
		self
	}

	#[must_use]
	pub(crate) fn build(self) -> Error {
		Error::WrongType {
			span: self.span,
			expected: self.expected,
			got: self.got,
			ctx: self.ctx,
		}
	}
}
