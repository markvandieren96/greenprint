use crate::{
	impl_node, nodes::Block, Access, Callback, Context, Error, GetType, INode, Node, ScopeId,
	SetAccess, Validate, Walkable,
};
use parser::Span;
use reflect::{
	meta::{ClassId, Meta},
	TypeName,
};

#[derive(Debug, Clone)]
pub struct Branch {
	pub span: Span,
	pub scope: Option<ScopeId>,
	pub condition: Box<Node>,
	pub then_branch: Block,
	pub else_branch: Option<Box<Node>>,
}

impl Branch {
	#[must_use]
	pub fn into_node(self) -> Node {
		Node::Branch(self)
	}
}

impl INode for Branch {
	impl_node!("Branch");

	#[must_use]
	fn to_src(&self, depth: usize) -> String {
		format!(
			"if {} {{\n{}\n}}{}",
			self.condition.to_src(depth),
			self.then_branch.to_src(depth + 1),
			self.else_branch
				.as_ref()
				.map(|else_branch| else_branch.to_src(depth + 1))
				.unwrap_or_default()
		)
	}
}

impl std::fmt::Display for Branch {
	fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
		let Self {
			condition,
			then_branch,
			else_branch,
			..
		} = self;

		if let Some(else_branch) = else_branch {
			write!(
				f,
				"If(if {condition} then {then_branch} else {else_branch})"
			)
		} else {
			write!(f, "If(if {condition} then {then_branch})")
		}
	}
}

impl From<parser::nodes::Branch> for Branch {
	fn from(node: parser::nodes::Branch) -> Self {
		Self {
			span: node.span,
			scope: None,
			condition: Box::new(node.condition.into()),
			then_branch: node.then_branch.into(),
			else_branch: node
				.else_branch
				.map(|else_branch| Box::new(else_branch.into())),
		}
	}
}

impl GetType for Branch {
	fn get_type(&self, meta: &Meta, ctx: &Context) -> Result<Option<ClassId>, Error> {
		self.then_branch.get_type(meta, ctx)
	}
}

impl Validate for Branch {
	fn pass1(&mut self, meta: &Meta, ctx: &mut Context) -> Result<(), Error> {
		let Self {
			scope,
			condition,
			then_branch,
			else_branch,
			..
		} = self;

		*scope = Some(ctx.current_scope_expect());

		condition.set_access(ctx, Access::Copy)?;
		condition.validate(meta, ctx)?;

		then_branch.validate(meta, ctx)?;

		if let Some(else_branch) = else_branch {
			else_branch.validate(meta, ctx)?;
		}

		Ok(())
	}

	fn check(&self, meta: &Meta, ctx: &Context) -> Result<(), Error> {
		let Self {
			condition,
			then_branch,
			else_branch,
			..
		} = self;

		let ty = condition.get_type(meta, ctx)?.ok_or_else(|| {
			Error::branch_condition_wrong_type(&condition.span())
				.expected(bool::class_id())
				.build()
		})?;
		if ty != ClassId(bool::type_name()) {
			return Err(Error::branch_condition_wrong_type(&condition.span())
				.expected(bool::class_id())
				.got(ty)
				.build());
		}

		let then_ty = then_branch.get_type(meta, ctx)?;
		if else_branch.is_none() {
			if let Some(then_ty) = then_ty {
				return Err(Error::branch_missing_else(&then_branch.span(), then_ty));
			}
		}

		if let Some(else_branch) = else_branch {
			let else_ty = else_branch.get_type(meta, ctx)?;
			if then_ty != else_ty {
				return Err(Error::branch_else_wrong_type(
					&else_branch.span(),
					then_ty,
					else_ty,
				));
			}
		}

		Ok(())
	}
}

impl Walkable for Branch {
	fn walk(&mut self, f: &mut Callback) -> Result<(), Error> {
		f(self)?;
		self.condition.walk(f)?;
		self.then_branch.walk(f)?;
		self.else_branch.walk(f)
	}
}
