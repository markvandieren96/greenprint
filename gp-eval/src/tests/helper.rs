use crate::{Context, Eval, State};
use ast::Node;
use ast::{parser, reflect};
use log::trace;
use reflect::{
	impl_type_name, meta::Meta, specs::World, Class, ClassRegistry, FreeFunc, MemberMutFunc,
	MemberRefFunc, Object, Register, Type, CLASSES, CLASS_REGISTRY,
};

#[allow(dead_code)]
pub fn enable_tracing() {
	simple_logger::SimpleLogger::new()
		.with_level(log::LevelFilter::Off)
		.with_module_level("gp_parser", log::LevelFilter::Off)
		.with_module_level("gp_ast", log::LevelFilter::Trace)
		.with_module_level("gp_eval", log::LevelFilter::Trace)
		.with_module_level("gp_program", log::LevelFilter::Trace)
		.init()
		.unwrap();
}

pub fn init() -> Meta {
	CLASS_REGISTRY.register::<Vec2>();
	CLASS_REGISTRY.register::<Transform>();
	CLASSES.meta()
}

#[derive(Debug)]
pub enum Error {
	Parser(parser::FailedParseInfo),
	Eval(crate::Error),
}

pub fn parse(meta: &Meta, src: &str) -> Result<(Node, ast::Context), Error> {
	let expr = parser::parse(src).map_err(Error::Parser)?;
	let (res, e) = ast::type_check(expr, meta, ast::Externs::default());
	e.expect("TypeCheck error");
	Ok(res)
}

pub fn eval(meta: &Meta, src: &str) -> Result<Option<Object>, Error> {
	let (node, ctx) = parse(meta, src)?;
	trace!("Parsed node: {:#?}", node);
	trace!("Context: {:#?}", ctx);

	let eval_ctx = Context::new(&ctx);
	let mut state = State::new(&ctx);
	let world = World::default();
	node.eval(&world, &eval_ctx, &mut state, 0)
		.map_err(Error::Eval)
}

#[derive(Default, Clone, Copy, Debug, PartialEq)]
pub struct Vec2 {
	pub x: f32,
	pub y: f32,
}

impl_type_name!(Vec2);

impl Register for Vec2 {
	fn register(registry: &mut ClassRegistry) {
		let mut class = Class::new::<Self>();
		reflect::field!(&mut class, x: f32);
		reflect::field!(&mut class, y: f32);

		reflect::block!(
			derive(default, clone)

			fn new(x: &f32, y: &f32) -> Vec2 {
				Vec2 { x: *x, y: *y }
			}

			fn get_x(&self) -> f32 {
				obj.x
			}

			fn set_x(&mut self, x: &f32) {
				obj.x = *x;
			}
		);
		registry.register(class);
	}
}

#[derive(Default, Clone, Copy, Debug, PartialEq)]
pub struct Transform {
	pub position: Vec2,
	pub scale: Vec2,
}

impl_type_name!(Transform);

impl Register for Transform {
	fn register(registry: &mut ClassRegistry) {
		let mut class = Class::new::<Self>();
		reflect::field!(&mut class, position: Vec2);
		reflect::field!(&mut class, scale: Vec2);

		reflect::block!(
			derive(default, clone)

			fn new(position: &Vec2, scale: &Vec2) -> Transform {
				Transform { position: *position, scale: *scale }
			}
		);
		registry.register(class);
	}
}
