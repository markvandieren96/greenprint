use proc_macro::TokenStream;
use quote::quote;

pub fn impl_derive(ast: &syn::DeriveInput) -> TokenStream {
	let name = &ast.ident;

	let (impl_generics, ty_generics, where_clause) = ast.generics.split_for_impl();

	let gen = quote! {
		impl #impl_generics greenprint::TypeName for #name #ty_generics #where_clause {
			fn type_name() -> String {
				stringify!(#name).to_owned()
			}
		}
	};
	gen.into()
}
