#![warn(clippy::all)]
#![warn(clippy::pedantic)]

extern crate proc_macro;

use proc_macro::TokenStream;

mod reflect;
mod type_name;

#[proc_macro_derive(Reflect)]
pub fn reflect_derive(input: TokenStream) -> TokenStream {
	let ast = syn::parse(input).expect("failed to parse input");
	reflect::impl_derive(&ast)
}

#[proc_macro_derive(TypeName)]
pub fn type_name_derive(input: TokenStream) -> TokenStream {
	let ast = syn::parse(input).expect("failed to parse input");
	type_name::impl_derive(&ast)
}
