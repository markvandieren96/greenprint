use super::*;
use program::reflect;
use reflect::{impl_type_name, ClassRegistry, TypeName};
use specs::shred::{Fetch, FetchMut};

pub struct DeltaTime {
	time: i64,
}

impl_type_name!(DeltaTime);

impl Register for DeltaTime {
	fn register(registry: &mut ClassRegistry) {
		let mut class = Class::new::<Self>();
		reflect::field!(&mut class, time: i64);
		reflect::block!(derive(resource));
		registry.register(class);
	}
}

#[derive(Default)]
pub struct GenericComponent<T> {
	t: T,
}

impl<T: TypeName> TypeName for GenericComponent<T> {
	fn type_name() -> String {
		format!("GenericComponent_{}", T::type_name())
	}
}

impl<T> Register for GenericComponent<T>
where
	T: 'static + Send + Sync + Register + Default + TypeName,
{
	fn register(registry: &mut ClassRegistry) {
		let mut class = Class::new::<Self>();
		reflect::field!(&mut class, t: T);
		reflect::block!(derive(default, resource));
		registry.register(class);
	}
}

#[test]
fn resource_get() {
	let meta = init();

	let program = Program::new(String::new(), &meta, "DeltaTime::resource().get().time").unwrap();
	let mut state = program.create_state();
	let mut world = World::new();
	world.insert(DeltaTime { time: 6 });

	assert_eq!(
		*program
			.eval(&mut state, &world)
			.expect("eval error")
			.expect("no return value")
			.downcast_ref::<i64>()
			.expect("return value is not i64"),
		6
	);
}

#[test]
fn resource_mutate() {
	let meta = init();

	let program = Program::new(
		String::new(),
		&meta,
		"DeltaTime::resource_mut().get_mut().time = 7;",
	)
	.unwrap();
	let mut state = program.create_state();
	let mut world = World::new();
	world.insert(DeltaTime { time: 6 });

	assert!(program
		.eval(&mut state, &world)
		.expect("eval error")
		.is_none());

	assert_eq!(world.read_resource::<DeltaTime>().time, 7);
}

#[test]
fn resource_templated_get() {
	let meta = init();

	let program = Program::new(
		String::new(),
		&meta,
		"GenericComponent_bool::resource().get().t",
	)
	.unwrap();
	let mut state = program.create_state();
	let mut world = World::new();
	world.insert(GenericComponent { t: true });

	assert!(*program
		.eval(&mut state, &world)
		.expect("eval error")
		.expect("no return value")
		.downcast_ref::<bool>()
		.expect("return value is not bool"));
}

#[test]
fn resource_drop() {
	let meta = init();

	let program = Program::new(
		String::new(),
		&meta,
		"{ let res = DeltaTime::resource_mut(); }",
	)
	.unwrap();
	let mut state = program.create_state();
	let mut world = World::new();
	world.insert(DeltaTime { time: 6 });

	program.eval(&mut state, &world).expect("eval error");

	// This will panic if the resource is still borrowed inside the program State
	world.write_resource::<DeltaTime>();
}

#[test]
fn resource_drop_nested() {
	let meta = init();

	let program = Program::new(
		String::new(),
		&meta,
		"{
			{
				let res = DeltaTime::resource_mut();
				let res2 = res.get_mut();
			}
		}",
	)
	.unwrap();
	let mut state = program.create_state();
	let mut world = World::new();
	world.insert(DeltaTime { time: 6 });

	program.eval(&mut state, &world).expect("eval error");

	// This will panic if the resource is still borrowed inside the program State
	world.write_resource::<DeltaTime>();
}

#[test]
fn resource_drop_in_function() {
	let meta = init();
	//meta.save("../meta.ron").unwrap();

	//enable_tracing();

	let program = Program::new(
		String::new(),
		&meta,
		"fn hello_world() {
		let res = DeltaTime::resource_mut();
	}
	hello_world();",
	)
	.unwrap();
	let mut state = program.create_state();
	let mut world = World::new();
	world.insert(DeltaTime { time: 6 });

	program.eval(&mut state, &world).expect("eval error");

	// This will panic if the resource is still borrowed inside the program State
	world.write_resource::<DeltaTime>();
}
