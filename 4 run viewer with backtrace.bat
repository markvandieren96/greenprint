@echo off
set RUST_BACKTRACE=1

cargo +stable run -p gp-view || goto wait

goto eof

:wait
pause

:eof