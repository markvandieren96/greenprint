use super::*;
use program::reflect;
use program::reflect::{impl_type_name, ClassRegistry};
use specs::prelude::*;

#[derive(Default)]
pub struct Position {
	x: i64,
}

impl_type_name!(Position);

impl specs::Component for Position {
	type Storage = specs::VecStorage<Self>;
}

impl Register for Position {
	fn register(registry: &mut ClassRegistry) {
		let mut class = Class::new::<Self>();
		reflect::field!(&mut class, x: i64);
		reflect::block!(
			derive(default, component)

			fn new(x: &i64) -> Self {
				Self { x: *x}
			}
		);
		registry.register(class);
	}
}

#[test]
#[cfg_attr(miri, ignore)]
fn component_add() {
	let meta = init();

	let mut world = World::new();
	world.register::<Position>();
	let src = "
			let entity = Entity::new();
			let position = Position::default();
			position.add_to_entity(entity);
			entity
		";
	let entity: Entity = *eval_with_world(&meta, &world, src)
		.expect("No return value")
		.downcast_ref::<Entity>()
		.expect("downcast failed");

	assert!(world.read_storage::<Position>().get(entity).is_some());
}

#[test]
#[cfg_attr(miri, ignore)]
fn component_get() {
	let meta = init();

	let mut world = World::new();
	world.register::<Position>();
	let src = "
			let entity = Entity::new();
			{
				let position = Position::new(6);
				position.add_to_entity(entity);
			}

			let bars = Position::storage();
			let position = bars.get(entity);
			position.x
		";
	let res: i64 = *eval_with_world(&meta, &world, src)
		.expect("No return value")
		.downcast_ref::<i64>()
		.expect("downcast failed");

	assert_eq!(res, 6);
}

#[test]
#[cfg_attr(miri, ignore)]
fn component_get_mut() {
	let meta = init();

	let mut world = World::new();
	world.register::<Position>();
	let src = "
			let entity = Entity::new();
			{
				let position = Position::new(6);
				position.add_to_entity(entity);
			}

			let bars = Position::storage_mut();
			let position = bars.get_mut(entity);
			position.x = 7;
			entity
		";
	let entity: Entity = *eval_with_world(&meta, &world, src)
		.expect("No return value")
		.downcast_ref::<Entity>()
		.expect("downcast failed");

	assert_eq!(world.read_storage::<Position>().get(entity).unwrap().x, 7);
}
