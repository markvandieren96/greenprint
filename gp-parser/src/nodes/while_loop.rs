use crate::{expression, nodes::Block, Error, Errors, Expression, ParseResult};
use lexer::{merge_spans, Lexer, LexerExt, Token};

/// Parses "while expr { ... }"
#[derive(Debug, PartialEq, Clone)]
pub struct WhileLoop {
	pub span: lexer::Span,
	pub condition: Expression,
	pub body: Block,
}

impl WhileLoop {
	pub fn try_parse(lexer: &mut Lexer, errors: &mut Errors) -> ParseResult<Self> {
		let span_start = if lexer.peek_match(&Token::While) {
			lexer.next().unwrap().1
		} else {
			return Ok(None);
		};

		let condition = expression(lexer, errors);
		let body = Block::try_parse(lexer, errors)?
			.ok_or_else(|| Error::expected(lexer, Token::BraceOpen, "WhileLoop"))?;

		Ok(Some(Self {
			span: merge_spans(&span_start, &body.span),
			condition,
			body,
		}))
	}
}
