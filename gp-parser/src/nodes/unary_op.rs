use crate::{unary, Errors, Expression, ParseResult, UnaryOperator};
use lexer::{Lexer, LexerExt, Span, Token};
use std::convert::TryInto;

#[derive(Debug, PartialEq, Clone)]
pub struct UnaryOp {
	pub span: Span,
	pub operator: UnaryOperator,
	pub operand: Box<Expression>,
}

impl UnaryOp {
	pub fn try_parse(lexer: &mut Lexer, errors: &mut Errors) -> ParseResult<Self> {
		if let Some((token, span_start)) = lexer.matches_any(&[Token::Bang, Token::Min]) {
			let operator: UnaryOperator = token.try_into().unwrap();
			let operand = unary(lexer, errors)?;
			Ok(Some(Self {
				span: span_start.start..operand.span().end,
				operator,
				operand: operand.into(),
			}))
		} else {
			Ok(None)
		}
	}
}
