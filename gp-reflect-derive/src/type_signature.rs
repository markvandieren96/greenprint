use super::Access;
use proc_macro2::{Span, TokenStream};
use quote::{quote, ToTokens, TokenStreamExt};
use syn::{
	parse::{Parse, ParseStream, Result},
	Ident, Lifetime, Token,
};

#[derive(Clone)]
pub struct TypeSignature {
	pub access: Access,
	pub ty: Type,
	pub generics: Option<Generics>,
}

impl Parse for TypeSignature {
	fn parse(input: ParseStream) -> Result<Self> {
		let access: Access = input.parse()?;
		let ty: Type = input.parse()?;
		let generics = if input.peek(Token![<]) {
			Some(input.parse()?)
		} else {
			None
		};

		Ok(Self {
			access,
			ty,
			generics,
		})
	}
}

impl ToTokens for TypeSignature {
	fn to_tokens(&self, tokens: &mut TokenStream) {
		let access = self.access;
		let ty = &self.ty;
		if let Some(generics) = &self.generics {
			tokens.append_all(quote! { #access#ty#generics });
		} else {
			tokens.append_all(quote! { #access#ty });
		}
	}
}

#[derive(Clone)]
pub enum Type {
	Type(Ident),
	SelfType,
}

impl Parse for Type {
	fn parse(input: ParseStream) -> Result<Self> {
		if input.peek(Token![Self]) {
			input.parse::<Token![Self]>()?;
			Ok(Self::SelfType)
		} else {
			Ok(Self::Type(input.parse()?))
		}
	}
}

impl ToTokens for Type {
	fn to_tokens(&self, tokens: &mut TokenStream) {
		match self {
			Self::Type(ident) => tokens.append_all(quote! { #ident }),
			Self::SelfType => tokens.append(Ident::new("Self", Span::call_site())),
		}
	}
}

#[derive(Clone, Default)]
pub struct Generics {
	pub lifetimes: Vec<Lifetime>,
	pub types: Vec<TypeSignature>,
}

impl Parse for Generics {
	fn parse(input: ParseStream) -> Result<Self> {
		input.parse::<Token![<]>()?;

		let mut out = Self::default();
		loop {
			if input.peek(Token![>]) {
				input.parse::<Token![>]>()?;
				break;
			} else if input.peek(Lifetime) {
				out.lifetimes.push(input.parse()?);
			} else {
				out.types.push(input.parse()?);
			}

			if input.peek(Token![,]) {
				input.parse::<Token![,]>()?;
			}
		}

		Ok(out)
	}
}

impl ToTokens for Generics {
	fn to_tokens(&self, tokens: &mut TokenStream) {
		tokens.append(proc_macro2::Punct::new('<', proc_macro2::Spacing::Joint));
		let mut add_comma = false;
		for lifetime in &self.lifetimes {
			if add_comma {
				tokens.append_all(quote! { , #lifetime });
			} else {
				tokens.append_all(quote! { #lifetime });
			}

			add_comma = true;
		}

		for ty in &self.types {
			if add_comma {
				tokens.append_all(quote! { , #ty });
			} else {
				tokens.append_all(quote! { #ty });
			}

			add_comma = true;
		}

		tokens.append(proc_macro2::Punct::new('>', proc_macro2::Spacing::Joint));
	}
}
