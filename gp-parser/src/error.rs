use lexer::{Lexer, Span, Token};

#[derive(Debug, Clone, PartialEq)]
pub enum Error {
	MissingTypeAnnotation {
		at: usize,
	},
	MissingArgumentList {
		at: usize,
	},
	UnexpectedToken {
		expected: Token,
		got: (Token, Span),
		ctx: String,
	},
	ExpectedExpression {
		got: (Token, Span),
	},
	UnexpectedEndOfFile {
		expected: Option<Token>,
		ctx: String,
	},
}

pub type ParseResult<T> = Result<Option<T>, Error>;

pub trait SpannedErrorMessage {
	fn span(&self) -> Option<Span>;
	fn message(&self) -> String;
}

impl Error {
	pub fn expected(lexer: &mut Lexer, expected: Token, ctx: &str) -> Error {
		match lexer.peek() {
			Some(got) => Error::UnexpectedToken {
				expected,
				got,
				ctx: ctx.to_string(),
			},
			None => Error::UnexpectedEndOfFile {
				expected: Some(expected),
				ctx: ctx.to_string(),
			},
		}
	}

	pub fn expected_identifier(lexer: &mut Lexer, ctx: &str) -> Error {
		match lexer.peek() {
			Some(got) => Error::UnexpectedToken {
				expected: Token::Identifier(String::new()),
				got,
				ctx: ctx.to_string(),
			},
			None => Error::UnexpectedEndOfFile {
				expected: Some(Token::Identifier(String::new())),
				ctx: ctx.to_string(),
			},
		}
	}
}

impl SpannedErrorMessage for Error {
	#[allow(clippy::range_plus_one)]
	#[must_use]
	fn span(&self) -> Option<Span> {
		match self {
			Self::MissingTypeAnnotation { at } | Self::MissingArgumentList { at } => {
				Some(*at..at + 1)
			}
			Self::UnexpectedToken { got, .. } | Self::ExpectedExpression { got } => {
				Some(got.1.clone())
			}
			Self::UnexpectedEndOfFile { .. } => None,
		}
	}

	#[must_use]
	fn message(&self) -> String {
		self.to_string()
	}
}

impl std::fmt::Display for Error {
	fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
		match self {
			Self::MissingTypeAnnotation { .. } => write!(f, "Missing type annotation."),
			Self::MissingArgumentList { .. } => write!(f, "Missing argument list."),
			Self::UnexpectedToken { expected, got, ctx } => {
				if let Token::Error = got.0 {
					write!(f, "Invalid token. Expected: {expected}. Context: {ctx}")
				} else {
					write!(
						f,
						"Unexpected token. Expected: {expected}. Context: {ctx}"
					)
				}
			}
			Self::ExpectedExpression { got } => {
				if let Token::Error = got.0 {
					write!(f, "Invalid token.")
				} else {
					write!(f, "Expected an expression.")
				}
			}
			Self::UnexpectedEndOfFile { expected, ctx } => {
				if let Some(token) = expected {
					write!(
						f,
						"Unexpected end-of-file. Expected: {token}. Context: {ctx}"
					)
				} else {
					write!(f, "Unexpected end-of-file. Context: {ctx}")
				}
			}
		}
	}
}
