use crate::tree;
use gp_parser::{
	nodes::{Block, TypeAnnotation},
	Expression, Span, Statement,
};

pub type AstNode = tree::Node<NodeData>;

pub fn expression_into_ast_node(depth: usize, expr: &Expression) -> AstNode {
	match expr {
		Expression::Group(e) => {
			let mut node = new_node(depth, &e.span, Variant::Group);
			node.add_child(expression_into_ast_node(depth + 1, &e.body));
			node
		}
		Expression::Literal(e) => new_node(depth, &e.span, Variant::Literal(e.value.clone())),
		Expression::Variable(e) => new_node(depth, &e.span, Variant::Variable(e.name.clone())),
		Expression::UnaryOp(e) => {
			let mut node = new_node(
				depth,
				&e.span,
				Variant::Call(e.operator.name().to_owned(), None),
			);

			node.add_child(expression_into_ast_node(depth + 1, &e.operand));

			node
		}
		Expression::BinaryOp(e) => {
			let mut node = new_node(
				depth,
				&e.span,
				Variant::Call(e.operator.name().to_owned(), None),
			);

			node.add_child(expression_into_ast_node(depth + 1, &e.left));
			node.add_child(expression_into_ast_node(depth + 1, &e.right));

			node
		}
		Expression::FreeFunctionCall(e) => {
			let mut node = new_node(depth, &e.span, Variant::Call(e.name.clone(), None));

			for arg in &e.arguments.arguments {
				node.add_child(expression_into_ast_node(depth + 1, arg));
			}

			node
		}
		Expression::StaticFunctionCall(e) => {
			let mut node = new_node(
				depth,
				&e.span,
				Variant::Call(e.name.clone(), Some(e.class.clone())),
			);

			for arg in &e.arguments.arguments {
				node.add_child(expression_into_ast_node(depth + 1, arg));
			}

			node
		}
		Expression::MethodCall(_e) => todo!(),
		Expression::Block(e) => block_to_node(depth, e),
		Expression::Branch(e) => {
			let mut node = new_node(depth, &e.span, Variant::Branch);

			node.add_child(expression_into_ast_node(depth + 1, &e.condition));
			node.add_child(block_to_node(depth + 1, &e.then_branch));
			if let Some(else_branch) = &e.else_branch {
				node.add_child(expression_into_ast_node(depth + 1, else_branch));
			}

			node
		}
		Expression::GetField(_e) => todo!(),
		Expression::Error(_) => panic!(),
	}
}

pub fn block_to_node(depth: usize, block: &Block) -> AstNode {
	let mut node = new_node(depth, &block.span, Variant::Block);

	for statement in &block.body {
		node.add_child(statement_into_ast_node(depth + 1, statement));
	}

	node
}

fn statement_into_ast_node(depth: usize, statement: &Statement) -> AstNode {
	match statement {
		Statement::Expression(e) => {
			let mut node = new_node(depth, &e.span, Variant::ExpressionStatement);
			node.add_child(expression_into_ast_node(depth + 1, &e.body));
			node
		}
		Statement::VariableDeclaration(e) => {
			let mut node = new_node(
				depth,
				&e.span,
				Variant::VariableDeclaration(VariableDeclaration {
					is_extern: e.is_extern,
					is_mut: e.is_mut,
					name: e.name.clone(),
					ty: e.ty.clone(),
				}),
			);

			if let Some(initializer) = &e.initializer {
				node.add_child(expression_into_ast_node(depth + 1, initializer));
			}

			node
		}
		Statement::Assignment(_e) => todo!(),
		Statement::While(_e) => todo!(),
		Statement::For(_e) => todo!(),
		Statement::FunctionDeclaration(e) => {
			let mut node = new_node(
				depth,
				&e.span,
				Variant::FunctionDeclaration(FunctionDeclaration {
					name: e.name.clone(),
					parameters: e
						.parameters
						.parameters
						.iter()
						.map(|p| (p.name.clone(), p.ty.clone()))
						.collect(),
					return_type: e.return_type.as_ref().map(|rt| rt.ty.clone()),
				}),
			);
			node.add_child(block_to_node(depth + 1, &e.body));
			node
		}
		Statement::SetField(_e) => todo!(),
		Statement::Error(_) => panic!(),
	}
}

fn new_node(depth: usize, span: &Span, data: Variant) -> AstNode {
	AstNode::new(NodeData {
		depth,
		span: span.clone(),
		data,
	})
}

#[derive(Debug, Clone)]
pub struct NodeData {
	pub depth: usize,
	pub span: Span,
	pub data: Variant,
}

#[derive(Debug, Clone)]
pub enum Variant {
	Block,
	ExpressionStatement,
	VariableDeclaration(VariableDeclaration),
	FunctionDeclaration(FunctionDeclaration),
	Literal(gp_parser::Literal),
	Variable(String),
	Call(String, Option<String>),
	Branch,
	Group,
}

#[derive(Debug, Clone)]
pub struct VariableDeclaration {
	pub is_extern: bool,
	pub is_mut: bool,
	pub name: String,
	pub ty: Option<TypeAnnotation>,
}

#[derive(Debug, Clone)]
pub struct FunctionDeclaration {
	pub name: String,
	pub parameters: Vec<(String, TypeAnnotation)>,
	pub return_type: Option<String>,
}
