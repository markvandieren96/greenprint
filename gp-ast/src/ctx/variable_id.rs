use super::ScopeId;

#[derive(Copy, Clone, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct VariableId(pub ScopeId, pub usize);

impl std::fmt::Debug for VariableId {
	fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
		write!(f, "VariableId({}:{})", self.0, self.1)
	}
}

impl std::fmt::Display for VariableId {
	fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
		write!(f, "{}:{}", self.0, self.1)
	}
}
