use crate::{expression, nodes::Block, Error, Errors, Expression, LexerExpectExt, ParseResult};
use lexer::{merge_spans, Lexer, LexerExt, Span, Token};

/// Parses "for var in expr..=expr2"
#[derive(Debug, PartialEq, Clone)]
pub struct ForLoop {
	pub span: Span,
	pub variable: String,
	pub range_begin: Expression,
	pub range_end: Expression,
	pub range_inclusive: bool,
	pub body: Block,
}

impl ForLoop {
	pub fn try_parse(lexer: &mut Lexer, errors: &mut Errors) -> ParseResult<Self> {
		let span_start = if lexer.peek_match(&Token::For) {
			lexer.next().unwrap().1
		} else {
			return Ok(None);
		};

		let (variable, _) = lexer.expect_identifier("ForLoop")?;
		lexer.expect(Token::In, "ForLoop")?;
		let range_begin = expression(lexer, errors);
		lexer.expect(Token::Range, "ForLoop")?;

		let range_inclusive = if lexer.peek_match(&Token::Assignment) {
			lexer.next();
			true
		} else {
			false
		};

		let range_end = expression(lexer, errors);
		let body = Block::try_parse(lexer, errors)?
			.ok_or_else(|| Error::expected(lexer, Token::BraceOpen, "ForLoop"))?;

		Ok(Some(Self {
			span: merge_spans(&span_start, &body.span),
			variable,
			range_begin,
			range_end,
			range_inclusive,
			body,
		}))
	}
}
