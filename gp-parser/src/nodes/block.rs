use crate::{declaration, Error, Errors, ParseResult, Span, Statement};
use lexer::{merge_spans, Lexer, LexerExt, Token};

/// Parses "{ ... }"
#[derive(Debug, PartialEq, Clone)]
pub struct Block {
	pub span: Span,
	pub body: Vec<Statement>,
	pub catch_returns: bool,
}

impl Block {
	pub fn try_parse(lexer: &mut Lexer, errors: &mut Errors) -> ParseResult<Self> {
		let span_start = if lexer.peek_match(&Token::BraceOpen) {
			lexer.next().unwrap().1
		} else {
			return Ok(None);
		};

		let mut statements = Vec::new();

		loop {
			if lexer.peek_match(&Token::BraceClose) {
				break;
			}

			if lexer.peek().is_some() {
				let num_errors_before = errors.len();
				match declaration(lexer, errors) {
					Ok(statement) => statements.push(statement),
					Err(e) => {
						errors.push(e.clone());
						statements.push(Statement::Error(e));
					}
				}

				let num_errors_after = errors.len();
				if num_errors_before != num_errors_after {
					while lexer.next().is_some() {
						if lexer.peek_match(&Token::Semicolon) {
							lexer.next();
							break;
						}
					}
				}
			} else {
				return Err(Error::expected(lexer, Token::BraceClose, "Block"));
			}
		}

		let span_end = lexer.next().unwrap().1; // consume closing brace

		Ok(Some(Self {
			span: merge_spans(&span_start, &span_end),
			body: statements,
			catch_returns: false,
		}))
	}
}
