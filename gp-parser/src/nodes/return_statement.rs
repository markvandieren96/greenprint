/*use crate::{expression, Errors, Expression, LexerExpectExt, ParseResult};
use lexer::{merge_spans, Lexer, LexerExt, Span, Token};

/// Parses "return expr;"
#[derive(Debug, PartialEq, Clone)]
pub struct ReturnStatement {
	pub span: Span,
	pub value: Option<Expression>,
}

impl ReturnStatement {
	pub fn try_parse(lexer: &mut Lexer, errors: &mut Errors) -> ParseResult<Self> {
		let span_start = if lexer.peek_match(&Token::Return) {
			lexer.next().unwrap().1
		} else {
			return Ok(None);
		};

		if lexer.peek_match(&Token::Semicolon) {
			let span_end = lexer.next().unwrap().1;

			return Ok(Some(Self {
				span: merge_spans(&span_start, &span_end),
				value: None,
			}));
		}

		let value = expression(lexer, errors);
		let span_end = lexer.expect(Token::Semicolon, "ReturnStatement")?;

		Ok(Some(Self {
			span: merge_spans(&span_start, &span_end),
			value: Some(value),
		}))
	}
}
*/
