use crate::{Lexer, Span, Token};

pub trait LexerExt {
	fn peek_match(&mut self, token: &Token) -> bool;
	fn matches(&mut self, token: &Token) -> bool;
	fn matches_any(&mut self, tokens: &[Token]) -> Option<(Token, Span)>;
	fn matches_spanned(&mut self, token: &Token) -> Option<Span>;
}

impl<'a> LexerExt for Lexer<'a> {
	fn peek_match(&mut self, token: &Token) -> bool {
		if let Some((t, _)) = self.peek() {
			if t.variant_eq(token) {
				return true;
			}
		}
		false
	}

	fn matches(&mut self, token: &Token) -> bool {
		if let Some((t, _)) = self.peek() {
			if t.variant_eq(token) {
				self.next();
				return true;
			}
		}
		false
	}

	fn matches_any(&mut self, tokens: &[Token]) -> Option<(Token, Span)> {
		if let Some((token, _)) = self.peek() {
			for t in tokens {
				if t.variant_eq(&token) {
					return Some(self.next().unwrap());
				}
			}
		}
		None
	}

	fn matches_spanned(&mut self, token: &Token) -> Option<Span> {
		if let Some((t, span)) = self.peek() {
			if t.variant_eq(token) {
				self.next();
				return Some(span);
			}
		}
		None
	}
}
