use super::{access::Access, type_signature::TypeSignature};
use proc_macro2::TokenStream;
use quote::{quote, ToTokens, TokenStreamExt};
use syn::{
	parenthesized,
	parse::{Parse, ParseStream, Result},
	Block, Ident, Token,
};

pub struct FnDeclaration {
	pub(crate) name: Ident,
	self_param: Option<SelfParam>,
	parameters: Vec<Parameter>,
	return_type: Option<TypeSignature>,
	body: Block,
}

#[derive(Clone, Copy, PartialEq, Eq)]
pub enum SelfParam {
	Owned,
	Ref,
	RefMut,
}

impl ToTokens for SelfParam {
	fn to_tokens(&self, tokens: &mut TokenStream) {
		match self {
			Self::Owned => tokens.append(proc_macro2::Ident::new(
				"self",
				proc_macro2::Span::call_site(),
			)),
			Self::Ref => {
				tokens.append(proc_macro2::Punct::new('&', proc_macro2::Spacing::Joint));
				tokens.append(proc_macro2::Ident::new(
					"self",
					proc_macro2::Span::call_site(),
				));
			}
			Self::RefMut => {
				tokens.append(proc_macro2::Punct::new('&', proc_macro2::Spacing::Joint));
				tokens.append(proc_macro2::Ident::new(
					"mut",
					proc_macro2::Span::call_site(),
				));
				tokens.append(proc_macro2::Ident::new(
					"self",
					proc_macro2::Span::call_site(),
				));
			}
		}
	}
}

impl Parse for SelfParam {
	fn parse(input: ParseStream) -> Result<Self> {
		if input.peek(Token![self]) {
			input.parse::<Token![self]>()?;
			Ok(Self::Owned)
		} else if input.peek(Token![&]) {
			input.parse::<Token![&]>()?;
			if input.peek(Token![self]) {
				input.parse::<Token![self]>()?;
				Ok(Self::Ref)
			} else if input.peek(Token![mut]) {
				input.parse::<Token![mut]>()?;
				input.parse::<Token![self]>()?;
				Ok(Self::RefMut)
			} else {
				Err(input.error("Expected self or mut self"))
			}
		} else {
			Err(input.error("Expected self, &self or &mut self"))
		}
	}
}

impl Parse for FnDeclaration {
	fn parse(input: ParseStream) -> Result<Self> {
		input.parse::<Token![fn]>()?;
		let name: Ident = input.parse()?;

		let mut parameters: Vec<Parameter> = Vec::new();
		let mut self_param = None;
		{
			let params;
			let _paren_token = parenthesized!(params in input);
			if let Ok(param) = params.parse::<SelfParam>() {
				self_param = Some(param);

				if params.peek(Token![,]) {
					params.parse::<Token![,]>()?;
				}
			}

			while let Ok(param) = params.parse::<Parameter>() {
				parameters.push(param);

				if params.peek(Token![,]) {
					params.parse::<Token![,]>()?;
				} else {
					break;
				}
			}
		}

		let return_type = if input.peek(Token![->]) {
			input.parse::<Token![->]>()?;

			Some(input.parse()?)
		} else {
			None
		};

		let body = input.parse()?;

		Ok(FnDeclaration {
			name,
			self_param,
			parameters,
			return_type,
			body,
		})
	}
}

#[derive(Clone)]
pub struct Parameter {
	name: Ident,
	is_ref: bool,
	is_mut: bool,
	ty: TypeSignature,
}

impl Parse for Parameter {
	fn parse(input: ParseStream) -> Result<Self> {
		let name: Ident = input.parse()?;
		input.parse::<Token![:]>()?;

		let is_ref = if input.peek(Token![&]) {
			input.parse::<Token![&]>()?;
			true
		} else {
			false
		};

		let is_mut = if input.peek(Token![mut]) {
			input.parse::<Token![mut]>()?;
			true
		} else {
			false
		};

		let ty: TypeSignature = input.parse()?;
		Ok(Self {
			name,
			is_ref,
			is_mut,
			ty,
		})
	}
}

impl ToTokens for Parameter {
	fn to_tokens(&self, tokens: &mut TokenStream) {
		tokens.append(self.name.clone());
		tokens.append(proc_macro2::Punct::new(':', proc_macro2::Spacing::Alone));
		if self.is_ref {
			tokens.append(proc_macro2::Punct::new('&', proc_macro2::Spacing::Joint));
		}
		if self.is_mut {
			tokens.append(proc_macro2::Ident::new(
				"mut",
				proc_macro2::Span::call_site(),
			));
		}
		let ty = self.ty.clone();
		tokens.append_all(quote! { #ty });
	}
}

pub struct TypeParameterWrapper(Parameter);

impl ToTokens for TypeParameterWrapper {
	fn to_tokens(&self, tokens: &mut TokenStream) {
		let ty: TypeSignature = self.0.ty.clone();
		let expanded = if self.0.is_ref && self.0.is_mut {
			quote! {
				Type::borrow_mut::<#ty>()
			}
		} else if self.0.is_ref {
			quote! {
				Type::borrow::<#ty>()
			}
		} else {
			quote! {
				Type::owned::<#ty>()
			}
		};
		tokens.append_all(expanded);
	}
}

pub struct VariableDeclarationParameterWrapper(Parameter);

impl ToTokens for VariableDeclarationParameterWrapper {
	fn to_tokens(&self, tokens: &mut TokenStream) {
		let name: Ident = self.0.name.clone();
		let ty: TypeSignature = self.0.ty.clone();

		tokens.append_all(quote! {
			let (arg, args) = args.split_first_mut().expect("Failed to split args");
		});

		let expanded = if self.0.is_ref && self.0.is_mut {
			quote! {
				let #name: &mut #ty = arg.downcast_mut::<#ty>()?;
			}
		} else if self.0.is_ref {
			quote! {
				let #name: &#ty = arg.downcast_ref::<#ty>()?;
			}
		} else {
			quote! {
				let #name: #ty = std::mem::take(arg).downcast::<#ty>()?;
			}
		};
		tokens.append_all(expanded);
	}
}

#[allow(clippy::too_many_lines)]
impl ToTokens for FnDeclaration {
	fn to_tokens(&self, tokens: &mut TokenStream) {
		let FnDeclaration {
			name,
			self_param,
			parameters,
			return_type,
			body,
		} = self;

		let mut used_types = Vec::new();

		let body = match return_type {
			Some(return_type) => {
				used_types.push(return_type.clone());
				match return_type.access {
					Access::Owned => quote! {
						let ret: #return_type = #body;
						Ok(Some(Value::new(ret)))
					},
					Access::Ref => quote! {
						let ret: #return_type = #body;
						Ok(Some(Value::borrow(ret)))
					},
					Access::RefMut => quote! {
						let ret: #return_type = #body;
						Ok(Some(Value::borrow_mut(ret)))
					},
				}
			}
			None => {
				quote! {
					let _: () = #body;
					Ok(None)
				}
			}
		};

		let return_type_id = match return_type {
			Some(return_type) => {
				let ty = &return_type.ty;
				if let Some(generics) = &return_type.generics {
					match return_type.access {
						Access::Owned => quote! { Some(Type::owned::<#ty#generics>()) },
						Access::Ref => quote! { Some(Type::borrow::<#ty#generics>()) },
						Access::RefMut => quote! { Some(Type::borrow_mut::<#ty#generics>()) },
					}
				} else {
					match return_type.access {
						Access::Owned => quote! { Some(Type::owned::<#ty>()) },
						Access::Ref => quote! { Some(Type::borrow::<#ty>()) },
						Access::RefMut => quote! { Some(Type::borrow_mut::<#ty>()) },
					}
				}
			}
			None => quote! { None },
		};

		for parameter in parameters {
			used_types.push(parameter.ty.clone());
		}

		let type_id_args: Vec<_> = parameters
			.iter()
			.map(|p| TypeParameterWrapper(p.clone()))
			.collect();
		let variable_declarations: Vec<_> = parameters
			.iter()
			.map(|p| VariableDeclarationParameterWrapper(p.clone()))
			.collect();

		let out = match self_param {
			Some(SelfParam::Owned) => quote! {
				class.add_owned_method(
					stringify!(#name),
					MemberOwnedFunc::new(
						Box::new(|world, obj, args| {
							let obj: Self = *obj.downcast::<Self>().expect(concat!("downcast of self in MemberOwnedFunc(", stringify!(#name), ") failed"));
							#(#variable_declarations);*
							#body
						}),
						vec![
							#(#type_id_args),*
						],
						#return_type_id,
					),
				);
			},
			Some(SelfParam::Ref) => quote! {
				class.add_method(
					stringify!(#name),
					MemberRefFunc::new(
						Box::new(|world, obj, args| {
							let obj: &Self = obj.downcast_ref::<Self>().expect(concat!("downcast of self in MemberRefFunc(", stringify!(#name), ") failed"));
							#(#variable_declarations);*
							#body
						}),
						vec![
							#(#type_id_args),*
						],
						#return_type_id,
					),
				);
			},
			Some(SelfParam::RefMut) => quote! {
				class.add_mut_method(
					stringify!(#name),
					MemberMutFunc::new(
						Box::new(|world, obj, args| {
							let obj: &mut Self = obj.downcast_mut::<Self>().expect(concat!("downcast of self in MemberMutFunc(", stringify!(#name), ") failed (1)")).expect(concat!("downcast of self in MemberMutFunc(", stringify!(#name), ") failed (2)"));
							#(#variable_declarations);*
							#body
						}),
						vec![
							#(#type_id_args),*
						],
						#return_type_id,
					),
				);
			},
			None => quote! {
				(
					FunctionSignature {
						name: stringify!(#name),
						args: vec![
							#(#type_id_args),*
						],
						returns: #return_type_id,
					},
					Function(|args| {
						#(#variable_declarations);*
						#body
					})
				)
			},
		};

		tokens.append_all(out);
	}
}
