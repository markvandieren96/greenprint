use crate::{
	impl_node, Callback, Context, Error, GetType, INode, Identifier, Node, ScopeId, SetAccess,
	State, Validate, VariableId, Walkable,
};
use parser::Span;
use reflect::meta::{ClassId, Meta};

#[derive(Debug, Clone)]
pub struct VariableDeclaration {
	pub span: Span,
	pub scope: Option<ScopeId>,
	pub is_extern: bool,
	pub is_mut: bool,
	pub name: Identifier,
	pub resolved_id: Option<VariableId>,
	pub ty: Option<ClassId>,
	pub initializer: Option<Box<Node>>,
}

impl VariableDeclaration {
	#[must_use]
	pub fn new(span: Span, name: &str) -> Self {
		Self {
			span,
			scope: None,
			is_extern: false,
			is_mut: false,
			name: name.into(),
			resolved_id: None,
			ty: None,
			initializer: None,
		}
	}

	#[must_use]
	pub fn with_initializer(mut self, initializer: Box<Node>) -> Self {
		self.initializer = Some(initializer);
		self
	}

	#[must_use]
	pub fn into_node(self) -> Node {
		Node::VariableDeclaration(self)
	}
}

impl INode for VariableDeclaration {
	impl_node!("VariableDeclaration");

	#[must_use]
	fn to_src(&self, _depth: usize) -> String {
		format!("let {};", self.name)
	}
}

impl std::fmt::Display for VariableDeclaration {
	fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
		write!(
			f,
			"VariableDeclaration(let {}{}{})",
			if self.is_mut { "mut " } else { "" },
			self.name,
			if let Some(initializer) = &self.initializer {
				format!(" = {initializer}")
			} else {
				String::new()
			}
		)
	}
}

impl From<parser::nodes::VariableDeclaration> for VariableDeclaration {
	fn from(node: parser::nodes::VariableDeclaration) -> Self {
		let ty = node.ty.map(|ty| ClassId(ty.ty));

		Self {
			span: node.span,
			scope: None,
			is_extern: node.is_extern,
			is_mut: node.is_mut,
			name: node.name.into(),
			resolved_id: None,
			ty,
			initializer: node
				.initializer
				.map(|initializer| Box::new(initializer.into())),
		}
	}
}

impl GetType for VariableDeclaration {
	fn get_type(&self, _meta: &Meta, _context: &Context) -> Result<Option<ClassId>, Error> {
		Ok(None)
	}
}

impl Validate for VariableDeclaration {
	fn pass1(&mut self, meta: &Meta, ctx: &mut Context) -> Result<(), Error> {
		let Self {
			span,
			scope,
			is_extern,
			is_mut,
			name,
			ty,
			initializer,
			resolved_id,
			..
		} = self;

		*scope = Some(ctx.current_scope_expect());

		if let Some(initializer) = initializer {
			initializer.validate(meta, ctx)?;

			if let Some(initializer_ty) = initializer.get_type(meta, ctx)? {
				initializer.set_access_copy_or_move(&initializer_ty, meta, ctx, span)?;

				ctx.declare_variable(name.to_string(), *is_extern, *is_mut, ty.clone());
				ctx.assign_variable(name, initializer_ty);
			} else {
				return Err(Error::variable_initializer_wrong_type(&initializer.span())
					.expected(ClassId::any())
					.build());
			}
		} else {
			ctx.declare_variable(name.to_string(), *is_extern, *is_mut, ty.clone());
		}

		ctx.resolve_variable(resolved_id, span, name)?;

		Ok(())
	}

	fn check(&self, meta: &Meta, ctx: &Context) -> Result<(), Error> {
		let Self {
			ty, initializer, ..
		} = self;

		if let Some(initializer) = initializer {
			if let Some(initializer_ty) = initializer.get_type(meta, ctx)? {
				if let Some(ty) = ty {
					if *ty != initializer_ty {
						return Err(Error::variable_initializer_wrong_type(&initializer.span())
							.expected(ty.clone())
							.got(initializer_ty)
							.build());
					}
				}
			} else {
				return Err(Error::variable_initializer_wrong_type(&initializer.span())
					.expected(ClassId::any())
					.build());
			}
		}
		Ok(())
	}

	fn check_variable_access(
		&self,
		variables: &mut crate::VariableStates,
		_declarations: &crate::Declarations,
	) -> Result<(), Error> {
		let id = self.resolved_id.expect("Missing resolved variable id");

		if self.is_extern || self.initializer.is_some() {
			variables.set(id, State::Initialized);
		}
		Ok(())
	}
}

impl Walkable for VariableDeclaration {
	fn walk(&mut self, f: &mut Callback) -> Result<(), Error> {
		f(self)?;
		self.initializer.walk(f)
	}
}

#[cfg(test)]
mod tests {
	use super::*;
	use crate::test_util::{create_meta, literal_int};

	#[test]
	fn variable_declaration_get_type() {
		let meta = create_meta();
		let ctx = Context::default();
		let node = VariableDeclaration::new(0..1, "x");

		let res = node.get_type(&meta, &ctx).expect("get_type failed");
		assert_eq!(res, None);
	}

	#[test]
	fn variable_declaration_validate() {
		let mut ctx = Context::default();
		let meta = create_meta();
		let mut node = VariableDeclaration::new(0..1, "x").into_node();

		ctx.scope(&meta, &(0..1), |ctx| {
			node.validate(&meta, ctx).expect("validate failed");

			let var = ctx.get_variable("x").expect("missing var");
			assert_eq!(var.name, "x");
			assert!(!var.is_extern);
			assert!(!var.is_mut);
			assert_eq!(var.ty, None);
			assert_eq!(var.id, VariableId(ScopeId(0), 0));
			Ok(())
		})
		.unwrap();

		let node = node.expect_variable_declaration();
		assert_eq!(node.scope.expect("missing scope id"), ScopeId(0));
		assert_eq!(
			node.resolved_id.expect("missing resolved name"),
			VariableId(ScopeId(0), 0)
		);
	}

	#[test]
	fn variable_declaration_with_initializer_validate() {
		let mut ctx = Context::default();
		let meta = create_meta();
		let mut node = VariableDeclaration::new(0..1, "x")
			.with_initializer(Box::new(literal_int(0..1, 1).into_node()))
			.into_node();

		ctx.scope(&meta, &(0..1), |ctx| {
			node.validate(&meta, ctx).expect("validate failed");

			let var = ctx.get_variable("x").expect("missing var");
			assert_eq!(var.name, "x");
			assert!(!var.is_extern);
			assert!(!var.is_mut);
			assert_eq!(var.ty, Some(ClassId("i64".to_string())));
			assert_eq!(var.id, VariableId(ScopeId(0), 0));
			Ok(())
		})
		.unwrap();

		let node = node.expect_variable_declaration();
		assert_eq!(node.scope.expect("missing scope id"), ScopeId(0));
		assert_eq!(
			node.resolved_id.expect("missing resolved name"),
			VariableId(ScopeId(0), 0)
		);
	}
}
