use crate::{Function, FunctionId, Scope, Variable, VariableId};

#[derive(Debug, Default)]
pub struct Declarations {
	pub scopes: Vec<Scope>,
}

impl std::ops::Index<VariableId> for Declarations {
	type Output = Variable;

	fn index(&self, index: VariableId) -> &Self::Output {
		&self.scopes[index.0 .0].variables[index.1]
	}
}

impl std::ops::Index<FunctionId> for Declarations {
	type Output = Function;

	fn index(&self, index: FunctionId) -> &Self::Output {
		&self.scopes[index.0 .0].functions[index.1]
	}
}
