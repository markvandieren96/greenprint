use crate::*;

#[test]
fn test_parser_expressions() {
	assert_eq!(primary(&mut lexer("1")).unwrap(), Expression::Literal(1.into()));
	assert_eq!(primary(&mut lexer("1.0")).unwrap(), Expression::Literal(1.0.into()));
	assert_eq!(primary(&mut lexer("true")).unwrap(), Expression::Literal(true.into()));
	assert_eq!(primary(&mut lexer("false")).unwrap(), Expression::Literal(false.into()));
	assert_eq!(primary(&mut lexer(r#""Hello, World!""#)).unwrap(), Expression::Literal("Hello, World!".into()));

	assert_eq!(unary(&mut lexer("-1")).unwrap(), Expression::UnaryOp(UnaryOperator::Neg, Expression::Literal(1.into()).into()));
	assert_eq!(unary(&mut lexer("-1.0")).unwrap(), Expression::UnaryOp(UnaryOperator::Neg, Expression::Literal(1.0.into()).into()));

	assert_eq!(
		factor(&mut lexer("2 * 3")).unwrap(),
		Expression::BinaryOp(Expression::Literal(2.into()).into(), BinaryOperator::Mul, Expression::Literal(3.into()).into())
	);

	assert_eq!(
		factor(&mut lexer("2 / 3")).unwrap(),
		Expression::BinaryOp(Expression::Literal(2.into()).into(), BinaryOperator::Div, Expression::Literal(3.into()).into())
	);

	assert_eq!(
		term(&mut lexer("2 + 3")).unwrap(),
		Expression::BinaryOp(Expression::Literal(2.into()).into(), BinaryOperator::Add, Expression::Literal(3.into()).into())
	);

	assert_eq!(
		term(&mut lexer("2 - 3")).unwrap(),
		Expression::BinaryOp(Expression::Literal(2.into()).into(), BinaryOperator::Sub, Expression::Literal(3.into()).into())
	);

	assert_eq!(
		comparison(&mut lexer("2 < 3")).unwrap(),
		Expression::BinaryOp(Expression::Literal(2.into()).into(), BinaryOperator::Less, Expression::Literal(3.into()).into())
	);

	assert_eq!(
		comparison(&mut lexer("2 > 3")).unwrap(),
		Expression::BinaryOp(Expression::Literal(2.into()).into(), BinaryOperator::Greater, Expression::Literal(3.into()).into())
	);

	assert_eq!(
		comparison(&mut lexer("2 <= 3")).unwrap(),
		Expression::BinaryOp(Expression::Literal(2.into()).into(), BinaryOperator::LessEqual, Expression::Literal(3.into()).into())
	);

	assert_eq!(
		comparison(&mut lexer("2 >= 3")).unwrap(),
		Expression::BinaryOp(Expression::Literal(2.into()).into(), BinaryOperator::GreaterEqual, Expression::Literal(3.into()).into())
	);

	assert_eq!(
		equality(&mut lexer("2 == 3")).unwrap(),
		Expression::BinaryOp(Expression::Literal(2.into()).into(), BinaryOperator::Equal, Expression::Literal(3.into()).into())
	);

	assert_eq!(
		equality(&mut lexer("2 != 3")).unwrap(),
		Expression::BinaryOp(Expression::Literal(2.into()).into(), BinaryOperator::NotEqual, Expression::Literal(3.into()).into())
	);
	assert_eq!(assignment(&mut lexer("a = 2;")).unwrap(), Expression::Assignment("a".into(), Expression::Literal(2.into()).into()));

	assert_eq!(
		expression(&mut lexer("{2; 3;}")).unwrap(),
		Expression::Block(vec![Statement::Expression(Expression::Literal(2.into()), true), Statement::Expression(Expression::Literal(3.into()), true)], false)
	);
}

#[test]
fn test_parser_statements() {
	assert_eq!(statement(&mut lexer("2;")).unwrap(), Statement::Expression(Expression::Literal(2.into()), true));
	assert_eq!(declaration(&mut lexer("let a;")).unwrap(), Statement::VariableDeclaration(false, "a".into(), None, None));
	assert_eq!(declaration(&mut lexer("let a = 2;")).unwrap(), Statement::VariableDeclaration(false, "a".into(), None, Some(Expression::Literal(2.into()))));
	assert_eq!(
		declaration(&mut lexer("let a: i64 = 2;")).unwrap(),
		Statement::VariableDeclaration(false, "a".into(), Some("i64".into()), Some(Expression::Literal(2.into())))
	);
	assert_eq!(
		statement(&mut lexer("if true { 1; } else { 2; }")).unwrap(),
		Statement::Expression(
			Expression::If(
				Expression::Literal(true.into()).into(),
				Expression::Block(vec![Statement::Expression(Expression::Literal(1.into()), true)], false).into(),
				Some(Expression::Block(vec![Statement::Expression(Expression::Literal(2.into()), true)], false).into())
			),
			false
		)
	);
	assert_eq!(
		statement(&mut lexer("while true { 1; }")).unwrap(),
		Statement::While(Expression::Literal(true.into()), Expression::Block(vec![Statement::Expression(Expression::Literal(1.into()), true)], false),)
	);
	assert_eq!(
		declaration(&mut lexer("fn hello_world(arg1, arg2) { 1; }")).unwrap(),
		Statement::FunctionDeclaration("hello_world".into(), vec![("arg1".into(), None), ("arg2".into(), None)], None, vec![Statement::Expression(Expression::Literal(1.into()), true)])
	);
	assert_eq!(
		declaration(&mut lexer("fn hello_world(arg1: i64, arg2: String) { 1; }")).unwrap(),
		Statement::FunctionDeclaration(
			"hello_world".into(),
			vec![("arg1".into(), Some("i64".into())), ("arg2".into(), Some("String".into()))],
			None,
			vec![Statement::Expression(Expression::Literal(1.into()), true)]
		)
	);
	assert_eq!(
		declaration(&mut lexer("fn hello_world() -> i64 { 1 }")).unwrap(),
		Statement::FunctionDeclaration("hello_world".into(), vec![], Some("i64".into()), vec![Statement::Expression(Expression::Literal(1.into()), false)])
	);
}

fn lexer(src: &str) -> lexer::Lexer {
	lexer::Lexer::new(src)
}

#[test]
fn test_ref() {
	assert_eq!(parse_statements("&a;").unwrap(), vec![Statement::Expression(Expression::Ref(false, "a".into()), true)]);
}

#[test]
fn test_ref_mut() {
	assert_eq!(parse_statements("&mut a;").unwrap(), vec![Statement::Expression(Expression::Ref(true, "a".into()), true)]);
}
