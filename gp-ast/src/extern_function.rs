use crate::Identifier;
use reflect::{meta::ClassId, specs::World, Object};
use std::rc::Rc;

pub type ExternFn = Rc<dyn Fn(&World, &mut [Object]) -> Option<Object>>;

#[derive(Clone)]
pub struct ExternFunction {
	pub name: String,
	pub parameters: Vec<(Identifier, ClassId)>,
	pub return_type: Option<ClassId>,
	pub body: ExternFn,
}

impl std::fmt::Display for ExternFunction {
	fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
		write!(f, "{}(", self.name)?;
		let parameters = self
			.parameters
			.iter()
			.map(|(name, ty)| format!("{name}: {ty}"))
			.collect::<Vec<_>>()
			.join(", ");
		write!(f, "{parameters})")?;

		if let Some(ty) = &self.return_type {
			write!(f, " -> {ty}")?;
		}
		Ok(())
	}
}

impl std::fmt::Debug for ExternFunction {
	fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
		write!(f, "{}(", self.name)?;
		let parameters = self
			.parameters
			.iter()
			.map(|(name, ty)| format!("{name}: {ty}"))
			.collect::<Vec<_>>()
			.join(", ");
		write!(f, "{parameters})")?;

		if let Some(ty) = &self.return_type {
			write!(f, " -> {ty}")?;
		}
		Ok(())
	}
}
