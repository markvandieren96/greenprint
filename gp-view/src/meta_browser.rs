use eframe::egui;
use program::reflect::meta::Meta;

pub fn window(ctx: &egui::CtxRef, show: &mut bool, meta: &Meta) {
	egui::Window::new("meta").open(show).show(ctx, |ui| {
		egui::ScrollArea::auto_sized().show(ui, |ui| {
			display_meta(ui, meta);
		});
	});
}

fn display_meta(ui: &mut egui::Ui, meta: &Meta) {
	for (name, class) in &meta.classes {
		ui.collapsing(&name.0, |ui| {
			for (field_name, field_type) in &class.fields {
				ui.label(format!("{}: {}", field_name, field_type.0));
			}

			for (method_name, method) in &class.methods {
				let mut params: Vec<String> = method
					.parameters
					.iter()
					.map(|param| format!("{}{}", param.access, param.ty.0))
					.collect();
				let self_param = method.self_param.to_src();
				if !self_param.is_empty() {
					params.insert(0, self_param);
				}
				let returns = method.returns.as_ref().map_or_else(String::new, |returns| {
					format!(" -> {}{}", returns.access, returns.ty)
				});
				ui.label(format!("{}({}){}", method_name, params.join(", "), returns));
			}
		});
	}
}
