use crate::{LexerExpectExt, ParseResult};
use lexer::{merge_spans, Lexer, LexerExt, Span, Token};

/// Parses ": T" ": &T" ": &mut T"
#[derive(Debug, PartialEq, Clone)]
pub struct TypeAnnotation {
	pub span: Span,
	pub access: Access,
	pub ty: String,
}

impl TypeAnnotation {
	pub fn try_parse(lexer: &mut Lexer) -> ParseResult<Self> {
		let span_start = if lexer.peek_match(&Token::Colon) {
			lexer.next().unwrap().1
		} else {
			return Ok(None);
		};

		let access = Access::parse(lexer);

		let (ty, span_end) = lexer.expect_identifier("TypeAnnotation")?;

		Ok(Some(Self {
			span: merge_spans(&span_start, &span_end),
			access,
			ty,
		}))
	}

	#[must_use]
	pub fn to_src(&self) -> String {
		format!(": {}", self.ty)
	}
}

#[derive(Debug, PartialEq, Clone, Copy)]
pub enum Access {
	Owned,
	Ref,
	RefMut,
}

impl Access {
	pub fn parse(lexer: &mut Lexer) -> Self {
		let is_ref = lexer.matches(&Token::Ref);
		let is_mut = if is_ref {
			lexer.matches(&Token::Mut)
		} else {
			false
		};
		if is_ref {
			if is_mut {
				Self::RefMut
			} else {
				Self::Ref
			}
		} else {
			Self::Owned
		}
	}

	#[must_use]
	pub fn to_src(&self) -> String {
		match self {
			Access::Owned => String::new(),
			Access::Ref => "&".to_string(),
			Access::RefMut => "&mut ".to_string(),
		}
	}
}
