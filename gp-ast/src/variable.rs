use crate::VariableId;
use reflect::meta::ClassId;

#[derive(Debug)]
pub struct Variable {
	pub name: String,
	pub is_extern: bool,
	pub is_mut: bool,
	pub ty: Option<ClassId>,
	pub id: VariableId,
}

impl std::fmt::Display for Variable {
	fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
		if self.is_extern {
			write!(f, "extern ")?;
		}
		if self.is_mut {
			write!(f, "mut ")?;
		}
		write!(f, "{}", self.name)?;
		if let Some(ty) = &self.ty {
			write!(f, ": {ty}")?;
		}
		Ok(())
	}
}
