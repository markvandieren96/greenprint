use eframe::egui;
use egui::{Color32 as Color, Pos2, Ui, Vec2};

pub fn toggle(ui: &mut Ui, label: &str, state: &mut bool, enabled: bool, hover_text: &str) {
	let size = Vec2::new(25.0, 25.0);
	let response = ui
		.allocate_response(size, egui::Sense::click().union(egui::Sense::hover()))
		.on_hover_text(hover_text);
	let color = {
		if enabled {
			if response.is_pointer_button_down_on() {
				Color::WHITE
			} else if response.hovered() {
				Color::LIGHT_GRAY
			} else {
				Color::GRAY
			}
		} else {
			Color::BLACK
		}
	};

	if enabled && response.clicked() {
		*state = !*state;
	}

	let stroke = (1.0, color);

	ui.painter().rect_stroke(response.rect, 2.0, stroke);

	ui.painter().text(
		response.rect.center(),
		egui::Align2::CENTER_CENTER,
		label,
		egui::TextStyle::Monospace,
		color,
	);

	if *state {
		ui.painter().line_segment(
			[
				Pos2::new(response.rect.center().x - 5.0, response.rect.bottom() - 5.0),
				Pos2::new(response.rect.center().x + 5.0, response.rect.bottom() - 5.0),
			],
			stroke,
		);
	} else {
		ui.painter()
			.circle_stroke(response.rect.center(), 10.0, stroke);
	}
}
