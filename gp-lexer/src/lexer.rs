use crate::Token;
use log::trace;

pub use logos::Span;

#[must_use]
pub fn merge_spans(a: &Span, b: &Span) -> Span {
	a.start..b.end
}

pub struct Lexer<'a> {
	lexer: logos::SpannedIter<'a, Token>,
	consumed: Vec<(Token, Span)>,
	cursor: usize,
	src: &'a str,
}

impl<'a> Lexer<'a> {
	#[must_use]
	pub fn new(src: &'a str) -> Self {
		Self {
			lexer: logos::Lexer::new(src).spanned(),
			consumed: Vec::new(),
			cursor: 0,
			src,
		}
	}

	#[must_use]
	pub fn current(&self) -> Option<(Token, Span)> {
		self.get_at(self.cursor)
	}

	#[must_use]
	pub(crate) fn current_span(&self) -> Option<Span> {
		self.get_at(self.cursor).map(|(_, span)| span)
	}

	#[must_use]
	pub fn current_src(&self) -> Option<&'a str> {
		self.current_span().and_then(|span| self.src.get(span))
	}

	fn get_at(&self, cursor: usize) -> Option<(Token, Span)> {
		self.consumed.get(self.consumed.len() - 1 - cursor).cloned()
	}

	#[must_use]
	pub fn current_line(&self) -> usize {
		let index = self.current_span().map_or(0, |span| span.start);
		let src = &self.src[0..index];
		src.chars().filter(|c| *c == '\n').count() + 1
	}

	pub(crate) fn prev(&mut self) -> Option<(Token, Span)> {
		if self.cursor == 0 {
			let token = self.consumed.last().cloned();
			if token.is_some() {
				self.cursor = 1;
			}
			token
		} else if self.cursor >= self.consumed.len() {
			None
		} else {
			let token = self.current();
			self.cursor += 1;
			token
		}
	}

	pub fn peek(&mut self) -> Option<(Token, Span)> {
		let token = self.next();
		if token.is_some() {
			self.prev();
		}
		token
	}

	#[cfg(test)]
	pub(crate) fn next_token(&mut self) -> Option<Token> {
		self.next().map(|(t, _)| t)
	}

	#[cfg(test)]
	pub(crate) fn prev_token(&mut self) -> Option<Token> {
		self.prev().map(|(t, _)| t)
	}

	/// Parse tokens with the ability to restore the lexer to the previous state after the closure ends
	/// The bool parameter of f = `restore_state`.
	/// `restore_state` defaults to true
	pub fn lookahead<F, R>(&mut self, f: F) -> R
	where
		F: Fn(&mut Self, &mut bool) -> R,
	{
		let before = self.consumed.len() - self.cursor;
		let mut restore_state = true;
		let r = f(self, &mut restore_state);
		if restore_state {
			while self.consumed.len() - self.cursor > before {
				self.prev();
			}
			while self.consumed.len() - self.cursor < before {
				self.next();
			}
		}
		r
	}
}

impl<'a> Iterator for Lexer<'a> {
	type Item = (Token, Span);

	fn next(&mut self) -> Option<Self::Item> {
		if self.cursor == 0 {
			let token = {
				let mut t = self.lexer.next();
				while let Some((Token::Comment(_), _span)) = t {
					t = self.lexer.next();
				}
				t
			};

			if let Some((token, span)) = token.clone() {
				if token == Token::Error {
					trace!("Lexer error:");
					trace!("\tSource: \"{}\"", &self.src[span.clone()]);
				}
				self.consumed.push((token, span));
			}
			token
		} else {
			self.cursor -= 1;
			self.current()
		}
	}
}
