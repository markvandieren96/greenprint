use ast::{reflect, ScopeId, VariableId};
use reflect::{specs::World, Object};

pub struct State {
	idents: Vec<Vec<String>>,
	variables: Vec<Vec<Object>>,
}

impl State {
	#[must_use]
	pub fn new(ctx: &ast::Context) -> Self {
		let (idents, variables) = ctx.allocate_variables();
		Self { idents, variables }
	}

	pub fn take(&mut self, id: VariableId) -> Object {
		let scope: usize = id.0 .0;
		let var: usize = id.1;
		let obj = &mut self.variables[scope][var];
		obj.take()
	}

	#[must_use]
	pub fn copy(&self, id: VariableId, world: &World) -> Object {
		let scope: usize = id.0 .0;
		let var: usize = id.1;
		let obj = &self.variables[scope][var];
		obj.copy(world)
	}

	#[must_use]
	pub fn get(&self, id: VariableId) -> &Object {
		let scope: usize = id.0 .0;
		let var: usize = id.1;
		&self.variables[scope][var]
	}

	pub fn get_mut(&mut self, id: VariableId) -> &mut Object {
		let scope: usize = id.0 .0;
		let var: usize = id.1;
		&mut self.variables[scope][var]
	}

	pub fn get_scope_mut(&mut self, scope: ScopeId) -> &mut [Object] {
		&mut self.variables[scope.0]
	}

	#[must_use]
	pub fn find(&self, name: &str) -> Option<&Object> {
		for (scope_id, scope) in self.idents.iter().enumerate() {
			for (var_id, var_name) in scope.iter().enumerate() {
				if var_name == name {
					return Some(&self.variables[scope_id][var_id]);
				}
			}
		}
		None
	}

	pub fn find_mut(&mut self, name: &str) -> Option<&mut Object> {
		for (scope_id, scope) in self.idents.iter().enumerate() {
			for (var_id, var_name) in scope.iter().enumerate() {
				if var_name == name {
					return Some(&mut self.variables[scope_id][var_id]);
				}
			}
		}
		None
	}
}
