use crate::{
	nodes::{Block, ParameterList, ReturnType},
	Error, Errors, LexerExpectExt, ParseResult,
};
use lexer::{merge_spans, Lexer, LexerExt, Span, Token};

/// Parses "fn foo(t: T, t2: T2, ...) -> T3 { ... }"
#[derive(Debug, PartialEq, Clone)]
pub struct FunctionDeclaration {
	pub span: Span,
	pub name: String,
	pub parameters: ParameterList,
	pub return_type: Option<ReturnType>,
	pub body: Block,
}

impl FunctionDeclaration {
	pub fn try_parse(lexer: &mut Lexer, errors: &mut Errors) -> ParseResult<Self> {
		let span_start = if lexer.peek_match(&Token::Fn) {
			lexer.next().unwrap().1
		} else {
			return Ok(None);
		};

		let (name, _) = lexer.expect_identifier("FunctionDeclaration name")?;
		let parameters = ParameterList::try_parse(lexer)?.ok_or_else(|| {
			Error::expected(
				lexer,
				Token::ParenthesisOpen,
				"FunctionDeclaration parameters",
			)
		})?;
		let return_type = ReturnType::try_parse(lexer)?;
		let mut body = Block::try_parse(lexer, errors)?
			.ok_or_else(|| Error::expected(lexer, Token::BraceOpen, "FunctionDeclaration body"))?;

		body.catch_returns = true;
		Ok(Some(Self {
			span: merge_spans(&span_start, &body.span),
			name,
			parameters,
			return_type,
			body,
		}))
	}
}
