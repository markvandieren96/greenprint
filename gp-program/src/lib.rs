#![warn(clippy::all)]
#![warn(clippy::pedantic)]
#![allow(clippy::missing_errors_doc)]
#![allow(clippy::missing_panics_doc)]

mod line_range;
pub mod program;
mod std_lib;

pub use eval;
pub use eval::{ast, ast::parser, ast::parser::lexer, ast::reflect};
pub use program::*;
