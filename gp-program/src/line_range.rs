use crate::parser;
use parser::Span;

#[derive(Debug)]
pub struct LineRange {
	range: std::ops::Range<usize>,
}

impl LineRange {
	#[must_use]
	pub fn new(span: Span, src: &str) -> Self {
		let range = parser::lexer::convert_span_bytes_to_lines_numbers(span, src);
		Self { range }
	}

	#[must_use]
	pub fn start(&self) -> usize {
		self.range.start
	}

	#[must_use]
	pub fn end(&self) -> usize {
		self.range.end
	}
}

impl std::fmt::Display for LineRange {
	fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
		if self.start() == self.end() {
			write!(f, "at line {}", self.start())
		} else {
			write!(f, "at lines {}-{}", self.start(), self.end())
		}
	}
}
