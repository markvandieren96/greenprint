use ast::{parser::Span, reflect::Object};

pub enum Error {
	Reflect(ast::reflect::Error, Span),
	Return(Option<Object>),
}

impl Error {
	#[must_use]
	pub fn span(&self) -> Option<Span> {
		if let Error::Reflect(_, span) = self {
			Some(span.clone())
		} else {
			None
		}
	}
}

impl std::fmt::Debug for Error {
	fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
		match self {
			Error::Reflect(e, span) => f.debug_tuple("Reflect").field(e).field(span).finish(),
			Error::Return(obj) => f
				.debug_tuple("Return")
				.field(if obj.is_some() { &"Some(...)" } else { &"None" })
				.finish(),
		}
	}
}
