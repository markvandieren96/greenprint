use super::ScopeId;

#[derive(Copy, Clone)]
pub struct FunctionId(pub ScopeId, pub usize);

impl std::fmt::Debug for FunctionId {
	fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
		write!(f, "FunctionId({}:{})", self.0, self.1)
	}
}

impl std::fmt::Display for FunctionId {
	fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
		write!(f, "{}:{}", self.0, self.1)
	}
}
