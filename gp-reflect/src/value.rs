/*use crate::{extend_world_lifetime, Class, Error, GetClass};
use serde::{Deserialize, Serialize};
use specs::World;
use std::any::Any;

pub trait ThreadsafeAny: Any + Send + Sync {}
impl<T> ThreadsafeAny for T where T: Any + Send + Sync {}

#[typetag::serde]
pub trait ObjectBase: ThreadsafeAny + std::fmt::Debug {}

macro_rules! obj_base {
	($ty:ty) => {
		#[typetag::serde]
		impl ObjectBase for $ty {}
	};
}

obj_base!(i32);
obj_base!(f32);
obj_base!(String);

pub struct Object {
	value: *mut dyn ThreadsafeAny,
	class: &'static Class,
}

impl Object {
	pub fn new_ref_mut<T: 'static + GetClass + Send + Sync>(t: &mut T) -> Self {
		Self {
			value: t as *mut dyn ThreadsafeAny,
			class: t.class(),
		}
	}

	pub fn call(&self, name: &str, world: &World, args: &mut [Object]) -> Result<Option<Object>, Error> {
		log::trace!("call({})", name);

		Ok(self.class.methods.find(name).ok_or_else(|| Error::no_such_method(name, self.class))?(
			extend_world_lifetime(world),
			self,
			args,
		))
	}

	pub fn downcast<T: 'static + Default>(self) -> Result<T, ()> {
		let t = self.downcast_mut::<T>().ok_or(())?;
		Ok(std::mem::take(t))
	}

	#[must_use]
	pub fn downcast_ref<T: 'static>(&self) -> Option<&T> {
		let t: &dyn Any = unsafe { &self.value as &dyn Any };
		t.downcast_ref()
	}

	#[must_use]
	pub fn downcast_mut<T: 'static>(&mut self) -> Option<&mut T> {
		let t: &mut dyn Any = unsafe { &mut self.value as &mut dyn Any };
		t.downcast_mut()
	}
}

#[derive(Default, Debug, Serialize, Deserialize)]
pub struct Allocator {
	values: Vec<Box<dyn ObjectBase>>,
}

impl Allocator {
	pub fn alloc<T: ObjectBase + GetClass>(&mut self, t: T) -> Object {
		let class = t.class();
		let boxed = Box::new(t);
		let index = self.values.len();
		self.values.push(boxed);
		let value = &mut self.values[index] as *mut dyn ThreadsafeAny;
		Object { value, class }
	}
}

#[cfg(test)]
mod tests {
	use super::*;

	#[test]
	fn value_test() {
		let s = {
			let mut allocator = Allocator::default();
			let _ = allocator.alloc::<i32>(6);
			let _ = allocator.alloc::<i32>(7);
			let _ = allocator.alloc::<f32>(8.0);
			let _ = allocator.alloc::<String>("Hello, world!".to_owned());

			ron::to_string(&allocator).unwrap()
		};
		println!("Serialized: {}", s);

		let allocator: Allocator = ron::from_str(&s).unwrap();
		println!("Deserialized: {:#?}", allocator);

		panic!("DONE");
	}
}
*/
