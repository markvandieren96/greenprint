#![warn(clippy::all)]
#![warn(clippy::pedantic)]
#![allow(clippy::too_many_lines)]

mod app;
mod arg;
mod counter;
mod meta_browser;
mod node;
mod ui_toolkit;

fn main() -> Result<(), String> {
	simple_logger::SimpleLogger::new()
		.with_level(log::LevelFilter::Off)
		.with_module_level("greenprint", log::LevelFilter::Off)
		.with_module_level("gp_ast", log::LevelFilter::Off)
		.with_module_level("gp_eval", log::LevelFilter::Off)
		.with_module_level("gp_program", log::LevelFilter::Off)
		.init()
		.unwrap();
	let options = eframe::NativeOptions::default();
	eframe::run_native(Box::new(app::App::new()?), options);
}
