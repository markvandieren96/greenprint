use crate::{
	impl_node, Callback, Context, Error, GetType, INode, Node, ScopeId, Validate, Walkable,
};
use parser::Span;
use reflect::{
	meta::{ClassId, Meta},
	TypeName,
};

#[derive(Debug, Clone)]
pub struct Literal {
	pub span: Span,
	pub scope: Option<ScopeId>,
	pub value: parser::Literal,
}

impl Literal {
	#[must_use]
	pub fn new(span: Span, value: parser::Literal) -> Self {
		Self {
			span,
			scope: None,
			value,
		}
	}

	#[must_use]
	pub fn into_node(self) -> Node {
		Node::Literal(self)
	}
}

impl INode for Literal {
	impl_node!("Literal");

	#[must_use]
	fn to_src(&self, _depth: usize) -> String {
		self.value.to_string()
	}
}

impl std::fmt::Display for Literal {
	fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
		write!(f, "Literal({})", self.value)
	}
}

impl From<parser::nodes::LiteralValue> for Literal {
	fn from(node: parser::nodes::LiteralValue) -> Self {
		Self {
			scope: None,
			span: node.span,
			value: node.value,
		}
	}
}

impl GetType for Literal {
	fn get_type(&self, _meta: &Meta, _context: &Context) -> Result<Option<ClassId>, Error> {
		Ok(Some(match self.value {
			parser::Literal::Bool(_) => ClassId(bool::type_name()),
			parser::Literal::Integer(_) => ClassId(i64::type_name()),
			parser::Literal::Float(_) => ClassId(f32::type_name()),
			parser::Literal::String(_) => ClassId(String::type_name()),
		}))
	}
}

impl Validate for Literal {
	fn pass1(&mut self, _meta: &Meta, ctx: &mut Context) -> Result<(), Error> {
		self.scope = Some(ctx.current_scope_expect());
		Ok(())
	}
}

impl Walkable for Literal {
	fn walk(&mut self, f: &mut Callback) -> Result<(), Error> {
		f(self)
	}
}
