use crate::{
	impl_node, nodes::Block, Callback, Context, Error, GetType, INode, Identifier, Node, ScopeId,
	Validate, Walkable,
};
use parser::{nodes::ParameterList, Span};
use reflect::meta::{ClassId, Meta};

#[derive(Debug, Clone)]
pub struct FunctionDeclaration {
	pub span: Span,
	pub scope: Option<ScopeId>,
	pub name: Identifier,
	pub parameters: ParameterList,
	pub return_type: Option<ClassId>,
	pub body: Block,
}

impl FunctionDeclaration {
	#[must_use]
	pub fn into_node(self) -> Node {
		Node::FunctionDeclaration(self)
	}
}

impl INode for FunctionDeclaration {
	impl_node!("FunctionDeclaration");

	#[must_use]
	fn to_src(&self, _depth: usize) -> String {
		format!("fn {}(...)", self.name)
	}
}

impl std::fmt::Display for FunctionDeclaration {
	fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
		write!(f, "FunctionDeclaration({})", self.name)
	}
}

impl From<parser::nodes::FunctionDeclaration> for FunctionDeclaration {
	fn from(function_declaration: parser::nodes::FunctionDeclaration) -> Self {
		Self {
			span: function_declaration.span,
			scope: None,
			name: function_declaration.name.into(),
			parameters: function_declaration.parameters,
			return_type: function_declaration.return_type.map(|ty| ClassId(ty.ty)),
			body: function_declaration.body.into(),
		}
	}
}

impl GetType for FunctionDeclaration {
	fn get_type(&self, _meta: &Meta, _context: &Context) -> Result<Option<ClassId>, Error> {
		Ok(None)
	}
}

impl Validate for FunctionDeclaration {
	fn pass1(&mut self, meta: &Meta, ctx: &mut Context) -> Result<(), Error> {
		self.scope = Some(ctx.current_scope_expect());
		ctx.define_function(
			meta,
			&self.span,
			self.name.to_string(),
			&self.parameters,
			self.return_type.clone(),
			self.body.clone(),
		)
	}
}

impl Walkable for FunctionDeclaration {
	fn walk(&mut self, f: &mut Callback) -> Result<(), Error> {
		f(self)
	}
}
