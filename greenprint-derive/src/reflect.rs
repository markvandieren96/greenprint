use proc_macro::TokenStream;
use quote::quote;

pub fn impl_derive(ast: &syn::DeriveInput) -> TokenStream {
	let name = &ast.ident;

	let (impl_generics, ty_generics, where_clause) = ast.generics.split_for_impl();

	if let syn::Data::Struct(ref data_struct) = ast.data {
		let mut field_names = Vec::new();
		let mut field_types = Vec::new();
		if let syn::Fields::Named(ref fields_named) = data_struct.fields {
			for field in &fields_named.named {
				if let Some(ref ident) = field.ident {
					field_names.push(ident);
					field_types.push(&field.ty);
				}
			}
		}

		let gen = quote! {
			impl #impl_generics greenprint::Register for #name #ty_generics #where_clause {
				fn register(registry: &mut greenprint::ClassRegistry) {
					use greenprint::*;
					let mut class = Class::new::<Self>();
					#(
						reflect::field!(&mut class, #field_names: #field_types);
					)*
					registry.register(class);
				}
			}
		};
		gen.into()
	} else {
		let gen = quote! {};
		gen.into()
	}
}
