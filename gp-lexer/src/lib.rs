#![warn(clippy::all)]
#![warn(clippy::pedantic)]

mod lexer;
mod lexer_ext;
mod line_count;
mod token;

pub use lexer::*;
pub use lexer_ext::*;
pub use line_count::*;
pub use token::*;

#[cfg(test)]
mod tests;
