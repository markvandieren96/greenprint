use crate::{meta::ClassId, Access, TypeName};

#[derive(Debug, Clone, PartialEq, Eq)]
pub struct Type {
	pub ty: ClassId,
	pub access: Access,
}

impl Type {
	#[must_use]
	pub fn owned<T: 'static + TypeName>() -> Self {
		Self {
			ty: ClassId(T::type_name()),
			access: Access::Owned,
		}
	}

	#[must_use]
	pub fn borrow<T: 'static + TypeName>() -> Self {
		Self {
			ty: ClassId(T::type_name()),
			access: Access::Ref,
		}
	}

	#[must_use]
	pub fn borrow_mut<T: 'static + TypeName>() -> Self {
		Self {
			ty: ClassId(T::type_name()),
			access: Access::RefMut,
		}
	}
}
