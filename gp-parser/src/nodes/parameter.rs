use crate::{nodes::TypeAnnotation, Error, ParseResult};
use lexer::{merge_spans, Lexer, Span, Token};

/// Parses "t: T" "t: &T" "t: &mut T"
#[derive(Debug, PartialEq, Clone)]
pub struct Parameter {
	pub span: Span,
	pub name: String,
	pub ty: TypeAnnotation,
}

impl Parameter {
	pub fn try_parse(lexer: &mut Lexer) -> ParseResult<Self> {
		let (name, span) = match lexer.peek() {
			Some((Token::Identifier(name), span)) => {
				lexer.next();
				(name, span)
			}
			_ => return Ok(None),
		};

		let ty = TypeAnnotation::try_parse(lexer)?
			.ok_or(Error::MissingTypeAnnotation { at: span.end })?;
		Ok(Some(Self {
			span: merge_spans(&span, &ty.span),
			name,
			ty,
		}))
	}

	#[must_use]
	pub fn to_src(&self) -> String {
		format!("{}{}", self.name, self.ty.to_src())
	}
}
