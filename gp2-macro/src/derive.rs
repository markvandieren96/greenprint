use proc_macro2::{Ident, TokenStream};
use quote::{quote, ToTokens, TokenStreamExt};
use syn::parse::{Parse, ParseStream, Result};
use syn::punctuated::Punctuated;
use syn::Token;

pub struct Derive {
	ty: Ident,
	_comma: Token![,],
	items: Punctuated<Item, Token![,]>,
}

impl Parse for Derive {
	fn parse(input: ParseStream) -> Result<Self> {
		Ok(Self {
			ty: input.parse()?,
			_comma: input.parse()?,
			items: input.parse_terminated(Item::parse)?,
		})
	}
}

impl ToTokens for Derive {
	fn to_tokens(&self, tokens: &mut TokenStream) {
		let ty = &self.ty;
		tokens.append_all(quote! {
			reg.register_type::<#ty>();
		});

		let items = &self.items;
		for item in items {
			item.append_tokens(tokens, ty);
		}
	}
}

pub enum Item {
	Arithmetic,
	Equality,
	Ordering,
	Logical,

	OpAdd,
	OpSub,
	OpMul,
	OpDiv,

	OpAssign,
	OpBang,
	OpEq,
	OpNe,
	OpLt,
	OpGt,
	OpLe,
	OpGe,
	OpNeg,
	OpAnd,
	OpOr,

	Clone,
	Copy,

	Default,
	Drop,
	Component,
	Resource,

	ToString,
	ToI8,
	ToI16,
	ToI32,
	ToI64,
	ToI128,
	ToIsize,
	ToU8,
	ToU16,
	ToU32,
	ToU64,
	ToU128,
	ToUsize,
	ToF32,
	ToF64,
}

impl Parse for Item {
	fn parse(input: ParseStream) -> Result<Self> {
		let ident: Ident = input.parse()?;
		match ident.to_string().as_str() {
			"arithmetic" => Ok(Self::Arithmetic),
			"equality" => Ok(Self::Equality),
			"ordering" => Ok(Self::Ordering),
			"logical" => Ok(Self::Logical),
			"op_add" => Ok(Self::OpAdd),
			"op_sub" => Ok(Self::OpSub),
			"op_mul" => Ok(Self::OpMul),
			"op_div" => Ok(Self::OpDiv),
			"op_assign" => Ok(Self::OpAssign),
			"op_bang" => Ok(Self::OpBang),
			"op_eq" => Ok(Self::OpEq),
			"op_ne" => Ok(Self::OpNe),
			"op_lt" => Ok(Self::OpLt),
			"op_gt" => Ok(Self::OpGt),
			"op_le" => Ok(Self::OpLe),
			"op_ge" => Ok(Self::OpGe),
			"op_neg" => Ok(Self::OpNeg),
			"op_and" => Ok(Self::OpAnd),
			"op_or" => Ok(Self::OpOr),

			"clone" => Ok(Self::Clone),
			"copy" => Ok(Self::Copy),
			"default" => Ok(Self::Default),
			"drop" => Ok(Self::Drop),
			"component" => Ok(Self::Component),
			"resource" => Ok(Self::Resource),
			"to_string" => Ok(Self::ToString),
			"to_i8" => Ok(Self::ToI8),
			"to_i16" => Ok(Self::ToI16),
			"to_i32" => Ok(Self::ToI32),
			"to_i64" => Ok(Self::ToI64),
			"to_i128" => Ok(Self::ToI128),
			"to_isize" => Ok(Self::ToIsize),
			"to_u8" => Ok(Self::ToU8),
			"to_u16" => Ok(Self::ToU16),
			"to_u32" => Ok(Self::ToU32),
			"to_u64" => Ok(Self::ToU64),
			"to_u128" => Ok(Self::ToU128),
			"to_usize" => Ok(Self::ToUsize),
			"to_f32" => Ok(Self::ToF32),
			"to_f64" => Ok(Self::ToF64),
			_ => Err(input.error("Unknown derive item")),
		}
	}
}

impl Item {
	#[allow(clippy::too_many_lines)]
	fn append_tokens(&self, tokens: &mut TokenStream, ty: &Ident) {
		match self {
			Self::Arithmetic => {
				Self::OpAdd.append_tokens(tokens, ty);
				Self::OpSub.append_tokens(tokens, ty);
				Self::OpMul.append_tokens(tokens, ty);
				Self::OpDiv.append_tokens(tokens, ty);
			}
			Self::Equality => {
				Self::OpEq.append_tokens(tokens, ty);
				Self::OpNe.append_tokens(tokens, ty);
			}
			Self::Ordering => {
				Self::OpLt.append_tokens(tokens, ty);
				Self::OpLe.append_tokens(tokens, ty);
				Self::OpGt.append_tokens(tokens, ty);
				Self::OpGe.append_tokens(tokens, ty);
			}
			Self::Logical => {
				Self::OpAnd.append_tokens(tokens, ty);
				Self::OpOr.append_tokens(tokens, ty);
			}
			Self::OpAdd => {
				tokens.append_all(quote! {
					reg.register_function(function!(
						fn op_add(a: &#ty, b: &#ty) -> #ty {
							*a + *b
						}
					));
				});
			}
			Self::OpSub => {
				tokens.append_all(quote! {
					reg.register_function(function!(
						fn op_sub(a: &#ty, b: &#ty) -> #ty {
							*a - *b
						}
					));
				});
			}
			Self::OpMul => {
				tokens.append_all(quote! {
					reg.register_function(function!(
						fn op_mul(a: &#ty, b: &#ty) -> #ty {
							*a * *b
						}
					));
				});
			}
			Self::OpDiv => {
				tokens.append_all(quote! {
					reg.register_function(function!(
						fn op_div(a: &#ty, b: &#ty) -> #ty {
							*a / *b
						}
					));
				});
			}
			Self::OpAssign => {
				tokens.append_all(quote! {
					reg.register_function(function!(
						fn op_assign(a: &mut #ty, b: &#ty) {
							*a = *b;
						}
					));
				});
			}
			Self::OpBang => {
				tokens.append_all(quote! {
					reg.register_function(function!(
						fn op_bang(a: &#ty) -> #ty {
							!*a
						}
					));
				});
			}
			Self::OpEq => {
				tokens.append_all(quote! {
					reg.register_function(function!(
						fn op_eq(a: &#ty, other: &#ty) -> bool {
							*a == *other
						}
					));
				});
			}
			Self::OpNe => {
				tokens.append_all(quote! {
					reg.register_function(function!(
						fn op_ne(a: &#ty, other: &#ty) -> bool {
							*a != *other
						}
					));
				});
			}
			Self::OpLt => {
				tokens.append_all(quote! {
					reg.register_function(function!(
						fn op_lt(a: &#ty, other: &#ty) -> bool {
							*a < *other
						}
					));
				});
			}
			Self::OpGt => {
				tokens.append_all(quote! {
					reg.register_function(function!(
						fn op_gt(a: &#ty, other: &#ty) -> bool {
							*a > *other
						}
					));
				});
			}
			Self::OpLe => {
				tokens.append_all(quote! {
					reg.register_function(function!(
						fn op_le(a: &#ty, other: &#ty) -> bool {
							*a <= *other
						}
					));
				});
			}
			Self::OpGe => {
				tokens.append_all(quote! {
					reg.register_function(function!(
						fn op_ge(a: &#ty, other: &#ty) -> bool {
							*a >= *other
						}
					));
				});
			}
			Self::OpNeg => {
				tokens.append_all(quote! {
					reg.register_function(function!(
						fn op_neg(a: &#ty) -> #ty {
							-*a
						}
					));
				});
			}
			Self::Clone => {
				tokens.append_all(quote! {
					reg.register_function(function!(
						fn clone(a: &#ty) -> #ty {
							(*a).clone()
						}
					));
				});
			}
			Self::Copy => {
				tokens.append_all(quote! {
					reg.register_function(function!(
						fn copy(a: &#ty) -> #ty {
							(*a).clone()
						}
					));
				});
			}
			Self::OpAnd => {
				tokens.append_all(quote! {
					reg.register_function(function!(
						fn op_and(a: &#ty, other: &#ty) -> bool {
							*a && *other
						}
					));
				});
			}
			Self::OpOr => {
				tokens.append_all(quote! {
					reg.register_function(function!(
						fn op_or(a: &#ty, other: &#ty) -> bool {
							*a || *other
						}
					));
				});
			}
			Self::Default => {
				tokens.append_all(quote! {
					reg.register_static_function(<#ty>::ID, function!(
						fn default() -> #ty {
							#ty::default()
						}
					));
				});
			}
			Self::Drop => {
				tokens.append_all(quote! {
					reg.register_function(function!(
						fn op_drop(self) {}
					));
				});
			}
			Self::Component => {
				tokens.append_all(quote! {
					reg.register_function(function!(
						fn add_to_entity(self, entity: Entity) {
							world.write_storage::<Self>().insert(entity, a).unwrap();
						}
					));
					reg.register_function(function!(
						fn storage() -> ReadStorage<'static, Self> {
							world.read_storage::<Self>()
						}
					));
					reg.register_function(function!(
						fn storage_mut() -> WriteStorage<'static, Self> {
							world.write_storage::<Self>()
						}
					));
				});
			}
			Self::Resource => {
				tokens.append_all(quote! {
					reg.register_function(function!(
						fn resource() -> Fetch<'static, Self> {
							world.read_resource::<Self>()
						}
					));
					reg.register_function(function!(
						fn resource_mut() -> FetchMut<'static, Self> {
							world.write_resource::<Self>()
						}
					));
				});
			}
			Self::ToString => {
				tokens.append_all(quote! {
					reg.register_function(function!(
						fn to_string(a: &#ty) -> String {
							a.to_string()
						}
					));
				});
			}
			Self::ToI8 => {
				tokens.append_all(quote! {
					reg.register_function(function!(
						fn to_i8(a: &#ty) -> i8 {
							*a as i8
						}
					));
				});
			}
			Self::ToI16 => {
				tokens.append_all(quote! {
					reg.register_function(function!(
						fn to_i16(a: &#ty) -> i16 {
							*a as i16
						}
					));
				});
			}
			Self::ToI32 => {
				tokens.append_all(quote! {
					reg.register_function(function!(
						fn to_i32(a: &#ty) -> i32 {
							*a as i32
						}
					));
				});
			}
			Self::ToI64 => {
				tokens.append_all(quote! {
					reg.register_function(function!(
						fn to_i64(a: &#ty) -> i64 {
							*a as i64
						}
					));
				});
			}
			Self::ToI128 => {
				tokens.append_all(quote! {
					reg.register_function(function!(
						fn to_i128(a: &#ty) -> i128 {
							*a as i128
						}
					));
				});
			}
			Self::ToIsize => {
				tokens.append_all(quote! {
					reg.register_function(function!(
						fn to_isize(a: &#ty) -> isize {
							*a as isize
						}
					));
				});
			}
			Self::ToU8 => {
				tokens.append_all(quote! {
					reg.register_function(function!(
						fn to_u8(a: &#ty) -> u8 {
							*a as u8
						}
					));
				});
			}
			Self::ToU16 => {
				tokens.append_all(quote! {
					reg.register_function(function!(
						fn to_u16(a: &#ty) -> u16 {
							*a as u16
						}
					));
				});
			}
			Self::ToU32 => {
				tokens.append_all(quote! {
					reg.register_function(function!(
						fn to_u32(a: &#ty) -> u32 {
							*a as u32
						}
					));
				});
			}
			Self::ToU64 => {
				tokens.append_all(quote! {
					reg.register_function(function!(
						fn to_u64(a: &#ty) -> u64 {
							*a as u64
						}
					));
				});
			}
			Self::ToU128 => {
				tokens.append_all(quote! {
					reg.register_function(function!(
						fn to_u128(a: &#ty) -> u128 {
							*a as u128
						}
					));
				});
			}
			Self::ToUsize => {
				tokens.append_all(quote! {
					reg.register_function(function!(
						fn to_usize(a: &#ty) -> usize {
							*a as usize
						}
					));
				});
			}
			Self::ToF32 => {
				tokens.append_all(quote! {
					reg.register_function(function!(
						fn to_f32(a: &#ty) -> f32 {
							*a as f32
						}
					));
				});
			}
			Self::ToF64 => {
				tokens.append_all(quote! {
					reg.register_function(function!(
						fn to_f64(a: &#ty) -> f64 {
							*a as f64
						}
					));
				});
			}
		}
	}
}
