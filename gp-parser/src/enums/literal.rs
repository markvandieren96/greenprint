#[derive(PartialEq, Clone)]
pub enum Literal {
	Bool(bool),
	Integer(i64),
	Float(f32),
	String(String),
}

impl std::convert::From<bool> for Literal {
	fn from(value: bool) -> Self {
		Self::Bool(value)
	}
}

impl std::convert::From<&bool> for Literal {
	fn from(value: &bool) -> Self {
		Self::Bool(*value)
	}
}

impl std::convert::From<i64> for Literal {
	fn from(value: i64) -> Self {
		Self::Integer(value)
	}
}

impl std::convert::From<&i64> for Literal {
	fn from(value: &i64) -> Self {
		Self::Integer(*value)
	}
}

impl std::convert::From<f32> for Literal {
	fn from(value: f32) -> Self {
		Self::Float(value)
	}
}

impl std::convert::From<&f32> for Literal {
	fn from(value: &f32) -> Self {
		Self::Float(*value)
	}
}

impl std::convert::From<String> for Literal {
	fn from(value: String) -> Self {
		Self::String(value)
	}
}

impl std::convert::From<&String> for Literal {
	fn from(value: &String) -> Self {
		Self::String(value.clone())
	}
}

impl std::convert::From<&str> for Literal {
	fn from(value: &str) -> Self {
		Self::String(value.to_owned())
	}
}

impl std::fmt::Display for Literal {
	fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
		match self {
			Self::Bool(val) => write!(f, "{}", *val),
			Self::Integer(val) => write!(f, "{}", *val),
			Self::Float(val) => write!(f, "{}", *val),
			Self::String(val) => write!(f, "{}", *val),
		}
	}
}

impl std::fmt::Debug for Literal {
	fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
		match self {
			Self::Bool(val) => write!(f, "Literal({})", *val),
			Self::Integer(val) => write!(f, "Literal({})", *val),
			Self::Float(val) => write!(f, "Literal({})", *val),
			Self::String(val) => write!(f, "Literal({})", *val),
		}
	}
}
