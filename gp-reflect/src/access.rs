use serde::{Deserialize, Serialize};

#[derive(Debug, Clone, Copy, PartialEq, Eq, Serialize, Deserialize)]
pub enum Access {
	Owned,
	Ref,
	RefMut,
}

impl std::fmt::Display for Access {
	fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
		match self {
			Self::Owned => Ok(()),
			Self::Ref => write!(f, "&"),
			Self::RefMut => write!(f, "&mut "),
		}
	}
}
