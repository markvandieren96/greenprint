#[derive(PartialEq, Clone)]
pub struct Identifier {
	raw: String,
}

impl Identifier {
	#[must_use]
	fn new(raw: String) -> Self {
		Self { raw }
	}
}

impl std::convert::From<String> for Identifier {
	fn from(value: String) -> Self {
		Self::new(value)
	}
}

impl std::convert::From<&str> for Identifier {
	fn from(value: &str) -> Self {
		Self::new(value.to_owned())
	}
}

impl std::convert::From<&String> for Identifier {
	fn from(value: &String) -> Self {
		Self::new(value.clone())
	}
}

impl std::ops::Deref for Identifier {
	type Target = String;

	fn deref(&self) -> &Self::Target {
		&self.raw
	}
}

impl std::fmt::Display for Identifier {
	fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
		write!(f, "{}", self.raw)
	}
}

impl std::fmt::Debug for Identifier {
	fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
		write!(f, "Identifier(\"{}\")", self.raw)
	}
}
