use reflect::meta::{ClassId, Meta};

use parser::nodes::Access as ParserAccess;

#[derive(Debug, Clone, Copy, PartialEq)]
pub enum Access {
	Copy,
	Move,
	Ref,
	RefMut,
}

impl Access {
	#[must_use]
	pub fn as_src(&self) -> &str {
		match self {
			Self::Move | Self::Copy => "",
			Self::Ref => "&",
			Self::RefMut => "&mut ",
		}
	}

	pub fn from(access: &ParserAccess, ty: &ClassId, meta: &Meta) -> Result<Self, reflect::Error> {
		Ok(match access {
			ParserAccess::Owned => {
				if meta.get(ty)?.has_method_ref("copy") {
					Self::Copy
				} else {
					Self::Move
				}
			}
			ParserAccess::Ref => Self::Ref,
			ParserAccess::RefMut => Self::RefMut,
		})
	}
}

impl From<Access> for reflect::Access {
	fn from(access: Access) -> Self {
		match access {
			Access::Copy | Access::Move => reflect::Access::Owned,
			Access::Ref => reflect::Access::Ref,
			Access::RefMut => reflect::Access::RefMut,
		}
	}
}
