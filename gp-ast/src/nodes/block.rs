use crate::{
	impl_node, Callback, Context, Error, GetType, INode, Node, ScopeId, Validate, VariableId,
	Walkable,
};
use parser::Span;
use reflect::meta::{ClassId, Meta};

#[derive(Debug, Clone, Default)]
pub struct Block {
	pub span: Span,
	pub scope: Option<ScopeId>,
	pub body: Vec<Node>,
	pub drops: Vec<VariableId>,
	pub catch_returns: bool,
}

impl Block {
	#[must_use]
	pub fn into_node(self) -> Node {
		Node::Block(self)
	}

	#[must_use]
	pub fn replace_free_with_extern_call_nodes(self, ctx: &Context) -> Self {
		let mut node = self.into_node();
		crate::replace_free_with_extern_call_nodes(&mut *node, &ctx.externs);
		node.expect_block()
	}
}

impl INode for Block {
	impl_node!("Block");

	#[must_use]
	fn to_src(&self, depth: usize) -> String {
		let content = self
			.body
			.iter()
			.map(|statement| format!("{}{}\n", "\t".repeat(depth + 1), statement.to_src(depth)))
			.collect::<String>();
		format!("{{\n{content}\n}}")
	}
}

impl std::fmt::Display for Block {
	fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
		write!(
			f,
			"Block({{\n{}\n}}{})",
			self.body
				.iter()
				.map(|statement| format!("\t{statement}"))
				.collect::<Vec<String>>()
				.join("\n"),
			if self.catch_returns { "(catches)" } else { "" }
		)
	}
}

impl From<parser::nodes::Block> for Block {
	fn from(node: parser::nodes::Block) -> Self {
		Self {
			span: node.span,
			scope: None,
			body: node.body.into_iter().map(std::convert::Into::into).collect(),
			drops: Vec::new(),
			catch_returns: node.catch_returns,
		}
	}
}

impl Validate for Block {
	fn pass1(&mut self, meta: &Meta, ctx: &mut Context) -> Result<(), Error> {
		log::trace!("validating block:\n{}", self.to_src(1));
		let Self {
			span,
			body,
			scope,
			drops,
			..
		} = self;

		let dropped_variables = ctx.scope(meta, span, |ctx| {
			*scope = Some(ctx.current_scope_expect());
			for statement in body.iter_mut() {
				statement.validate(meta, ctx)?;
			}
			Ok(())
		})?;
		*drops = dropped_variables;

		log::trace!("validated block");
		Ok(())
	}
}

impl GetType for Block {
	fn get_type(&self, meta: &Meta, ctx: &Context) -> Result<Option<ClassId>, Error> {
		log::trace!("getting type of block");

		let mut ret = None;
		for statement in &self.body {
			ret = statement.get_type(meta, ctx)?;
		}

		log::trace!("got type of block: {:?}", ret);
		Ok(ret)
	}
}

impl Walkable for Block {
	fn walk(&mut self, f: &mut Callback) -> Result<(), Error> {
		f(self)?;
		self.body.walk(f)
	}
}
