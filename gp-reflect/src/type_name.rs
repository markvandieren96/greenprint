use crate::meta::ClassId;
use specs::Entity;

pub trait TypeName {
	fn type_name() -> String;

	#[must_use]
	fn class_id() -> ClassId {
		ClassId(Self::type_name())
	}
}

#[macro_export]
macro_rules! impl_type_name {
	($t:ty) => {
		impl $crate::TypeName for $t {
			fn type_name() -> String {
				stringify!($t).to_owned()
			}
		}
	};
	($t:ty, $name:expr) => {
		impl $crate::TypeName for $t {
			fn type_name() -> String {
				$name
			}
		}
	};
}

impl_type_name!(());
impl_type_name!(bool);
impl_type_name!(i8);
impl_type_name!(i16);
impl_type_name!(i32);
impl_type_name!(i64);
impl_type_name!(u8);
impl_type_name!(u16);
impl_type_name!(u32);
impl_type_name!(u64);
impl_type_name!(f32);
impl_type_name!(f64);
impl_type_name!(usize);
impl_type_name!(String);
impl_type_name!(Entity);

impl<T: TypeName> TypeName for Option<T> {
	fn type_name() -> String {
		format!("Option_{}", T::type_name())
	}
}

impl<T: TypeName + specs::Component> TypeName for specs::ReadStorage<'static, T> {
	fn type_name() -> String {
		format!("ReadStorage_{}", T::type_name())
	}
}

impl<T: TypeName + specs::Component> TypeName for specs::WriteStorage<'static, T> {
	fn type_name() -> String {
		format!("WriteStorage_{}", T::type_name())
	}
}

impl<T: TypeName> TypeName for specs::shred::Fetch<'static, T> {
	fn type_name() -> String {
		format!("Fetch_{}", T::type_name())
	}
}

impl<T: TypeName> TypeName for specs::shred::FetchMut<'static, T> {
	fn type_name() -> String {
		format!("FetchMut_{}", T::type_name())
	}
}
