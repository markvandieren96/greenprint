use crate::{
	field, impl_type_name, Class, ClassRegistry, FreeFunc, MemberMutFunc, MemberRefFunc, Object,
	Register, Type,
};
use gp_reflect_derive::reflect;

use glam::f32::Vec3;
impl_type_name!(Vec3, "Vec3f".to_owned());
impl Register for Vec3 {
	fn register(registry: &mut ClassRegistry) {
		let mut class = Class::new::<Self>();
		field!(&mut class, x: f32);
		field!(&mut class, y: f32);
		field!(&mut class, z: f32);
		reflect!(
			derive(=, default, clone, copy, arithmetic, equality, neg, to_string)

			fn new(x: f32, y: f32, z: f32) -> Self {
				Self::new(x, y, z)
			}

			fn normalized(&self) -> Self {
				obj.normalize()
			}

			fn length(&self) -> f32 {
				obj.length()
			}
		);
		registry.register(class);
	}
}

use glam::f32::Vec2;
impl_type_name!(Vec2, "Vec2f".to_owned());
impl Register for Vec2 {
	fn register(registry: &mut ClassRegistry) {
		let mut class = Class::new::<Self>();
		field!(&mut class, x: f32);
		field!(&mut class, y: f32);
		reflect!(
			derive(=, default, clone, copy, arithmetic, equality, neg, to_string)

			fn new(x: f32, y: f32) -> Self {
				Self::new(x, y)
			}

			fn normalized(&self) -> Self {
				obj.normalize()
			}

			fn length(&self) -> f32 {
				obj.length()
			}
		);
		registry.register(class);
	}
}

use glam::f32::Quat;
impl_type_name!(Quat, "Quat".to_owned());
impl Register for Quat {
	fn register(registry: &mut ClassRegistry) {
		let mut class = Class::new::<Self>();
		reflect!(
			derive(=, *, default, clone, copy, equality, neg, to_string)

			fn from_axis_angle(axis: Vec3, angle: f32) -> Self {
				Self::from_axis_angle(axis, angle)
			}

			fn from_euler(x: f32, y: f32, z: f32) -> Self {
				Self::from_euler(glam::EulerRot::XYZ, x, y, z)
			}

			fn mul_vec3f(&self, v: &Vec3) -> Vec3 {
				obj.mul_vec3(*v)
			}
		);
		registry.register(class);
	}
}
