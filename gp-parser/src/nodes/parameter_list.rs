use crate::{nodes::Parameter, LexerExpectExt, ParseResult};
use lexer::{merge_spans, Lexer, LexerExt, Span, Token};

/// Parses "(t: T, t2: T2, ...)"
#[derive(Debug, PartialEq, Clone)]
pub struct ParameterList {
	pub span: Span,
	pub parameters: Vec<Parameter>,
}

impl ParameterList {
	pub fn try_parse(lexer: &mut Lexer) -> ParseResult<Self> {
		let span_start = if lexer.peek_match(&Token::ParenthesisOpen) {
			lexer.next().unwrap().1
		} else {
			return Ok(None);
		};

		let mut parameters = Vec::new();
		if !lexer.peek_match(&Token::ParenthesisClose) {
			loop {
				if let Some(param) = Parameter::try_parse(lexer)? {
					parameters.push(param);
				}

				if !lexer.matches(&Token::Comma) {
					break;
				}
			}
		}

		let span_end = lexer.expect(Token::ParenthesisClose, "ParameterList")?;

		Ok(Some(Self {
			span: merge_spans(&span_start, &span_end),
			parameters,
		}))
	}
}
