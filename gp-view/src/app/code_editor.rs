use eframe::egui;
use egui::Ui;
use std::path::{Path, PathBuf};

pub struct CodeEditor {
	file: PathBuf,
	src_unchanged: String,
	src: String,
	unsaved_changes: bool,
	save_on_exit: bool,
}

impl CodeEditor {
	pub fn new(file: &Path) -> Result<Self, io::Error> {
		let src = io::read_text(&file)?;
		Ok(Self {
			file: file.to_owned(),
			src_unchanged: src.clone(),
			src,
			unsaved_changes: false,
			save_on_exit: true,
		})
	}

	fn try_save(&mut self) {
		if self.unsaved_changes {
			match io::write_text(&self.file, &self.src) {
				Ok(()) => {
					println!(
						"File{} saved successfully",
						self.file
							.file_stem()
							.and_then(std::ffi::OsStr::to_str)
							.map(|s| format!("({})", s))
							.unwrap_or_default()
					);
					self.unsaved_changes = false;
				}
				Err(e) => println!("Error: {}", e),
			}
		}
	}

	pub fn display(&mut self, ui: &mut Ui) -> Option<String> {
		let Self {
			src_unchanged,
			src,
			unsaved_changes,
			save_on_exit,
			..
		} = self;
		let mut save_file = false;

		ui.horizontal(|ui| {
			if ui
				.add(
					egui::Button::new(format!("{}save", if *unsaved_changes { "* " } else { "" }))
						.enabled(*unsaved_changes),
				)
				.clicked()
			{
				save_file = true;
			}
			ui.separator();

			ui.checkbox(save_on_exit, "save on exit");
			ui.separator();
		});

		ui.separator();

		let mut something_changed = false;
		if ui.code_editor(src).changed() {
			something_changed = true;
			*unsaved_changes = src_unchanged != src;
		}

		if save_file {
			self.try_save();
		}

		if something_changed {
			Some(self.src.clone())
		} else {
			None
		}
	}
}

impl Drop for CodeEditor {
	fn drop(&mut self) {
		if self.save_on_exit {
			self.try_save();
		}
	}
}
