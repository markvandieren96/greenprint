#![warn(clippy::all)]
#![warn(clippy::pedantic)]
#![allow(clippy::missing_errors_doc)]
#![allow(clippy::missing_panics_doc)]

mod context;
mod convert;
mod error;
mod eval;
mod tree;

use crate::{context::Context, convert::Variant};
use convert::AstNode;
use error::Error;
use gp2::{Registry, Value};
use gp_parser::nodes::Access;

#[must_use]
pub fn eval_src(reg: &Registry, src: &str) -> Option<Value> {
	let expr = gp_parser::parse(src).unwrap();
	let node = convert::expression_into_ast_node(0, &expr);
	print_tree(&node).unwrap();
	let mut ctx = Context::new();
	eval::eval(reg, &mut ctx, &node, Access::Owned).unwrap()
}

pub fn print_tree(ast: &AstNode) -> Result<(), Error> {
	ast.visit(&|node| {
		println!("{}{:?}", "    ".repeat(node.data.depth), node.data.data);
		Ok(())
	})
}

#[cfg(test)]
mod tests {
	use super::*;
	use gp2::stid::{self, Stid};

	fn eval<T: 'static + Stid + Copy>(src: &str) -> T {
		eval_src(&Registry::new(), src)
			.unwrap()
			.downcast_copy::<T>()
			.unwrap()
	}

	fn eval_x<T: 'static + Stid + Copy>(reg: &Registry, src: &str) -> T {
		eval_src(reg, src).unwrap().downcast_copy::<T>().unwrap()
	}

	fn eval_clone<T: 'static + Stid + Clone>(src: &str) -> T {
		eval_src(&Registry::new(), src)
			.unwrap()
			.downcast_ref::<T>()
			.unwrap()
			.clone()
	}

	#[test]
	fn eval_literal() {
		assert_eq!(eval::<i64>("6"), 6);
	}

	#[test]
	fn eval_binary_op() {
		assert_eq!(eval::<i64>("4 + 2"), 6);
		assert_eq!(
			eval_clone::<String>(r#""hello" + " world""#),
			"hello world".to_string()
		);
		assert!(eval::<bool>("true or false"));
		assert!(eval::<bool>("true and true"));
		assert!(!eval::<bool>("true and false"));
	}

	#[test]
	fn eval_unary_op() {
		assert_eq!(eval::<i64>("-6"), -6);
		assert!(!eval::<bool>("!true"));
	}

	#[test]
	fn eval_variable() {
		assert_eq!(eval::<i64>("{ let a = 4; a + 2 }"), 6);
	}

	#[test]
	fn eval_group() {
		assert_eq!(eval::<i64>("(1 + 2) * 2"), 6);
	}

	#[test]
	fn eval_branch() {
		assert_eq!(eval::<i64>("if true { 1 }"), 1);
		assert_eq!(eval::<i64>("{ let a = true; if a { 1 } }"), 1);
		assert_eq!(eval::<i64>("{ let a = 1; if true { a } }"), 1);

		assert_eq!(eval::<i64>("if false { 1 } else { 2 }"), 2);
		assert_eq!(eval::<i64>("{ let a = false; if a { 1 } else { 2 } }"), 2);
		assert_eq!(eval::<i64>("{ let a = 2; if false { 1 } else { a } }"), 2);

		assert_eq!(
			eval::<i64>("if false { 1 } else if false { 2 } else { 3 }"),
			3
		);
		assert_eq!(
			eval::<i64>("{ let a = false; let b = false; if a { 1 } else if b { 2 } else { 3 } }"),
			3
		);
		assert_eq!(
			eval::<i64>("{ let a = 3; if false { 1 } else if false { 2 } else { a } }"),
			3
		);
	}

	#[test]
	fn eval_fn_declaration1() {
		assert_eq!(
			eval::<i64>(
				"{
			fn hello_world() {
				6
			}
			hello_world()
		}"
			),
			6
		);
		assert_eq!(
			eval::<i64>(
				"{
			fn hello_world(a: i64) {
				2 + a
			}
			hello_world(4)
		}"
			),
			6
		);
		assert_eq!(
			eval_clone::<String>(
				r#"{
			"hello"
		}"#
			),
			"hello".to_owned()
		);
		assert_eq!(
			eval_clone::<String>(
				r#"{
			fn hello_world() {
				"hello"
			}
			hello_world()
		}"#
			),
			"hello".to_owned()
		);
		assert_eq!(
			eval_clone::<String>(
				r#"{
			fn hello_world(a: &String) {
				"hello" + a
			}
			hello_world(" world")
		}"#
			),
			"hello world".to_owned()
		);
	}

	#[derive(Debug, PartialEq, Clone, Copy)]
	struct Dummy {
		i: u64,
	}
	stid::stid!(Dummy);

	fn register_dummy(reg: &mut Registry) {
		use gp2::{Function, FunctionSignature, Type};

		reg.register_type::<Dummy>();
		reg.register_static_function(
			Dummy::ID,
			gp2::gp2_macro::function!(
				fn new() -> Dummy {
					Dummy { i: 6 }
				}
			),
		);
	}

	#[test]
	fn eval_static_fn() {
		let mut reg = Registry::new();
		register_dummy(&mut reg);
		assert_eq!(
			eval_x::<Dummy>(
				&reg,
				"{
			Dummy::new()
		}"
			),
			Dummy { i: 6 }
		);
	}
}
