use crate::{
	context::{Context, Func, Var},
	convert::{FunctionDeclaration, VariableDeclaration},
	AstNode, Error, Variant,
};
use gp2::{
	stid::{Id, Stid},
	Function, FunctionSignature, Registry, Value,
};
use gp_parser::{nodes::Access, Literal};

pub fn eval(
	reg: &Registry,
	ctx: &mut Context,
	node: &AstNode,
	access: Access,
) -> Result<Option<Value>, Error> {
	match &node.data.data {
		Variant::FunctionDeclaration(declaration) => eval_func_decl(ctx, node, declaration),
		Variant::Variable(name) => eval_var(ctx, name, access),
		Variant::VariableDeclaration(declaration) => eval_var_decl(reg, ctx, node, declaration),
		Variant::Block => eval_block(reg, ctx, node),
		Variant::ExpressionStatement => eval_expression_statement(reg, ctx, node),
		Variant::Literal(literal) => Ok(Some(eval_literal(literal))),
		Variant::Call(name, class) => eval_call(reg, ctx, node, class, name, access),
		Variant::Branch => eval_branch(reg, ctx, node),
		Variant::Group => eval_group(reg, ctx, node, access),
	}
}

fn eval_func_decl(
	ctx: &mut Context,
	node: &AstNode,
	declaration: &FunctionDeclaration,
) -> Result<Option<Value>, Error> {
	ctx.declare_func(Func {
		declaration: declaration.clone(),
		body: node
			.children
			.first()
			.ok_or(Error::FunctionDeclarationHasNoBody)?
			.clone(),
	});
	Ok(None)
}

fn eval_var(ctx: &mut Context, name: &str, access: Access) -> Result<Option<Value>, Error> {
	match access {
		Access::Owned => Ok(Some(
			ctx.get_mut(name)?.take().ok_or(Error::VariableHasNoValue)?,
		)),
		Access::Ref => Ok(Some(
			ctx.get(name)?
				.as_ref()
				.ok_or(Error::VariableHasNoValue)?
				.create_ref(),
		)),
		Access::RefMut => Ok(Some(
			ctx.get_mut(name)?
				.as_mut()
				.ok_or(Error::VariableHasNoValue)?
				.create_ref_mut()?,
		)),
	}
}

fn eval_var_decl(
	reg: &Registry,
	ctx: &mut Context,
	node: &AstNode,
	declaration: &VariableDeclaration,
) -> Result<Option<Value>, Error> {
	let value = {
		if let Some(initializer) = node.children.first() {
			eval(reg, ctx, initializer, Access::Owned)?
		} else {
			None
		}
	};
	ctx.declare_var(Var {
		name: declaration.name.clone(),
		value,
	});
	Ok(None)
}

fn eval_block(reg: &Registry, ctx: &mut Context, node: &AstNode) -> Result<Option<Value>, Error> {
	let mut out = None;
	ctx.scope(reg, |ctx| {
		for child in &node.children {
			out = Some(eval(reg, ctx, child, Access::Owned)?);
		}
		Ok(())
	})?;

	out.ok_or(Error::BlockDidNotReturnAValue)
}

fn eval_expression_statement(
	reg: &Registry,
	ctx: &mut Context,
	node: &AstNode,
) -> Result<Option<Value>, Error> {
	node.children
		.first()
		.map_or(Err(Error::ExpressionWithoutBody), |child| {
			eval(reg, ctx, child, Access::Owned)
		})
}

fn eval_literal(literal: &Literal) -> Value {
	match literal {
		Literal::Bool(val) => Value::new(*val),
		Literal::Integer(val) => Value::new(*val),
		Literal::Float(val) => Value::new(*val),
		Literal::String(val) => {
			let s: String = val.clone();
			Value::new(s)
		}
	}
}

fn eval_call(
	reg: &Registry,
	ctx: &mut Context,
	node: &AstNode,
	class: &Option<String>,
	name: &str,
	access: Access,
) -> Result<Option<Value>, Error> {
	let args = get_typelist(reg, ctx, &node.children)?;
	let class = lookup_opt(reg, class)?;
	if let Some((sig, f)) = reg.get_function_x(class, name, &args) {
		call_func(reg, ctx, &node.children, sig, *f)
	} else if let Some(func) = ctx.get_func(name).cloned() {
		ctx.scope(reg, |ctx| {
			for ((param_name, ty), node) in
				func.declaration.parameters.iter().zip(node.children.iter())
			{
				let value = eval(reg, ctx, node, ty.access)?;
				ctx.declare_var(Var {
					name: param_name.clone(),
					value,
				});
			}
			eval(reg, ctx, &func.body, access)
		})
	} else {
		Err(Error::FunctionNotFound)
	}
}

fn lookup_opt(reg: &Registry, class: &Option<String>) -> Result<Option<Id>, Error> {
	if let Some(class) = class {
		Ok(Some(lookup(reg, class)?))
	} else {
		Ok(None)
	}
}

fn lookup(reg: &Registry, class: &str) -> Result<Id, Error> {
	reg.typemap.find(class).ok_or(Error::TypeIsNotRegistered)
}

fn get_typelist(reg: &Registry, ctx: &Context, nodes: &[AstNode]) -> Result<Vec<Id>, Error> {
	nodes
		.iter()
		.map(|child| {
			let ty = get_type(reg, ctx, child)?;
			ty.ok_or(Error::ArgumentHasNoType)
		})
		.collect::<Result<Vec<_>, Error>>()
}

fn call_func(
	reg: &Registry,
	ctx: &mut Context,
	args: &[AstNode],
	sig: &FunctionSignature,
	f: Function,
) -> Result<Option<Value>, Error> {
	let mut args: Vec<_> = args
		.iter()
		.zip(sig.args.iter())
		.map(|(child, ty)| {
			let val = eval(reg, ctx, child, ty.access())?;
			val.ok_or(Error::ArgumentHasNoType)
		})
		.collect::<Result<_, _>>()?;
	let v = f.0(&mut args)?;
	Ok(v)
}

fn eval_branch(reg: &Registry, ctx: &mut Context, node: &AstNode) -> Result<Option<Value>, Error> {
	if let Some(condition) = node.children.first() {
		let condition = eval(reg, ctx, condition, Access::Ref)?
			.ok_or(Error::BranchConditionHasNoValue)?
			.downcast_copy::<bool>()?;

		if condition {
			eval(
				reg,
				ctx,
				node.children.get(1).ok_or(Error::BranchHasNoBody)?,
				Access::Owned,
			)
		} else if let Some(else_branch) = node.children.get(2) {
			eval(reg, ctx, else_branch, Access::Owned)
		} else {
			Ok(None)
		}
	} else {
		Err(Error::BranchHasNoCondition)
	}
}

fn eval_group(
	reg: &Registry,
	ctx: &mut Context,
	node: &AstNode,
	access: Access,
) -> Result<Option<Value>, Error> {
	node.children.first().map_or_else(
		|| Ok(Some(Value::new(()))),
		|node| eval(reg, ctx, node, access),
	)
}

fn get_type(reg: &Registry, ctx: &Context, node: &AstNode) -> Result<Option<Id>, Error> {
	match &node.data.data {
		Variant::VariableDeclaration(_) | Variant::FunctionDeclaration(_) => Ok(None),
		Variant::Literal(literal) => match literal {
			gp_parser::Literal::Bool(_) => Ok(Some(bool::ID)),
			gp_parser::Literal::Integer(_) => Ok(Some(i64::ID)),
			gp_parser::Literal::Float(_) => Ok(Some(f32::ID)),
			gp_parser::Literal::String(_) => Ok(Some(String::ID)),
		},
		Variant::Variable(name) => ctx.get(name).map(|v| v.as_ref().map(Value::id)),
		Variant::Group => node
			.children
			.first()
			.map_or(Ok(Some(<()>::ID)), |node| get_type(reg, ctx, node)),
		Variant::Call(name, class) => {
			let args = get_typelist(reg, ctx, &node.children)?;
			let class = lookup_opt(reg, class)?;
			if let Some((sig, _f)) = reg.get_function_x(class, name, &args) {
				Ok(sig.returns.as_ref().map(|ty| ty.id()))
			} else if let Some(func) = ctx.get_func(name).cloned() {
				get_type(reg, ctx, &func.body)
			} else {
				Err(Error::FunctionNotFound)
			}
		}
		Variant::Block | Variant::ExpressionStatement | Variant::Branch => {
			todo!()
		}
	}
}
