use crate::{
	impl_node, nodes::Block, Access, CallContext, Callback, Context, Error, FunctionId, GetType,
	INode, Identifier, Node, ScopeId, SetAccess, State, Validate, VariableId, Walkable,
};
use parser::Span;
use reflect::meta::{ClassId, Meta, TypeWithAccess};

#[derive(Debug, Clone)]
pub struct Call {
	pub span: Span,
	pub scope: Option<ScopeId>,
	pub name: Identifier,
	pub arguments: Vec<Node>,
	pub call_type: CallType,
}

#[allow(clippy::module_name_repetitions)]
#[derive(Debug, Clone)]
pub enum CallType {
	Free {
		resolved_id: Option<FunctionId>,
		inlined_body: Option<Block>,
		argument_ids: Vec<VariableId>,
	},
	Method {
		object: Box<Node>,
		self_type: Access,
	},
	Extern {
		id: Option<usize>,
	},
	Static {
		class: ClassId,
	},
}

impl Call {
	#[must_use]
	pub fn into_node(self) -> Node {
		Node::Call(self)
	}
}

impl INode for Call {
	impl_node!("Call");

	#[must_use]
	fn to_src(&self, _depth: usize) -> String {
		let mut args = Vec::new();
		for arg in &self.arguments {
			args.push(arg.to_src(0));
		}

		match &self.call_type {
			CallType::Free { .. } => format!("{}({})", self.name, args.join(", ")),
			CallType::Method { object, self_type } => {
				args.insert(0, format!("{}self", self_type.as_src()));

				format!("{}.{}({})", object.to_src(0), self.name, args.join(", "))
			}
			CallType::Extern { id } => {
				format!(
					"ExternCall(id={})",
					id.map_or_else(|| "MISSING".to_owned(), |id| id.to_string())
				)
			}
			CallType::Static { class } => format!("{}::{}({})", class, self.name, args.join(", ")),
		}
	}
}

impl From<parser::nodes::FreeFunctionCall> for Call {
	fn from(node: parser::nodes::FreeFunctionCall) -> Self {
		Self {
			span: node.span,
			scope: None,
			name: node.name.into(),
			arguments: node
				.arguments
				.arguments
				.into_iter()
				.map(std::convert::Into::into)
				.collect(),
			call_type: CallType::Free {
				resolved_id: None,
				inlined_body: None,
				argument_ids: Vec::new(),
			},
		}
	}
}

impl From<parser::nodes::StaticFunctionCall> for Call {
	fn from(node: parser::nodes::StaticFunctionCall) -> Self {
		Self {
			span: node.span,
			scope: None,
			name: node.name.into(),
			arguments: node
				.arguments
				.arguments
				.into_iter()
				.map(std::convert::Into::into)
				.collect(),
			call_type: CallType::Static {
				class: ClassId(node.class),
			},
		}
	}
}

impl From<parser::nodes::MethodCall> for Call {
	fn from(node: parser::nodes::MethodCall) -> Self {
		Self {
			span: node.span,
			scope: None,
			name: node.name.into(),
			arguments: node
				.arguments
				.arguments
				.into_iter()
				.map(std::convert::Into::into)
				.collect(),
			call_type: CallType::Method {
				object: Box::new(node.object.into()),
				self_type: Access::Ref,
			},
		}
	}
}

#[allow(clippy::option_if_let_else)]
impl GetType for Call {
	fn get_type(&self, meta: &Meta, ctx: &Context) -> Result<Option<ClassId>, Error> {
		let Self {
			span,
			name,
			call_type,
			..
		} = self;

		match call_type {
			CallType::Free { resolved_id, .. } => {
				let function =
					&ctx.declarations[resolved_id.expect("Missing resolved function id")];
				Ok(function.return_type.clone())
			}
			CallType::Method { object, .. } => {
				let object_type = object
					.get_type(meta, ctx)?
					.ok_or_else(|| Error::method_call_on_undefined(span, name))?;

				let class = meta
					.get(&object_type)
					.map_err(|error| Error::reflect(span, error))?;
				if let Some(function) = class.get_method_ref(name) {
					Ok(function.returns.as_ref().map(|returns| returns.ty.clone()))
				} else if let Some(function) = class.get_method_ref_mut(name) {
					Ok(function.returns.as_ref().map(|returns| returns.ty.clone()))
				} else if let Some(function) = class.get_method_owned(name) {
					Ok(function.returns.as_ref().map(|returns| returns.ty.clone()))
				} else {
					Err(Error::undefined_method(span, object_type, name))
				}
			}
			CallType::Extern { .. } => Ok(ctx
				.get_return_type(&self.name)
				.expect("Extern fn not found")),
			CallType::Static { class } => {
				let class_meta = meta
					.get(class)
					.map_err(|error| Error::reflect(span, error))?;
				let function = class_meta
					.get_method_static(name)
					.ok_or_else(|| Error::undefined_static_function(span, class.clone(), name))?;
				Ok(function.returns.as_ref().map(|returns| returns.ty.clone()))
			}
		}
	}
}

impl std::fmt::Display for Call {
	fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
		match &self.call_type {
			CallType::Free { .. } => write!(f, "Call({}(...))", self.name),
			CallType::Method { object, self_type } => write!(
				f,
				"Call({}.{}({}self, ...))",
				object,
				self.name,
				self_type.as_src()
			),
			CallType::Extern { id } => write!(
				f,
				"Call(extern id={})",
				id.map_or_else(|| "MISSING".to_owned(), |id| id.to_string())
			),
			CallType::Static { class } => write!(f, "Call({}::{}(...))", class, self.name),
		}
	}
}

impl Validate for Call {
	#[allow(clippy::too_many_lines)]
	fn pass1(&mut self, meta: &Meta, ctx: &mut Context) -> Result<(), Error> {
		self.scope = Some(ctx.current_scope_expect());

		/*if let CallType::Free { .. } = self.call_type {
			println!("Checking if {} is extern: {}", self.name, ctx.externs.has_function(&self.name));
			if ctx.externs.has_function(&self.name) {
				self.call_type = CallType::Extern { id: None };
			}
		}*/

		let Self {
			span,
			name,
			arguments,
			call_type,
			..
		} = self;

		match call_type {
			CallType::Free {
				resolved_id,
				inlined_body,
				argument_ids,
				..
			} => {
				ctx.resolve_function(resolved_id, span, name)?;

				let function = &ctx.declarations[resolved_id.unwrap()];
				log::trace!("inlining free function call body ({})", name);
				let mut body = function.body.clone();
				body = body.replace_free_with_extern_call_nodes(ctx);
				*inlined_body = Some(body);

				ctx.scope(meta, span, |ctx| {
					let function = &ctx.declarations[resolved_id.unwrap()];
					let params = function.parameters.clone();
					for (param, arg_node) in params.iter().zip(arguments.iter_mut()) {
						ctx.declare_variable(
							param.name.to_string(),
							false,
							true,
							Some(param.ty.clone()),
						);
						ctx.assign_variable(&param.name, param.ty.clone());
						argument_ids.push(ctx.resolve_variable_scoped(&param.name).ok_or_else(
							|| Error::undefined_variable(&arg_node.span(), &param.name),
						)?);

						arg_node.validate(meta, ctx)?;
						arg_node.set_access_copy_or_move(&param.ty, meta, ctx, span)?;
					}

					inlined_body.as_mut().unwrap().validate(meta, ctx)?;

					Ok(())
				})?;
			}
			CallType::Method { object, self_type } => {
				//TODO verify that the object is mutable
				object.validate(meta, ctx)?;
				let object_type = object
					.get_type(meta, ctx)?
					.ok_or_else(|| Error::method_call_on_undefined(span, name))?;

				let class = meta
					.get(&object_type)
					.map_err(|error| Error::reflect(span, error))?;
				if let Some(function) = class.get_method_ref(name) {
					*self_type = Access::Ref;
					object.set_access(ctx, *self_type)?;
					validate_function_arguments(meta, ctx, span, arguments, &function.parameters)?;
				} else if let Some(function) = class.get_method_ref_mut(name) {
					*self_type = Access::RefMut;
					object.set_access(ctx, *self_type)?;
					validate_function_arguments(meta, ctx, span, arguments, &function.parameters)?;
				} else if let Some(function) = class.get_method_owned(name) {
					*self_type = if class.get_method_ref("copy").is_some() {
						Access::Copy
					} else {
						Access::Move
					};

					validate_function_arguments(meta, ctx, span, arguments, &function.parameters)?;
					object.set_access_copy_or_move(&object_type, meta, ctx, span)?;
				} else {
					return Err(Error::undefined_method(span, object_type, name));
				}
			}
			CallType::Extern { id } => {
				for arg in arguments.iter_mut() {
					arg.validate(meta, ctx)?;
				}

				let args: Vec<ClassId> = arguments
					.iter()
					.map(|node| match node.get_type(meta, ctx)? {
						Some(ty) => Ok(ty),
						None => Err(Error::function_call_wrong_type(&node.span())
							.expected(ClassId::unknown())
							.build()),
					})
					.collect::<Result<_, _>>()?;

				let (resolved_id, function) = ctx.find_extern_function(name, &args).unwrap();
				*id = Some(resolved_id);
				let params = function.parameters.clone();
				for ((_, ty), arg) in params.iter().zip(arguments.iter_mut()) {
					let arg_type = arg.get_type(meta, ctx)?.ok_or_else(|| {
						Error::function_call_wrong_type(&arg.span())
							.expected(ty.clone())
							.build()
					})?;
					if arg_type != *ty {
						return Err(Error::function_call_wrong_type(&arg.span())
							.expected(ty.clone())
							.got(arg_type)
							.build());
					}

					arg.set_access_copy_or_move(&arg_type, meta, ctx, span)?;
				}
			}
			CallType::Static { class } => {
				let class_meta = meta
					.get(class)
					.map_err(|error| Error::reflect(span, error))?;
				let func = class_meta
					.get_method_static(name)
					.ok_or_else(|| Error::undefined_static_function(span, class.clone(), name))?;
				validate_function_arguments(meta, ctx, span, arguments, &func.parameters)?;
			}
		}

		Ok(())
	}

	fn check(&self, meta: &Meta, ctx: &Context) -> Result<(), Error> {
		let Self {
			span,
			name,
			arguments,
			call_type,
			..
		} = self;

		match call_type {
			CallType::Free {
				resolved_id,
				inlined_body,
				..
			} => {
				let func = &ctx.declarations[resolved_id.expect("Missing resolved function id")];
				let parameters = convert_parameters(func);
				check_function_arguments(
					meta,
					ctx,
					span,
					CallContext::new_free(),
					name,
					arguments,
					&parameters,
				)?;

				let deduced_return_type = inlined_body.as_ref().unwrap().get_type(meta, ctx)?;
				if func.return_type != deduced_return_type {
					return Err(Error::function_declaration_wrong_type(
						span,
						func.return_type.clone(),
						deduced_return_type,
					));
				}
			}
			CallType::Method { .. } | CallType::Extern { .. } => (),
			CallType::Static { class } => {
				let class_meta = meta
					.get(class)
					.map_err(|error| Error::reflect(span, error))?;
				let func = class_meta
					.get_method_static(name)
					.ok_or_else(|| Error::undefined_static_function(span, class.clone(), name))?;
				check_function_arguments(
					meta,
					ctx,
					span,
					CallContext::new_static(class),
					name,
					arguments,
					&func.parameters,
				)?;
			}
		}
		Ok(())
	}

	fn check_variable_access(
		&self,
		variables: &mut crate::VariableStates,
		_declarations: &crate::Declarations,
	) -> Result<(), Error> {
		if let CallType::Free { argument_ids, .. } = &self.call_type {
			for id in argument_ids {
				variables.set(*id, State::Initialized);
			}
		}
		Ok(())
	}
}

fn convert_parameters(func: &crate::Function) -> Vec<TypeWithAccess> {
	let parameters: Vec<_> = func
		.parameters
		.iter()
		.map(|p| TypeWithAccess {
			access: p.access.into(),
			ty: p.ty.clone(),
		})
		.collect();
	parameters
}

impl Walkable for Call {
	fn walk(&mut self, f: &mut Callback) -> Result<(), Error> {
		f(self)?;

		if let CallType::Method { object, .. } = &mut self.call_type {
			object.walk(f)?;
		}

		self.arguments.walk(f)?;

		if let CallType::Free {
			inlined_body: Some(body),
			..
		} = &mut self.call_type
		{
			body.walk(f)?;
		}

		Ok(())
	}
}

fn validate_function_arguments(
	meta: &Meta,
	ctx: &mut Context,
	span: &Span,
	arguments: &mut Vec<Node>,
	parameters: &[TypeWithAccess],
) -> Result<(), Error> {
	for (argument, parameter) in arguments.iter_mut().zip(parameters.iter()) {
		let mode = match parameter.access {
			reflect::Access::Owned => {
				if meta
					.get(&parameter.ty)
					.map_err(|error| Error::reflect(span, error))?
					.has_method_ref("copy")
				{
					Access::Copy
				} else {
					Access::Move
				}
			}
			reflect::Access::Ref => Access::Ref,
			reflect::Access::RefMut => Access::RefMut,
		};
		argument.set_access(ctx, mode)?;
		argument.validate(meta, ctx)?;
	}

	Ok(())
}

fn check_function_arguments(
	meta: &Meta,
	ctx: &Context,
	span: &Span,
	context: CallContext,
	name: &Identifier,
	arguments: &[Node],
	parameters: &[TypeWithAccess],
) -> Result<(), Error> {
	if arguments.len() != parameters.len() {
		return Err(Error::call_wrong_number_of_arguments(
			span,
			context,
			name,
			parameters.len(),
			arguments.len(),
		));
	}
	for (argument, parameter) in arguments.iter().zip(parameters.iter()) {
		let ty = argument.get_type(meta, ctx)?.ok_or_else(|| {
			Error::function_call_wrong_type(&argument.span())
				.expected(ClassId::unknown())
				.build()
		})?;
		if ty != parameter.ty {
			return Err(Error::function_call_wrong_type(&argument.span())
				.expected(parameter.ty.clone())
				.got(ty)
				.build());
		}
	}

	Ok(())
}
