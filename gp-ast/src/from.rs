use crate::nodes::CallType;

use super::{Access, Node};
use parser::{
	nodes::{BinaryOp, SetField, UnaryOp},
	BinaryOperator, Expression, Statement, UnaryOperator,
};

fn convert_binary_op(op: &BinaryOperator) -> &'static str {
	match op {
		BinaryOperator::Add => "op_add",
		BinaryOperator::Sub => "op_sub",
		BinaryOperator::Mul => "op_mul",
		BinaryOperator::Div => "op_div",
		BinaryOperator::Less => "op_lt",
		BinaryOperator::LessEqual => "op_le",
		BinaryOperator::Greater => "op_gt",
		BinaryOperator::GreaterEqual => "op_ge",
		BinaryOperator::Equal => "op_eq",
		BinaryOperator::NotEqual => "op_ne",
		BinaryOperator::And => "op_and",
		BinaryOperator::Or => "op_or",
	}
}

fn convert_unary_op(op: &UnaryOperator) -> &'static str {
	match op {
		UnaryOperator::Bang => "op_bang",
		UnaryOperator::Neg => "op_neg",
	}
}

impl From<Expression> for Node {
	fn from(expression: Expression) -> Self {
		match expression {
			Expression::Group(node) => Self::Group(node.into()),
			Expression::Literal(node) => Self::Literal(node.into()),
			Expression::Variable(node) => Self::Variable(node.into()),
			Expression::UnaryOp(unary_op) => unary_op.into(),
			Expression::BinaryOp(binary_op) => binary_op.into(),
			Expression::FreeFunctionCall(node) => Self::Call(node.into()),
			Expression::StaticFunctionCall(node) => Self::Call(node.into()),
			Expression::MethodCall(node) => Self::Call(node.into()),
			Expression::Block(node) => Self::Block(node.into()),
			Expression::Branch(node) => Self::Branch(node.into()),
			Expression::GetField(node) => Self::GetField(node.into()),
			Expression::Error(_) => panic!(),
		}
	}
}

impl From<Statement> for Node {
	fn from(statement: Statement) -> Self {
		match statement {
			Statement::Expression(node) => Self::ExpressionStatement(node.into()),
			Statement::VariableDeclaration(node) => Self::VariableDeclaration(node.into()),
			Statement::Assignment(node) => Self::Assignment(node.into()),
			Statement::While(node) => Self::While(node.into()),
			Statement::For(node) => Self::For(node.into()),
			Statement::FunctionDeclaration(node) => Self::FunctionDeclaration(node.into()),
			//Statement::Return(node) => Self::ReturnStatement(node.into()),
			Statement::SetField(set_field) => set_field.into(),
			Statement::Error(_) => panic!(),
		}
	}
}

impl From<UnaryOp> for Node {
	fn from(unary_op: UnaryOp) -> Self {
		Self::Call(crate::nodes::Call {
			span: unary_op.span,
			scope: None,
			name: convert_unary_op(&unary_op.operator).into(),
			arguments: Vec::new(),
			call_type: CallType::Method {
				object: Box::new(unary_op.operand.into()),
				self_type: Access::Ref,
			},
		})
	}
}

impl From<BinaryOp> for Node {
	fn from(binary_op: BinaryOp) -> Self {
		Self::Call(crate::nodes::Call {
			span: binary_op.span,
			scope: None,
			name: convert_binary_op(&binary_op.operator).into(),
			arguments: vec![binary_op.right.into()],
			call_type: CallType::Method {
				object: Box::new(binary_op.left.into()),
				self_type: Access::Ref,
			},
		})
	}
}

impl From<Box<Expression>> for Node {
	fn from(expression: Box<Expression>) -> Self {
		(*expression).into()
	}
}

impl From<SetField> for Node {
	fn from(set_field: SetField) -> Self {
		Self::ExpressionStatement(crate::nodes::ExpressionStatement {
			span: set_field.span.clone(), // TODO verify this span,
			scope: None,
			body: Box::new(Self::Call(crate::nodes::Call {
				span: set_field.span.clone(), // TODO verify this span
				scope: None,
				name: "op_assign".into(),
				arguments: vec![set_field.value.into()],
				call_type: CallType::Method {
					object: Box::new(Node::GetField(crate::nodes::GetField {
						span: set_field.span, // TODO verify this span
						scope: None,
						object: Box::new(set_field.object.into()),
						name: set_field.name.into(),
						access: None,
					})),
					self_type: Access::RefMut,
				},
			})),
			has_semicolon: true,
		})
	}
}
