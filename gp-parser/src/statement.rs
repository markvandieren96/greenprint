use crate::{
	nodes::{
		Assignment, ExpressionStatement, ForLoop, FunctionDeclaration, SetField,
		VariableDeclaration, WhileLoop,
	},
	Error,
};

#[derive(Debug, PartialEq, Clone)]
pub enum Statement {
	Expression(ExpressionStatement),
	VariableDeclaration(VariableDeclaration),
	Assignment(Assignment),
	While(WhileLoop),
	For(ForLoop),
	FunctionDeclaration(FunctionDeclaration),
	//Return(ReturnStatement),
	SetField(SetField),
	Error(Error),
}
