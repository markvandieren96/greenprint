use crate::counter::Counter;
use eframe::egui;
use egui::{Color32 as Color, Pos2, Ui, Vec2};
use program::lexer::Span;

pub const CORNER_RADIUS: f32 = 2.0;
pub const CONNECTION_CORNER_OFFSET: f32 = 10.0;
pub const MARGIN: f32 = 4.0;

pub const EXEC_COLOR: Color = Color::WHITE;
pub const BOOL_COLOR: Color = Color::RED;
pub const INT_COLOR: Color = Color::GREEN;
pub const UNKNOWN_COLOR: Color = Color::BLUE;

pub struct Child {
	pub node: Node,
	pub name: String,
	pub connection_color: Color,
}

pub struct Node {
	id: egui::Id,
	header: String,
	hover_text: String,
	children: Vec<Child>,
	outline_color: Color,
	text_color: Color,
}

impl Node {
	pub fn new(id: egui::Id) -> Self {
		Self {
			id,
			header: String::new(),
			hover_text: String::new(),
			children: Vec::new(),
			outline_color: Color::LIGHT_GRAY,
			text_color: Color::WHITE,
		}
	}

	pub fn header(mut self, header: &impl ToString) -> Self {
		self.header = header.to_string();
		self
	}

	pub fn hover_src(mut self, src: &str, span: &Span) -> Self {
		self.hover_text = src.get(span.clone()).unwrap_or("").to_string();
		self
	}

	pub fn child(mut self, counter: &mut Counter, child: &dyn ToNode, src: &str) -> Self {
		self.children.push(Child {
			node: child.to_node(counter, src),
			name: String::new(),
			connection_color: Color::WHITE,
		});
		self
	}

	pub fn named_child(
		mut self,
		counter: &mut Counter,
		child: &dyn ToNode,
		name: &impl ToString,
		connection_color: Color,
		src: &str,
	) -> Self {
		self.children.push(Child {
			node: child.to_node(counter, src),
			name: name.to_string(),
			connection_color,
		});
		self
	}

	pub fn draw(&self, ui: &mut Ui, parent_out_pin: Option<Pos2>) -> egui::Response {
		ui.horizontal(|ui| {
			let size = ui
				.memory()
				.id_data_temp
				.get::<Vec2>(&self.id)
				.copied()
				.unwrap_or_else(|| Vec2::new(100.0, 20.0));
			let (mut rect, mut response) = ui.allocate_exact_size(size, egui::Sense::hover());
			let painter = ui.painter();
			painter.rect(
				rect,
				CORNER_RADIUS,
				Color::BLACK,
				egui::Stroke::new(2.0, self.outline_color),
			);

			if let Some(connection) = parent_out_pin {
				let target = Pos2::new(rect.left(), rect.center().y);
				if (connection.y - target.y).abs() < std::f32::EPSILON {
					painter
						.line_segment([connection, target], egui::Stroke::new(1.0, Color::WHITE));
				} else {
					let p1 = connection + Vec2::new(CONNECTION_CORNER_OFFSET, 0.0);
					painter.line_segment([connection, p1], egui::Stroke::new(1.0, Color::WHITE));
					let p2 = Pos2::new(p1.x, target.y);
					painter.line_segment([p1, p2], egui::Stroke::new(1.0, Color::WHITE));
					let p3 = Pos2::new(rect.left(), p2.y);
					painter.line_segment([p2, p3], egui::Stroke::new(1.0, Color::WHITE));
				}
			}

			let mut cursor_y = rect.top();
			if !self.header.is_empty() {
				let text_r = painter.text(
					Pos2::new(rect.center().x, rect.top() + MARGIN),
					egui::Align2::CENTER_TOP,
					&self.header,
					egui::TextStyle::Monospace,
					self.text_color,
				);
				rect = rect.union(text_r.expand2(Vec2::new(MARGIN, 0.0)));
				painter.line_segment(
					[
						Pos2::new(rect.left(), text_r.bottom() + MARGIN),
						Pos2::new(rect.right(), text_r.bottom() + MARGIN),
					],
					egui::Stroke::new(1.0, Color::WHITE),
				);

				cursor_y = text_r.bottom() + MARGIN * 3.0;
			}

			if !self.hover_text.is_empty() {
				response = response.on_hover_text(&self.hover_text);
			}

			let out_pin = Pos2::new(rect.right(), rect.center().y);
			ui.vertical(|ui| {
				for child in &self.children {
					if child.name.is_empty() {
						child.node.draw(ui, Some(out_pin));
					} else {
						let name_rect = ui.painter().text(
							Pos2::new(rect.right() - MARGIN, cursor_y),
							egui::Align2::RIGHT_CENTER,
							&child.name,
							egui::TextStyle::Monospace,
							self.text_color,
						);
						rect = rect.union(name_rect.expand2(Vec2::new(0.0, MARGIN)));
						cursor_y = name_rect.bottom() + MARGIN * 3.0;

						let out_pin = Pos2::new(rect.right(), name_rect.center().y);
						child.node.draw(ui, Some(out_pin));
						ui.painter()
							.circle_filled(out_pin, 3.0, child.connection_color);
					}
				}
			});

			ui.memory().id_data_temp.insert(self.id, rect.size());

			response
		})
		.inner
	}
}

#[allow(clippy::module_name_repetitions)]
pub trait ToNode {
	fn to_node(&self, counter: &mut Counter, src: &str) -> Node;
}
