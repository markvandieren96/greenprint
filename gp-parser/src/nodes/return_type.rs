use crate::{LexerExpectExt, ParseResult};
use lexer::{merge_spans, Lexer, LexerExt, Span, Token};

/// Parses "-> T"
#[derive(Debug, PartialEq, Clone)]
pub struct ReturnType {
	pub span: Span,
	pub ty: String,
}

impl ReturnType {
	pub fn try_parse(lexer: &mut Lexer) -> ParseResult<Self> {
		let span_start = if lexer.peek_match(&Token::Arrow) {
			lexer.next().unwrap().1
		} else {
			return Ok(None);
		};

		let (ty, span_end) = lexer.expect_identifier("ReturnType")?;

		Ok(Some(Self {
			span: merge_spans(&span_start, &span_end),
			ty,
		}))
	}

	#[must_use]
	pub fn to_src(&self) -> String {
		format!(" -> {}", self.ty)
	}
}
