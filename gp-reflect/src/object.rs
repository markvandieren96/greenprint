use crate::{extend_world_lifetime, Class, Error, GetClass};
use log::trace;
use specs::World;
use std::any::Any;

pub struct Object {
	class: &'static Class,
	value: Inner,
	was_moved_from: bool,
}

impl Object {
	pub fn new<T: 'static + GetClass + Send + Sync>(is_mut: bool, t: T) -> Self {
		Self {
			class: t.class(),
			value: Inner::Owned(is_mut, Box::new(t)),
			was_moved_from: false,
		}
	}

	pub fn new_ref<T: 'static + GetClass + Send + Sync>(t: &T) -> Self {
		let t: &'static T = unsafe { std::mem::transmute(t) };
		Self {
			class: t.class(),
			value: Inner::Ref(t),
			was_moved_from: false,
		}
	}

	pub fn new_ref_mut<T: 'static + GetClass + Send + Sync>(t: &mut T) -> Self {
		let t: &'static mut T = unsafe { std::mem::transmute(t) };
		Self {
			class: t.class(),
			value: Inner::RefMut(t),
			was_moved_from: false,
		}
	}

	#[must_use]
	pub fn create_ref(&self) -> Self {
		assert!(!self.was_moved_from);

		let val: &(dyn Any + Send + Sync) = self.value.as_any();
		let val: &'static (dyn Any + Send + Sync) = unsafe { std::mem::transmute(val) };
		Self {
			class: self.class,
			value: Inner::Ref(val),
			was_moved_from: false,
		}
	}

	pub fn create_ref_mut(&mut self) -> Result<Self, Error> {
		assert!(!self.was_moved_from);

		let val: &mut (dyn Any + Send + Sync) = self.value.as_any_mut()?;
		let val: &'static mut (dyn Any + Send + Sync) = unsafe { std::mem::transmute(val) };
		Ok(Self {
			class: self.class,
			value: Inner::RefMut(val),
			was_moved_from: false,
		})
	}

	pub fn call(
		&self,
		name: &str,
		world: &World,
		args: &mut [Object],
	) -> Result<Option<Object>, Error> {
		trace!("call({})", name);
		assert!(!self.was_moved_from);

		Ok(self
			.class
			.methods
			.find(name)
			.ok_or_else(|| Error::no_such_method(name, self.class))?(
			extend_world_lifetime(world),
			self,
			args,
		))
	}

	pub fn call_mut(
		&mut self,
		name: &str,
		world: &World,
		args: &mut [Object],
	) -> Result<Option<Object>, Error> {
		trace!("call_mut({})", name);
		assert!(!self.was_moved_from);

		if self.value.is_mut() {
			Ok(self
				.class
				.methods_mut
				.find(name)
				.ok_or_else(|| Error::no_such_method(name, self.class))?(
				extend_world_lifetime(world),
				self,
				args,
			))
		} else {
			Error::const_violation(format!(
				"Object::call_mut({}) called on const ref {}",
				name,
				self.class.name()
			))
			.into()
		}
	}

	pub fn call_owned(
		self,
		name: &str,
		world: &World,
		args: &mut [Object],
	) -> Result<Option<Object>, Error> {
		trace!("call_owned({})", name);
		assert!(!self.was_moved_from);

		let method = self
			.class
			.methods_owned
			.find(name)
			.ok_or_else(|| Error::no_such_method(name, self.class))?;
		Ok(method(extend_world_lifetime(world), self, args))
	}

	pub fn field(&self, name: &str) -> Result<Object, Error> {
		assert!(!self.was_moved_from);

		let field = self
			.class
			.fields
			.find(name)
			.ok_or_else(|| Error::no_such_field(name, self.class))?;
		Ok(field.get(&self.value))
	}

	pub fn field_mut(&mut self, name: &str) -> Result<Object, Error> {
		assert!(!self.was_moved_from);

		let field = self
			.class
			.fields
			.find(name)
			.ok_or_else(|| Error::no_such_field(name, self.class))?;
		field.get_mut(&mut self.value)
	}

	pub fn downcast<T: 'static>(self) -> Result<Box<T>, Box<(dyn Any + Send + Sync + 'static)>> {
		assert!(!self.was_moved_from);

		match self.value {
			Inner::Owned(_, value) => value.downcast(),
			_ => panic!(),
		}
	}

	#[must_use]
	pub fn downcast_ref<T: 'static>(&self) -> Option<&T> {
		assert!(!self.was_moved_from);

		self.value.as_any().downcast_ref()
	}

	pub fn downcast_mut<T: 'static>(&mut self) -> Result<Option<&mut T>, Error> {
		assert!(!self.was_moved_from);

		Ok(self.value.as_any_mut()?.downcast_mut())
	}

	#[must_use]
	pub fn class(&self) -> &Class {
		self.class
	}

	pub fn copy_or_take(&mut self, world: &World) -> Object {
		assert!(!self.was_moved_from);

		if let Ok(res) = self.call("copy", world, &mut []) {
			res.expect("copy implementation needs to return a value")
		} else {
			trace!("Taking value! (of {})", self.class.name());
			let ret = std::mem::take(self);
			self.was_moved_from = true;
			ret
		}
	}

	#[must_use]
	pub fn copy(&self, world: &World) -> Object {
		if let Ok(res) = self.call("copy", world, &mut []) {
			res.expect("copy implementation needs to return a value")
		} else {
			panic!()
		}
	}

	pub fn take(&mut self) -> Object {
		trace!("Taking value! (of {})", self.class.name());
		let ret = std::mem::take(self);
		self.was_moved_from = true;
		ret
	}
}

impl Default for Object {
	fn default() -> Self {
		Self::new(true, ())
	}
}

pub(crate) enum Inner {
	Owned(bool, Box<dyn Any + Send + Sync>),
	Ref(&'static (dyn Any + Send + Sync)),
	RefMut(&'static mut (dyn Any + Send + Sync)),
}

impl Inner {
	pub fn as_any(&self) -> &(dyn Any + Send + Sync) {
		match self {
			Inner::Owned(_, val) => &**val,
			Inner::Ref(val) => *val,
			Inner::RefMut(val) => *val,
		}
	}

	pub fn as_any_mut(&mut self) -> Result<&mut (dyn Any + Send + Sync), Error> {
		match self {
			Inner::Owned(is_mut, val) => {
				if *is_mut {
					Ok(&mut **val)
				} else {
					Error::const_violation("Inner::as_any_mut called on const owned").into()
				}
			}
			Inner::Ref(_) => Error::const_violation("Inner::as_any_mut called on const ref").into(),
			Inner::RefMut(val) => Ok(&mut **val),
		}
	}

	pub fn is_mut(&self) -> bool {
		match self {
			Inner::Owned(is_mut, _) => *is_mut,
			Inner::Ref(_) => false,
			Inner::RefMut(_) => true,
		}
	}
}

impl std::fmt::Display for Object {
	fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
		write!(f, "Object({})", self.class.name())
	}
}
