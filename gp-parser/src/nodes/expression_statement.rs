use crate::{Expression, Span};

/// Parses "expr;"
#[derive(Debug, PartialEq, Clone)]
pub struct ExpressionStatement {
	pub span: Span,
	pub body: Expression,
	pub has_semicolon: bool,
}
