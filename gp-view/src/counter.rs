#[derive(Default)]
pub struct Counter {
	count: usize,
}

impl Counter {
	pub fn get(&mut self) -> usize {
		let res = self.count;
		self.count += 1;
		res
	}
}
