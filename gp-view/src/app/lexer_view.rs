use eframe::egui;
use egui::Ui;
use program::lexer::{Lexer, Token};

#[allow(clippy::module_name_repetitions)]
pub struct LexerView {
	lines: Vec<Line>,
	error_flag: bool,
}

struct Line {
	line_number: usize,
	tokens: Vec<(Token, Option<String>)>,
}

impl LexerView {
	pub fn new(src: &str) -> Self {
		let mut lines = Vec::new();
		let mut line = Vec::new();
		let mut lexer = Lexer::new(src);
		let mut wip_line_number = 1;
		let mut error_flag = false;
		while let Some((token, _span)) = lexer.next() {
			let line_nr = lexer.current_line();
			while line_nr != wip_line_number {
				lines.push(Line {
					line_number: wip_line_number,
					tokens: std::mem::take(&mut line),
				});
				wip_line_number += 1;
			}
			if let Token::Error = token {
				error_flag = true;
			}
			line.push((token, lexer.current_src().map(str::to_string)));
		}
		if !line.is_empty() {
			lines.push(Line {
				line_number: wip_line_number,
				tokens: line,
			});
		}
		Self { lines, error_flag }
	}

	pub fn display(&mut self, ui: &mut Ui) {
		let Self { lines, error_flag } = self;

		if *error_flag {
			ui.add(egui::Label::new("Lexing failed").text_color(egui::Color32::RED));
		} else {
			ui.add(egui::Label::new("Lexing success").text_color(egui::Color32::GREEN));
		}

		let spacing_before = ui.spacing().item_spacing;
		ui.spacing_mut().item_spacing = egui::vec2(0.0, 0.0);
		for line in lines.iter() {
			ui.horizontal(|ui| {
				ui.monospace(format!("{}", line.line_number));
				ui.separator();
				for (token, source) in &line.tokens {
					if let Token::Error = token {
						let hint = source.as_ref().map_or("", |s| s.as_str());
						ui.add(
							egui::Label::new(format!("ERROR: {}", hint))
								.text_color(egui::Color32::RED),
						)
						.on_hover_text(format!("{:?}", token));
					} else {
						ui.selectable_label(false, token.to_src())
							.on_hover_text(format!("{:?}", token));
					}
				}
			});
		}
		ui.spacing_mut().item_spacing = spacing_before;
	}

	pub fn display_window(&mut self, ctx: &egui::CtxRef, show: &mut bool) {
		egui::Window::new("lexer").open(show).show(ctx, |ui| {
			egui::ScrollArea::auto_sized().show(ui, |ui| {
				self.display(ui);
			});
		});
	}
}
