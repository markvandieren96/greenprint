use crate::{
	block, field, impl_type_name, Class, ClassRegistry, FreeFunc, MemberMutFunc, MemberRefFunc,
	Object, Register, Type,
};

pub(crate) fn init_test_classes(registry: &mut ClassRegistry) {
	Foo::register(registry);
	Bar::register(registry);
	Vec2::register(registry);
	Transform::register(registry);

	registry.modify("Foo", |class| {
		class.add_method(
			"get_6",
			MemberRefFunc::new(
				Box::new(|_world, _obj, _args| Some(Object::new(true, 6))),
				vec![],
				Some(Type::owned::<i64>()),
			),
		);
	});

	registry.modify("Foo", |class| {
		class.add_method(
			"get_7",
			MemberRefFunc::new(
				Box::new(|_world, _obj, _args| Some(Object::new(true, 7))),
				vec![],
				Some(Type::owned::<i64>()),
			),
		);
	});
}

#[derive(Debug)]
pub struct Foo {
	pub i: i32,
	pub i2: i32,
}

impl_type_name!(Foo);

impl Register for Foo {
	fn register(registry: &mut ClassRegistry) {
		let mut class = Class::new::<Self>();
		field!(&mut class, i: i32);
		block!(derive(=));
		registry.register(class);
	}
}

#[derive(Debug)]
pub struct Bar {
	pub foo: Foo,
}

impl_type_name!(Bar);

impl Register for Bar {
	fn register(registry: &mut ClassRegistry) {
		let mut class = Class::new::<Self>();
		field!(&mut class, foo: Foo);
		registry.register(class);
	}
}

struct DummyComponent {
	_0: Object,
}
// This verifies that Object is Send + Sync
impl specs::Component for DummyComponent {
	type Storage = specs::VecStorage<Self>;
}

#[derive(Debug, Default, PartialEq)]
pub struct Vec2 {
	pub x: f32,
	pub y: f32,
}

impl_type_name!(Vec2);

impl Register for Vec2 {
	fn register(registry: &mut ClassRegistry) {
		let mut class = Class::new::<Self>();
		block!(
			derive(=)

			fn new(x: &f32, y: &f32) -> Vec2 {
				Vec2 { x: *x, y: *y }
			}
		);
		field!(&mut class, x: f32);
		field!(&mut class, y: f32);

		registry.register(class);
	}
}

#[derive(Debug, Default, PartialEq)]
pub struct Transform {
	pub position: Vec2,
	pub scale: Vec2,
}

impl_type_name!(Transform);

impl Register for Transform {
	fn register(registry: &mut ClassRegistry) {
		let mut class = Class::new::<Self>();
		field!(&mut class, position: Vec2);
		field!(&mut class, scale: Vec2);

		block!(
			derive(default)

			fn new() -> Transform {
				Transform::default()
			}
		);
		registry.register(class);
	}
}
