use crate::{Lexer, Token};
use logos::Logos;

#[test]
fn test_token_stream() {
	let src = "let a = 1; let b = 2;";
	let mut stream = Lexer::new(src);

	assert_eq!(stream.next_token(), Some(Token::Let));
	assert_eq!(stream.next_token(), Some(Token::Identifier("a".to_owned())));
	assert_eq!(stream.next_token(), Some(Token::Assignment));
	assert_eq!(stream.next_token(), Some(Token::Integer(1)));

	assert_eq!(stream.prev_token(), Some(Token::Integer(1)));
	assert_eq!(stream.prev_token(), Some(Token::Assignment));
	assert_eq!(stream.prev_token(), Some(Token::Identifier("a".to_owned())));
	assert_eq!(stream.prev_token(), Some(Token::Let));
	assert_eq!(stream.prev_token(), None);
	assert_eq!(stream.prev_token(), None);

	assert_eq!(stream.next_token(), Some(Token::Let));
	assert_eq!(stream.next_token(), Some(Token::Identifier("a".to_owned())));
	assert_eq!(stream.next_token(), Some(Token::Assignment));
	assert_eq!(stream.next_token(), Some(Token::Integer(1)));
	assert_eq!(stream.next_token(), Some(Token::Semicolon));
	assert_eq!(stream.next_token(), Some(Token::Let));
	assert_eq!(stream.next_token(), Some(Token::Identifier("b".to_owned())));
	assert_eq!(stream.next_token(), Some(Token::Assignment));
	assert_eq!(stream.next_token(), Some(Token::Integer(2)));
	assert_eq!(stream.next_token(), Some(Token::Semicolon));
	assert_eq!(stream.next_token(), None);
}

#[test]
fn test_token_stream_peek_stream_end() {
	let src = "1;";
	let mut stream = Lexer::new(src);

	assert_eq!(stream.next_token(), Some(Token::Integer(1)));
	assert_eq!(stream.next_token(), Some(Token::Semicolon));
	assert_eq!(stream.peek(), None);
	assert_eq!(stream.peek(), None);
	assert_eq!(stream.next_token(), None);
}

#[test]
fn test_lexer_assignment_integer() {
	let src = "let x = 1";
	let mut lex = Token::lexer(src);

	assert_eq!(lex.next(), Some(Token::Let));
	assert_eq!(lex.next(), Some(Token::Identifier("x".to_owned())));
	assert_eq!(lex.next(), Some(Token::Assignment));
	assert_eq!(lex.next(), Some(Token::Integer(1)));
}

#[test]
fn test_lexer_assignment_integer2() {
	let src = "let x = 100";
	let mut lex = Token::lexer(src);

	assert_eq!(lex.next(), Some(Token::Let));
	assert_eq!(lex.next(), Some(Token::Identifier("x".to_owned())));
	assert_eq!(lex.next(), Some(Token::Assignment));
	assert_eq!(lex.next(), Some(Token::Integer(100)));
}

#[test]
fn test_lexer_assignment_float() {
	let src = "let x = 1.5";
	let mut lex = Token::lexer(src);

	assert_eq!(lex.next(), Some(Token::Let));
	assert_eq!(lex.next(), Some(Token::Identifier("x".to_owned())));
	assert_eq!(lex.next(), Some(Token::Assignment));
	assert_eq!(lex.next(), Some(Token::Float(1.5)));
}

#[test]
fn test_lexer_branch() {
	let src = "if 1 < 2 { 
    let a = 1;
    let b = a;
}";

	let mut lex = Token::lexer(src);

	assert_eq!(lex.next(), Some(Token::If));
	assert_eq!(lex.next(), Some(Token::Integer(1)));
	assert_eq!(lex.next(), Some(Token::LessThan));
	assert_eq!(lex.next(), Some(Token::Integer(2)));
	assert_eq!(lex.next(), Some(Token::BraceOpen));
	assert_eq!(lex.next(), Some(Token::Let));
	assert_eq!(lex.next(), Some(Token::Identifier("a".to_owned())));
	assert_eq!(lex.next(), Some(Token::Assignment));
	assert_eq!(lex.next(), Some(Token::Integer(1)));
	assert_eq!(lex.next(), Some(Token::Semicolon));
	assert_eq!(lex.next(), Some(Token::Let));
	assert_eq!(lex.next(), Some(Token::Identifier("b".to_owned())));
	assert_eq!(lex.next(), Some(Token::Assignment));
	assert_eq!(lex.next(), Some(Token::Identifier("a".to_owned())));
	assert_eq!(lex.next(), Some(Token::Semicolon));
	assert_eq!(lex.next(), Some(Token::BraceClose));
}

#[test]
fn test_lexer_function_call() {
	let src = r#"print("Hello, world!");"#;

	let mut lex = Token::lexer(src);

	assert_eq!(lex.next(), Some(Token::Identifier("print".to_owned())));
	assert_eq!(lex.next(), Some(Token::ParenthesisOpen));
	assert_eq!(lex.next(), Some(Token::Literal("Hello, world!".to_owned())));
	assert_eq!(lex.next(), Some(Token::ParenthesisClose));
	assert_eq!(lex.next(), Some(Token::Semicolon));
}

#[test]
fn test_token_eq() {
	let a = Token::Identifier("a".to_owned());
	let b = Token::Identifier("a".to_owned());
	assert_eq!(a, b);
}

#[test]
fn test_token_nq() {
	let a = Token::Identifier("a".to_owned());
	let b = Token::Identifier("b".to_owned());
	assert_ne!(a, b);
}

#[test]
fn test_token_variant_eq() {
	let a = Token::Identifier("a".to_owned());
	let b = Token::Identifier("b".to_owned());
	assert!(a.variant_eq(&b));
}

#[test]
fn test_identifier() {
	let src = "a _a a_ _ _0 0a a0 a.b.c";
	let mut lex = Token::lexer(src);

	assert_eq!(lex.next(), Some(Token::Identifier("a".to_owned())));
	assert_eq!(lex.next(), Some(Token::Identifier("_a".to_owned())));
	assert_eq!(lex.next(), Some(Token::Identifier("a_".to_owned())));
	assert_eq!(lex.next(), Some(Token::Identifier("_".to_owned())));
	assert_eq!(lex.next(), Some(Token::Identifier("_0".to_owned())));
	assert_eq!(lex.next(), Some(Token::Integer(0)));
	assert_eq!(lex.next(), Some(Token::Identifier("a".to_owned())));
	assert_eq!(lex.next(), Some(Token::Identifier("a0".to_owned())));
	assert_eq!(lex.next(), Some(Token::Identifier("a".to_owned())));
	assert_eq!(lex.next(), Some(Token::Dot));
	assert_eq!(lex.next(), Some(Token::Identifier("b".to_owned())));
	assert_eq!(lex.next(), Some(Token::Dot));
	assert_eq!(lex.next(), Some(Token::Identifier("c".to_owned())));
	assert_eq!(lex.next(), None);
}

#[test]
fn test_literal() {
	let src = r#""Hello, world!""#;
	let mut lex = Token::lexer(src);

	assert_eq!(
		lex.next(),
		Some(Token::Literal(r"Hello, world!".to_owned()))
	);
	assert_eq!(lex.next(), None);
}

#[test]
fn test_single_line_comment() {
	let src = "0 // This is a comment
		1
		";
	let mut lex = Token::lexer(src);

	assert_eq!(lex.next(), Some(Token::Integer(0)));
	assert_eq!(
		lex.next(),
		Some(Token::Comment("This is a comment".to_owned()))
	);
	assert_eq!(lex.next(), Some(Token::Integer(1)));
	assert_eq!(lex.next(), None);
}

#[test]
fn test_multi_line_comment() {
	let src = "0 /* This is a comment
		1
		2
		*/ 3
		";
	let mut lex = Token::lexer(src);

	assert_eq!(lex.next(), Some(Token::Integer(0)));
	assert_eq!(
		lex.next(),
		Some(Token::Comment(
			"This is a comment
		1
		2"
			.to_owned()
		))
	);
	assert_eq!(lex.next(), Some(Token::Integer(3)));
	assert_eq!(lex.next(), None);
}

#[test]
fn test_nested_multi_line_comment() {
	let src = "0 /* This is a comment
		/* 1 */
		2
		*/ 3
		";
	let mut lex = Token::lexer(src);

	assert_eq!(lex.next(), Some(Token::Integer(0)));
	assert_eq!(
		lex.next(),
		Some(Token::Comment(
			"This is a comment
		/* 1 */
		2"
			.to_owned()
		))
	);
	assert_eq!(lex.next(), Some(Token::Integer(3)));
	assert_eq!(lex.next(), None);
}

#[test]
fn test_lexer_lookahead_forward() {
	let src = "let a = 1; let b = 2;";
	let mut lexer = Lexer::new(src);

	assert_eq!(lexer.next_token(), Some(Token::Let));
	assert_eq!(lexer.next_token(), Some(Token::Identifier("a".to_owned())));
	assert_eq!(lexer.next_token(), Some(Token::Assignment));
	assert_eq!(lexer.next_token(), Some(Token::Integer(1)));
	assert_eq!(lexer.next_token(), Some(Token::Semicolon));

	lexer.lookahead(|lexer, _restore_state| {
		assert_eq!(lexer.next_token(), Some(Token::Let));
		assert_eq!(lexer.next_token(), Some(Token::Identifier("b".to_owned())));
		assert_eq!(lexer.next_token(), Some(Token::Assignment));
		assert_eq!(lexer.next_token(), Some(Token::Integer(2)));
		assert_eq!(lexer.next_token(), Some(Token::Semicolon));
		assert_eq!(lexer.next_token(), None);
	});

	assert_eq!(lexer.next_token(), Some(Token::Let));
	assert_eq!(lexer.next_token(), Some(Token::Identifier("b".to_owned())));
	assert_eq!(lexer.next_token(), Some(Token::Assignment));
	assert_eq!(lexer.next_token(), Some(Token::Integer(2)));
	assert_eq!(lexer.next_token(), Some(Token::Semicolon));
	assert_eq!(lexer.next_token(), None);
}

#[test]
fn test_lexer_lookahead_backward() {
	let src = "let a = 1; let b = 2;";
	let mut lexer = Lexer::new(src);

	assert_eq!(lexer.next_token(), Some(Token::Let));
	assert_eq!(lexer.next_token(), Some(Token::Identifier("a".to_owned())));
	assert_eq!(lexer.next_token(), Some(Token::Assignment));
	assert_eq!(lexer.next_token(), Some(Token::Integer(1)));
	assert_eq!(lexer.next_token(), Some(Token::Semicolon));

	lexer.lookahead(|lexer, _restore_state| {
		assert_eq!(lexer.prev_token(), Some(Token::Semicolon));
		assert_eq!(lexer.prev_token(), Some(Token::Integer(1)));
		assert_eq!(lexer.prev_token(), Some(Token::Assignment));
		assert_eq!(lexer.prev_token(), Some(Token::Identifier("a".to_owned())));
		assert_eq!(lexer.prev_token(), Some(Token::Let));
		assert_eq!(lexer.prev_token(), None);
		assert_eq!(lexer.prev_token(), None);
	});

	assert_eq!(lexer.next_token(), Some(Token::Let));
	assert_eq!(lexer.next_token(), Some(Token::Identifier("b".to_owned())));
	assert_eq!(lexer.next_token(), Some(Token::Assignment));
	assert_eq!(lexer.next_token(), Some(Token::Integer(2)));
	assert_eq!(lexer.next_token(), Some(Token::Semicolon));
	assert_eq!(lexer.next_token(), None);
}
