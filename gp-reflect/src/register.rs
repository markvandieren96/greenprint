use crate::ClassRegistry;

pub trait Register
where
	Self: 'static,
{
	fn register(registry: &mut ClassRegistry);
}
