mod ast_view;
mod code_editor;
mod file_browser;
mod lexer_view;
mod scope_view;
mod validator;

use ast_view::AstView;
use code_editor::CodeEditor;
use file_browser::FileBrowser;
use lexer_view::LexerView;
use scope_view::ScopeView;
use validator::Validator;

use crate::ui_toolkit;
use eframe::{egui, epi};
use egui::Ui;
use program::{reflect::meta::Meta, Program};

pub trait Menu {
	fn tab_label(&self) -> &'static str;
	fn display(&mut self, ui: &mut Ui);
	fn destroy(&mut self, _ui: &mut Ui) {}
}

#[allow(clippy::struct_excessive_bools)]
pub struct App {
	show_sidebar: bool,
	show_meta: bool,
	show_lexer: bool,
	show_scopes: bool,
	show_ast: bool,
	program_is_valid: bool,
	file_browser: FileBrowser,
	state: AppState,
	meta: Meta,
}

enum AppState {
	NoFile,
	File {
		editor: Box<CodeEditor>,
		validator: Box<Validator>,
		scope_view: Box<ScopeView>,
		ast_view: Box<AstView>,
	},
}

#[allow(clippy::cast_precision_loss)]
impl epi::App for App {
	fn name(&self) -> &str {
		"gp-editor"
	}

	fn update(&mut self, ctx: &egui::CtxRef, _frame: &mut epi::Frame<'_>) {
		self.sidebar(ctx);

		let Self {
			state,
			meta,
			show_lexer,
			show_scopes,
			show_ast,
			program_is_valid,
			..
		} = self;

		egui::CentralPanel::default().show(ctx, |ui| {
			ui.style_mut().body_text_style = egui::TextStyle::Monospace;

			if let AppState::File {
				editor, validator, ..
			} = state
			{
				let bounds = ui.max_rect();

				let width = (bounds.width() / 2.0) - 100.0;

				let mut new_src = None;
				egui::ScrollArea::auto_sized().show(ui, |ui| {
					ui.horizontal(|ui| {
						ui.group(|ui| {
							ui.set_max_width(width);
							ui.vertical(|ui| {
								new_src = editor.display(ui);
							});
						});

						if let Some(src) = new_src {
							*validator = Box::new(Validator::new(&src, meta));
						}

						ui.group(|ui| {
							ui.set_max_width(width);
							ui.vertical(|ui| {
								validator.display(ui);
							});
						});
					});
				});
			}
		});

		*program_is_valid = false;
		if let AppState::File {
			scope_view,
			ast_view,
			..
		} = state
		{
			if let Some(path) = self.file_browser.selection().next() {
				if let Ok(src) = io::read_text(&path) {
					let res = Program::new("".to_owned(), meta, &src);
					match res {
						Ok(program) => {
							*program_is_valid = true;
							scope_view.display_window(ctx, &program.ctx, show_scopes);
							ast_view.display_window(ctx, show_ast, &program.ast, &src);
						}
						Err(e) => {
							if let program::CompileError::TypeCheck(e) = e {
								scope_view.display_window(ctx, &e.ctx, show_scopes);
								ast_view.display_window(ctx, show_ast, &e.ast, &src);
							}
						}
					}

					if *show_lexer {
						LexerView::new(&src).display_window(ctx, show_lexer);
					}
				}
			}
		}
	}
}

impl App {
	pub fn new() -> Result<Self, String> {
		let meta_path = crate::arg::get("meta", "meta.ron");
		let meta =
			Meta::load(&meta_path).map_err(|e| format!("Failed to load Meta file: {:?}", e))?;

		Ok(Self {
			show_sidebar: true,
			show_meta: false,
			show_lexer: false,
			show_scopes: false,
			show_ast: false,
			program_is_valid: false,
			file_browser: FileBrowser::default().hide_multi_select(),
			state: AppState::NoFile,
			meta,
		})
	}

	fn sidebar(&mut self, ctx: &egui::CtxRef) {
		let Self {
			show_sidebar,
			show_meta,
			show_lexer,
			show_scopes,
			show_ast,
			file_browser,
			state,
			meta,
			..
		} = self;

		let has_file = matches!(state, AppState::File { .. });

		egui::SidePanel::left("sidebar", 0.0).show(ctx, |ui| {
			if *show_sidebar {
				ui.horizontal(|ui| {
					ui_toolkit::toggle(ui, "", show_sidebar, true, "toggle sidebar");
					ui_toolkit::toggle(ui, "m", show_meta, true, "show type metadata");
					ui_toolkit::toggle(ui, "l", show_lexer, has_file, "show lexer");
					ui_toolkit::toggle(ui, "{}", show_scopes, has_file, "show program scopes");
					ui_toolkit::toggle(ui, "a", show_ast, has_file, "show ast nodes");
				});
			} else {
				ui_toolkit::toggle(ui, "", show_sidebar, true, "toggle sidebar");
				ui_toolkit::toggle(ui, "m", show_meta, true, "show type metadata");
				ui_toolkit::toggle(ui, "l", show_lexer, has_file, "show lexer");
				ui_toolkit::toggle(ui, "{}", show_scopes, has_file, "show program scopes");
				ui_toolkit::toggle(ui, "a", show_ast, has_file, "show ast nodes");
			}

			crate::meta_browser::window(ui.ctx(), show_meta, meta);

			if !*show_sidebar {
				return;
			}

			if *show_sidebar {
				ui.set_min_width(350.0);
			} else {
				ui.set_min_width(25.0);
			}

			if file_browser.display(ui) {
				let path = file_browser.selection().next().unwrap().clone();
				let editor = match CodeEditor::new(&path) {
					Ok(editor) => editor,
					Err(e) => {
						println!("Failed to create editor: {}", e);
						return;
					}
				};
				let validator = match io::read_text(&path) {
					Ok(src) => Validator::new(&src, meta),
					Err(e) => {
						println!("Failed to create validator: {:?}", e);
						return;
					}
				};

				if let AppState::File { ast_view, .. } = state {
					ast_view.destroy(ui);
				}

				*state = AppState::File {
					editor: Box::new(editor),
					validator: Box::new(validator),
					scope_view: Box::new(ScopeView::default()),
					ast_view: Box::new(AstView::default()),
				};
			}
		});
	}
}
