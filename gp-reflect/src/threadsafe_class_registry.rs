use crate::{ClassRegistry, Register};
use once_cell::sync::Lazy;
use parking_lot::RwLock;
use std::sync::Arc;

pub static CLASS_REGISTRY: Lazy<ThreadsafeClassRegistry> =
	Lazy::new(ThreadsafeClassRegistry::default);
pub static CLASSES: Lazy<ClassRegistry> = Lazy::new(|| CLASS_REGISTRY.finalise());

#[derive(Default)]
pub struct ThreadsafeClassRegistry {
	registry: Arc<RwLock<ClassRegistry>>,
}

impl ThreadsafeClassRegistry {
	pub fn register<T: 'static + Register>(&self) {
		let registry = &mut self.registry.write();
		if registry.get(std::any::TypeId::of::<T>()).is_some() {
			return;
		}

		T::register(registry);
	}

	fn finalise(&self) -> ClassRegistry {
		let registry = &mut self.registry.write();
		registry.process_queue();
		std::mem::take(registry)
	}
}
