#![warn(clippy::all)]
#![warn(clippy::pedantic)]

extern crate proc_macro;

mod access;
mod derive;
mod function;
mod signature;
mod type_signature;

use proc_macro::TokenStream;
use quote::quote;
use syn::parse_macro_input;

#[proc_macro]
pub fn signature(input: TokenStream) -> TokenStream {
	let parsed = parse_macro_input!(input as signature::Signature);
	proc_macro::TokenStream::from(quote! { #parsed })
}

#[proc_macro]
pub fn function(input: TokenStream) -> TokenStream {
	let parsed = parse_macro_input!(input as function::FnDeclaration);
	proc_macro::TokenStream::from(quote! { #parsed })
}

#[proc_macro]
pub fn derive(input: TokenStream) -> TokenStream {
	let parsed = parse_macro_input!(input as derive::Derive);
	proc_macro::TokenStream::from(quote! { #parsed })
}
