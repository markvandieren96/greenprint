use crate::nodes::{Assignment, Variable};
use parser::Span;
use reflect::meta::{Class, ClassId, Meta};

pub fn literal_int(span: Span, i: i64) -> crate::nodes::Literal {
	crate::nodes::Literal::new(span, parser::Literal::Integer(i))
}

pub fn create_assignment() -> Assignment {
	Assignment::new(0..1, "x", Box::new(literal_int(0..1, 1).into_node()))
}

pub fn create_variable() -> Variable {
	Variable::new(0..1, "x")
}

pub fn create_meta() -> Meta {
	let mut meta = Meta::default();
	meta.classes
		.insert(ClassId("i64".to_string()), Class::default());
	meta
}
