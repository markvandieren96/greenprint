use proc_macro2::TokenStream;
use quote::{ToTokens, TokenStreamExt};
use syn::parse::{Parse, ParseStream, Result};
use syn::Token;

#[derive(Clone, Copy, Debug)]
pub enum Access {
	Owned,
	Ref,
	RefMut,
}

impl Parse for Access {
	fn parse(input: ParseStream) -> Result<Self> {
		if input.peek(Token![&]) {
			input.parse::<Token![&]>()?;
			if input.peek(Token![mut]) {
				input.parse::<Token![mut]>()?;
				Ok(Self::RefMut)
			} else {
				Ok(Self::Ref)
			}
		} else {
			Ok(Self::Owned)
		}
	}
}

impl ToTokens for Access {
	fn to_tokens(&self, tokens: &mut TokenStream) {
		match self {
			Self::Owned => (),
			Self::Ref => {
				tokens.append(proc_macro2::Punct::new('&', proc_macro2::Spacing::Joint));
			}
			Self::RefMut => {
				tokens.append(proc_macro2::Punct::new('&', proc_macro2::Spacing::Joint));
				tokens.append(proc_macro2::Ident::new(
					"mut",
					proc_macro2::Span::call_site(),
				));
			}
		}
	}
}
