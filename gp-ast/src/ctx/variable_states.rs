use super::VariableId;
use std::collections::HashMap;

#[derive(Debug, Default)]
pub struct VariableStates {
	states: HashMap<VariableId, State>,
}

impl VariableStates {
	pub fn set(&mut self, variable: VariableId, state: State) {
		self.states.insert(variable, state);
	}

	#[must_use]
	pub fn get(&self, variable: VariableId) -> State {
		self.states
			.get(&variable)
			.copied()
			.unwrap_or(State::Uninitialized)
	}
}

#[derive(Debug, PartialEq, Eq, Copy, Clone)]
pub enum State {
	Uninitialized,
	Initialized,
	Moved,
}
