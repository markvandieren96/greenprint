use crate::access::Access;
use proc_macro2::{Ident, TokenStream};
use quote::{quote, ToTokens, TokenStreamExt};
use syn::{
	parenthesized,
	parse::{Parse, ParseStream, Result},
	punctuated::Punctuated,
	token::Paren,
	Token,
};

pub struct Signature {
	name: Ident,
	args: Option<ArgumentList>,
	returns: Option<Argument>,
}

impl Parse for Signature {
	fn parse(input: ParseStream) -> Result<Self> {
		let name: Ident = input.parse()?;

		let args = if input.peek(Paren) {
			Some(input.parse::<ArgumentList>()?)
		} else {
			None
		};

		let returns = if input.peek(Token![->]) {
			input.parse::<Token![->]>()?;
			Some(input.parse::<Argument>()?)
		} else {
			None
		};

		Ok(Self {
			name,
			args,
			returns,
		})
	}
}

impl ToTokens for Signature {
	fn to_tokens(&self, tokens: &mut TokenStream) {
		let name = &self.name;
		let args = &self.args;
		let returns = match &self.returns {
			Some(returns) => quote! { Some(#returns)},
			None => quote! { None },
		};

		tokens.append_all(quote! {
			FunctionSignature {
				name: stringify!(#name),
				args: vec![
					#args
				],
				returns: #returns,
			}
		});
	}
}

struct ArgumentList {
	args: Punctuated<Argument, Token![,]>,
}

impl Parse for ArgumentList {
	fn parse(input: ParseStream) -> Result<Self> {
		let parameters;
		let _paren = parenthesized!(parameters in input);

		Ok(Self {
			args: parameters.parse_terminated(Argument::parse)?,
		})
	}
}

impl ToTokens for ArgumentList {
	fn to_tokens(&self, tokens: &mut TokenStream) {
		let args = &self.args;
		tokens.append_all(quote! {
			#args
		});
	}
}

pub struct Argument {
	access: Access,
	ty: Ident,
}

impl Parse for Argument {
	fn parse(input: ParseStream) -> Result<Self> {
		Ok(Self {
			access: input.parse()?,
			ty: input.parse()?,
		})
	}
}

impl ToTokens for Argument {
	fn to_tokens(&self, tokens: &mut TokenStream) {
		let ty = &self.ty;
		match self.access {
			Access::Owned => {
				tokens.append_all(quote! {
					Type::owned::<#ty>()
				});
			}
			Access::Ref => {
				tokens.append_all(quote! {
					Type::borrow::<#ty>()
				});
			}
			Access::RefMut => {
				tokens.append_all(quote! {
					Type::borrow_mut::<#ty>()
				});
			}
		}
	}
}
